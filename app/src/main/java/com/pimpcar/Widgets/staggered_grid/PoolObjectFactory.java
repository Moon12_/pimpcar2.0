package com.pimpcar.Widgets.staggered_grid;

public interface PoolObjectFactory<T> {
  T createObject();
}

package com.pimpcar.Notification;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.comments.adapters.PostCommentAdapter;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.PostCommentWithUserTag;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentsBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.OtherUserProfileActivity;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostCommentRequestBody;
import com.pimpcar.Network.model.posts.PostCommentResponse;
import com.pimpcar.Network.model.posts.PostLikeDislikeRequestBody;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.Network.model.posts.UserPostDetailsRequestBody;
import com.pimpcar.Network.model.posts.UserPostDetailsResponseBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.ProportionalImageView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.models.comments.models.PostLikesModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class NotificationDetailsPostActivity extends AppCompatActivity {


    @BindView(R.id.back_button)
    AppCompatImageView back_button;
    List<UserPostDetailsResponseBody.DataBean.PostBean.PostLikesBean> postLikesModels;
    @BindView(R.id.title)
    AppCompatTextView title;

    @BindView(R.id.three_vertical_dots)
    AppCompatImageView three_vertical_dots;

    @BindView(R.id.user_profile_pic_rounded)
    CircleImageView user_profile_pic_rounded;

    @BindView(R.id.user_name_tv)
    AppCompatTextView user_name_tv;

    @BindView(R.id.user_location)
    AppCompatTextView user_location;


    @BindView(R.id.user_post_image)
    ProportionalImageView user_post_image;

    @BindView(R.id.user_number_of_likes)
    AppCompatTextView user_number_of_likes;

    @BindView(R.id.user_number_of_comments)
    AppCompatTextView user_number_of_comments;

    @BindView(R.id.user_post_comments_list)
    RecyclerView user_post_comments_list;

    @BindView(R.id.first_person_image)
    AppCompatImageView first_person_image;

    @BindView(R.id.second_person_dp)
    AppCompatImageView second_person_dp;

    @BindView(R.id.third_person_dp)
    AppCompatImageView third_person_dp;

    @BindView(R.id.fourth_person_dp)
    AppCompatImageView fourth_person_dp;

    @BindView(R.id.user_comment_profile)
    CircleImageView user_comment_profile;

    @BindView(R.id.comment_post_edit_text)
    AppCompatEditText comment_post_edit_text;

    @BindView(R.id.post_button_tv)
    AppCompatImageView post_button_tv;

    @BindView(R.id.post_time)
    AppCompatTextView post_time;

    @BindView(R.id.post_like_button)
    AppCompatImageView post_like_button;

    @BindView(R.id.below_image_user_info)
    RelativeLayout below_image_user_info;

    private String user_id;
    private String post_id;

    private int page_number;


    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    private List<PostAllCommentResponseModel.DataBean.CommentsBean> all_comments;
    private PostCommentAdapter postCommentAdapter;
    private int total_number_of_comments = 0;
    private KProgressHUD hud;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    PostAllCommentsBodyModel user_post_all_comment = HelperMethods.create_post_comments_body(getPage_number(), getPost_id());
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getAllPostComments(HelperMethods.get_user_jwt_key(), user_post_all_comment)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<PostAllCommentResponseModel>() {
                                    @Override
                                    public void onSuccess(PostAllCommentResponseModel s) {

                                        Timber.d("#$%#$%#$%#$%#$%    " + s.toString());

                                        if (s.getData().getComments() != null && s.getData().getComments().size() > 0) {

                                            for (int i = 0; i < s.getData().getComments().size(); i++) {
                                                all_comments.add(s.getData().getComments().get(i));
                                                postCommentAdapter.notifyItemInserted(all_comments.size() - 1);
                                            }
                                            postCommentAdapter.moreDataLoaded(all_comments.size(), s.getData().getComments().size());

                                            if (s.getData().getNext_page() == 1) {
                                                increase_page_number();
                                                postCommentAdapter.setShouldLoadMore(true);
                                            } else {
                                                postCommentAdapter.setShouldLoadMore(false);
                                            }

                                        } else {
                                            postCommentAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };
    private int post_total_number_of_likes = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailed_view_of_a_post);
        ButterKnife.bind(this);

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

        if (getIntent() != null) {

            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);

            setPost_id(getIntent().getStringExtra(Constants.POST_ID_SENT));
            String userID = getIntent().getStringExtra("USER_ID");

            get_post_details(getPost_id());
            get_post_comment(getPost_id());

            user_profile_pic_rounded.setOnClickListener(v -> {
                if (userID.equals(HelperMethods.get_user_id())) {
                    Toast.makeText(this, "This is your own post", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(this, OtherUserProfileActivity.class);
                    i.putExtra(Constants.THIRD_USER_ID, userID);
                    startActivity(i);
                }

            });
        }


    }

    private void get_post_comment(String post_id) {

        PostAllCommentsBodyModel user_post_all_comment = HelperMethods.create_post_comments_body(getPage_number(), post_id);
        disposable.add(apiService.getAllPostComments(HelperMethods.get_user_jwt_key(), user_post_all_comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostAllCommentResponseModel>() {
                    @Override
                    public void onSuccess(PostAllCommentResponseModel s) {
//
//                        dismiss_progress_dialog();

                        if (s.getData().getComments() != null && s.getData().getComments().size() > 0) {
                            if (all_comments != null)
                                all_comments.clear();
                            all_comments = s.getData().getComments();
                            postCommentAdapter = new PostCommentAdapter(NotificationDetailsPostActivity.this, all_comments);
                            user_post_comments_list.setLayoutManager(new LinearLayoutManager(NotificationDetailsPostActivity.this));
                            user_post_comments_list.setAdapter(postCommentAdapter);
                            postCommentAdapter.setOnLoadMoreListener(onLoadMoreListener);
                            postCommentAdapter.notifyDataSetChanged();
                            if (s.getData().getNext_page() == 1) {
                                postCommentAdapter.setShouldLoadMore(true);
                                increase_page_number();
                            } else {
                                postCommentAdapter.setShouldLoadMore(false);
                            }
                        }
                    }

                    @Override
                    protected void onStart() {
                        super.onStart();
                        Timber.d("method called[onStart()]");
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
//                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                Timber.d("#### Network call fucked");
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }));

    }

    @OnClick(R.id.post_like_button)
    public void like_post() {
        if (post_like_button.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24).getConstantState()) {
            likeDislikePost(post_id, "dolike");
//            Toast.makeText(NotificationDetailsPostActivity.this, "dolike", Toast.LENGTH_SHORT).show();


        } else {
//            Toast.makeText(NotificationDetailsPostActivity.this, "unlike", Toast.LENGTH_SHORT).show();

            likeDislikePost(post_id, "unlike");

        }


    }

    private void get_post_details(String post_id) {
        UserPostDetailsRequestBody userPostDetailsRequestBody = Utils.create_post_details_request_body(post_id);
        disposable.add(apiService.getPostDetails(HelperMethods.get_user_jwt_key(), userPostDetailsRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserPostDetailsResponseBody>() {
                    @Override
                    public void onSuccess(UserPostDetailsResponseBody s) {

                        postLikesModels = new ArrayList<>();

                        postLikesModels = s.getData().getPost().getPost_likes();

                        if (postLikesModels.size() == 0) {
                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);

                        }
                        for (int i = 0; i <= postLikesModels.size() - 1; i++) {
                            if (postLikesModels.size() > 0) {
                                String underScoreIdd = s.getData().getPost().getPost_likes().get(i).getUser_id();
                                if (underScoreIdd.equals(HelperMethods.get_user_id())) {
                                    post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
//                                    Toast.makeText(NotificationDetailsPostActivity.this, "matched", Toast.LENGTH_SHORT).show();

                                } else {
                                    post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);

                                }
                            }


                        }

                        set_post_details(s);
                    }


                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);


                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    @OnClick(R.id.post_button_tv)
    public void post_comment() {
        if (!comment_post_edit_text.getText().toString().isEmpty()) {
            if (HelperMethods.isConnectedToInternet(NotificationDetailsPostActivity.this)) {
                make_comment_on_post(comment_post_edit_text.getText().toString());
            } else {

            }

        } else {

        }
    }

    private void make_comment_on_post(String comment_on_post) {
        show_progress_dialog();
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        PostCommentRequestBody post_comments_body = HelperMethods.create_post_comment_body(getPost_id(), comment_on_post);
        if (disposable != null && apiService != null) {
            disposable.add(compositeDisposable);
            disposable.add(apiService.postCommentOnUserPost(HelperMethods.get_user_jwt_key(), post_comments_body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<PostCommentResponse>() {
                        @Override
                        public void onSuccess(PostCommentResponse s) {
                            dismiss_progress_dialog();
                            get_post_comment(getPost_id());
                            total_number_of_comments = total_number_of_comments + 1;
                            user_number_of_comments.setText(total_number_of_comments + " comments");
                        }

                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof HttpException) {
                                ResponseBody body = ((HttpException) e).response().errorBody();
                                try {
                                    String error_body = body.string();
                                    String error = Utils.get_error_from_response(error_body);
                                    dismiss_progress_dialog();
                                    if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                    }

                                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }));


        }

    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    @OnClick(R.id.back_button)
    public void click_on_back_button() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private int increase_number_of_likes_by_one() {
        return post_total_number_of_likes = post_total_number_of_likes + 1;
    }

    private int decrease_number_of_likes_by_one() {

        return post_total_number_of_likes = post_total_number_of_likes - 1;
    }

    private void set_post_details(UserPostDetailsResponseBody post_details) {

//        if (post_details.getData().getPost().isIs_user_self_liked()) {
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
////            post_like_button.getLayoutParams().height = Utils.dpToPx(40);
////            post_like_button.getLayoutParams().width = Utils.dpToPx(40);
////            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
////            marginParams.setMargins(25, 25, 25, 25);
//
//            post_like_button.setTag("user_liked");
//        } else {
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
////            post_like_button.getLayoutParams().height = Utils.dpToPx(40);
////            post_like_button.getLayoutParams().width = Utils.dpToPx(40);
////            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
////            marginParams.setMargins(25, 25, 25, 25);
//            post_like_button.setTag("user_not_liked");
//        }

        if (post_details.getData().getPost().getPost_image_url() != null && !post_details.getData().getPost().getPost_image_url().isEmpty()) {
            Picasso.with(NotificationDetailsPostActivity.this)
                    .load(post_details.getData().getPost().getPost_image_url())
                    .into(user_post_image);
        }

        if (post_details.getData().getPost().getUser_profile_pic_url() != null && !post_details.getData().getPost().getUser_profile_pic_url().isEmpty()) {
            Picasso.with(NotificationDetailsPostActivity.this)
                    .load(post_details.getData().getPost().getUser_profile_pic_url())
                    .fit()
                    .into(user_profile_pic_rounded);
            Picasso.with(NotificationDetailsPostActivity.this)
                    .load(post_details.getData().getPost().getUser_profile_pic_url())
                    .fit()
                    .into(user_comment_profile);

        }


        if (post_details.getData().getPost() != null && post_details.getData().getPost().getPost_timestamp() != 0) {
            Long l2 = Long.valueOf(post_details.getData().getPost().getPost_timestamp());
            post_time.setText(HelperMethods.timeAgo(l2) + "");
        }


        if (post_details.getData().getPost().getPost_location() != null && !post_details.getData().getPost().getPost_location().isEmpty()) {
            user_location.setText(post_details.getData().getPost().getPost_location());
        }

//        if (post_details.getData().getPost().getPost_user_name() != null && !post_details.getData().getPost().getPost_user_name().isEmpty()) {
//            user_name_tv.setText(post_details.getData().getPost().getPost_user_name());
//        }
        if (!post_details.getData().getPost().getUser_name().equalsIgnoreCase("")) {
            user_name_tv.setText(post_details.getData().getPost().getUser_name());
        } else {
            if (post_details.getData().getPost().getPost_user_name() != null && !post_details.getData().getPost().getPost_user_name().isEmpty()) {
                user_name_tv.setText(post_details.getData().getPost().getPost_user_name());
            }
        }

        if (post_details.getData().getPost().getPost_comments() != null && post_details.getData().getPost().getPost_comments().size() > 0) {
            total_number_of_comments = post_details.getData().getPost().getPost_comments().size();
            user_number_of_comments.setText(total_number_of_comments + " comments");
        } else {
            user_number_of_comments.setText(0 + " comments");
        }
        if (post_details.getData().getPost().getPost_likes() != null && post_details.getData().getPost().getPost_likes().size() > 0) {

            post_total_number_of_likes = post_details.getData().getPost().getPost_likes().size();


//        getCommentsApi(videoItem.getUnderScoreId());
            postLikesModels = new ArrayList<>();

            postLikesModels = post_details.getData().getPost().getPost_likes();

            if (postLikesModels.size() == 0) {
                post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);

            }
            for (int i = 0; i <= postLikesModels.size() - 1; i++) {
                if (postLikesModels.size() > 0) {
                    String underScoreIdd = post_details.getData().getPost().getPost_likes().get(i).getUser_id();
                    if (underScoreIdd.equals(HelperMethods.get_user_id())) {
                        post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);

                    } else {
                        post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);

                    }
                }


            }
            user_number_of_likes.setText(String.valueOf(post_details.getData().getPost().getPost_likes().size()));
//            Toast.makeText(this, "" + post_details.getData().getPost().getPost_likes().size(), Toast.LENGTH_SHORT).show();
            if (post_details.getData().getPost().getPost_likes().size() >= 4) {


                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.VISIBLE);
                fourth_person_dp.setVisibility(View.VISIBLE);


                String first_person_dp_url = post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() : "";


                String second_person_dp_url = post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() : "";

                String third_person_dp_url = post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url() : "";


                String fourth_person_dp_url = post_details.getData().getPost().getPost_likes().get(3).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(3).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(3).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(first_person_dp_url)
                        .fit()
                        .into(first_person_image);

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(second_person_dp_url)
                        .fit()
                        .into(second_person_dp);

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(third_person_dp_url)
                        .fit()
                        .into(third_person_dp);

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(fourth_person_dp_url)
                        .fit()
                        .into(fourth_person_dp);
            }


            if (post_details.getData().getPost().getPost_likes().size() == 1) {

                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.INVISIBLE);
                third_person_dp.setVisibility(View.INVISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);


                String first_person_dp_url = post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(first_person_dp_url)
                        .fit()
                        .into(first_person_image);


            }

            if (post_details.getData().getPost().getPost_likes().size() == 2) {

                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.INVISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);

                String first_person_dp_url = post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(first_person_dp_url)
                        .fit()
                        .into(first_person_image);

                String second_person_dp_url = post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(second_person_dp_url)
                        .fit()
                        .into(second_person_dp);


            }
            if (post_details.getData().getPost().getPost_likes().size() == 3) {
                first_person_image.setVisibility(View.VISIBLE);
                second_person_dp.setVisibility(View.VISIBLE);
                third_person_dp.setVisibility(View.VISIBLE);
                fourth_person_dp.setVisibility(View.INVISIBLE);

                String first_person_dp_url = post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(0).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(first_person_dp_url)
                        .fit()
                        .into(first_person_image);

                String second_person_dp_url = post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(1).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(second_person_dp_url)
                        .fit()
                        .into(second_person_dp);

                String third_person_dp_url = post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url() != null &&
                        !post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url().isEmpty() ?
                        post_details.getData().getPost().getPost_likes().get(2).getUser_profile_pic_url() : "";

                Picasso.with(NotificationDetailsPostActivity.this)
                        .load(third_person_dp_url)
                        .fit()
                        .into(third_person_dp);

            }


        } else {
            user_number_of_likes.setText("0");
            first_person_image.setVisibility(View.INVISIBLE);
            second_person_dp.setVisibility(View.INVISIBLE);
            third_person_dp.setVisibility(View.INVISIBLE);
            fourth_person_dp.setVisibility(View.INVISIBLE);
        }
    }


    @Subscribe
    public void comment_with_tag(PostCommentWithUserTag postCommentWithUserTag) {
        comment_post_edit_text.setText("@" + postCommentWithUserTag.getUser_name() + " ");
        comment_post_edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(comment_post_edit_text, InputMethodManager.SHOW_IMPLICIT);
        comment_post_edit_text.setSelection(comment_post_edit_text.getText().length());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }


    public void likeDislikePost(String post_id, String status) {
        PostLikeDislikeRequestBody post_like_dislike_body = HelperMethods.create_post_like_dislike_body(post_id);
        disposable.add(apiService.postLikeDislikeOnPost(HelperMethods.get_user_jwt_key(), post_like_dislike_body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostLikeDislikeResponse>() {
                    @Override
                    public void onSuccess(PostLikeDislikeResponse s) {
                        Timber.d("post like dislike done message " + s.getData().getMessage());
//                        if (post_like_button.getTag().toString().contentEquals("user_liked")) {
////            post_like_button.setBackgroundResource(R.drawable.like_unpressed);
////            post_like_button.getLayoutParams().height = Utils.dpToPx(40);
////            post_like_button.getLayoutParams().width = Utils.dpToPx(40);
////            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
////            marginParams.setMargins(25, 25, 25, 25);
//                            post_like_button.setTag("user_not_liked");
////            user_number_of_likes.setText(decrease_number_of_likes_by_one() + " likes");
//                            Toast.makeText(NotificationDetailsPostActivity.this, "Post Unliked", Toast.LENGTH_SHORT).show();
//                            int totallikesIncremnt = Integer.parseInt(user_number_of_likes.getText().toString()) - 1;
//                            user_number_of_likes.setText(String.valueOf(totallikesIncremnt));
//                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);
//
//
//                        } else {
////            post_like_button.setBackgroundResource(R.drawable.liked_pressed);
////            post_like_button.getLayoutParams().height = Utils.dpToPx(40);
////            post_like_button.getLayoutParams().width = Utils.dpToPx(40);
////            ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
////            marginParams.setMargins(25, 25, 25, 25);
//                            Toast.makeText(NotificationDetailsPostActivity.this, "Post liked", Toast.LENGTH_SHORT).show();
//
//                            int totallikesIncremnt = Integer.parseInt(user_number_of_likes.getText().toString()) + 1;
//                            user_number_of_likes.setText(String.valueOf(totallikesIncremnt));
//                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
//
//                            post_like_button.setTag("user_liked");
////            user_number_of_likes.setText(increase_number_of_likes_by_one() + " likes");
//                        }

                        if (status.equals("dolike")) {
                            int totallikesIncremnt = Integer.parseInt(user_number_of_likes.getText().toString()) + 1;
                            user_number_of_likes.setText(String.valueOf(totallikesIncremnt));
                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
                            post_like_button.setTag("user_liked");

                            Toast.makeText(NotificationDetailsPostActivity.this, s.getData().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            post_like_button.setTag("user_not_liked");
                            if (user_number_of_likes.getText().toString().equals("1")) {
                                user_number_of_likes.setText("0");
//                            Toast.makeText(ctx, "call", Toast.LENGTH_SHORT).show();
                                post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                                Toast.makeText(NotificationDetailsPostActivity.this, s.getData().getMessage(), Toast.LENGTH_SHORT).show();

                            } else {
                                int totallikesIncremnt = Integer.parseInt(user_number_of_likes.getText().toString()) - 1;
                                user_number_of_likes.setText(String.valueOf(totallikesIncremnt));
                                post_like_button.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                                Toast.makeText(NotificationDetailsPostActivity.this, s.getData().getMessage(), Toast.LENGTH_SHORT).show();

                            }


                        }


                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                }

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }


    private void increase_page_number() {
        page_number = page_number + 1;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }
}

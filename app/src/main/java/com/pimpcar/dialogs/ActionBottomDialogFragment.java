package com.pimpcar.dialogs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.JsonObject;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.models.comments.ParentChildComments;
import com.pimpcar.models.comments.adapter.ParentAdapter;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsReplyTextModel;
import com.pimpcar.models.comments.models.CommentsResponseModel;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActionBottomDialogFragment extends BottomSheetDialogFragment
        implements View.OnClickListener, ParentAdapter.Callback {
    public static final String TAG = "ActionBottomDialog";
    private ItemClickListener mListener;
    private RecyclerView recyclerViewParent;
    TextView tvTotalComments;
    private EditText etComments;
    List<CommentsDataModel> commentsDataModels;
    List<CommentsReplyTextModel> commentsReplyTextModels;
    KProgressHUD hud;
    BottomSheetDialog bottomSheetDialog;
    ArrayList<ParentChildComments> parentChildObj;
    ImageView ivBack, ivSEnd;
    View view;
    String posUunderScoreid = "", user_token = "";
    ParentAdapter parentAdapter;

    public static ActionBottomDialogFragment newInstance() {
        return new ActionBottomDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.comments_dialog, container, false);
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        commentsDataModels = new ArrayList<>();
        commentsReplyTextModels = new ArrayList<>();
        ParentAdapter adapter = new ParentAdapter(ActionBottomDialogFragment.this);
        String CurrentunderScoreId = HelperMethods.get_underScoreId();
        String currentUserid = HelperMethods.get_user_id();


        posUunderScoreid = Utilities.getString(getActivity(), "postunderScoreId");
//        String underScoreId = Utilities.getString(getActivity(), "underScoreId");
//        Toast.makeText(getActivity(), underScoreId, Toast.LENGTH_SHORT).show();
        String totalComments = Utilities.getString(getActivity(), "totalComments");
        user_token = Utilities.getString(getActivity(), "user_token");
//        Toast.makeText(getActivity(), ""+user_token, Toast.LENGTH_SHORT).show();
        ivBack = view.findViewById(R.id.ivBack);
        tvTotalComments = view.findViewById(R.id.tvTotalComments);
        etComments = view.findViewById(R.id.etComments);
        ivSEnd = view.findViewById(R.id.ivSEnd);
//        tvTryMantiest = view.findViewById(R.id.tvTryMantiest);
        recyclerViewParent = (RecyclerView) view.findViewById(R.id.rv_parent);
//        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getCommentsApi(posUunderScoreid);
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        tvTotalComments.setText(totalComments + " Comments");
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        ivSEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentText = etComments.getText().toString();
                if (!commentText.equals("")) {

                    createCommentApi(commentText, posUunderScoreid, HelperMethods.get_user_id(), timeStamp);

                } else {
                    Toast.makeText(getActivity(), "write Something", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ItemClickListener) {
            mListener = (ItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        TextView tvSelected = (TextView) view;
        mListener.onItemClick(tvSelected.getText().toString());
        dismiss();
    }

    @Override
    public void onItemClick(int pos) {
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        String posUunderScoreid = Utilities.getString(getActivity(), "postunderScoreId");
        bottomSheetDialog = new BottomSheetDialog(getActivity());
        bottomSheetDialog.setContentView(R.layout.create_comment_dialog);
        getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout rlSend = bottomSheetDialog.findViewById(R.id.rlSend);
        final EditText etComments = bottomSheetDialog.findViewById(R.id.etComments);
//        Toast.makeText(getActivity(), "" + commentsDataModels.get(pos).get_id(), Toast.LENGTH_SHORT).show();
        rlSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String posUunderScoreid = Utilities.getString(getActivity(), "postunderScoreId");
                String commentText = etComments.getText().toString();
                if (!commentText.equals("")) {
                    createCommentApi(commentText, commentsDataModels.get(pos).get_id(), posUunderScoreid, HelperMethods.get_user_id(), timeStamp);
                    bottomSheetDialog.cancel();

                } else {
                    Toast.makeText(getActivity(), "write Something", Toast.LENGTH_SHORT).show();
                }
            }
        });


        bottomSheetDialog.show();


    }

    public interface ItemClickListener {
        void onItemClick(String item);
    }

    private void getCommentsApi(String id) {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<CommentsResponseModel> call = getResponse.getCommetnsApi(id);
        Log.d("assss", "asss");
        call.enqueue(new Callback<CommentsResponseModel>() {
            @Override
            public void onResponse(Call<CommentsResponseModel> call, Response<CommentsResponseModel> response) {
                if (response.code() == 200) {
                    hud.dismiss();
                    commentsDataModels = response.body().getComeentdata();
//                    Toast.makeText(getActivity(), "Parent Success", Toast.LENGTH_SHORT).show();

                    for (int i = 0; i <= commentsDataModels.size() - 1; i++) {
                        commentsReplyTextModels = response.body().getComeentdata().get(i).getComment_replies();

//                        Toast.makeText(getActivity(), "Child Success", Toast.LENGTH_SHORT).show();
                    }
                    LinearLayoutManager manager = new LinearLayoutManager(getActivity());
                    manager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerViewParent.setLayoutManager(manager);
                    recyclerViewParent.setHasFixedSize(false);
                    parentAdapter = new ParentAdapter(getActivity(), commentsDataModels, ActionBottomDialogFragment.this);
                    recyclerViewParent.setAdapter(parentAdapter);
                } else {
                    Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void createCommentApi(String commentText, String postId, String userid, String timestamp) {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", userid);
        jsonObject.addProperty("comment_timestamp", timestamp);
        jsonObject.addProperty("comment_statement", commentText);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<CommentsLikeResponseModel> call = getResponse.createCommentsApi(postId, jsonObject);
        call.enqueue(new Callback<CommentsLikeResponseModel>() {
            @Override
            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
                if (response.code() == 200) {
                    hud.dismiss();
//                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();
//                    dismiss();
                    getCommentsApi(posUunderScoreid);
                    etComments.setText("");
                    Toast.makeText(getActivity(), "comment add Successfully", Toast.LENGTH_SHORT).show();


                } else {
                    Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }



    private void createCommentApi(String comment_text, String comment_id, String postId, String user_id, String timestamp) {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        JsonObject comment_replies = new JsonObject();
        jsonObject.add("comment_replies", comment_replies);
        jsonObject.addProperty("comment_id", comment_id);
        comment_replies.addProperty("user_id", user_id);
        comment_replies.addProperty("timestamp", timestamp);
        comment_replies.addProperty("comment_text", comment_text);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<CommentsLikeResponseModel> call = getResponse.createCommentsReplyApi(postId, jsonObject);
        call.enqueue(new Callback<CommentsLikeResponseModel>() {
            @Override
            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
                int code = response.code();

                if (code == 200) {
                    hud.dismiss();
//                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();
                    bottomSheetDialog.dismiss();
                    getCommentsApi(posUunderScoreid);
                    Toast.makeText(getActivity(), "comment add Successfully", Toast.LENGTH_SHORT).show();


                } else {
                    hud.dismiss();
                    Toast.makeText(getActivity(), "something is went wrong", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }


}
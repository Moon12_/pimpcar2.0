package com.pimpcar.Application;

import android.app.Application;
import android.content.Context;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.pimpcar.BuildConfig;
import com.pimpcar.utils.timber.NotLoggingTree;

import timber.log.Timber;


public class PimpCarApplication extends Application {

    private static PimpCarApplication application;
    private Context userSelfProfileActivity = null;

    public static synchronized PimpCarApplication getInstance() {
        return application;
    }

    public static PimpCarApplication getApplicationInstance() {
        if (application != null)
            return application;
        return null;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        //facebook SDK init
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());
        else
            Timber.plant(new NotLoggingTree());

    }

}

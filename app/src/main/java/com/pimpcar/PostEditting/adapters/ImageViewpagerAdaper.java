package com.pimpcar.PostEditting.adapters;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.pimpcar.PostEditting.fragments.CarComponentsFragment;

public class ImageViewpagerAdaper extends FragmentPagerAdapter {
    private final Context ctxt;
    private int pageCount = 0;
    private String[] cat;
    public ImageViewpagerAdaper(Context ctxt, FragmentManager mgr, String[] category) {
        super(mgr);
        this.ctxt = ctxt;
        this.cat=category;
    }

    @Override
    public int getCount() {
        return (pageCount);
    }

    @Override
    public Fragment getItem(int position) {
        return (CarComponentsFragment.newInstance(position,cat));
    }

    @Override
    public String getPageTitle(int position) {
        return (CarComponentsFragment.getTitle(ctxt, position));
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}

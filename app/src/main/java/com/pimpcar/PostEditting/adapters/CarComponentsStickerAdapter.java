package com.pimpcar.PostEditting.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pimpcar.PostEditting.custom_interfaces.RecyclerViewClickListener;
import com.pimpcar.PostEditting.fragments.pojos.StickerSelectionEvent;
import com.pimpcar.R;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarComponentsStickerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context _mContext;
    private LayoutInflater layoutInflater;
    private RecyclerViewClickListener mListener;
    private List<String> _Mwh;

    public CarComponentsStickerAdapter(Context context, RecyclerViewClickListener listener, List<String> wheels) {
        this._mContext = context;
        this.layoutInflater = LayoutInflater.from(_mContext);
        this.mListener = listener;
        this._Mwh = wheels;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == _Mwh.size()) {
            return 1;
        } else {
            return 0;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        switch (i) {
            case 0: {
                View itemView = layoutInflater.inflate(R.layout.stickers_item_layout, parent, false);
                ButterKnife.bind(this, itemView);
                return new MyStickerViewHolder(itemView, mListener);
            }
            case 1: {
                View itemView = layoutInflater.inflate(R.layout.get_more_item, parent, false);
                ButterKnife.bind(this, itemView);
                return new MyGetMoreItemHolder(itemView, mListener);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case 0: {
                Picasso.with(_mContext)
                        .load(_Mwh.get(i))
                        .fit()
                        .into(((MyStickerViewHolder) viewHolder).sticker_image_view);

                ((MyStickerViewHolder) viewHolder).sticker_image_view.setOnClickListener(v -> EventBus.getDefault().post(new StickerSelectionEvent(_Mwh.get(i))));
                break;
            }
            case 1: {
                ((MyGetMoreItemHolder) viewHolder).sticker_image.setVisibility(View.GONE);
                ((MyGetMoreItemHolder) viewHolder).sticker_image.setOnClickListener(v -> Toast.makeText(_mContext, "get more item", Toast.LENGTH_SHORT).show());
                break;
            }
        }
    }


    @Override
    public int getItemCount() {
        return _Mwh.size() + 1;
    }

    class MyStickerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sticker_image_view)
        AppCompatImageView sticker_image_view;
        private RecyclerViewClickListener mListener;

        MyStickerViewHolder(@NonNull View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
        }
    }

    class MyGetMoreItemHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.sticker_image)
        AppCompatImageView sticker_image;
        private RecyclerViewClickListener Listener;

        MyGetMoreItemHolder(View itemView, RecyclerViewClickListener mListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Listener = mListener;
        }
    }
}

package com.pimpcar.PostEditting.fragments.pojos;

public class StickerSelectionEvent {
    private String image_resource_id = "";

    public StickerSelectionEvent(String image_resource_id) {

        this.image_resource_id = image_resource_id;
    }

    public String getImage_resource_id() {
        return image_resource_id;
    }

    public void setImage_resource_id(String image_resource_id) {
        this.image_resource_id = image_resource_id;
    }
}

package com.pimpcar.PostEditting.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.PostEditting.adapters.CarComponentsStickerAdapter;
import com.pimpcar.PostEditting.custom_interfaces.RecyclerViewClickListener;
import com.pimpcar.PostEditting.fragments.pojos.StickerApiResponse;
import com.pimpcar.PostEditting.fragments.pojos.WheelApiResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.progressbar.SpinView;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;

import static com.facebook.FacebookSdk.getApplicationContext;

@SuppressLint("ValidFragment")
public class CarComponentsFragment extends Fragment {
    private static final String KEY_POSITION = "position";
    private static final String KEY_String = "catName";
    SpinView spinView;
    @BindView(R.id.recycler_view_components)
    RecyclerView recycler_view_components;
    RecyclerViewClickListener recyclerViewClickListener;
    int position = 0;
    String categroyName = "";
    private ApiService apiService;
    private Unbinder unbinder;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int wheels[] = {R.drawable.wheel_1, R.drawable.wheel_2, R.drawable.wheel_3, R.drawable.wheel_4,
            R.drawable.wheel_5, R.drawable.wheel_6, R.drawable.wheel_7, R.drawable.wheel_8, R.drawable.wheel_9, R.drawable.wheel_10,
            R.drawable.wheel_11, R.drawable.wheel_12, R.drawable.wheel_13, R.drawable.wheel_14, R.drawable.wheel_15, R.drawable.wheel_16,
            R.drawable.wheel_17, R.drawable.wheel_18, R.drawable.wheel_19, R.drawable.wheel_20, R.drawable.wheel_21, R.drawable.wheel_22,
            R.drawable.wheel_22, R.drawable.wheel_23, R.drawable.wheel_24, R.drawable.wheel_25, R.drawable.wheel_26, R.drawable.wheel_27,
            R.drawable.wheel_28, R.drawable.wheel_29, R.drawable.wheel_30, R.drawable.wheel_31, R.drawable.wheel_33};

    private CarComponentsStickerAdapter carComponentsStickerAdapter;

    public static CarComponentsFragment newInstance(int position, String[] cat) {
        CarComponentsFragment frag = new CarComponentsFragment();
        Bundle args = new Bundle();
        args.putString(KEY_String, cat[position]);
        args.putInt(KEY_POSITION, position);
        frag.setArguments(args);

        return (frag);
    }

    public static String getTitle(Context context, int position) {
        return (String.format(context.getString(R.string.hint), position + 1));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.components_stickers, container, false);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        spinView = view.findViewById(R.id.progress_bar);
        if (getArguments() != null) {
            position = getArguments().getInt(KEY_POSITION, -1);
            categroyName = getArguments().getString(KEY_String);
        }

        if (categroyName.equals("Wheel")) {
            showWheelList();
        } else {
            showStickerList();
        }
    }

    private void showStickerList() {
        showProgress(spinView);
        disposable.add(apiService.getStickerWheel(HelperMethods.get_user_jwt_key())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<StickerApiResponse>() {
                    @Override
                    public void onSuccess(StickerApiResponse s) {
                        if (s != null) {
                            hideProgress(spinView);
                            carComponentsStickerAdapter = new CarComponentsStickerAdapter(getActivity(), recyclerViewClickListener, s.getData().getSticker());
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                            recycler_view_components.setLayoutManager(gridLayoutManager);
                            recycler_view_components.setAdapter(carComponentsStickerAdapter);

                        }


                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        hideProgress(spinView);

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, "Something Went Wrong !!"));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    private void showWheelList() {
        showProgress(spinView);
        disposable.add(apiService.getWheel(HelperMethods.get_user_jwt_key())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<WheelApiResponse>() {
                    @Override
                    public void onSuccess(WheelApiResponse s) {
                        if (s != null) {
                            hideProgress(spinView);
                            carComponentsStickerAdapter = new CarComponentsStickerAdapter(getActivity(), recyclerViewClickListener, s.getData().getWheels());
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                            recycler_view_components.setLayoutManager(gridLayoutManager);
                            recycler_view_components.setAdapter(carComponentsStickerAdapter);

                        }


                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        hideProgress(spinView);
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    private void showProgress(SpinView spinView) {
        if (spinView != null) {
            if (spinView.getVisibility() == View.GONE) {
                spinView.setVisibility(View.VISIBLE);
            }

        }

    }


    private void hideProgress(SpinView spinView) {
        if (spinView != null) {
            if (spinView.getVisibility() == View.VISIBLE) {
                spinView.setVisibility(View.GONE);
            }

        }

    }


}

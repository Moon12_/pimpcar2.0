package com.pimpcar.PostEditting.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_search.UserSearchRequestBody;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.PostEditting.adapters.SearchUserAdapter;
import com.pimpcar.PostEditting.eventbus.models.TagUserGetDataInActivityEvent;
import com.pimpcar.PostEditting.fragments.events.RecyclerViewItemClickMode;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class UserSearchDialogFragment extends DialogFragment {


    @BindView(R.id.search_user_et)
    AppCompatEditText search_user_et;
    @BindView(R.id.user_search_list)
    RecyclerView user_search_list;
    @BindView(R.id.search_nothing_result)
    RelativeLayout search_nothing_result;
    @BindView(R.id.search_ic_empty)
    AppCompatImageView search_ic_empty;
    @BindView(R.id.search_result_foundanything_tv)
    AppCompatTextView search_result_foundanything_tv;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private SearchUserAdapter searchUserAdapter;
    private String user_tagged_data[];
    private Unbinder unbinder;
    private OnCompleteListener mListener;

    public static UserSearchDialogFragment newInstance(String title) {
        UserSearchDialogFragment frag = new UserSearchDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
            EventBus.getDefault().register(this);

        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root_view = inflater.inflate(R.layout.search_user_for_tag, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        return root_view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final AppCompatEditText searchEditText = view.findViewById(R.id.search_user_et);

        search_result_foundanything_tv.setVisibility(View.GONE);
        user_search_list.setVisibility(View.GONE);
        search_nothing_result.setVisibility(View.VISIBLE);
        search_ic_empty.setVisibility(View.VISIBLE);


        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() > 2)
                    search_user_text_regex(s);

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


    }

    private void search_user_text_regex(CharSequence s) {


        String search_text_change = s.toString();

        UserSearchRequestBody userSearchRequestBody = Utils.create_user_search_body(search_text_change);

        disposable.add(apiService.getSearchUsers(HelperMethods.get_user_jwt_key(), userSearchRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserSearchResponseBody>() {
                    @Override
                    public void onSuccess(UserSearchResponseBody s) {


                        if (s.getData() != null && s.getData().getSerached_data() != null) {
                            Timber.d("user search database" + s.getData().getSerached_data().size());


                            if (s.getData().getSerached_data().size() > 0) {

                                search_ic_empty.setVisibility(View.GONE);
                                search_result_foundanything_tv.setVisibility(View.GONE);
                                search_nothing_result.setVisibility(View.GONE);
                                user_search_list.setVisibility(View.VISIBLE);

                                searchUserAdapter = new SearchUserAdapter(getActivity(), s.getData().getSerached_data(), false);
                                user_search_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                                user_search_list.setItemAnimator(new DefaultItemAnimator());
                                user_search_list.setAdapter(searchUserAdapter);


                            } else {
                                user_search_list.setVisibility(View.GONE);
                                search_ic_empty.setVisibility(View.VISIBLE);
                                search_result_foundanything_tv.setVisibility(View.VISIBLE);
                                search_nothing_result.setVisibility(View.VISIBLE);

                            }

                        } else {
                            user_search_list.setVisibility(View.GONE);
                            search_ic_empty.setVisibility(View.VISIBLE);
                            search_result_foundanything_tv.setVisibility(View.VISIBLE);
                            search_nothing_result.setVisibility(View.VISIBLE);

                        }


                    }


                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                user_search_list.setVisibility(View.GONE);
                                search_ic_empty.setVisibility(View.VISIBLE);
                                search_result_foundanything_tv.setVisibility(View.VISIBLE);
                                search_nothing_result.setVisibility(View.VISIBLE);

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    private void search_data_on_server(String toString) {

    }

    @Subscribe
    public void clickOnItem(RecyclerViewItemClickMode recyclerViewItemClickMode) {
        EventBus.getDefault().post(new TagUserGetDataInActivityEvent(recyclerViewItemClickMode));
        this.mListener.onComplete(recyclerViewItemClickMode);
        Timber.d("user name " + recyclerViewItemClickMode.getUser_name() + " user _id " + recyclerViewItemClickMode.getUser_id());
        Timber.d("profile pic " + recyclerViewItemClickMode.getUser_profile_pic());

        dismiss();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }


    public static interface OnCompleteListener {
        public abstract void onComplete(RecyclerViewItemClickMode time);
    }
}

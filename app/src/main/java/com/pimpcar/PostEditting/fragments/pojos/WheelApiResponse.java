package com.pimpcar.PostEditting.fragments.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WheelApiResponse {
    @SerializedName("data")
    @Expose
    private WheelApiPOJO data;

    public WheelApiPOJO getData() {
        return data;
    }

    public void setData(WheelApiPOJO data) {
        this.data = data;
    }


    public class WheelApiPOJO {
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("wheels")
        @Expose
        private List<String> wheels = null;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<String> getWheels() {
            return wheels;
        }

        public void setWheels(List<String> wheels) {
            this.wheels = wheels;
        }
    }

}

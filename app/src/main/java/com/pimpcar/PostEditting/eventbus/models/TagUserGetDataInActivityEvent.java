package com.pimpcar.PostEditting.eventbus.models;

import com.pimpcar.PostEditting.fragments.events.RecyclerViewItemClickMode;

public class TagUserGetDataInActivityEvent {

    private RecyclerViewItemClickMode recyclerViewItemClickMode;

    public TagUserGetDataInActivityEvent(RecyclerViewItemClickMode user_data_object) {

        this.recyclerViewItemClickMode = user_data_object;

    }

    public RecyclerViewItemClickMode getRecyclerViewItemClickMode() {
        return recyclerViewItemClickMode;
    }

    public void setRecyclerViewItemClickMode(RecyclerViewItemClickMode recyclerViewItemClickMode) {
        this.recyclerViewItemClickMode = recyclerViewItemClickMode;
    }
}

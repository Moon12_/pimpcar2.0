package com.pimpcar.PostEditting.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pimpcar.HomeFeed.pojos.BitmapDataSending;
import com.pimpcar.PostEditting.adapters.BottomScrollOptionAdapter;
import com.pimpcar.PostEditting.adapters.ImageViewpagerAdaper;
import com.pimpcar.PostEditting.additionalfiles.BitmapStickerIcon;
import com.pimpcar.PostEditting.additionalfiles.DrawableSticker;
import com.pimpcar.PostEditting.additionalfiles.Sticker;
import com.pimpcar.PostEditting.additionalfiles.StickerView;
import com.pimpcar.PostEditting.fragments.pojos.StickerSelectionEvent;
import com.pimpcar.R;
import com.pimpcar.Widgets.ProportionalImageView;
import com.pimpcar.Widgets.districtscrollview.DSVOrientation;
import com.pimpcar.Widgets.districtscrollview.DiscreteScrollView;
import com.pimpcar.Widgets.districtscrollview.ScaleTransformer;
import com.pimpcar.Widgets.districtscrollview.transform.Pivot;
import com.pimpcar.Widgets.image_editview.BrushImageView;
import com.pimpcar.Widgets.image_editview.TouchImageView;
import com.pimpcar.Widgets.staticviewpager.NonSwipeableViewPager;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImagePimpingActivity extends AppCompatActivity {

    private static final int POST_SUBMIT_REQUEST_CODE = 23987;
    @BindView(R.id.horizontalScrollView)
    DiscreteScrollView horizontalScrollViewl;

    @BindView(R.id.sticker_view)
    StickerView sticker_view;

    @BindView(R.id.major_major_image)
    ProportionalImageView major_major_image;

    @BindView(R.id.icons_view_pager)
    NonSwipeableViewPager icons_view_pager;

    @BindView(R.id.middle_fab_icon)
    FloatingActionButton middle_fab_icon;
    @BindView(R.id.rl_image_view_container)
    RelativeLayout rlImageViewContainer;
    @BindView(R.id.touchImageView)
    TouchImageView touchImageView;
    @BindView(R.id.brushContainingView)
    BrushImageView brushImageView;
    @BindView(R.id.sb_offset)
    SeekBar sbOffset;
    @BindView(R.id.sb_width)
    SeekBar sbWidth;
    @BindView(R.id.ll_top_bar)
    LinearLayout llTopBar;
    @BindView(R.id.iv_undo)
    AppCompatImageView ivUndo;
    @BindView(R.id.iv_redo)
    AppCompatImageView ivRedo;
    @BindView(R.id.iv_done)
    AppCompatImageView ivDone;
    @BindView(R.id.widthcontainer)
    LinearLayout widthcontainer;
    @BindView(R.id.next_post_button)
    FloatingActionButton next_post_button;
    @BindView(R.id.edit_done_button)
    FloatingActionButton edit_done_button;
    @BindView(R.id.undo_relative_layout)
    RelativeLayout undo_relative_layout;
    @BindView(R.id.image_rotate_full)
    FloatingActionButton image_rotate_full;
    int angle = 0;
    String current_resource_id = "-1";
    Drawable drawable;
    Drawable d;
    int dra;
    private int bottom_icons[] = {R.drawable.wheel, R.drawable.wheel_1};
    private String category[] = {"Wheel", "Sticker"};
    private BottomScrollOptionAdapter bottomScrollOptionAdapter;
    private int smallBrush, mediumBrush, largeBrush;
    private Bitmap main_bitmap_image;
    private BitmapDataSending bitmapDataSending;
    private String selected_image_path;
    private Vector<Integer> brushSizes;
    private Vector<Integer> redoBrushSizes;
    private ArrayList<Path> paths;
    private ArrayList<Path> redoPaths;
    private Canvas canvasMaster;
    private Point mainViewSize;
    private Path drawingPath;
    private int MODE;
    private Bitmap bitmapMaster;
    private Bitmap lastEditedBitmap;
    private Bitmap originalBitmap;
    private Bitmap resizedBitmap;
    private boolean isMultipleTouchErasing;
    private boolean isTouchOnBitmap;
    private int initialDrawingCount;
    private int updatedBrushSize;
    private int imageViewWidth;
    private float currentx;
    private float currenty;
    private int initialDrawingCountLimit = 20;
    private int offset = 250;
    private int undoLimit = 10;
    private float brushSize = 70.0f;
    private boolean isImageResized;
    private int imageViewHeight;
    private ImageViewpagerAdaper sampleAdapter;
    private InterstitialAd interstitial;

    public ImagePimpingActivity() {
        paths = new ArrayList();
        redoPaths = new ArrayList();
        brushSizes = new Vector();
        redoBrushSizes = new Vector();
        MODE = 0;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pimp_post_image_editor);
        ButterKnife.bind(this);

        sbWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            float progress = 0f;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;

                if (drawable != null) {
//                    sticker_view.removeCurrentSticker();
                    Bitmap myLogo = drawableToBitmap(drawable);
                    Matrix mMatrix = new Matrix();
                    // Bitmap bmp = BitmapFactory.decodeResource(getResources(), myLogo);

                    Camera camera = new Camera();
                    camera.save();
                    // camera.rotateY(progress);
                    camera.rotateX(-progress);
                    //camera.rotateZ(progress);
                    camera.getMatrix(mMatrix);
                    camera.restore();

                    Bitmap bm = Bitmap.createBitmap(myLogo, 0, 0, myLogo.getWidth(),
                            myLogo.getHeight(), mMatrix, true);
                    d = new BitmapDrawable(getResources(), bm);
                    sticker_view.addSticker(new DrawableSticker(d));


                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        sbOffset.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            float progress = 0f;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                progress = progresValue;

                if (drawable != null) {
                    sticker_view.removeCurrentSticker();
                    Bitmap myLogo = drawableToBitmap(drawable);
                    Matrix mMatrix = new Matrix();
                    // Bitmap bmp = BitmapFactory.decodeResource(getResources(), myLogo);

                    Camera camera = new Camera();
                    camera.save();
                    camera.rotateY(progress);
                    // camera.rotateX(-progress);
                    //camera.rotateZ(progress);
                    camera.getMatrix(mMatrix);
                    camera.restore();

                    Bitmap bm = Bitmap.createBitmap(myLogo, 0, 0, myLogo.getWidth(),
                            myLogo.getHeight(), mMatrix, true);
                    d = new BitmapDrawable(getResources(), bm);
                    sticker_view.addSticker(new DrawableSticker(d));


                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);


        Bundle bundle = getIntent().getExtras();

        selected_image_path = bundle.getString("selected_image");


        if (selected_image_path != null) {

            File imageFile = new File(selected_image_path);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            main_bitmap_image = BitmapFactory.decodeFile(imageFile.getPath(), bmOptions);
            //  main_bitmap_image = rotate_bitmap(main_bitmap_image);
        }

        if (main_bitmap_image != null)
            major_major_image.setImageBitmap(main_bitmap_image);


        bottomScrollOptionAdapter = new BottomScrollOptionAdapter(this, bottom_icons, category);
        horizontalScrollViewl.setAdapter(bottomScrollOptionAdapter);
        horizontalScrollViewl.setOrientation(DSVOrientation.HORIZONTAL);
        horizontalScrollViewl.setOverScrollEnabled(true);
        horizontalScrollViewl.setOffscreenItems(3);
        horizontalScrollViewl.setItemTransitionTimeMillis(700);
        horizontalScrollViewl.setItemTransformer(new ScaleTransformer.Builder().setMaxScale(1.05f)
                .setMinScale(0.8f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default onex
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build());


        horizontalScrollViewl.addScrollListener(new DiscreteScrollView.ScrollListener<RecyclerView.ViewHolder>() {
            @Override
            public void onScroll(float scrollPosition, int currentPosition, int newPosition, @Nullable RecyclerView.ViewHolder currentHolder, @Nullable RecyclerView.ViewHolder newCurrent) {

                icons_view_pager.setCurrentItem(newPosition);
            }
        });


        set_view_pager_for_pimping_stickers();
        set_sticker_listener();
        icons_view_pager.setTag("visible");
        horizontalScrollViewl.setTag("visible");


        set_up_image_eraser();
        setup_view();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.image_rotate_full)
    public void click_on_bitmap() {

        Bitmap rotated_bitmap = rotate_bitmap(main_bitmap_image);
        main_bitmap_image = rotated_bitmap;
        major_major_image.setImageBitmap(main_bitmap_image);


    }

    public Bitmap rotate_bitmap(Bitmap bitmap_org) {
        Matrix matrix = new Matrix();
        angle = angle + 90;
        Log.d("checkingValue", "" + angle);
        if (angle == 360) {
            angle = 0;
            matrix.postRotate(angle);
        } else {
            matrix.postRotate(angle);
        }


        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap_org, bitmap_org.getWidth(), bitmap_org.getHeight(), true);

        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    @SuppressLint("RestrictedApi")
    @OnClick(R.id.edit_done_button)
    void edit_done_button_on_click() {
        if (brushImageView != null && sticker_view != null && major_major_image != null) {
            brushImageView.setVisibility(View.GONE);
            Bitmap temp_bitmap = sticker_view.createBitmap();
            widthcontainer.setVisibility(View.GONE);
            rlImageViewContainer.setVisibility(View.GONE);
            edit_done_button.setVisibility(View.GONE);
            undo_relative_layout.setVisibility(View.GONE);
            next_post_button.setVisibility(View.VISIBLE);
            horizontalScrollViewl.setVisibility(View.VISIBLE);
            horizontalScrollViewl.setTag("visible");
            sticker_view.setLocked(true);
            major_major_image.setImageBitmap(temp_bitmap);
            sticker_view.setLocked(false);
        } else {

        }
    }

    private void setup_view() {


        rlImageViewContainer.getLayoutParams().height = mainViewSize.y;
        imageViewWidth = mainViewSize.x;
        imageViewHeight = rlImageViewContainer.getLayoutParams().height;

        ivUndo.setOnClickListener(v -> undo());

        ivRedo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                redo();
            }
        });

        ivDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                saveImage();
            }
        });

        touchImageView.setOnTouchListener(new OnTouchListner());
//        sbWidth.setMax(150);
////        sbWidth.setProgress((int) (brushSize - 20.0f));
////        sbWidth.setOnSeekBarChangeListener(new OnWidthSeekbarChangeListner());
//        sbOffset.setMax(350);
//        sbOffset.setProgress(offset);
//        sbOffset.setOnSeekBarChangeListener(new OnOffsetSeekbarChangeListner());
    }

    private void set_up_image_eraser() {
        drawingPath = new Path();
        Display display = getWindowManager().getDefaultDisplay();
        mainViewSize = new Point();
        display.getSize(mainViewSize);

    }

    public void undo() {
        int size = this.paths.size();
        if (size != 0) {
            if (size == 1) {
                this.ivUndo.setEnabled(false);
            }
            size--;
            redoPaths.add(paths.remove(size));
            redoBrushSizes.add(brushSizes.remove(size));
            if (!ivRedo.isEnabled()) {
                ivRedo.setEnabled(true);
            }
            UpdateCanvas();
        }
    }

    public void redo() {
        int size = redoPaths.size();
        if (size != 0) {
            if (size == 1) {
                ivRedo.setEnabled(false);
            }
            size--;
            paths.add(redoPaths.remove(size));
            brushSizes.add(redoBrushSizes.remove(size));
            if (!ivUndo.isEnabled()) {
                ivUndo.setEnabled(true);
            }
            UpdateCanvas();
        }
    }

    public void updateBrushWidth() {
        brushImageView.width = brushSize / 2.0f;
        brushImageView.invalidate();
    }

    public void updateBrushOffset() {
        float doffest = ((float) offset) - brushImageView.offset;
        BrushImageView brushImageViewView = brushImageView;
        brushImageViewView.centery += doffest;
        brushImageView.offset = (float) offset;
        brushImageView.invalidate();
    }

    public void setBitMap() {
        this.isImageResized = false;
        if (resizedBitmap != null) {
            resizedBitmap.recycle();
            resizedBitmap = null;
        }
        if (bitmapMaster != null) {
            bitmapMaster.recycle();
            bitmapMaster = null;
        }
        canvasMaster = null;
        resizedBitmap = resizeBitmapByCanvas();

        lastEditedBitmap = resizedBitmap.copy(Bitmap.Config.ARGB_8888, true);
        bitmapMaster = Bitmap.createBitmap(lastEditedBitmap.getWidth(), lastEditedBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        canvasMaster = new Canvas(bitmapMaster);
        canvasMaster.drawBitmap(lastEditedBitmap, 0.0f, 0.0f, null);
        touchImageView.setImageBitmap(bitmapMaster);
        resetPathArrays();
        touchImageView.setPan(false);
        brushImageView.invalidate();
    }

    public void resetPathArrays() {
        ivUndo.setEnabled(false);
        ivRedo.setEnabled(false);
        paths.clear();
        brushSizes.clear();
        redoPaths.clear();
        redoBrushSizes.clear();
    }

    public Bitmap resizeBitmapByCanvas() {
        float width;
        float heigth;
        float orginalWidth = (float) originalBitmap.getWidth();
        float orginalHeight = (float) originalBitmap.getHeight();
        if (orginalWidth > orginalHeight) {
            width = (float) imageViewWidth;
            heigth = (((float) imageViewWidth) * orginalHeight) / orginalWidth;
        } else {
            heigth = (float) imageViewHeight;
            width = (((float) imageViewHeight) * orginalWidth) / orginalHeight;
        }
        if (width > orginalWidth || heigth > orginalHeight) {
            return originalBitmap;
        }
        Bitmap background = Bitmap.createBitmap((int) width, (int) heigth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(background);
        float scale = width / orginalWidth;
        float yTranslation = (heigth - (orginalHeight * scale)) / 2.0f;
        Matrix transformation = new Matrix();
        transformation.postTranslate(0.0f, yTranslation);
        transformation.preScale(scale, scale);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawBitmap(originalBitmap, transformation, paint);
        this.isImageResized = true;
        return background;
    }

    @OnClick(R.id.next_post_button)
    public void next_post_button_click() {
        showAds();

    }

    private void showAds() {

//        AdRequest adIRequest = new AdRequest.Builder().build();
//
//        interstitial = new InterstitialAd(ImagePimpingActivity.this);
//
//        interstitial.setAdUnitId(getString(R.string.after_click_nect_in_pimp_fashion));
//
//        interstitial.loadAd(adIRequest);
//
//        interstitial.setAdListener(new AdListener() {
//            public void onAdLoaded() {
//                displayInterstitial();
//            }
//
//            @Override
//            public void onAdClosed() {
                to_next_activity();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                to_next_activity();
//            }
//
//        });
    }

    private void to_next_activity() {
        if (brushImageView.getVisibility() == View.VISIBLE)
            brushImageView.setVisibility(View.GONE);
        if (sticker_view != null) {
            Bitmap photo = sticker_view.createBitmap();
            Uri tempUri = getImageUri(ImagePimpingActivity.this, photo);
            File finalFile = new File(getRealPathFromURI(tempUri));

            Intent in1 = new Intent(getApplicationContext(), PostEditActivity.class);
            in1.putExtra("finished_image", finalFile.getAbsolutePath());
            in1.putExtra("angleValue", angle);
            startActivityForResult(in1, POST_SUBMIT_REQUEST_CODE);
        }
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == POST_SUBMIT_REQUEST_CODE) {
            finish();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

    private void set_sticker_listener() {


        sticker_view.setOnStickerOperationListener(new StickerView.OnStickerOperationListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onStickerAdded(@NonNull Sticker sticker) {

                if (sticker_view.isLocked()) {
                    sticker_view.setLocked(false);
                }
            }

            @Override
            public void onStickerClicked(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDeleted(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDragFinished(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerTouchedDown(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerZoomFinished(@NonNull Sticker sticker) {


            }

            @Override
            public void onStickerFlipped(@NonNull Sticker sticker) {

            }

            @Override
            public void onStickerDoubleTapped(@NonNull Sticker sticker) {
                if (sticker_view != null && sticker_view.getStickerCount() > 0) {
                    try {
                        drawable = Utils.drawableFromUrl(current_resource_id);
                        sticker_view.addSticker(new DrawableSticker(drawable));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            }
        });

    }


    @SuppressLint("NewApi")
    private void set_view_pager_for_pimping_stickers() {
//
//        fragments = new ArrayList<>();
//
//        CarComponentsFragment wheel_fragment = new CarComponentsFragment(ResourcesUtil.getString(R.string.string_wheel));
//        Bundle bundle = new Bundle();
//        bundle.putString("title", getString(R.string.string_wheel));
//        wheel_fragment.setArguments(bundle);
//
//        CarComponentsFragment exhaust_fragment = new CarComponentsFragment(ResourcesUtil.getString(R.string.string_exhaust));
//        Bundle exhaust_bundle = new Bundle();
//        exhaust_bundle.putString("title", getString(R.string.string_exhaust));
//        exhaust_fragment.setArguments(exhaust_bundle);
//
//        fragments.add(wheel_fragment);
//        fragments.add(exhaust_fragment);
//
//        adapter = new VpAdapter(getSupportFragmentManager(), fragments,"tag");
//        icons_view_pager.setAdapter(adapter);
        sampleAdapter = new ImageViewpagerAdaper(this, getSupportFragmentManager(), category);

        icons_view_pager.setAdapter(sampleAdapter);
        sampleAdapter.setPageCount(category.length);
        sampleAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void sticker_selected(StickerSelectionEvent stickerSelectionEventBust) {


        current_resource_id = stickerSelectionEventBust.getImage_resource_id();
        Log.d("checking_type", "  " + current_resource_id);
        try {

            drawable = Utils.drawableFromUrl(current_resource_id);
            sticker_view.addSticker(new DrawableSticker(drawable));
        } catch (IOException e) {
            e.printStackTrace();
        }
        icons_view_pager.setTag("invisible");
        horizontalScrollViewl.setTag("invisible");
        icons_view_pager.setVisibility(View.INVISIBLE);
        horizontalScrollViewl.setVisibility(View.INVISIBLE);

    }

    @OnClick(R.id.middle_fab_icon)
    public void click_on_fab_button() {
        rlImageViewContainer.setVisibility(View.GONE);


        if (icons_view_pager.getTag().toString().contentEquals("invisible")) {
            icons_view_pager.setTag("visible");
            horizontalScrollViewl.setTag("visible");
            icons_view_pager.setVisibility(View.VISIBLE);
            horizontalScrollViewl.setVisibility(View.VISIBLE);
        } else {
            icons_view_pager.setTag("invisible");
            horizontalScrollViewl.setTag("invisible");
            icons_view_pager.setVisibility(View.INVISIBLE);
            horizontalScrollViewl.setVisibility(View.INVISIBLE);
        }

    }

    public void updateBrush(float x, float y) {
        brushImageView.offset = (float) offset;
        brushImageView.centerx = x;
        brushImageView.centery = y;
        brushImageView.width = brushSize / 2.0f;
        brushImageView.invalidate();
    }

    public void UpdateCanvas() {
        canvasMaster.drawColor(0, PorterDuff.Mode.CLEAR);
        canvasMaster.drawBitmap(lastEditedBitmap, 0.0f, 0.0f, null);
        int i = 0;
        while (true) {
            if (i >= paths.size()) {
                break;
            }
            int brushSize = brushSizes.get(i);
            Paint paint = new Paint();
            paint.setColor(0);
            paint.setStyle(Paint.Style.STROKE);
            paint.setAntiAlias(true);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            paint.setStrokeWidth((float) brushSize);
            canvasMaster.drawPath(paths.get(i), paint);
            i += 1;
        }
        touchImageView.invalidate();
    }


    private void moveTopoint(float startx, float starty) {
        float zoomScale = getImageViewZoom();
        starty -= (float) offset;
        if (redoPaths.size() > 0) {
            resetRedoPathArrays();
        }
        PointF transLation = getImageViewTranslation();
        int projectedX = (int) ((float) (((double) (startx - transLation.x)) / ((double) zoomScale)));
        int projectedY = (int) ((float) (((double) (starty - transLation.y)) / ((double) zoomScale)));
        drawingPath.moveTo((float) projectedX, (float) projectedY);

        updatedBrushSize = (int) (brushSize / zoomScale);
    }

    public void resetRedoPathArrays() {
        ivRedo.setEnabled(false);
        redoPaths.clear();
        redoBrushSizes.clear();
    }

    public PointF getImageViewTranslation() {
        return touchImageView.getTransForm();
    }

    public float getImageViewZoom() {
        return touchImageView.getCurrentZoom();
    }

    private void addDrawingPathToArrayList() {
        if (paths.size() >= undoLimit) {
            UpdateLastEiditedBitmapForUndoLimit();
            paths.remove(0);
            brushSizes.remove(0);
        }
        if (paths.size() == 0) {
            ivUndo.setEnabled(true);
            ivRedo.setEnabled(false);
        }
        brushSizes.add(updatedBrushSize);
        paths.add(drawingPath);
        drawingPath = new Path();
    }

    public void UpdateLastEiditedBitmapForUndoLimit() {
        Canvas canvas = new Canvas(lastEditedBitmap);
        for (int i = 0; i < 1; i += 1) {
            int brushSize = brushSizes.get(i);
            Paint paint = new Paint();
            paint.setColor(0);
            paint.setStyle(Paint.Style.STROKE);
            paint.setAntiAlias(true);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
            paint.setStrokeWidth((float) brushSize);
            canvas.drawPath(paths.get(i), paint);
        }

    }

    private void drawOnTouchMove() {
        Paint paint = new Paint();
        paint.setStrokeWidth((float) updatedBrushSize);
        paint.setColor(0);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
        canvasMaster.drawPath(drawingPath, paint);
        touchImageView.invalidate();
    }

    private void lineTopoint(Bitmap bm, float startx, float starty) {
        if (initialDrawingCount < initialDrawingCountLimit) {
            initialDrawingCount += 1;
            if (initialDrawingCount == initialDrawingCountLimit) {
                isMultipleTouchErasing = true;
            }
        }
        float zoomScale = getImageViewZoom();
        starty -= (float) offset;
        PointF transLation = getImageViewTranslation();
        int projectedX = (int) ((float) (((double) (startx - transLation.x)) / ((double) zoomScale)));
        int projectedY = (int) ((float) (((double) (starty - transLation.y)) / ((double) zoomScale)));
        if (!isTouchOnBitmap && projectedX > 0 && projectedX < bm.getWidth() && projectedY > 0 && projectedY < bm.getHeight()) {
            isTouchOnBitmap = true;
        }
        drawingPath.lineTo((float) projectedX, (float) projectedY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    private static class VpAdapter extends FragmentStatePagerAdapter {
        String tagType = "";
        private List<Fragment> data;

        public VpAdapter(FragmentManager fm, List<Fragment> data, String tag) {
            super(fm);
            this.data = data;
            this.tagType = tag;
        }

        @Override
        public int getCount() {
            return data.size();
        }


        @Override
        public Fragment getItem(int position) {
            return data.get(position);
        }

    }

    private class OnWidthSeekbarChangeListner implements SeekBar.OnSeekBarChangeListener {
        OnWidthSeekbarChangeListner() {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            brushSize = ((float) progress) + 20.0f;
            updateBrushWidth();
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    private class OnOffsetSeekbarChangeListner implements SeekBar.OnSeekBarChangeListener {
        OnOffsetSeekbarChangeListner() {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            offset = progress;
            updateBrushOffset();
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    //
    private class OnTouchListner implements View.OnTouchListener {
        OnTouchListner() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            if (!(event.getPointerCount() == 1 || isMultipleTouchErasing)) {
                if (initialDrawingCount > 0) {
                    UpdateCanvas();
                    drawingPath.reset();
                    initialDrawingCount = 0;
                }
                touchImageView.onTouchEvent(event);
                MODE = 2;
            } else if (action == MotionEvent.ACTION_DOWN) {
                isTouchOnBitmap = false;
                touchImageView.onTouchEvent(event);
                MODE = 1;
                initialDrawingCount = 0;
                isMultipleTouchErasing = false;
                moveTopoint(event.getX(), event.getY());

                updateBrush(event.getX(), event.getY());
            } else if (action == MotionEvent.ACTION_MOVE) {
                if (MODE == 1) {
                    currentx = event.getX();
                    currenty = event.getY();

                    updateBrush(currentx, currenty);
                    lineTopoint(bitmapMaster, currentx, currenty);

                    drawOnTouchMove();
                }
            } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
                if (MODE == 1) {
                    if (isTouchOnBitmap) {
                        addDrawingPathToArrayList();
                    }
                }
                isMultipleTouchErasing = false;
                initialDrawingCount = 0;
                MODE = 0;
            }
            if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) {
                MODE = 0;
            }
            return true;
        }
    }

}

package com.pimpcar.HomeFeed.activity;

public interface OnK4LVideoListener {
    void onTrimStarted();

    void onError(String message);

    void onVideoPrepared();
}

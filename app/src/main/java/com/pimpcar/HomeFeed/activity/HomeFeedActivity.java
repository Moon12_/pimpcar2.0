package com.pimpcar.HomeFeed.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.BannerListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.pimpcar.HomeFeed.eventbus.models.GetUserProfileFragmentData;
import com.pimpcar.HomeFeed.eventbus.models.LogOutUserProfile;
import com.pimpcar.HomeFeed.eventbus.models.PostsArrayListUpdateActivityEventBus;
import com.pimpcar.HomeFeed.eventbus.models.SaveUserProfileDataToHomeActivity;
import com.pimpcar.HomeFeed.eventbus.models.SendProfileDataToProfileFragment;
import com.pimpcar.HomeFeed.fragments.home.FeedFragment;
import com.pimpcar.HomeFeed.fragments.home.eventbus.TakeUserToHisOwnProfileEvent;
import com.pimpcar.HomeFeed.fragments.notification.NotificationFragment;
import com.pimpcar.HomeFeed.fragments.profile.ProfileFragment;
import com.pimpcar.HomeFeed.fragments.search.SearchFragment;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;
import com.pimpcar.PostEditting.activity.ImagePimpingActivity;
import com.pimpcar.PostEditting.activity.PostEditActivity;
import com.pimpcar.PostEditting.activity.VideoPimpingActivity;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.activity.UserAuthentication;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.staticviewpager.BottomNavigationViewEx;
import com.pimpcar.dialogs.ActionBottomDialogFragment;
import com.pimpcar.dialogs.CommentsBottomSheetFragment;
import com.pimpcar.utils.CameraUtils;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.PermissionUtils;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class HomeFeedActivity extends AppCompatActivity implements CommentsBottomSheetFragment.ItemClickListener, ActionBottomDialogFragment.ItemClickListener {


    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final String VIDEO_EXTENSION = "mp4";
    private static final int PICK_PHOTO = 12345;
    private static final int CAMERA_PIC_REQUEST = 54321;
    private static final int REQUEST_ID_PERMISSIONS_READ_WRITE = 1936;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    private static String imageStoragePath;
    @BindView(R.id.bottom_navigation_layout)
    BottomNavigationViewEx bottom_navigation_layout;
    @BindView(R.id.middle_fab_icon)
    FloatingActionButton middle_fab_icon;
    @BindView(R.id.activity_with_view_pager)
    RelativeLayout activity_with_view_pager;

    String Imagetype = "";
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> total_user_post_infilated = new ArrayList<>();
    private int total_page_loaded = 0;
    private String mCurrentPhotoPath;
    private UserProfileFirstData userProfileFirstData;
    private InterstitialAd interstitial;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.homefeed_activity);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        loadFragment(new FeedFragment(), "feed");
        initevent();
//        Toast.makeText(getApplicationContext(), "user id"+HelperMethods.get_user_id(), Toast.LENGTH_SHORT).show();
//        Toast.makeText(getApplicationContext(), ""+HelperMethods.get_user_jwt_key(), Toast.LENGTH_SHORT).show();
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.BANNER);
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.INTERSTITIAL);
//        final FrameLayout bannerContainer = findViewById(R.id.bannerContainer);
//        IronSourceBannerLayout banner = IronSource.createBanner(this, ISBannerSize.BANNER);
//        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
//                FrameLayout.LayoutParams.WRAP_CONTENT);
//        bannerContainer.addView(banner, 0, layoutParams);
//        banner.setBannerListener(new BannerListener() {
//            @Override
//            public void onBannerAdLoaded() {
//// Called after a banner ad has been successfully loaded
//                banner.setVisibility(View.VISIBLE);
//            }
//            @Override
//            public void onBannerAdLoadFailed(IronSourceError error) {
//// Called after a banner has attempted to load an ad but failed.
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        bannerContainer.removeAllViews();
//                    }
//                });
//            }
//            @Override
//            public void onBannerAdClicked() {
//// Called after a banner has been clicked.
//            }
//            @Override
//            public void onBannerAdScreenPresented() {
//// Called when a banner is about to present a full screen content.
//            }
//            @Override
//            public void onBannerAdScreenDismissed() {
//// Called after a full screen content has been dismissed
//            }
//            @Override
//            public void onBannerAdLeftApplication() {
//// Called when a user would be taken out of the application context.
//            }
//        });
//        IronSource.loadBanner(banner);
        IronSource.loadInterstitial();
// YOUR OTHER CODE //
// YOUR OTHER CODE //
// YOUR OTHER CODE //
        IronSource.setInterstitialListener(new InterstitialListener() {

            /**
             * Invoked when Interstitial Ad is ready to be shown after load function was called.
             */
            @Override
            public void onInterstitialAdReady() {

            }

            /**
             * invoked when there is no Interstitial Ad available after calling load function.
             */
            @Override
            public void onInterstitialAdLoadFailed(IronSourceError error) {

            }

            /**
             * Invoked when the Interstitial Ad Unit is opened
             */
            @Override
            public void onInterstitialAdOpened() {

            }

            /*
             * Invoked when the ad is closed and the user is about to return to the application.
             */
            @Override
            public void onInterstitialAdClosed() {
//                showAds("homeButton");

            }

            /**
             * Invoked when Interstitial ad failed to show.
             * @param error - An object which represents the reason of showInterstitial failure.
             */
            @Override
            public void onInterstitialAdShowFailed(IronSourceError error) {

            }

            /*
             * Invoked when the end user clicked on the interstitial ad, for supported networks only.
             */
            @Override
            public void onInterstitialAdClicked() {

            }

            /** Invoked right before the Interstitial screen is about to open.
             *  NOTE - This event is available only for some of the networks.
             *  You should NOT treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
             */
            @Override
            public void onInterstitialAdShowSucceeded() {

            }
        });
    }


    private void check_permission() {
        if (PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.CAMERA) &&
                PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Timber.d("user have given all permission");


            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);

        } else {
            ask_permission_camera_read_write();
        }
    }

    private void takeVideoFromCamera() {

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_VIDEO);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    public void takePhotoFromCamera() {
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                }
                if (photoFile != null) {

                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.pimpcar.provider",
                            photoFile);

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.setClipData(ClipData.newRawUri("", photoURI));
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }

                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void ask_permission_camera_read_write() {

        int read_Permission = ContextCompat.checkSelfPermission(HomeFeedActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int write_premission = ContextCompat.checkSelfPermission(HomeFeedActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int camera_permission = ContextCompat.checkSelfPermission(HomeFeedActivity.this,
                Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (camera_permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read_Permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (write_premission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        Timber.d("No. of permission left or not given by user" + listPermissionsNeeded.size());

        ActivityCompat.requestPermissions(HomeFeedActivity.this, listPermissionsNeeded
                        .toArray(new String[listPermissionsNeeded.size()]),
                REQUEST_ID_PERMISSIONS_READ_WRITE);
    }

    private void initevent() {

        bottom_navigation_layout.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.ic_home_menu:
                    loadFragment(new FeedFragment(), "feed");
                    break;
                case R.id.ic_search_menu:
                    loadFragment(new SearchFragment(), "other");
                    break;
                case R.id.ic_notification_menu:
                    loadFragment(new NotificationFragment(), "other");
                    break;
                case R.id.ic_user_profile_menu:
                    loadFragment(new ProfileFragment(), "other");
                    break;
                case R.id.i_empty: {
                    return false;
                }
            }
            return true;

        });
    }

    private void loadFragment(Fragment fragment, String tags) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.home_view_pager, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Subscribe
    public void update_total_user_post_loaded_list_home_activity(PostsArrayListUpdateActivityEventBus postsArrayListUpdateActivityEventBus) {

        total_page_loaded = postsArrayListUpdateActivityEventBus.getTotal_page_loaded();
        if (total_user_post_infilated.size() == 0) {
            total_user_post_infilated = postsArrayListUpdateActivityEventBus.get_mUser_post_data();
        } else {
            for (int i = 0; i < total_user_post_infilated.size(); i++) {
                total_user_post_infilated.add(postsArrayListUpdateActivityEventBus.get_mUser_post_data().get(i));
            }
        }
    }

    @Subscribe
    public void get_user_profile_data(SaveUserProfileDataToHomeActivity saveUserProfileDataToHomeActivity) {
        Timber.d("user data is being saved");
        userProfileFirstData = saveUserProfileDataToHomeActivity.getUserProfileFirstData();
    }

    @Subscribe
    public void send_profile_data(GetUserProfileFragmentData getUserProfileFragmentData) {
        if (userProfileFirstData != null) {
            Timber.d("so there is data in activity:: ");
            EventBus.getDefault().post(new SendProfileDataToProfileFragment(userProfileFirstData));
        } else {
            Timber.d("there is nothing about the user in activity to call for");
        }
    }

    @Subscribe
    public void take_user_to_his_own_profile(TakeUserToHisOwnProfileEvent takeUserToHisOwnProfileEvent) {
//        home_view_pager.setCurrentItem(3);
        bottom_navigation_layout.setCurrentItem(4);
    }

    @Subscribe
    public void logout_user(LogOutUserProfile logOutUserProfile) {
        HelperMethods.set_email_verification_status(false);
        HelperMethods.set_api_key("");
        HelperMethods.set_user_name_for_pref("");
        HelperMethods.set_facebook_token_key("");
        HelperMethods.set_user_login_status(false);
        HelperMethods.set_user_id_for_pref("");
        startActivity(new Intent(this, UserAuthentication.class));
        finish();
    }

    @OnClick(R.id.middle_fab_icon)
    public void open_image_pimping() {

        if (PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.CAMERA) &&
                PermissionUtils.checkPermissions(HomeFeedActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            IronSource.showInterstitial("DefaultInterstitial");
            showAds("homeButton");

        } else {
            ask_permission_camera_read_write();
        }
    }

    public void showDialog(Activity activity) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_home);
        CircleImageView pimpmode = bottomSheetDialog.findViewById(R.id.circleImageView_pimpmode);
        CircleImageView upload = bottomSheetDialog.findViewById(R.id.circleImageView_upload);

        pimpmode.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "pimpmode");
        });

        upload.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "upload");
        });

        bottomSheetDialog.show();

    }

    private void showDialodPimpMode(Activity activity, String str) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout gallery_button = dialog.findViewById(R.id.gallery_button);
        RelativeLayout camera_button = dialog.findViewById(R.id.camera_button);
        RelativeLayout video_button = dialog.findViewById(R.id.video_button);

        Imagetype = str;
        gallery_button.setOnClickListener(v -> {
            openGalleryWindow();
            dialog.dismiss();

        });

        camera_button.setOnClickListener(v -> {
            takePhotoFromCamera();
            dialog.dismiss();
        });

        video_button.setOnClickListener(v -> {
            dialog.dismiss();
            if (str.equals("pimpmode")) {
//            video_button.setVisibility(View.GONE);
//                openGalleryWindow();
                openGalleryForVideos();
            } else {
                takeVideoFromCamera();

            }

        });
        dialog.show();
    }

    private void showAds(String type) {
//        AdRequest adIRequest = new AdRequest.Builder().build();
//
//        interstitial = new InterstitialAd(HomeFeedActivity.this);
//
////        if (type.equals("homeButton")) {
////            interstitial.setAdUnitId(getString(R.string.click_pimp_button));
////        } else {
////            interstitial.setAdUnitId(getString(R.string.click_pimp_button));
////        }
//
//
//        interstitial.loadAd(adIRequest);
//
//        interstitial.setAdListener(new AdListener() {
//            public void onAdLoaded() {
        //                displayInterstitial();
//            }
//
//            @Override
//            public void onAdClosed() {
        if (type.equals("homeButton")) {
            showDialog(HomeFeedActivity.this);
        } else {
            openGalleryWindow();
        }

//            }
//
//            @Override
//            public void onAdFailedToLoad(int i) {
//                if (type.equals("homeButton")) {
//                    showDialog(HomeFeedActivity.this);
//                } else {
//                    openGalleryWindow();
//                }
//            }
//
//        });
    }

//    public void displayInterstitial() {
//        if (interstitial.isLoaded()) {
//            interstitial.show();
//        }
//    }

    private void openGalleryWindow() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/* video/*");
            startActivityForResult(intent, PICK_PHOTO);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
            startActivityForResult(intent, PICK_PHOTO);
        }
    }

    private void openGalleryForVideos() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("video/*");
            startActivityForResult(intent, PICK_PHOTO);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("video/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"video/*"});
            startActivityForResult(intent, PICK_PHOTO);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK) {

            try {
                Uri uri = data.getData();
                Log.d("checkingData", "" + data);
                String selectedFilePath = HelperMethods.getPath(getApplicationContext(), uri);
                File file = new File(selectedFilePath);

                if (data.toString().contains("image") || data.toString().contains("IMG") || data.toString().endsWith(".jpg")
                        || data.toString().endsWith(".png") || data.toString().endsWith(".jpeg") || data.toString().endsWith(".JPEG")) {
                    Intent in1;
                    if (Imagetype.equals("upload")) {
                        in1 = new Intent(this, PostEditActivity.class);
                        in1.putExtra("finished_image", file.getAbsolutePath());
                        in1.putExtra("home", "home");
                    } else {
                        in1 = new Intent(this, ImagePimpingActivity.class);
                        in1.putExtra("selected_image", file.getAbsolutePath());
                    }
                    startActivity(in1);
                } else {
                    Intent in1 = new Intent(this, VideoPimpingActivity.class);
                    in1.putExtra("selected_video", file.getAbsolutePath());
                    in1.putExtra("selected_uri", uri.toString());
                    in1.putExtra("data", "camera");
                    Utilities.saveString(HomeFeedActivity.this, "selected_video", imageStoragePath);
                    Utilities.saveString(HomeFeedActivity.this, "selected_uri", uri.toString());
                    Utilities.saveString(HomeFeedActivity.this, "data", "camera");
                    startActivity(in1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Image extension is not in proper formate", Toast.LENGTH_SHORT).show();
            }


        }


        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (Imagetype.equals("upload")) {
                Intent in1 = new Intent(this, PostEditActivity.class);
                in1.putExtra("finished_image", mCurrentPhotoPath);
                startActivity(in1);
            } else {
                Intent in1 = new Intent(this, ImagePimpingActivity.class);
                in1.putExtra("selected_image", mCurrentPhotoPath);
                startActivity(in1);
            }
        }
        if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
            Intent in1 = new Intent(this, VideoPimpingActivity.class);
            in1.putExtra("selected_video", imageStoragePath);
            in1.putExtra("data", "camera");
            Utilities.saveString(HomeFeedActivity.this, "selected_video", imageStoragePath);
            Utilities.saveString(HomeFeedActivity.this, "selected_uri", uri.toString());
            Utilities.saveString(HomeFeedActivity.this, "data", "camera");
            startActivity(in1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_ID_PERMISSIONS_READ_WRITE) {

            int read_Permission = ContextCompat.checkSelfPermission(HomeFeedActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            int write_premission = ContextCompat.checkSelfPermission(HomeFeedActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int camera_permission = ContextCompat.checkSelfPermission(HomeFeedActivity.this,
                    Manifest.permission.CAMERA);

            List<String> listPermissionsNeeded = new ArrayList<>();
            if (camera_permission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (read_Permission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded
                        .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (write_premission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (listPermissionsNeeded.size() == 0) {
                show_snakbar(ResourcesUtil.getString(R.string.string_thanks_for_providing_permission));

                HelperMethods.showToastbar(this, ResourcesUtil.getString(R.string.string_thanks_for_providing_permission));

                showAds("homeButton");

            } else {
                show_snakbar(ResourcesUtil.getString(R.string.permission_not_provided));
            }

        }
    }

    private void show_snakbar(String message) {


        if (message.contentEquals(ResourcesUtil.getString(R.string.string_thanks_for_providing_permission))) {
            final Snackbar snackbar_ask_again = Snackbar
                    .make(HomeFeedActivity.this.findViewById(R.id.activity_with_view_pager), message, Snackbar.LENGTH_LONG)
                    .setAction(ResourcesUtil.getString(R.string.string_provide), v -> {

                        ask_permission_camera_read_write();
                    });

            snackbar_ask_again.setActionTextColor(Color.RED);

            View sbView = snackbar_ask_again.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar_ask_again.show();
        } else {

            final Snackbar snackbar_okey = Snackbar
                    .make(HomeFeedActivity.this.findViewById(R.id.activity_with_view_pager), message, Snackbar.LENGTH_SHORT);

            snackbar_okey.setAction(ResourcesUtil.getString(R.string.string_ok), v -> {
                snackbar_okey.dismiss();
            });
            snackbar_okey.setActionTextColor(Color.RED);

            View sbView = snackbar_okey.getView();
            TextView textView = sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar_okey.show();


        }


    }

    public void show_work_in_progress(Context context, String message) {

        HelperMethods.showToastbar(context, message);

    }

    @Override
    public void onBackPressed() {
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else {
            if (getFragmentManager().getBackStackEntryCount() > 1) {
                getFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    protected void onResume() {
        super.onResume();
        IronSource.onResume(this);
    }

    protected void onPause() {
        super.onPause();
        IronSource.onPause(this);
    }

    @Override
    public void onItemClick(String item) {

    }
}

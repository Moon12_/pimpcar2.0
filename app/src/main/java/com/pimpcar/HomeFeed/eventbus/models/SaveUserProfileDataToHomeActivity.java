package com.pimpcar.HomeFeed.eventbus.models;

import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;

public class SaveUserProfileDataToHomeActivity {

    private UserProfileFirstData userProfileFirstData;

    public SaveUserProfileDataToHomeActivity(UserProfileFirstData userProfileFirstData){
        this.userProfileFirstData = userProfileFirstData;
    }

    public UserProfileFirstData getUserProfileFirstData() {
        return userProfileFirstData;
    }

    public void setUserProfileFirstData(UserProfileFirstData userProfileFirstData) {
        this.userProfileFirstData = userProfileFirstData;
    }
}

package com.pimpcar.HomeFeed.eventbus.models;

import com.pimpcar.Network.model.posts.UserPostBodyResponse;

import java.util.ArrayList;

public class PostsArrayListUpdateActivityEventBus {


    private int total_page_loaded;
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> _mUser_post_data;


    public PostsArrayListUpdateActivityEventBus(ArrayList<UserPostBodyResponse.DataBean.PostsBean> array_list, int page_number){

    }


    public int getTotal_page_loaded() {
        return total_page_loaded;
    }

    public void setTotal_page_loaded(int total_page_loaded) {
        this.total_page_loaded = total_page_loaded;
    }

    public ArrayList<UserPostBodyResponse.DataBean.PostsBean> get_mUser_post_data() {
        return _mUser_post_data;
    }

    public void set_mUser_post_data(ArrayList<UserPostBodyResponse.DataBean.PostsBean> _mUser_post_data) {
        this._mUser_post_data = _mUser_post_data;
    }
}

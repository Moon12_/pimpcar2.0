package com.pimpcar.HomeFeed.eventbus.models;

import com.pimpcar.Network.model.posts.UserPostBodyResponse;

import java.util.ArrayList;

public class PostArrayListUpdateEventBus {


    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> _mUser_post_data;
    private int updated_page_number;

    public PostArrayListUpdateEventBus(ArrayList<UserPostBodyResponse.DataBean.PostsBean> user_post_data, int page_number) {
        this._mUser_post_data = user_post_data;
        this.updated_page_number = page_number;
    }

    public ArrayList<UserPostBodyResponse.DataBean.PostsBean> get_mUser_post_data() {
        return _mUser_post_data;
    }

    public void set_mUser_post_data(ArrayList<UserPostBodyResponse.DataBean.PostsBean> _mUser_post_data) {
        this._mUser_post_data = _mUser_post_data;
    }

    public int getUpdated_page_number() {
        return updated_page_number;
    }

    public void setUpdated_page_number(int updated_page_number) {
        this.updated_page_number = updated_page_number;
    }
}

package com.pimpcar.HomeFeed.fragments.profile.setting.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;

import com.pimpcar.HomeFeed.fragments.profile.ProfileFragment;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.R;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;

public class Settings extends AppCompatActivity {


    @BindView(R.id.blocked_accounts_rl)
    RelativeLayout blocked_accounts_rl;

    @BindView(R.id.accounts_follow_Rl)
    RelativeLayout accounts_follow_Rl;

    @BindView(R.id.help_rl)
    RelativeLayout help_rl;

    @BindView(R.id.about_rl)
    RelativeLayout about_rl;

    @BindView(R.id.privacy_policy_rl)
    RelativeLayout privacy_policy_rl;

    @BindView(R.id.logout_rl)
    RelativeLayout logout_rl;

    @BindView(R.id.notification_switch)
    SwitchCompat notification_switch;

    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;

    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int LOUT_RESULT_CODE = 43876;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_setting);
        ButterKnife.bind(this);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.string_settings));

        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

    }

    @OnClick(R.id.close_button)
    void close_activity() {
        onBackPressed();
    }

    @OnClick(R.id.blocked_accounts_rl)
    void open_blocked_accounts() {

    }

    @OnClick(R.id.accounts_follow_Rl)
    void open_accounts_you_followed() {

    }

    @OnClick(R.id.help_rl)
    void open_help() {

        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"Pimpcar@outlook.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Need Help");
        try {
            startActivity(Intent.createChooser(i, "Mail us for any Help..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(Settings.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void submit_help_to_server(String help_message) {


    }

    @OnClick(R.id.about_rl)
    void open_about() {

    }

    @OnClick(R.id.privacy_policy_rl)
    void open_privacy() {

    }

    @OnClick(R.id.logout_rl)
    void logout() {
        ProfileFragment.LogoutUser();
        Utilities.clearSharedPref(this);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

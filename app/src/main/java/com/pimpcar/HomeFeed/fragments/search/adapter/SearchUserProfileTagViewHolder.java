package com.pimpcar.HomeFeed.fragments.search.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class SearchUserProfileTagViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tag_name)
    TextView tag_name;
    public SearchUserProfileTagViewHolder(@NonNull View itemView, Context _mContext, List<UserSearchResponseBody.DataBean.SerachedDataBean> _mSerachedDataBeans) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}

package com.pimpcar.HomeFeed.fragments.notification.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationResponseBody {
    /**
     * data : {"message":"user notification activity","notification":[{"_id":"5de350583c83b140fbf7dc13","n_id":"p8gq2rV6N1575178328550","timestamp":1575178328,"notification_action":"Commented on your post","user_caused_notification_name":"John Wick","user_caused_notification_uid":"147b9033-7292-5d82-a50e-7927a7b7eafd","user_caused_notification_profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","post_id":"Mes7P2Rw21575093379750","post_pic":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","__v":0,"is_user_already_follow":false}],"next_page":0}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : user notification activity
         * notification : [{"_id":"5de350583c83b140fbf7dc13","n_id":"p8gq2rV6N1575178328550","timestamp":1575178328,"notification_action":"Commented on your post","user_caused_notification_name":"John Wick","user_caused_notification_uid":"147b9033-7292-5d82-a50e-7927a7b7eafd","user_caused_notification_profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","post_id":"Mes7P2Rw21575093379750","post_pic":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","__v":0,"is_user_already_follow":false}]
         * next_page : 0
         */

        @SerializedName("message")
        private String message;
        @SerializedName("next_page")
        private int next_page;
        @SerializedName("notification")
        private List<NotificationBean> notification;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getNext_page() {
            return next_page;
        }

        public void setNext_page(int next_page) {
            this.next_page = next_page;
        }

        public List<NotificationBean> getNotification() {
            return notification;
        }

        public void setNotification(List<NotificationBean> notification) {
            this.notification = notification;
        }

        public static class NotificationBean {
            /**
             * _id : 5de350583c83b140fbf7dc13
             * n_id : p8gq2rV6N1575178328550
             * timestamp : 1575178328
             * notification_action : Commented on your post
             * user_caused_notification_name : John Wick
             * user_caused_notification_uid : 147b9033-7292-5d82-a50e-7927a7b7eafd
             * user_caused_notification_profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg
             * post_id : Mes7P2Rw21575093379750
             * post_pic : https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg
             * user_id : 3d248ae7-91c3-5d6e-898a-665435049dad
             * __v : 0
             * is_user_already_follow : false
             */

            @SerializedName("_id")
            private String _id;
            @SerializedName("n_id")
            private String n_id;
            @SerializedName("timestamp")
            private int timestamp;
            @SerializedName("notification_action")
            private String notification_action;
            @SerializedName("user_caused_notification_name")
            private String user_caused_notification_name;
            @SerializedName("user_caused_notification_uid")
            private String user_caused_notification_uid;
            @SerializedName("user_caused_notification_profile_pic")
            private String user_caused_notification_profile_pic;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("post_pic")
            private String post_pic;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("is_user_already_follow")
            private boolean is_user_already_follow;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getN_id() {
                return n_id;
            }

            public void setN_id(String n_id) {
                this.n_id = n_id;
            }

            public int getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(int timestamp) {
                this.timestamp = timestamp;
            }

            public String getNotification_action() {
                return notification_action;
            }

            public void setNotification_action(String notification_action) {
                this.notification_action = notification_action;
            }

            public String getUser_caused_notification_name() {
                return user_caused_notification_name;
            }

            public void setUser_caused_notification_name(String user_caused_notification_name) {
                this.user_caused_notification_name = user_caused_notification_name;
            }

            public String getUser_caused_notification_uid() {
                return user_caused_notification_uid;
            }

            public void setUser_caused_notification_uid(String user_caused_notification_uid) {
                this.user_caused_notification_uid = user_caused_notification_uid;
            }

            public String getUser_caused_notification_profile_pic() {
                return user_caused_notification_profile_pic;
            }

            public void setUser_caused_notification_profile_pic(String user_caused_notification_profile_pic) {
                this.user_caused_notification_profile_pic = user_caused_notification_profile_pic;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getPost_pic() {
                return post_pic;
            }

            public void setPost_pic(String post_pic) {
                this.post_pic = post_pic;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }


            public boolean isIs_user_already_follow() {
                return is_user_already_follow;
            }

            public void setIs_user_already_follow(boolean is_user_already_follow) {
                this.is_user_already_follow = is_user_already_follow;
            }
        }
    }
}

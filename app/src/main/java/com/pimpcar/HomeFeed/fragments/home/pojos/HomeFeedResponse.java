package com.pimpcar.HomeFeed.fragments.home.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class HomeFeedResponse implements Parcelable {

    public static final Creator<HomeFeedResponse> CREATOR = new Creator<HomeFeedResponse>() {
        @Override
        public HomeFeedResponse createFromParcel(Parcel in) {
            return new HomeFeedResponse(in);
        }

        @Override
        public HomeFeedResponse[] newArray(int size) {
            return new HomeFeedResponse[size];
        }
    };
    /**
     * data : {"message":"home post","posts":[{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["mH3ua8cT41566551012898","gq0Q9K2181566551061034","-kX591dYW1566551068083","22CwcQOPZ1566551076289","k1qicraJ11566551079855","t1R8MS_Cp1566551083641"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d5b966e0af45a08df143e4a","post_id":"mNPb4PEBj1566283373733","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1566283373-9eAg7ybW1QPOb5fIBVMkEu2xYImq52lB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_mNPb4PEBj1566283373733_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1566283374,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0,"post_users_tag":"nKpuTaP4m1575108792153"},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d5b97190af45a08df143e4b","post_id":"l8O6tVwbv1566283544323","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1566283544-bl3g3svjlrb2XHVPxpYe87E4xHGn90II_3d248ae7-91c3-5d6e-898a-665435049dad_l8O6tVwbv1566283544323_Henry.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1566283545,"post_user_name":"Henry Cavil","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/3d248ae7-91c3-5d6e-898a-665435049dad/1564141645-ausUdSPRWqBRS1qAiXSJytDBs8UNwJnl_3d248ae7-91c3-5d6e-898a-665435049dad_3d248ae7-91c3-5d6e-898a-665435049dad_Henry.jpg","post_likes":[{"_id":"5d5fae268c3813258751c1d4","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1566551590"}],"__v":0},{"post_location":" ","post_hash_tags":null,"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de240b7dfddf221273c9153","post_id":"_LAZRMLTK1575108790630","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1575108790-2uKUU582z4xOV5fpx5zxEJOV1b6HgWjS_22855eb3-fe40-5832-b54e-6f8042ba0f6d__LAZRMLTK1575108790630_Zack.jpg","post_description":" ","post_timestamp":1575108791,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0,"post_users_tag":"nKpuTaP4m1575108792153"},{"post_location":" ","post_hash_tags":null,"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de2445bdfddf221273c9157","post_id":"8X4EG5axO1575109723006","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1575109723-aYjuwcGWgf8hSZhkdYcSEfvNqTUgOYfB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_8X4EG5axO1575109723006_Zack.jpg","post_description":" ","post_timestamp":1575109723,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0,"post_users_tag":"hacUcA6Xx1575109724333"},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["nmrrwiR4V1565778225244","w3q-UqTjx1565778268557","PgTLLqIIf1565778689749","s_RW-h_161565779138466","uOdWrkFB31565950099811","mQpmlMGc41565951144682","UNo6_gx4I1565951688204","FbFzxrBeo1565951969218","9b0AOtdHG1565952112991","Hs6FYbUTW1565952258497"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb17b0b9de120544d93f1","post_id":"0jBF30F5d1564389754918","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389755,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d528c703471a1437252a925","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565690992"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":["O11MfZMT01575219804955","pOHeOw2WU1575219836706","aHOf2agnh1575219855108","NXxI17IEe1575271715582"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb18f0b9de120544d93f2","post_id":"Xh4UDH51x1564389774579","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389774-GkyB44mVhs6sgcGhRIvtGjZscTJPHlZX_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Xh4UDH51x1564389774579_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389775,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d5267103471a1437252a8ed","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"},{"_id":"5d5267103471a1437252a8ef","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"},{"_id":"5d5267103471a1437252a8ee","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb19d0b9de120544d93f3","post_id":"frtEthtX11564389788685","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389788-BwDTZZyFxY6l2lj7BW7rpUzXeQ1Kk69w_22855eb3-fe40-5832-b54e-6f8042ba0f6d_frtEthtX11564389788685_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389789,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d514d993471a1437252a8e5","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565609369"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a20b9de120544d93f4","post_id":"DYDXIh0i61564389793541","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389793-NC0In2PjeqpmrSFwvqg4WKAeEnJ3vDPe_22855eb3-fe40-5832-b54e-6f8042ba0f6d_DYDXIh0i61564389793541_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389794,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d5282e13471a1437252a913","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565688545"},{"_id":"5d5282e13471a1437252a914","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565688545"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a70b9de120544d93f5","post_id":"94EP4u-ZR1564389797215","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389799-67BhLg7vE8B8iLHYMCDseKMNbf4Ab3Ta_22855eb3-fe40-5832-b54e-6f8042ba0f6d_94EP4u-ZR1564389797215_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389799,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d526bb73471a1437252a8fb","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682615"},{"_id":"5d526bb73471a1437252a8fc","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682615"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1aa0b9de120544d93f6","post_id":"TQd0uKMPR1564389802128","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389802-6v6tHGIPelr5UUizrjhNSbIl1HQKbW68_22855eb3-fe40-5832-b54e-6f8042ba0f6d_TQd0uKMPR1564389802128_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389802,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d526c2d3471a1437252a8ff","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682733"},{"_id":"5d526c2d3471a1437252a900","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682733"}],"__v":0}],"next_page":1}
     */

    @SerializedName("data")
    private DataBean data;

    protected HomeFeedResponse(Parcel in) {
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean implements Parcelable {
        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
        /**
         * message : home post
         * posts : [{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["mH3ua8cT41566551012898","gq0Q9K2181566551061034","-kX591dYW1566551068083","22CwcQOPZ1566551076289","k1qicraJ11566551079855","t1R8MS_Cp1566551083641"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d5b966e0af45a08df143e4a","post_id":"mNPb4PEBj1566283373733","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1566283373-9eAg7ybW1QPOb5fIBVMkEu2xYImq52lB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_mNPb4PEBj1566283373733_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1566283374,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d5b97190af45a08df143e4b","post_id":"l8O6tVwbv1566283544323","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1566283544-bl3g3svjlrb2XHVPxpYe87E4xHGn90II_3d248ae7-91c3-5d6e-898a-665435049dad_l8O6tVwbv1566283544323_Henry.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1566283545,"post_user_name":"Henry Cavil","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/3d248ae7-91c3-5d6e-898a-665435049dad/1564141645-ausUdSPRWqBRS1qAiXSJytDBs8UNwJnl_3d248ae7-91c3-5d6e-898a-665435049dad_3d248ae7-91c3-5d6e-898a-665435049dad_Henry.jpg","post_likes":[{"_id":"5d5fae268c3813258751c1d4","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1566551590"}],"__v":0},{"post_location":" ","post_hash_tags":null,"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de240b7dfddf221273c9153","post_id":"_LAZRMLTK1575108790630","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1575108790-2uKUU582z4xOV5fpx5zxEJOV1b6HgWjS_22855eb3-fe40-5832-b54e-6f8042ba0f6d__LAZRMLTK1575108790630_Zack.jpg","post_description":" ","post_timestamp":1575108791,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0,"post_users_tag":"nKpuTaP4m1575108792153"},{"post_location":" ","post_hash_tags":null,"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de2445bdfddf221273c9157","post_id":"8X4EG5axO1575109723006","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1575109723-aYjuwcGWgf8hSZhkdYcSEfvNqTUgOYfB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_8X4EG5axO1575109723006_Zack.jpg","post_description":" ","post_timestamp":1575109723,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"__v":0,"post_users_tag":"hacUcA6Xx1575109724333"},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["nmrrwiR4V1565778225244","w3q-UqTjx1565778268557","PgTLLqIIf1565778689749","s_RW-h_161565779138466","uOdWrkFB31565950099811","mQpmlMGc41565951144682","UNo6_gx4I1565951688204","FbFzxrBeo1565951969218","9b0AOtdHG1565952112991","Hs6FYbUTW1565952258497"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb17b0b9de120544d93f1","post_id":"0jBF30F5d1564389754918","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389755,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d528c703471a1437252a925","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565690992"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":["O11MfZMT01575219804955","pOHeOw2WU1575219836706","aHOf2agnh1575219855108","NXxI17IEe1575271715582"],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb18f0b9de120544d93f2","post_id":"Xh4UDH51x1564389774579","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389774-GkyB44mVhs6sgcGhRIvtGjZscTJPHlZX_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Xh4UDH51x1564389774579_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389775,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d5267103471a1437252a8ed","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"},{"_id":"5d5267103471a1437252a8ef","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"},{"_id":"5d5267103471a1437252a8ee","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565681424"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb19d0b9de120544d93f3","post_id":"frtEthtX11564389788685","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389788-BwDTZZyFxY6l2lj7BW7rpUzXeQ1Kk69w_22855eb3-fe40-5832-b54e-6f8042ba0f6d_frtEthtX11564389788685_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389789,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d514d993471a1437252a8e5","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565609369"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a20b9de120544d93f4","post_id":"DYDXIh0i61564389793541","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389793-NC0In2PjeqpmrSFwvqg4WKAeEnJ3vDPe_22855eb3-fe40-5832-b54e-6f8042ba0f6d_DYDXIh0i61564389793541_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389794,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d5282e13471a1437252a913","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565688545"},{"_id":"5d5282e13471a1437252a914","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565688545"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a70b9de120544d93f5","post_id":"94EP4u-ZR1564389797215","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389799-67BhLg7vE8B8iLHYMCDseKMNbf4Ab3Ta_22855eb3-fe40-5832-b54e-6f8042ba0f6d_94EP4u-ZR1564389797215_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389799,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d526bb73471a1437252a8fb","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682615"},{"_id":"5d526bb73471a1437252a8fc","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682615"}],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1aa0b9de120544d93f6","post_id":"TQd0uKMPR1564389802128","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389802-6v6tHGIPelr5UUizrjhNSbIl1HQKbW68_22855eb3-fe40-5832-b54e-6f8042ba0f6d_TQd0uKMPR1564389802128_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389802,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d526c2d3471a1437252a8ff","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682733"},{"_id":"5d526c2d3471a1437252a900","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565682733"}],"__v":0}]
         * next_page : 1
         */

        @SerializedName("message")
        private String message;
        @SerializedName("next_page")
        private int next_page;
        @SerializedName("posts")
        private ArrayList<PostsBean> posts;

        protected DataBean(Parcel in) {
            message = in.readString();
            next_page = in.readInt();
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getNext_page() {
            return next_page;
        }

        public void setNext_page(int next_page) {
            this.next_page = next_page;
        }

        public ArrayList<PostsBean> getPosts() {
            return posts;
        }

        public void setPosts(ArrayList<PostsBean> posts) {
            this.posts = posts;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(message);
            dest.writeInt(next_page);
        }

        public static class PostsBean implements Parcelable {
            public static final Creator<PostsBean> CREATOR = new Creator<PostsBean>() {
                @Override
                public PostsBean createFromParcel(Parcel in) {
                    return new PostsBean(in);
                }

                @Override
                public PostsBean[] newArray(int size) {
                    return new PostsBean[size];
                }
            };
            /**
             * post_location : Pimp Car
             * post_hash_tags : ["#henrycavil","#Superman","#ManOfSteel"]
             * post_comments : ["mH3ua8cT41566551012898","gq0Q9K2181566551061034","-kX591dYW1566551068083","22CwcQOPZ1566551076289","k1qicraJ11566551079855","t1R8MS_Cp1566551083641"]
             * post_involved_users : []
             * post_reports : []
             * is_user_self_liked : false
             * _id : 5d5b966e0af45a08df143e4a
             * post_id : mNPb4PEBj1566283373733
             * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
             * post_image_url : https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1566283373-9eAg7ybW1QPOb5fIBVMkEu2xYImq52lB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_mNPb4PEBj1566283373733_Zack.jpg
             * post_description : My Superman and Man of Steel. Thank you Cavil
             * post_timestamp : 1566283374
             * post_user_name : Zack Snyder
             * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
             * post_likes : []
             * __v : 0
             * post_users_tag : nKpuTaP4m1575108792153
             */

            @SerializedName("post_location")
            private String post_location;
            @SerializedName("is_user_self_liked")
            private boolean is_user_self_liked;
            @SerializedName("_id")
            private String _id;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("post_image_url")
            private String post_image_url;
            @SerializedName("post_description")
            private String post_description;
            @SerializedName("post_timestamp")
            private long post_timestamp;
            @SerializedName("post_user_name")
            private String post_user_name;
            @SerializedName("user_profile_pic_url")
            private String user_profile_pic_url;
            @SerializedName("post_users_tag")
            private String post_users_tag;
            @SerializedName("post_hash_tags")
            private List<String> post_hash_tags;
            @SerializedName("post_comments")
            private List<PostCommentsBean> post_comments;
            @SerializedName("post_involved_users")
            private List<String> post_involved_users;
            @SerializedName("post_reports")
            private List<String> post_reports;
            @SerializedName("post_likes")
            private List<PostLikesBean> post_likes;
            @SerializedName("user_name")
            private String user_name;

            protected PostsBean(Parcel in) {
                post_location = in.readString();
                is_user_self_liked = in.readByte() != 0;
                _id = in.readString();
                post_id = in.readString();
                user_id = in.readString();
                post_image_url = in.readString();
                post_description = in.readString();
                post_timestamp = in.readInt();
                post_user_name = in.readString();
                user_profile_pic_url = in.readString();
                post_users_tag = in.readString();
                post_hash_tags = in.createStringArrayList();
//                post_comments = in.createStringArrayList();
                post_involved_users = in.createStringArrayList();
                post_reports = in.createStringArrayList();
                user_name = in.readString();
            }

            public String getPost_location() {
                return post_location;
            }

            public void setPost_location(String post_location) {
                this.post_location = post_location;
            }

            public List<PostCommentsBean> getPost_comments() {
                return post_comments;
            }

            public void setPost_comments(List<PostCommentsBean> post_comments) {
                this.post_comments = post_comments;
            }

            public boolean isIs_user_self_liked() {
                return is_user_self_liked;
            }

            public void setIs_user_self_liked(boolean is_user_self_liked) {
                this.is_user_self_liked = is_user_self_liked;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPost_image_url() {
                return post_image_url;
            }

            public void setPost_image_url(String post_image_url) {
                this.post_image_url = post_image_url;
            }

            public String getPost_description() {
                return post_description;
            }

            public void setPost_description(String post_description) {
                this.post_description = post_description;
            }

            public long getPost_timestamp() {
                return post_timestamp;
            }

            public void setPost_timestamp(long post_timestamp) {
                this.post_timestamp = post_timestamp;
            }

            public String getPost_user_name() {
                return post_user_name;
            }

            public void setPost_user_name(String post_user_name) {
                this.post_user_name = post_user_name;
            }


            public String getUser_profile_pic_url() {
                return user_profile_pic_url;
            }

            public void setUser_profile_pic_url(String user_profile_pic_url) {
                this.user_profile_pic_url = user_profile_pic_url;
            }


            public String getPost_users_tag() {
                return post_users_tag;
            }

            public void setPost_users_tag(String post_users_tag) {
                this.post_users_tag = post_users_tag;
            }

            public List<String> getPost_hash_tags() {
                return post_hash_tags;
            }

            public void setPost_hash_tags(List<String> post_hash_tags) {
                this.post_hash_tags = post_hash_tags;
            }


            public List<?> getPost_involved_users() {
                return post_involved_users;
            }

            public void setPost_involved_users(List<String> post_involved_users) {
                this.post_involved_users = post_involved_users;
            }

            public List<?> getPost_reports() {
                return post_reports;
            }

            public void setPost_reports(List<String> post_reports) {
                this.post_reports = post_reports;
            }

            public List<PostLikesBean> getPost_likes() {
                return post_likes;
            }

            public void setPost_likes(List<PostLikesBean> post_likes) {
                this.post_likes = post_likes;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(post_location);
                dest.writeByte((byte) (is_user_self_liked ? 1 : 0));
                dest.writeString(_id);
                dest.writeString(post_id);
                dest.writeString(user_id);
                dest.writeString(post_image_url);
                dest.writeString(post_description);
                dest.writeLong(post_timestamp);
                dest.writeString(post_user_name);
                dest.writeString(user_profile_pic_url);
                dest.writeString(post_users_tag);
                dest.writeStringList(post_hash_tags);
//                dest.writeStringList(post_comments);
                dest.writeStringList(post_involved_users);
                dest.writeStringList(post_reports);
                dest.writeString(user_name);
            }

            public class PostLikesBean {

                /**
                 * _id : 5d5fae268c3813258751c1d4
                 * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * user_name : Zack Snyder
                 * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                 * like_timestamp : 1566551590
                 */

                @SerializedName("_id")
                private String _id;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("user_name")
                private String user_name;
                @SerializedName("user_profile_pic_url")
                private String user_profile_pic_url;
                @SerializedName("like_timestamp")
                private String like_timestamp;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public String getUser_name() {
                    return user_name;
                }

                public void setUser_name(String user_name) {
                    this.user_name = user_name;
                }

                public String getUser_profile_pic_url() {
                    return user_profile_pic_url;
                }

                public void setUser_profile_pic_url(String user_profile_pic_url) {
                    this.user_profile_pic_url = user_profile_pic_url;
                }

                public String getLike_timestamp() {
                    return like_timestamp;
                }

                public void setLike_timestamp(String like_timestamp) {
                    this.like_timestamp = like_timestamp;
                }
            }


            public class PostCommentsBean {

                /**
                 * _id : 5d5fae268c3813258751c1d4
                 * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * user_name : Zack Snyder
                 * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                 * like_timestamp : 1566551590
                 */
                @SerializedName("_id")
                private String _id;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("comment_timestamp")
                private String comment_timestamp;
//                @SerializedName("comment_likes")
//                private List<AllPostImageCommentsLikeModel> allPostImageCommentsLikeModels;


                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public String getComment_timestamp() {
                    return comment_timestamp;
                }

                public void setComment_timestamp(String comment_timestamp) {
                    this.comment_timestamp = comment_timestamp;
                }
            }
        }
    }
}

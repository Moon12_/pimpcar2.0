package com.pimpcar.HomeFeed.fragments.notification.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.dialogs.ActionBottomDialogFragment;
import com.pimpcar.models.PostFollowerModel;
import com.pimpcar.models.comments.NestedRecyclerLinearLayoutManager;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsLikesModel;
import com.pimpcar.models.comments.models.CommentsReplyTextModel;
import com.pimpcar.models.notifications.NotificationDataModel;
import com.pimpcar.models.notifications.NotificationDataaaModel;
import com.pimpcar.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NotificationDataModel> parentChildData;
    Context ctx;
    BottomSheetDialogFragment fragment = null;
    KProgressHUD hud;
    Callback callback;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    BottomSheetDialog bottomSheetDialog;
    List<CommentsLikesModel> commentsLikesModels;

    public NotificationAdapter(Context ctx, List<NotificationDataModel> parentChildData, Callback callback) {
        this.ctx = ctx;
        this.parentChildData = parentChildData;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_parent_row, parent, false);
        ViewHolder pavh = new ViewHolder(itemLayoutView);
        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        NotificationDataModel p = parentChildData.get(position);
        vh.bind(position);
        initChildLayoutManager(vh.rv_child, p.getNotifyData());
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,yyyy");
//        String dateString = formatter.format(new Date(Long.parseLong(p.getDate_time())));
        String MyFinalValue = covertTimeToText(p.getL_date());
        vh.tvTimeDate.setText(MyFinalValue);
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(ctx, OtherUserProfileActivity.class);
//                i.putExtra(Constants.THIRD_USER_ID, p.getD_id());
//                ctx.startActivity(i);
            }
        });

    }

    private void initChildLayoutManager(RecyclerView rv_child, List<NotificationDataaaModel> childData) {

        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx));
        NotificationChildAdapter childAdapter = new NotificationChildAdapter(ctx, childData);
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return parentChildData.size();
    }


    //fdfd

    public interface Callback {
        void onItemClick(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rv_child;
        RelativeLayout rl;

        AppCompatTextView tvTimeDate;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            rv_child = itemView.findViewById(R.id.rv_child);
            rl = itemView.findViewById(R.id.rl);
            tvTimeDate = itemView.findViewById(R.id.tvTime);
        }

        private void bind(int pos) {
            NotificationDataModel messagesTabModel = parentChildData.get(pos);
            initClickListener();
        }

        private void initClickListener() {
//            tvReply.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

    public String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " Sec " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Min " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hrs " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Yrs " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7) {
                convTime = day + " d " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }


}
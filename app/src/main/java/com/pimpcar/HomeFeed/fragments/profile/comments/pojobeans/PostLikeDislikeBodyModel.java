package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

public class PostLikeDislikeBodyModel {
    /**
     * data : {"attributes":{"post_id":"IHPL7t--y1563866942793"}}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"post_id":"IHPL7t--y1563866942793"}
         */

        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * post_id : IHPL7t--y1563866942793
             */

            private String post_id;

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }
        }
    }
}

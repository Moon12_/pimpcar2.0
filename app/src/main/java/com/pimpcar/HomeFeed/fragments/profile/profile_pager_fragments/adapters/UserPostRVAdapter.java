package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostComment;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostLikes;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedPostViewHolder;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.ButterKnife;
import timber.log.Timber;

public class UserPostRVAdapter extends InfiniteAdapter<FeedPostViewHolder> {


    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> _mUser_post_data;
    private Context _mContext;
    private FeedPostViewHolder feedViewHolder;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;


    public UserPostRVAdapter(Context context, ArrayList<UserPostBodyResponse.DataBean.PostsBean> user_post_data) {
        this._mUser_post_data = user_post_data;
        this._mContext = context;
        if (_mContext != null) {
            _mLayoutInflater_item = LayoutInflater.from(_mContext);
            _mLayoutInflater_loading = LayoutInflater.from(_mContext);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof FeedInfiniteLoadingView) {

            Log.d("checkValue", " statement");
        } else {
            feedViewHolder = (FeedPostViewHolder) holder;

            UserPostBodyResponse.DataBean.PostsBean single_post_details = _mUser_post_data.get(position);
            if (!single_post_details.getUser_name().equalsIgnoreCase("")) {
                feedViewHolder.user_name_tv.setText(single_post_details.getUser_name());
            } else {
                feedViewHolder.user_name_tv.setText(single_post_details.getPost_user_name());
            }


            if (single_post_details.getUser_profile_pic_url() != null && !single_post_details.getUser_profile_pic_url().isEmpty())
                Picasso.with(_mContext)
                        .load(single_post_details.getUser_profile_pic_url())
                        .fit()
                        .into(feedViewHolder.user_profile_pic_rounded);


            if (single_post_details != null && single_post_details.getPost_image_url() != null && !single_post_details.getPost_image_url().isEmpty()) {


                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) _mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;


                Transformation transformation = new Transformation() {

                    @Override
                    public Bitmap transform(Bitmap source) {


                        int targetWidth = width;

                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "transformation" + " desiredWidth";
                    }
                };


                Picasso.with(_mContext)
                        .load(single_post_details.getPost_image_url())
                        .transform(transformation)
                        .into(feedViewHolder.user_post_image);
            } else {

            }


            String name_text = "<b>" + single_post_details.getUser_name() + "</b>";
            String description = single_post_details.getPost_description();


            feedViewHolder.post_description.setText(Html.fromHtml(name_text + " " + description));

            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0) {
                feedViewHolder.post_number_of_likes.setText(single_post_details.getPost_likes().size() + " likes");
                feedViewHolder.post_number_of_likes.setVisibility(View.VISIBLE);
            } else {
                feedViewHolder.post_number_of_likes.setVisibility(View.GONE);
            }
            feedViewHolder.user_location.setText(single_post_details.getPost_location());
            feedViewHolder.post_time.setText(Utils.toDuration(Long.parseLong(single_post_details.getPost_timestamp())));

            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 4) {


                feedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                feedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                feedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                feedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty())
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                            .fit()
                            .into(feedViewHolder.first_person_image);

                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty())
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                            .fit()
                            .into(feedViewHolder.second_person_dp);

                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty())
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                            .fit()
                            .into(feedViewHolder.third_person_dp);

                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(3).getUser_profile_pic_url().isEmpty())
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                            .fit()
                            .into(feedViewHolder.fourth_person_dp);


            } else {
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 0) {
                    feedViewHolder.first_person_image.setVisibility(View.GONE);
                    feedViewHolder.second_person_dp.setVisibility(View.GONE);
                    feedViewHolder.third_person_dp.setVisibility(View.GONE);
                    feedViewHolder.fourth_person_dp.setVisibility(View.GONE);
                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 1) {
                    feedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    feedViewHolder.second_person_dp.setVisibility(View.GONE);
                    feedViewHolder.third_person_dp.setVisibility(View.GONE);
                    feedViewHolder.fourth_person_dp.setVisibility(View.GONE);

                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                            .fit()
                            .into(feedViewHolder.first_person_image);


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 2) {
                    feedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    feedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    feedViewHolder.third_person_dp.setVisibility(View.GONE);
                    feedViewHolder.fourth_person_dp.setVisibility(View.GONE);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.first_person_image);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.second_person_dp);
                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 3) {

                    feedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    feedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    feedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    feedViewHolder.fourth_person_dp.setVisibility(View.GONE);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty())

                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.first_person_image);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.second_person_dp);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.third_person_dp);
                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 4) {

                    feedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    feedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    feedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    feedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.first_person_image);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.second_person_dp);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty())
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.third_person_dp);

                    if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0 && single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(3).getUser_profile_pic_url().isEmpty())

                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                                .fit()
                                .into(feedViewHolder.fourth_person_dp);
                }
            }


            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_comments().size() == 0) {
                feedViewHolder.user_comments_see.setText(ResourcesUtil.getString(R.string.add_comment));
            } else {
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_comments().size() == 1) {
                    feedViewHolder.user_comments_see.setText("view " + 1 + " comment");
                } else {
                    feedViewHolder.user_comments_see.setText("view all " + single_post_details.getPost_comments().size() + " comment");
                }
            }


            if (HelperMethods.does_like_is_done_by_user(single_post_details.getPost_likes())) {
                feedViewHolder.post_like_button.setBackgroundResource(R.drawable.liked_pressed);
                feedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                feedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) feedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                feedViewHolder.post_like_button.setTag("user_liked");
            } else {
                feedViewHolder.post_like_button.setBackgroundResource(R.drawable.like_unpressed);
                feedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                feedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) feedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                feedViewHolder.post_like_button.setTag("user_not_liked");

            }


            feedViewHolder.user_comments_see.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, PostComment.class);
                intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.PROFILE_COMING);
                intent.putExtra(Constants.SINGLE_POST_DATA, single_post_details);
                _mContext.startActivity(intent);
            });


            feedViewHolder.post_comment.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, PostComment.class);
                intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.PROFILE_COMING);
                intent.putExtra(Constants.SINGLE_POST_DATA, single_post_details);
                _mContext.startActivity(intent);
            });

            feedViewHolder.post_number_of_likes.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, PostLikes.class);
                intent.putExtra(Constants.SINGLE_POST_DATA, single_post_details);
                intent.putExtra(Constants.POST_NUMBER_OF_LIKES, single_post_details.getPost_likes().size() + "");
                _mContext.startActivity(intent);
            });


            feedViewHolder.three_vertical_dots.setOnClickListener(v -> {

                show_dialog_for_share(feedViewHolder.user_post_image, single_post_details.getPost_image_url(), _mContext);

            });

        }

        super.onBindViewHolder(holder, position);

    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mUser_post_data != null && _mUser_post_data.size() > 0) {

            Timber.d("size-------####" + _mUser_post_data.size());
            return _mUser_post_data.size();

        }
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public FeedPostViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.list_car_layout, parent, false);
        ButterKnife.bind(this, itemView);
        feedViewHolder = new FeedPostViewHolder(itemView, _mUser_post_data, _mContext);
        return feedViewHolder;
    }


    private void show_dialog_for_share(AppCompatImageView user_post_image, String post_image_url, Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.share_post_dialog);

        AppCompatTextView share_layout = dialog.findViewById(R.id.share_layout);
        AppCompatTextView cancel = dialog.findViewById(R.id.cancel_layout);

        share_layout.setOnClickListener(v -> {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("image/*");
            i.putExtra(Intent.EXTRA_STREAM, getImageUri(_mContext, getBitmapFromView(user_post_image)));
            try {
                context.startActivity(Intent.createChooser(i, "Post Image..."));
            } catch (android.content.ActivityNotFoundException ex) {

                ex.printStackTrace();
            }

            dialog.dismiss();

        });


        cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });


        dialog.show();

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }
}

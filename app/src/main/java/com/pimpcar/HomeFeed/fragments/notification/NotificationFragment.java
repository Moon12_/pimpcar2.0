package com.pimpcar.HomeFeed.fragments.notification;

import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.home.videoSwiping.VideoSwipingAdapter;
import com.pimpcar.HomeFeed.fragments.home.videoSwiping.VideoSwipingFragment;
import com.pimpcar.HomeFeed.fragments.notification.adapters.NotificationAdapter;
import com.pimpcar.HomeFeed.fragments.notification.models.NotificationRequestBody;
import com.pimpcar.HomeFeed.fragments.notification.models.NotificationResponseBody;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.FollowUser;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.UnfollowUser;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Notification.NotificationDetailsPostActivity;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.models.VideoDataModel;
import com.pimpcar.models.VideoListResponseModel;
import com.pimpcar.models.notifications.NotifcationResponseModel;
import com.pimpcar.models.notifications.NotificationDataModel;
import com.pimpcar.models.notifications.NotificationDataaaModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;
import com.pimpcar.videoPart.VideoInterface;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotificationFragment extends Fragment implements NotificationAdapter.Callback {
    RecyclerView notification_RV;
    private KProgressHUD hud;
    RelativeLayout search_nothing_result;
    List<NotificationDataaaModel> lists;
    List<NotificationDataModel> notificationDataModels;
    //    @BindView(R.id.drawer_layout_open_close_button)
//    AppCompatImageView drawer_layout_open_close_button;
////    @BindView(R.id.search_nothing_result)
////    RelativeLayout search_nothing_result;
//    @BindView(R.id.search_ic_empty)
//    AppCompatImageView search_ic_empty;
//    @BindView(R.id.search_result_foundanything_tv)
//    AppCompatTextView search_result_foundanything_tv;
//    private Unbinder unbinder;
//    private ApiService apiService;
    private NotificationAdapter notificationAdapter;
    //    private CompositeDisposable disposable = new CompositeDisposable();
//    private int first_next_page;
//    private int page_number = 0;
    private List<NotificationResponseBody.DataBean.NotificationBean> all_notification;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.notification_layout, container, false);
//        unbinder = ButterKnife.bind(this, root_view);
        notification_RV = root_view.findViewById(R.id.notification_RV);
        search_nothing_result = root_view.findViewById(R.id.search_nothing_result);
        lists = new ArrayList<>();
        notificationDataModels = new ArrayList<>();
        getNotificationApi();
        return root_view;
    }

    private void getNotificationApi() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<NotifcationResponseModel> call = getResponse.getAllActivityApi(HelperMethods.get_user_id());
        call.enqueue(new Callback<NotifcationResponseModel>() {
            @Override
            public void onResponse(Call<NotifcationResponseModel> call, Response<NotifcationResponseModel> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
                        hud.dismiss();

                        notificationDataModels = response.body().getNotificationDataModels();
                        if (notificationDataModels.isEmpty() || notificationDataModels == null || notificationDataModels.size() == 0) {
                            search_nothing_result.setVisibility(View.VISIBLE);
                        } else {
                            Toast.makeText(getActivity(), "Notification Found", Toast.LENGTH_SHORT).show();
                            search_nothing_result.setVisibility(View.GONE);
                            notification_RV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                            notificationAdapter = new NotificationAdapter(getActivity(), notificationDataModels, NotificationFragment.this);
                            notification_RV.setAdapter(notificationAdapter);
                        }



                    } else {
                        hud.dismiss();
                        search_nothing_result.setVisibility(View.VISIBLE);

                        Toast.makeText(getActivity(), "not found", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    search_nothing_result.setVisibility(View.VISIBLE);

                    hud.dismiss();
                    Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                String tt = t.toString();
                Toast.makeText(getActivity(), "something went wrong" + tt, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onItemClick(int pos) {

    }
}

package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CommentLikeDislikeResponseModel implements Parcelable {


    /**
     * data : {"message":"comment liked successfully"}
     */

    @SerializedName("data")
    private DataBean data;

    protected CommentLikeDislikeResponseModel(Parcel in) {
        data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Creator<CommentLikeDislikeResponseModel> CREATOR = new Creator<CommentLikeDislikeResponseModel>() {
        @Override
        public CommentLikeDislikeResponseModel createFromParcel(Parcel in) {
            return new CommentLikeDislikeResponseModel(in);
        }

        @Override
        public CommentLikeDislikeResponseModel[] newArray(int size) {
            return new CommentLikeDislikeResponseModel[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
    }

    public static class DataBean implements Parcelable {
        /**
         * message : comment liked successfully
         */

        @SerializedName("message")
        private String message;

        protected DataBean(Parcel in) {
            message = in.readString();
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(message);
        }
    }
}

package com.pimpcar.HomeFeed.fragments.profile.comments.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesResponse;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostLikeAdapter extends InfiniteAdapter<PostLikeAdapter.PostLikeViewHolder> {


    private Context mContext;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private PostLikeViewHolder postLikeViewHolder;
    private PostLikesFollowerAdapterListener postLikesFollowerAdapterListener;


    private List<PostAllLikesResponse.DataBean.PostLikesBean> _mAllLikes;

    public PostLikeAdapter(Context context, List<PostAllLikesResponse.DataBean.PostLikesBean> allLikes) {
        this.mContext = context;
        this._mAllLikes = allLikes;
        _mLayoutInflater_item = LayoutInflater.from(mContext);
        _mLayoutInflater_loading = LayoutInflater.from(mContext);
    }


    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mAllLikes != null && _mAllLikes.size() > 0)
            return _mAllLikes.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public PostLikeViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.post_likes_item_layout, parent, false);
        ButterKnife.bind(this, itemView);
        postLikeViewHolder = new PostLikeViewHolder(itemView);
        return postLikeViewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {

        } else {


            PostAllLikesResponse.DataBean.PostLikesBean single_post_data = _mAllLikes.get(position);


            single_post_data.setUsername(single_post_data.getUsername() != null && !single_post_data.getUsername().isEmpty() ? single_post_data.getUsername() : "");

            postLikeViewHolder.user_location.setText(single_post_data.getUsername() + "");


//            if (single_post_data.isIs_follower()) {
//                postLikeViewHolder.follow_button.setText(ResourcesUtil.getString(R.string.string_unfollow_button));
//                postLikeViewHolder.follow_button.setTextColor(ResourcesUtil.getColor(R.color.white));
//                postLikeViewHolder.follow_button.setBackgroundResource(R.drawable.unfollow_button_rounded_background);
//            } else {
//                postLikeViewHolder.follow_button.setText(ResourcesUtil.getString(R.string.string_follow_button));
//                postLikeViewHolder.follow_button.setTextColor(ResourcesUtil.getColor(R.color.white));
//                postLikeViewHolder.follow_button.setBackgroundResource(R.drawable.rounded_follow_button);
//            }


            postLikeViewHolder = (PostLikeViewHolder) holder;
            postLikeViewHolder.user_name_tv.setText(single_post_data.getUser_name());
            Picasso.with(mContext)
                    .load(single_post_data.getUser_profile_pic_url())
                    .fit()
                    .into(postLikeViewHolder.user_profile_pic_rounded);

//            postLikeViewHolder.follow_button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int selected = Integer.parseInt(v.getTag().toString());
//                    for (int i = 0; i < _mAllLikes.size(); i++) {
//                        _mAllLikes.get(i).setIs_follower(false);
//                    }
//                    _mAllLikes.get(selected).setIs_follower(true);
//                    notifyDataSetChanged();
//                }
//            });


//            if (single_post_data.getUser_id() != null && HelperMethods.get_user_id() != null
//                    && !HelperMethods.get_user_id().isEmpty() && single_post_data.getUser_id().contentEquals(HelperMethods.get_user_id())) {
//                postLikeViewHolder.follow_button.setVisibility(View.GONE);
//            } else {
//                postLikeViewHolder.follow_button.setVisibility(View.VISIBLE);
//            }
        }

        super.onBindViewHolder(holder, position);
    }


    public interface PostLikesFollowerAdapterListener {
        void followFollowAndUnfollowOnClick(View v, int position);
    }

    public class PostLikeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_profile_pic_rounded)
        public CircleImageView user_profile_pic_rounded;

        @BindView(R.id.user_name_tv)
        public AppCompatTextView user_name_tv;

        @BindView(R.id.user_location)
        public AppCompatTextView user_location;


        public PostLikeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

}

package com.pimpcar.HomeFeed.fragments.profile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonObject;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.HomeFeed.eventbus.models.LogOutUserProfile;
import com.pimpcar.HomeFeed.eventbus.models.PostArrayListUpdateEventBus;
import com.pimpcar.HomeFeed.eventbus.models.SendProfileDataToProfileFragment;
import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.FollowFollowingActivity;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameResponse;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserStatusRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserStatusResponse;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.GridviewPicsFragment;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.ListPicsFragment;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.UserTaggedFragment;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters.UserStoriesAdapter;
import com.pimpcar.HomeFeed.fragments.profile.setting.activity.Settings;
import com.pimpcar.HomeFeed.fragments.profile.stories.activity.StoryCreateActivity;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesResponse;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.posts.UserPostsBodyRequest;
import com.pimpcar.Network.model.usergender.UserGenderProfileResponse;
import com.pimpcar.Network.model.usergender.UserGenderProfileUpdateBody;
import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.Widgets.staticviewpager.NonSwipeableViewPager;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.compressor.Compressor;
import com.pimpcar.utils.compressor.ImageUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.varunjohn1990.iosdialogs4android.IOSDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import timber.log.Timber;

public class ProfileFragment extends Fragment implements View.OnClickListener {


    private static final int PICK_PHOTO = 12;
    private static final int CAMERA_PIC_REQUEST = 11;
    private static final int PICK_PHOTO_FOR_STORIES = 1221;
    private static final int PICK_CAMERA_PIC_FOR_STORIES = 1331;
    @BindView(R.id.portfolio_pic)
    CircleImageView portfolio_pic;
    @BindView(R.id.user_name_tv)
    AppCompatTextView user_name_tv;
    @BindView(R.id.user_bio_quite_tv)
    AppCompatTextView user_bio_quite_tv;
    @BindView(R.id.user_follow_button)
    AppCompatTextView user_follow_button;
    @BindView(R.id.total_post_tv)
    AppCompatTextView total_post_tv;
    @BindView(R.id.total_followers_tv)
    AppCompatTextView total_followers_tv;
    @BindView(R.id.total_following_tv)
    AppCompatTextView total_following_tv;
    @BindView(R.id.user_stories_rv)
    RecyclerView user_stories_rv;
    @BindView(R.id.ic_gridview_thumbnail_data)
    AppCompatImageView ic_gridview_thumbnail_data;
    @BindView(R.id.ic_list_user_data)
    AppCompatImageView ic_list_user_data;
    @BindView(R.id.user_tagged_posts)
    AppCompatImageView user_tagged_posts;
    @BindView(R.id.view_pager)
    NonSwipeableViewPager view_pager;
    @BindView(R.id.new_stories_create)
    RelativeLayout new_stories_create;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.drawer_layout_open_close_button)
    AppCompatImageView drawer_layout_open_close_button;
    @BindView(R.id.add_remove_image_profile)
    View add_remove_image_profile;
    @BindView(R.id.navigation_view_drawer)
    NavigationView navigation_view_drawer;
    @BindView(R.id.username_tv)
    AppCompatTextView username_tv;
    @BindView(R.id.user_name_edit)
    AppCompatImageView user_name_edit;
    @BindView(R.id.user_status_update)
    AppCompatImageView user_status_update;
    @BindView(R.id.user_un_follow_button)
    AppCompatTextView user_un_follow_button;
    @BindView(R.id.progress_bar_profile)
    ProgressBar progress_bar_profile;
    @BindView(R.id.follower)
    LinearLayout follower;
    @BindView(R.id.following)
    LinearLayout following;
    private String user_name = "";
    private String user_id = "";
    private String userName = "";
    private Unbinder unbinder;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private UserProfileFirstData user_profile_data;
    private List<Fragment> fragments;// used for ViewPager adapter
    private VpAdapter adapter;
    Dialog dialog;
    private int first_next_page;
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> total_user_post_infilated = new ArrayList<>();
    private int LOUT_RESULT_CODE = 43876;
    private int stories_next_page_number;
    private UserStoriesAdapter userStoriesAdapter;
    private ArrayList<GetUserStoriesResponse.DataBean.StoriesBean> user_all_stories;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    GetUserStoriesRequestBody getUserStoriesRequestBody = HelperMethods.create_request_body_for_stories(getStories_next_page_number());
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getUserStories(HelperMethods.get_user_jwt_key(), getUserStoriesRequestBody)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<GetUserStoriesResponse>() {
                                    @Override
                                    public void onSuccess(GetUserStoriesResponse s) {

                                        if (s.getData().getStories().size() > 0) {
                                            for (int i = 0; i < s.getData().getStories().size(); i++) {
                                                user_all_stories.add(s.getData().getStories().get(i));

                                                userStoriesAdapter.notifyItemInserted(user_all_stories.size() - 1);
                                            }
                                            userStoriesAdapter.moreDataLoaded(user_all_stories.size(), s.getData().getStories().size());

                                            if (s.getData().getNext_page() == 1) {
                                                increase_page_number();
                                                userStoriesAdapter.setShouldLoadMore(true);
                                            } else {
                                                userStoriesAdapter.setShouldLoadMore(false);

                                            }

                                        } else {
                                            userStoriesAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };
    private KProgressHUD hud;
    private String profilename_ss;

    public static void LogoutUser() {
        EventBus.getDefault().post(new LogOutUserProfile());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        }
    }

    private void get_user_post_first_page() {
        UserPostsBodyRequest userPostsBodyRequest = HelperMethods.create_user_profile_post_get(first_next_page);
        disposable.add(apiService.getUserPosts(HelperMethods.get_user_jwt_key(), userPostsBodyRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserPostBodyResponse>() {
                    @Override
                    public void onSuccess(UserPostBodyResponse s) {
                        total_user_post_infilated = s.getData().getPosts();
                        first_next_page = s.getData().getNext_page();
                        Toast.makeText(getActivity(), "firstpage"+total_user_post_infilated, Toast.LENGTH_SHORT).show();
                        init_view_pager_view();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.profile_layout_test, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        userName = Utilities.getString(getActivity(), "userName");
        username_tv.setText(userName.toLowerCase());
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        user_un_follow_button.setVisibility(View.GONE);
        user_follow_button.setVisibility(View.GONE);

        if (getArguments() != null)
            if (getArguments().getParcelableArrayList(Constants.USER_TOTAL_POST_FROM_HOME_TO_PROFILE) != null) {
                total_user_post_infilated = getArguments().getParcelableArrayList(Constants.USER_TOTAL_POST_FROM_HOME_TO_PROFILE);
                first_next_page = getArguments().getInt(Constants.USER_NEXT_PAGE_DETAILS_FROM_HOME_TO_PROFILE);

            } else {
                Timber.d("kuch ni le aya tha activity se");
            }

        init_view_pager_view();

        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));

        follower.setOnClickListener(this);
        following.setOnClickListener(this);
        String token = HelperMethods.get_user_jwt_key().toString();
//        Toast.makeText(getActivity(), "" + token, Toast.LENGTH_SHORT).show();
        set_click_event();
        Log.d("checkingValue", "  " + HelperMethods.get_user_jwt_key());
        get_profile_data();
        return root_view;
    }

    private void set_click_event() {

        navigation_view_drawer.bringToFront();
        navigation_view_drawer.setNavigationItemSelectedListener(menuItem -> {

            Timber.d("item selected " + menuItem.getItemId());

            switch (menuItem.getItemId()) {
                case R.id.nav_item_profile:
                    drawer_layout.closeDrawer(Gravity.LEFT);
                    break;
                case R.id.nav_item_settings:
                    Timber.d("setting is clicked");
                    Intent intent = new Intent(getActivity(), Settings.class);
                    getActivity().startActivityForResult(intent, LOUT_RESULT_CODE);
                    break;
            }
            return true;
        });
    }

    @OnClick(R.id.new_stories_create)
    public void create_story() {
        show_dialog_for_image_edit(getActivity());
    }

    @OnClick(R.id.add_remove_image_profile)
    public void change_profile_image() {
        showDialog(getActivity());
    }

    private void get_profile_data() {
        disposable.add(apiService.getUserProfileData(HelperMethods.get_user_jwt_key())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserProfileFirstData>() {
                    @Override
                    public void onSuccess(UserProfileFirstData s) {
                        Timber.d("data is here ");
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_PROFILE_DATA)) {
                            user_profile_data = s;
                            Log.d("valuseChecking", "" + s);
                            set_user_profile_data();
                        } else {
                            Toast.makeText(getActivity(), "things did not get", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {

                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                }
                                Log.d("[get_profile_data()]", "error " + error);

                            } catch (IOException e1) {
                                e1.printStackTrace();

                            }
                        }
                    }
                }));
    }

    private void set_user_profile_data() {
        showProgress();
        String profile_name = user_profile_data.getData().getUser_data().getUsr_info().getFname() + " " + user_profile_data.getData().getUser_data().getUsr_info().getLname();
        user_name = profile_name;
        user_id = user_profile_data.getData().getUser_data().getUsr_info().getUid();
        String user_profile_pic_url = user_profile_data.getData().getUser_data().getUsr_info().getProfile_pic();
        int user_followers = user_profile_data.getData().getUser_data().getUsr_info().getUser_followers().size();
        Log.d("checkingFollwers", " " + user_profile_data.getData().getUser_data().getUsr_info().getUser_followers());
        int user_posts = user_profile_data.getData().getUser_data().getUsr_info().getUser_total_posts();
        int user_following = user_profile_data.getData().getUser_data().getUsr_info().getUser_following().size();
        HelperMethods.save_users_following_list(user_profile_data.getData().getUser_data().getUsr_info().getUser_following());

        HelperMethods.save_user_gender(user_profile_data.getData().getUser_data().getUsr_info().getGender());

        String user_name = user_profile_data.getData().getUser_data().getUsr_info().getUser_name();
        profilename_ss = user_name;
        if (!user_name.equalsIgnoreCase("")) {
            user_name_tv.setText(user_name.toLowerCase());
        } else {
            user_name_tv.setText(profile_name.toLowerCase());
        }

        total_followers_tv.setText(user_followers + "");
        total_post_tv.setText(user_posts + "");
        total_following_tv.setText(user_following + "");

        user_profile_data.getData().getUser_data().getUsr_info().setUser_name(user_profile_data.getData().getUser_data().getUsr_info().getUser_name() != null && !user_profile_data.getData().getUser_data().getUsr_info().getUser_name().isEmpty() ? user_profile_data.getData().getUser_data().getUsr_info().getUser_name() : "");

        username_tv.setText(user_profile_data.getData().getUser_data().getUsr_info().getUser_name().toLowerCase() + "");
        HelperMethods.save_user_name(user_profile_data.getData().getUser_data().getUsr_info().getUser_name() + "");

        if (user_profile_data.getData().getUser_data().getUsr_info().getUser_status() != null &&
                user_profile_data.getData().getUser_data().getUsr_info().getUser_status().getCurrent_status() != null) {
            user_bio_quite_tv.setText("' " + user_profile_data.getData().getUser_data().getUsr_info().getUser_status().getCurrent_status().toLowerCase() + " '");
            user_bio_quite_tv.setVisibility(View.VISIBLE);
            HelperMethods.save_user_status(user_bio_quite_tv.getText().toString());
        } else {
            user_bio_quite_tv.setVisibility(View.GONE);
        }

        // --- null check with application context

//        Glide.with(PimpCarApplication.getApplicationInstance().getApplicationContext())
//                .load(user_profile_pic_url)
//                .into(portfolio_pic);
        Picasso.with(PimpCarApplication.getApplicationInstance().getApplicationContext())
                .load(user_profile_pic_url)
                .into(portfolio_pic, new Callback() {
                    @Override
                    public void onSuccess() {
                        hideProgress();
                    }

                    @Override
                    public void onError() {

                    }
                });
        HelperMethods.save_user_profile_image(user_profile_pic_url);
        HelperMethods.save_user_id(user_profile_data.getData().getUser_data().getUsr_info().getUid());

        if (total_user_post_infilated != null) {
            if (total_user_post_infilated.size() == 0 && first_next_page == 0) {
                get_user_post_first_page();
            }
        }

        get_user_stories();

        if (username_tv.getText().toString().isEmpty()) {
            username_tv.setText("choose username");
            username_tv.setTextColor(ResourcesUtil.getColor(R.color.grey));
        }

        if (user_bio_quite_tv.getText().toString().isEmpty()) {
            user_bio_quite_tv.setText("share status");
            user_bio_quite_tv.setTextColor(ResourcesUtil.getColor(R.color.grey));
        }

    }

    private void get_user_stories() {
        show_progress_dialog();
        GetUserStoriesRequestBody getUserStoriesRequestBody = HelperMethods.create_request_body_for_stories(getStories_next_page_number());
        disposable.add(apiService.getUserStories(HelperMethods.get_user_jwt_key(), getUserStoriesRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<GetUserStoriesResponse>() {
                    @Override
                    public void onSuccess(GetUserStoriesResponse s) {
                        dismiss_progress_dialog();

                        if (s.getData().getStories() != null) {
                            user_all_stories = s.getData().getStories();
                            Toast.makeText(getActivity(), "data"+user_all_stories.toString(), Toast.LENGTH_SHORT).show();
                            userStoriesAdapter = new UserStoriesAdapter(getActivity(), user_all_stories);
                            user_stories_rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
                            user_stories_rv.setAdapter(userStoriesAdapter);
                            user_stories_rv.scrollToPosition(0);
                            userStoriesAdapter.setOnLoadMoreListener(onLoadMoreListener);
                            if (s.getData().getNext_page() == 1) {
                                increase_page_number();
                                userStoriesAdapter.setShouldLoadMore(true);
                            } else {
                                userStoriesAdapter.setShouldLoadMore(false);
                            }
                        } else {
                            if (s.getData().getStories() != null) {
                                userStoriesAdapter.setShouldLoadMore(false);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(getActivity().getApplicationContext(), error + "");

                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    private void increase_page_number() {
        first_next_page = first_next_page + 1;
    }

    private int getCameraPicRequest() {
        return first_next_page;
    }

    @OnClick(R.id.drawer_layout_open_close_button)
    public void click_on_close_open_drawer_button() {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.ic_gridview_thumbnail_data)
    public void click_on_grid_view() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));
        if (view_pager != null && view_pager.getCurrentItem() != 0) {
            view_pager.setCurrentItem(0);
        }
    }

    @OnClick(R.id.ic_list_user_data)
    public void click_on_list_view() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.white));
        if (view_pager != null && view_pager.getCurrentItem() != 1) {
            view_pager.setCurrentItem(1);
        }
    }

    @OnClick(R.id.user_tagged_posts)
    public void click_on_tagged_post() {
        ic_gridview_thumbnail_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        ic_list_user_data.setColorFilter(ResourcesUtil.getColor(R.color.white));
        user_tagged_posts.setColorFilter(ResourcesUtil.getColor(R.color.theme_red));
        if (view_pager != null && view_pager.getCurrentItem() != 2) {
            view_pager.setCurrentItem(2);
        }
    }

    public void init_view_pager_view() {
        fragments = new ArrayList<>(3);

        // create music fragment and add it
        GridviewPicsFragment gridviewPicsFragment = new GridviewPicsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.USER_POST_LIST, total_user_post_infilated);
        bundle.putInt(Constants.USER_NEXT_PAGE_DETAILS, first_next_page);
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_SELF);
        bundle.putString("title", getString(R.string.string_home));
        gridviewPicsFragment.setArguments(bundle);

        // create backup fragment and add it
        ListPicsFragment listPicsFragment = new ListPicsFragment();
        bundle = new Bundle();
        bundle.putString("title", getString(R.string.string_search));
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_SELF);
        bundle.putParcelableArrayList(Constants.USER_POST_LIST, total_user_post_infilated);
        bundle.putInt(Constants.USER_NEXT_PAGE_DETAILS, first_next_page);
        listPicsFragment.setArguments(bundle);

        // create friends fragment and add it
        UserTaggedFragment user_tagged_view = new UserTaggedFragment();
        bundle = new Bundle();
        bundle.putString("title", getString(R.string.string_notification));
        bundle.putString(Constants.USER_TYPE, Constants.USER_TYPE_SELF);
        user_tagged_view.setArguments(bundle);

        // add to fragments for adapter
        fragments.add(gridviewPicsFragment);
        fragments.add(listPicsFragment);
        fragments.add(user_tagged_view);

        adapter = new VpAdapter(getChildFragmentManager(), fragments);
        view_pager.setAdapter(adapter);
    }

    @Subscribe
    public void update_post_data_array_list(PostArrayListUpdateEventBus postArrayListUpdateEventBus) {
        for (int i = 0; i < postArrayListUpdateEventBus.get_mUser_post_data().size(); i++) {
            total_user_post_infilated.add(postArrayListUpdateEventBus.get_mUser_post_data().get(i));
        }
        first_next_page = postArrayListUpdateEventBus.getUpdated_page_number();
    }

    public int getStories_next_page_number() {
        return stories_next_page_number;
    }

    public void setStories_next_page_number(int stories_next_page_number) {
        this.stories_next_page_number = stories_next_page_number;
    }

    public void show_progress_dialog() {
        if (hud != null) {
            hud = KProgressHUD.create(getActivity())
                    .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
            hud.setSize(50, 50);
            hud.show();
        }

    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView gallery_layout = dialog.findViewById(R.id.gallery_layout);
        AppCompatTextView camera_layout = dialog.findViewById(R.id.camera_layout);

        gallery_layout.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_PHOTO);
            dialog.dismiss();
        });
        camera_layout.setOnClickListener(v -> {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
            dialog.dismiss();
        });
        dialog.show();
    }

    public void show_dialog_for_image_edit(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView dialog_heading = dialog.findViewById(R.id.dialog_heading);
        AppCompatTextView gallery_layout = dialog.findViewById(R.id.gallery_layout);
        AppCompatTextView camera_layout = dialog.findViewById(R.id.camera_layout);
        RelativeLayout video_button = dialog.findViewById(R.id.video_button);
        video_button.setVisibility(View.GONE);
        dialog_heading.setText(ResourcesUtil.getString(R.string.string_select_story));

        gallery_layout.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_PHOTO_FOR_STORIES);
            dialog.dismiss();
        });


        camera_layout.setOnClickListener(v -> {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, PICK_CAMERA_PIC_FOR_STORIES);
            dialog.dismiss();
        });

        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            String selectedFilePath = HelperMethods.getPath(getActivity(), uri);
            File file = new File(selectedFilePath);
            String filePath = file.getPath();
            Bitmap profile_bitmap = BitmapFactory.decodeFile(filePath);
            portfolio_pic.setImageBitmap(profile_bitmap);
            upload_bitmap_to_server(profile_bitmap);
        }

        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK) {
            Timber.d("Image from camera data :: " + data.getStringExtra(MediaStore.EXTRA_OUTPUT));
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            portfolio_pic.setImageBitmap(photo);
            upload_bitmap_to_server(photo);
        }

        if (requestCode == LOUT_RESULT_CODE && resultCode == Activity.RESULT_OK) {

            Timber.d("activity result ::  :: wapas toh aya hai");
            EventBus.getDefault().post(new LogOutUserProfile());
        }

        if (requestCode == PICK_CAMERA_PIC_FOR_STORIES && resultCode == Activity.RESULT_OK) {

            Timber.d("Image from camera data :: " + data.getStringExtra(MediaStore.EXTRA_OUTPUT));

            Bitmap bitmap_image_for_story = (Bitmap) data.getExtras().get("data");

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap_image_for_story.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Intent in1 = new Intent(getActivity(), StoryCreateActivity.class);
            in1.putExtra("story_image", byteArray);
            Timber.d("image is ready for activity from camera ");
            getActivity().startActivity(in1);

        }

        if (requestCode == PICK_PHOTO_FOR_STORIES && resultCode == Activity.RESULT_OK) {

            Log.d("------", "Image for the story create ::::" + data.getData().getPath());
            Uri uri = data.getData();
            String selectedFilePath = HelperMethods.getPath(getActivity(), uri);
            File file = new File(selectedFilePath);
            String filePath = file.getPath();
            Bitmap bitmap_image_for_stories = BitmapFactory.decodeFile(filePath);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap_image_for_stories.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            Intent in1 = new Intent(getActivity(), StoryCreateActivity.class);
            in1.putExtra("story_image", byteArray);
            Timber.d("image is ready for activity from gallry");
            getActivity().startActivity(in1);
//            Uri uri = data.getData();
//            String selectedFilePath = HelperMethods.getPath(getActivity(), uri);
//            File file = new File(selectedFilePath);
//            Intent in1 = new Intent(getActivity(), StoryCreateActivity.class);
//            in1.putExtra("story_image", file.getAbsolutePath());
//            startActivity(in1);
        }

    }

    private void upload_bitmap_to_server(Bitmap profile_bitmap) {
        update_user_data_to_server(profile_bitmap);
    }

    private void update_user_data_to_server(Bitmap profile_bitmap) {
        show_progress_dialog();
        File image_file = ImageUtil.bitmapToFile(profile_bitmap, HelperMethods.get_user_jwt_key());
        Bitmap compressed_bitmap = Compressor.getDefault(getActivity()).compressToBitmap(image_file);
        UserGenderProfileUpdateBody userGenderProfileUpdateBody = Utils.create_body_for_gender_update(compressed_bitmap, HelperMethods.get_user_gender());
        disposable.add(apiService.updateProfileGender(HelperMethods.get_user_jwt_key(), userGenderProfileUpdateBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserGenderProfileResponse>() {
                    @Override
                    public void onSuccess(UserGenderProfileResponse s) {
                        dismiss_progress_dialog();
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.PROFILE_PIC_UPDATED)) {
                            dismiss_progress_dialog();
                            HelperMethods.save_user_profile_image(s.getData().getImage_url());
                            Glide.with(getContext())
                                    .load(HelperMethods.get_saved_user_profile_image())
                                    .into(portfolio_pic);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    @OnClick(R.id.user_status_update)
    public void change_user_status() {
        show_dialog_box_to_change_status();
    }

    private void show_dialog_box_to_change_status() {
        show_dialog_box_to_change_username("status");
    }

    @OnClick(R.id.user_name_edit)
    public void change_user_name() {
        show_dialog_box_to_change_username("username");
    }

    private void show_dialog_box_to_change_username(String context_regarding) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.change_user_name_dialog_box);

        AppCompatEditText user_name_et = dialog.findViewById(R.id.user_name_et_c);
        AppCompatTextView tvHeaderr = dialog.findViewById(R.id.tvHeaderr);
        AppCompatTextView cancel_button = dialog.findViewById(R.id.cancel_button_c);
        AppCompatTextView change_button = dialog.findViewById(R.id.change_button_c);

        if (context_regarding.contentEquals("status")) {
            user_name_et.setHint(getResources().getString(R.string.string_status));
            user_name_et.setText(HelperMethods.get_user_status());
            tvHeaderr.setText("Enter Bio");
            user_name_et.setSelection(user_name_et.getText().length());
        } else {
            user_name_et.setHint(getResources().getString(R.string.string_user_name));
            user_name_et.setText(HelperMethods.get_user_name());
            user_name_et.setSelection(user_name_et.getText().length());
        }

        cancel_button.setOnClickListener(v -> {
            dialog.dismiss();
        });


        change_button.setOnClickListener(v -> {

            if (HelperMethods.isConnectedToInternet(getActivity())) {
                if (!user_name_et.getText().toString().isEmpty()) {

                    if (context_regarding.contentEquals("status")) {
                        dialog.dismiss();
                        change_user_status_at_server(user_name_et.getText().toString());
                    } else {

                        if (user_name_et.getText().toString().contentEquals(HelperMethods.get_user_name())) {
                            Toasty.info(getActivity(), ResourcesUtil.getString(R.string.currently_you_have_same_username));

                            showAlertMessageDialog("username already exists, choose different username");
                        } else {

                            dialog.dismiss();
                            change_user_name_at_server(user_name_et.getText().toString());
//                            changggeUserName(user_name_et.getText().toString());
                        }

                    }

                } else {

                    Toasty.error(getActivity(), ResourcesUtil.getString(R.string.please_provide_input));
                }
            } else {

                Toasty.error(getActivity(), ResourcesUtil.getString(R.string.error_internet_not_available));
            }

        });

        dialog.show();
    }

    private void change_user_status_at_server(String new_user_status) {

        show_progress_dialog();

        ChangeUserStatusRequestBody changeUserStatusRequestBody = HelperMethods.create_change_user_status_body(new_user_status);
        disposable.add(apiService.updateUserStatus(HelperMethods.get_user_jwt_key(), changeUserStatusRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ChangeUserStatusResponse>() {
                    @Override
                    public void onSuccess(ChangeUserStatusResponse s) {
                        dismiss_progress_dialog();
                        Toasty.success(getActivity().getApplicationContext(), s.getData().getMessage() + "");
                        user_bio_quite_tv.setText(new_user_status + "".toLowerCase());
                        HelperMethods.save_user_status(new_user_status + "");
                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(getActivity().getApplicationContext(), error + "");


                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    private void changggeUserName(String name) {

        JsonObject jsonObject = new JsonObject();
        JsonObject data = new JsonObject();
        JsonObject attributes = new JsonObject();
        data.add("attributes", attributes);
        attributes.addProperty("username", name);
        jsonObject.add("data", data);


        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        ApiService service = ApiClient.getClient(getActivity()).create(ApiService.class);
        Call<ChangeUserNameResponse> call = service.changeUserNameApi(jsonObject, HelperMethods.get_user_jwt_key());
        call.enqueue(new retrofit2.Callback<ChangeUserNameResponse>() {
            @Override
            public void onResponse(Call<ChangeUserNameResponse> call, retrofit2.Response<ChangeUserNameResponse> response) {

                assert response.body() != null;
                if (response.isSuccessful()) {
                    hud.dismiss();
                    Toast.makeText(getActivity(), "success", Toast.LENGTH_SHORT).show();

                } else {
                    hud.dismiss();
                    Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChangeUserNameResponse> call, Throwable t) {
                hud.dismiss();
//                t.printStackTrace();
                Toast.makeText(getActivity(), "onFailure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void change_user_name_at_server(String user_name) {

        show_progress_dialog();

        ChangeUserNameRequestBody changeUserNameRequestBody = HelperMethods.create_change_username_body(user_name);
        disposable.add(apiService.changeUsername(HelperMethods.get_user_jwt_key(), changeUserNameRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ChangeUserNameResponse>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void onSuccess(ChangeUserNameResponse s) {
                        dismiss_progress_dialog();
                        String userName = String.valueOf(s.getData().getUser_name().getUser_name());
                        String token = HelperMethods.get_user_jwt_key();
                        Toasty.success(getActivity().getApplicationContext(), s.getData().getMessage() + "");
                        Toast.makeText(getActivity(), userName, Toast.LENGTH_SHORT).show();
                        username_tv.setText(s.getData().getUser_name().getUser_name() + "".toLowerCase());
                        user_name_tv.setText(s.getData().getUser_name().getUser_name() + "".toLowerCase());
                        HelperMethods.save_user_name(s.getData().getUser_name().getUser_name() + "");

                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(getActivity().getApplicationContext(), error + "");


                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @Override
    public void onResume() {
        super.onResume();

        Timber.d("[onResume()] method called");

        if (HelperMethods.isConnectedToInternet(getActivity().getApplicationContext())) {
            Timber.d("internet connected");
            if (apiService != null)
                get_profile_data();
        } else {
            Timber.d("internet is not connected bro");
        }

    }

    @Subscribe
    public void getting_profile_data(SendProfileDataToProfileFragment sendProfileDataToProfileFragment) {
        Timber.d("Well we get the data");
        user_profile_data = sendProfileDataToProfileFragment.getUserProfileFirstData();
        set_user_profile_data();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.follower: {
                openActivity("follwer");
                break;
            }
            case R.id.following: {
                openActivity("following");
                break;
            }
        }
    }

    private void openActivity(String tag) {
        Intent i = new Intent(getActivity(), FollowFollowingActivity.class);
        i.putExtra("checkingState", tag);
        i.putExtra("userName", profilename_ss);
        i.putExtra("userID", user_id);
        startActivity(i);
    }

    private void showProgress() {
        if (progress_bar_profile != null) {
            if (progress_bar_profile.getVisibility() == View.GONE) {
                progress_bar_profile.setVisibility(View.VISIBLE);
            }
        }
    }

    private void hideProgress() {
        if (progress_bar_profile != null) {
            if (progress_bar_profile.getVisibility() == View.VISIBLE) {
                progress_bar_profile.setVisibility(View.GONE);
            }
        }
    }

    private static class VpAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> data;

        public VpAdapter(FragmentManager fm, List<Fragment> data) {
            super(fm);
            this.data = data;
        }

        @Override
        public int getCount() {
            return data.size();
        }


        @Override
        public Fragment getItem(int position) {
            return data.get(position);
        }
    }

    public void showAlertMessageDialog(String message) {

        new IOSDialog.Builder(getActivity())
                .title(R.string.app_name)              // String or String Resource ID
                .message(message)  // String or String Resource ID
                .positiveButtonText("Yeah, sure")  // String or String Resource ID
                .negativeButtonText("No Thanks")   // String or String Resource ID
                .positiveClickListener(new IOSDialog.Listener() {
                    @Override
                    public void onClick(IOSDialog iosDialog) {
                        iosDialog.dismiss();

                    }
                }).negativeClickListener(new IOSDialog.Listener() {
            @Override
            public void onClick(IOSDialog iosDialog) {
                iosDialog.dismiss();
                dialog.dismiss();

            }
        })
                .build()
                .show();
    }
}
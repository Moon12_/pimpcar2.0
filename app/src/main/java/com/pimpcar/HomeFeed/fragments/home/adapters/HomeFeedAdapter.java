package com.pimpcar.HomeFeed.fragments.home.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pimpcar.HomeFeed.fragments.home.eventbus.OpenUserProfileEvent;
import com.pimpcar.HomeFeed.fragments.home.eventbus.StartCommentActivity;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.home.viewholder.HomeFeedViewHolder;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostComment;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostLikes;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;


public class HomeFeedAdapter extends InfiniteAdapter<HomeFeedViewHolder> {
    private Context _mContext;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private ArrayList<HomeFeedResponse.DataBean.PostsBean> _mPostsBeans;
    private HomeFeedViewHolder homeFeedViewHolder;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public HomeFeedAdapter(Context context, ArrayList<HomeFeedResponse.DataBean.PostsBean> postsBeans) {
        this._mContext = context;
        this._mPostsBeans = postsBeans;
        if (_mContext != null) {
            _mLayoutInflater_item = LayoutInflater.from(_mContext);
            _mLayoutInflater_loading = LayoutInflater.from(_mContext);
        }
    }


    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mPostsBeans != null && _mPostsBeans.size() > 0)
            return _mPostsBeans.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public HomeFeedViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.profile_rv_single_item, parent, false);
        ButterKnife.bind(this, itemView);
        homeFeedViewHolder = new HomeFeedViewHolder(itemView, _mPostsBeans, _mContext);
        return homeFeedViewHolder;

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {


        } else {

            homeFeedViewHolder = (HomeFeedViewHolder) holder;
            homeFeedViewHolder.r1.setVisibility(View.VISIBLE);
            HomeFeedResponse.DataBean.PostsBean single_post_details = _mPostsBeans.get(position);
            if (!single_post_details.getUser_name().equalsIgnoreCase("")) {
                homeFeedViewHolder.user_name_tv.setText(single_post_details.getUser_name());
            } else {
                homeFeedViewHolder.user_name_tv.setText(single_post_details.getPost_user_name());
            }


            if (single_post_details != null && single_post_details.getUser_profile_pic_url() != null && !single_post_details.getUser_profile_pic_url().isEmpty()) {
                Picasso.with(_mContext)
                        .load(single_post_details.getUser_profile_pic_url())
                        .fit()
                        .into(homeFeedViewHolder.user_profile_pic_rounded);
            } else {

            }

            if (single_post_details != null && single_post_details.getPost_image_url() != null && !single_post_details.getPost_image_url().isEmpty()) {


                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) _mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;


                Transformation transformation = new Transformation() {

                    @Override
                    public Bitmap transform(Bitmap source) {


                        int targetWidth = width;

                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "transformation" + " desiredWidth";
                    }
                };

                String type = getFileTypeFromURL(single_post_details.getPost_image_url());
                if (type.equals("video")) {
                    try {
//                        Bitmap bitmap=retriveVideoFrameFromVideo(single_post_details.getPost_image_url());


                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
// Set video url as data source
                        retriever.setDataSource(single_post_details.getPost_image_url(), new HashMap<String, String>());
// Get frame at 2nd second as Bitmap image
                        Bitmap bitmap = retriever.getFrameAtTime(2000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
//                        Toast.makeText(_mContext, "" + bitmap, Toast.LENGTH_SHORT).show();

// Display the Bitmap image in an ImageView
                        homeFeedViewHolder.user_post_image.setImageBitmap(bitmap);
//                        Glide.with(_mContext)
//                                .asBitmap()
//                                .load(bitmap)
//                                .diskCacheStrategy(DiskCacheStrategy.DATA)
//                                .into(homeFeedViewHolder.user_post_image);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                } else {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_image_url())
                            .transform(transformation)
                            .into(homeFeedViewHolder.user_post_image);
                }

            } else {

            }


            if (single_post_details != null && single_post_details.getPost_user_name() != null && single_post_details.getPost_description() != null) {
                String description = single_post_details.getPost_description();
                homeFeedViewHolder.post_description.setText(Html.fromHtml(description));
            }


            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0) {
                homeFeedViewHolder.post_number_of_likes.setText(single_post_details.getPost_likes().size() + " likes");
                homeFeedViewHolder.post_number_of_likes.setVisibility(View.VISIBLE);
            } else {
                homeFeedViewHolder.post_number_of_likes.setText(0 + " likes");

                homeFeedViewHolder.post_number_of_likes.setVisibility(View.VISIBLE);
            }

            if (single_post_details != null && single_post_details.getPost_location() != null) {
                homeFeedViewHolder.user_location.setText(single_post_details.getPost_location());

            }

            if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 4) {


                homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                homeFeedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.first_person_image);

                }

                if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.second_person_dp);
                }


                if (single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.third_person_dp);

                }

                if (single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(2).getUser_profile_pic_url().isEmpty()) {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                            .fit()
                            .into(homeFeedViewHolder.fourth_person_dp);
                }


            } else {
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 0) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.GONE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);
                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 1) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 2) {
                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.GONE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 3) {

                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.GONE);

                    if (!TextUtils.isEmpty(single_post_details.getPost_likes().get(0).getUser_profile_pic_url()) && !TextUtils.isEmpty(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (!TextUtils.isEmpty(single_post_details.getPost_likes().get(1).getUser_profile_pic_url()) && !TextUtils.isEmpty(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())) {

                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                    if (!TextUtils.isEmpty(single_post_details.getPost_likes().get(2).getUser_profile_pic_url()) && !TextUtils.isEmpty(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.third_person_dp);

                    }


                }
                if (single_post_details != null && single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() == 4) {

                    homeFeedViewHolder.first_person_image.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.second_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.third_person_dp.setVisibility(View.VISIBLE);
                    homeFeedViewHolder.fourth_person_dp.setVisibility(View.VISIBLE);


                    if (single_post_details.getPost_likes().get(0).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(0).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(0).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.first_person_image);

                    }

                    if (single_post_details.getPost_likes().get(1).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(1).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.second_person_dp);
                    }


                    if (single_post_details.getPost_likes().get(2).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(2).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.third_person_dp);

                    }
                    if (single_post_details.getPost_likes().get(3).getUser_profile_pic_url() != null && !single_post_details.getPost_likes().get(1).getUser_profile_pic_url().isEmpty()) {
                        Picasso.with(_mContext)
                                .load(single_post_details.getPost_likes().get(3).getUser_profile_pic_url())
                                .fit()
                                .into(homeFeedViewHolder.fourth_person_dp);
                    }

                }


            }
//            Toast.makeText(_mContext, "" + single_post_details.getPost_likes().size(), Toast.LENGTH_SHORT).show();
            if (single_post_details != null && single_post_details.getPost_comments() != null && single_post_details.getPost_comments().size() == 0) {
                homeFeedViewHolder.user_comments_see.setText(ResourcesUtil.getString(R.string.add_comment));
            } else {
                if (single_post_details != null && single_post_details.getPost_comments() != null && single_post_details.getPost_comments().size() == 1) {
                    homeFeedViewHolder.user_comments_see.setText(1 + " comment");
//                    Toast.makeText(_mContext, "" + single_post_details.getPost_comments(), Toast.LENGTH_SHORT).show();

                } else {
//                    Toast.makeText(_mContext, "else" + single_post_details.getPost_comments(), Toast.LENGTH_SHORT).show();

                    homeFeedViewHolder.user_comments_see.setText(single_post_details.getPost_comments().size() + " comments");
                }
            }

            if (HelperMethods.does_post_like_is_done_by_user(single_post_details.getPost_likes())) {
                homeFeedViewHolder.post_like_button.setBackgroundResource(R.drawable.liked_pressed);
                homeFeedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                homeFeedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) homeFeedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                homeFeedViewHolder.post_like_button.setTag("user_liked");
            } else {
                homeFeedViewHolder.post_like_button.setBackgroundResource(R.drawable.like_unpressed);
                homeFeedViewHolder.post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                homeFeedViewHolder.post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) homeFeedViewHolder.post_like_button.getLayoutParams();
                marginParams.setMargins(20, 20, 20, 20);
                homeFeedViewHolder.post_like_button.setTag("user_not_liked");

            }


            homeFeedViewHolder.user_comments_see.setOnClickListener(v -> {
                EventBus.getDefault().post(new StartCommentActivity(_mPostsBeans.get(position)));
            });


            homeFeedViewHolder.comment_made_rl.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, PostComment.class);
                intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.FEED_COMMENTS);
                intent.putExtra(Constants.SINGLE_POST_DATA, _mPostsBeans.get(position));
                _mContext.startActivity(intent);
            });


            homeFeedViewHolder.post_number_of_likes.setOnClickListener(v -> {
                if (single_post_details.getPost_likes() != null && single_post_details.getPost_likes().size() > 0) {
                    Intent intent = new Intent(_mContext, PostLikes.class);
                    intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.FEED_COMMENTS);
                    intent.putExtra(Constants.SINGLE_POST_DATA, single_post_details);
                    intent.putExtra(Constants.POST_NUMBER_OF_LIKES, single_post_details.getPost_likes().size() + "");
                    _mContext.startActivity(intent);
                } else {
                    Toasty.error(_mContext, " No comments to check!", Toast.LENGTH_SHORT, true).show();
                }
            });


            homeFeedViewHolder.user_profile_pic_rounded.setOnClickListener(v -> {
                throw_event_to_open_profile(_mPostsBeans.get(position).getUser_id());
            });

            homeFeedViewHolder.user_name_tv.setOnClickListener(v -> {
                throw_event_to_open_profile(_mPostsBeans.get(position).getUser_id());
            });


            homeFeedViewHolder.three_vertical_dots.setOnClickListener(v -> {
                if (HelperMethods.is_user_logged_in()) {
                    show_dialog_for_share(_mPostsBeans.get(position).getPost_image_url(), _mContext);

                } else {
                    Toasty.error(_mContext, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();

                }
            });

            if (single_post_details != null && single_post_details.getPost_timestamp() != 0) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    Long timelong = single_post_details.getPost_timestamp();
                    String MyFinalValue = getlongtoago(timelong);

                    homeFeedViewHolder.post_time.setText(MyFinalValue);
                }
            }


        }

        super.onBindViewHolder(holder, position);
    }

    public String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " Sec " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Min " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hrs " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Yrs " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7) {
                convTime = day + " d " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();

        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
//        return time;
    }

    private void show_dialog_for_share(String postImage, Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.share_post_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView share_layout = dialog.findViewById(R.id.share_layout);
        AppCompatTextView cancel = dialog.findViewById(R.id.cancel_layout);

        share_layout.setOnClickListener(v -> {

            Picasso.with(_mContext).load(postImage).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("image/*");
                    i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                    _mContext.startActivity(Intent.createChooser(i, "Share Image"));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });

            dialog.dismiss();

        });


        cancel.setOnClickListener(v -> {
            dialog.dismiss();
        });


        dialog.show();

    }

    private Uri getLocalBitmapUri(Bitmap bitmap) {
        Uri bmpUri = null;
        try {
            File file = new File(_mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void throw_event_to_open_profile(String user_id) {
        EventBus.getDefault().post(new OpenUserProfileEvent(user_id));
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());
        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
//    public Uri getImageUri(Context inContext, Bitmap inImage) {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
//        return Uri.parse(path);
//    }
//
//
//    private Bitmap getBitmapFromView(View view) {
//        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(returnedBitmap);
//        Drawable bgDrawable = view.getBackground();
//        if (bgDrawable != null) {
//            //has background drawable, then draw it on the canvas
//            bgDrawable.draw(canvas);
//        } else {
//            //does not have background drawable, then draw white background on the canvas
//            canvas.drawColor(Color.WHITE);
//        }
//        view.draw(canvas);
//        return returnedBitmap;
//    }

    private String getFileTypeFromURL(String url) {
        String[] splitedArray = url.split("\\.");
        String lastValueOfArray = splitedArray[splitedArray.length - 1];
        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
            return "video";
        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
            return "audio";
        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
            return "photo";
        } else {
            return "";
        }
    }

}

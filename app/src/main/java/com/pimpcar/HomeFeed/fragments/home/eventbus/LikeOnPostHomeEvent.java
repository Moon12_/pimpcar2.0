package com.pimpcar.HomeFeed.fragments.home.eventbus;

public class LikeOnPostHomeEvent {

    private String post_id;

    public LikeOnPostHomeEvent(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }
}

package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PostCommentBodyModel implements Parcelable {
    /**
     * data : {"attributes":{"post_id":"IHPL7t--y1563866942793","user_comment":"This son of bitch brought war to us"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected PostCommentBodyModel(Parcel in) {
        data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Creator<PostCommentBodyModel> CREATOR = new Creator<PostCommentBodyModel>() {
        @Override
        public PostCommentBodyModel createFromParcel(Parcel in) {
            return new PostCommentBodyModel(in);
        }

        @Override
        public PostCommentBodyModel[] newArray(int size) {
            return new PostCommentBodyModel[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
    }

    public static class DataBean implements Parcelable {
        /**
         * attributes : {"post_id":"IHPL7t--y1563866942793","user_comment":"This son of bitch brought war to us"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        protected DataBean(Parcel in) {
            attributes = in.readParcelable(AttributesBean.class.getClassLoader());
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(attributes, flags);
        }

        public static class AttributesBean implements Parcelable {
            /**
             * post_id : IHPL7t--y1563866942793
             * user_comment : This son of bitch brought war to us
             */

            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_comment")
            private String user_comment;

            protected AttributesBean(Parcel in) {
                post_id = in.readString();
                user_comment = in.readString();
            }

            public static final Creator<AttributesBean> CREATOR = new Creator<AttributesBean>() {
                @Override
                public AttributesBean createFromParcel(Parcel in) {
                    return new AttributesBean(in);
                }

                @Override
                public AttributesBean[] newArray(int size) {
                    return new AttributesBean[size];
                }
            };

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_comment() {
                return user_comment;
            }

            public void setUser_comment(String user_comment) {
                this.user_comment = user_comment;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(post_id);
                dest.writeString(user_comment);
            }
        }
    }
}

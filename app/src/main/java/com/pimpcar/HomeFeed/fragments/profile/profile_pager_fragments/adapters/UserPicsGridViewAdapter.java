package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.GridFeedViewHolder;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Notification.NotificationDetailsPostActivity;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.roundedimage.RoundedTransformationBuilder;
import com.pimpcar.utils.Constants;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class UserPicsGridViewAdapter extends InfiniteAdapter<GridFeedViewHolder> {


    private Context mContext;

    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> user_post_data;

    private LayoutInflater layoutInflater, _mLayoutInflater_loading;
    private GridFeedViewHolder feedItemViewHolder = null;
    private FeedInfiniteLoadingView feedInfiniteLoadingView;


    public UserPicsGridViewAdapter(Context c, ArrayList<UserPostBodyResponse.DataBean.PostsBean> _user_post_data) {
        mContext = c;
        this.user_post_data = _user_post_data;
        layoutInflater = LayoutInflater.from(mContext);
        _mLayoutInflater_loading = LayoutInflater.from(mContext);
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        feedInfiniteLoadingView = new FeedInfiniteLoadingView(itemView);
        return feedInfiniteLoadingView;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);


        if (holder instanceof FeedInfiniteLoadingView) {


        } else {

            feedItemViewHolder = (GridFeedViewHolder) holder;

            Transformation transformation = new RoundedTransformationBuilder()
                    .borderColor(Color.BLACK)
                    .borderWidthDp(1)
                    .cornerRadiusDp(8)
                    .oval(false)
                    .build();


            Picasso.with(mContext)
                    .load(user_post_data.get(position).getPost_image_url())
                    .transform(transformation)
                    .into(feedItemViewHolder.grid_view_image_view);


            feedItemViewHolder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(mContext, NotificationDetailsPostActivity.class);
                intent.putExtra(Constants.POST_ID_SENT, user_post_data.get(position).getPost_id());
                mContext.startActivity(intent);
            });

        }


    }

    @Override
    public int getCount() {
        if (user_post_data != null && user_post_data.size() > 0)
            return user_post_data.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public GridFeedViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.custom_layout_gridview, parent, false);
        ButterKnife.bind(this, itemView);
        feedItemViewHolder = new GridFeedViewHolder(itemView);
        return feedItemViewHolder;
    }


}

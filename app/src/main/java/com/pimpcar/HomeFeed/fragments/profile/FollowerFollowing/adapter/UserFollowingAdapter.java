package com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountResponse;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.OtherUserProfileActivity;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.ResourcesUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class UserFollowingAdapter extends InfiniteAdapter<UserFeedViewHolder> {
    private Context _mContext;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private ArrayList<FollowingAccountResponse.DataBean.FollowAccount> _mPostsBeans;
    private UserFeedViewHolder userFeedViewHolder;

    public UserFollowingAdapter(Context context, ArrayList<FollowingAccountResponse.DataBean.FollowAccount> postsBeans) {
        this._mContext = context;
        this._mPostsBeans = postsBeans;
        if (_mContext != null) {
            _mLayoutInflater_item = LayoutInflater.from(_mContext);
            _mLayoutInflater_loading = LayoutInflater.from(_mContext);
        }
    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mPostsBeans != null && _mPostsBeans.size() > 0)
            return _mPostsBeans.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }
    @Override
    public UserFeedViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.user_serach_item_layout, parent, false);
        ButterKnife.bind(this, itemView);
        userFeedViewHolder = new UserFeedViewHolder(itemView, _mContext);
        return userFeedViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FeedInfiniteLoadingView) {

        }else {

            userFeedViewHolder = (UserFeedViewHolder) holder;

            if (!_mPostsBeans.get(position).getUser_name().equalsIgnoreCase("")){
                userFeedViewHolder.user_name_full_name.setText(_mPostsBeans.get(position).getUser_name());
            }else {
                userFeedViewHolder.user_name_full_name.setText(_mPostsBeans.get(position).getFname()+" "+_mPostsBeans.get(position).getLname());
            }

            userFeedViewHolder.user_name.setVisibility(View.GONE);
            userFeedViewHolder.user_follow_button.setVisibility(View.GONE);
            userFeedViewHolder.user_un_follow_button.setVisibility(View.GONE);
            if (_mPostsBeans.get(position).getProfilePic() != null && !_mPostsBeans.get(position).getProfilePic() .isEmpty()) {
                Picasso.with(_mContext)
                        .load(_mPostsBeans.get(position).getProfilePic() )
                        .fit()
                        .into(userFeedViewHolder.user_profile_pic);
            } else {
                userFeedViewHolder.user_profile_pic.setImageDrawable(ResourcesUtil.getDrawableById(R.drawable.user_profile_place_holder));
            }

            Log.d("checkingValueID",""+_mPostsBeans.get(position).getId());
            userFeedViewHolder.itemView.setOnClickListener(v -> {
                Intent intent = new Intent(_mContext, OtherUserProfileActivity.class);
                if (_mPostsBeans.get(position).getUserId() != null) {
                    intent.putExtra(Constants.THIRD_USER_ID, _mPostsBeans.get(position).getUserId());
                } else {
                    intent.putExtra(Constants.THIRD_USER_ID, _mPostsBeans.get(position).getId());
                }

                _mContext.startActivity(intent);
            });
        }
    }
}

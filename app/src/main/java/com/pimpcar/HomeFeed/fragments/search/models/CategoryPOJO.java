package com.pimpcar.HomeFeed.fragments.search.models;

public class CategoryPOJO {

    private String title, genre, year;
    private int image;
    public CategoryPOJO() {
    }

//    public CategoryPOJO(String title, String genre, String year, int image) {
//        this.title = title;
//        this.genre = genre;
//        this.year = year;
//        this.image=image;
//    }
public CategoryPOJO(String title, int image) {
    this.title = title;
    this.image=image;
}

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }

//    public String getYear() {
//        return year;
//    }
//
//    public void setYear(String year) {
//        this.year = year;
//    }

//    public String getGenre() {
//        return genre;
//    }
//
//    public void setGenre(String genre) {
//        this.genre = genre;
//    }


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

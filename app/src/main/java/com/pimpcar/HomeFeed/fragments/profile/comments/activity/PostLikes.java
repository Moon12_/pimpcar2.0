package com.pimpcar.HomeFeed.fragments.profile.comments.activity;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.WindowManager;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.profile.comments.adapters.PostLikeAdapter;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_subscriber.FollowUserEvent;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_subscriber.UnfollowUserEvent;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesResponse;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class PostLikes extends AppCompatActivity {


    @BindView(R.id.user_likes_recycler_view)
    public RecyclerView user_likes_recycler_view;

    @BindView(R.id.user_likes_search_et)
    public AppCompatEditText user_likes_search_et;

    HomeFeedResponse.DataBean.PostsBean single_post_details_feed = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int page_number;
    private String post_id;
    private List<PostAllLikesResponse.DataBean.PostLikesBean> all_likes;
    private String number_of_likes = "0";
    private PostLikeAdapter postLikeAdapter;
    private UserPostBodyResponse.DataBean.PostsBean single_post_details = null;

    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    PostAllLikesBodyModel postAllLikesBodyModel = HelperMethods.create_post_all_like(post_id, getPage_number());
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getAllPostLikes(HelperMethods.get_user_jwt_key(), postAllLikesBodyModel).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<PostAllLikesResponse>() {
                                    @Override
                                    public void onSuccess(PostAllLikesResponse s) {

                                        if (s.getData().getPost_likes().size() > 0) {
                                            for (int i = 0; i < s.getData().getPost_likes().size(); i++) {
                                                if (HelperMethods.is_user_already_following(getApplicationContext().getApplicationContext(),
                                                        s.getData().getPost_likes().get(i).getUser_id())) {
                                                    s.getData().getPost_likes().get(i).setIs_follower(true);
                                                } else {
                                                    s.getData().getPost_likes().get(i).setIs_follower(false);
                                                }
                                                all_likes.add(s.getData().getPost_likes().get(i));
                                                postLikeAdapter.notifyItemInserted(all_likes.size() - 1);
                                            }
                                            postLikeAdapter.moreDataLoaded(all_likes.size(), s.getData().getPost_likes().size());

                                            if (s.getData().getNext_page() == 1) {
                                                increase_page_number();
                                                postLikeAdapter.setShouldLoadMore(true);
                                            } else {
                                                postLikeAdapter.setShouldLoadMore(false);

                                            }

                                        } else {
                                            postLikeAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.likes_activity_layout);
        ButterKnife.bind(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);
        number_of_likes = getIntent().getStringExtra(Constants.POST_NUMBER_OF_LIKES);
        Timber.d("Number of likes " + number_of_likes);
//        post_id = getIntent().getStringExtra(Constants.POST_ID_SENT);
        set_page_number(0);

        if (getIntent().hasExtra(Constants.COMMENTS_COMING_FROM) && getIntent().getStringExtra(Constants.COMMENTS_COMING_FROM).contains(Constants.FEED_COMMENTS)) {
            single_post_details_feed = getIntent().getParcelableExtra(Constants.SINGLE_POST_DATA);
            single_post_details = null;
        } else {
            single_post_details = getIntent().getParcelableExtra(Constants.SINGLE_POST_DATA);
            single_post_details_feed = null;
        }


        setPost_id(single_post_details == null ? single_post_details_feed.getPost_id() : single_post_details.getPost_id());

        if (number_of_likes.contentEquals("0")) {
            Timber.d("No likes on the post");
        } else {
            get_first_post_likes();
        }


    }

    public String getPost_id() {
        return this.post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    private void get_first_post_likes() {

        PostAllLikesBodyModel postAllLikesBodyModel = HelperMethods.create_post_all_like(post_id, getPage_number());
        Timber.d("Pose like body model");

        disposable.add(apiService.getAllPostLikes(HelperMethods.get_user_jwt_key(), postAllLikesBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostAllLikesResponse>() {
                    @Override
                    public void onSuccess(PostAllLikesResponse s) {

                        Timber.d("number of first page likes " + s.getData().getPost_likes().size());

                        if (s.getData().getPost_likes().size() > 0) {
                            all_likes = s.getData().getPost_likes();


                            for (int i = 0; i < s.getData().getPost_likes().size(); i++) {

                                if (HelperMethods.is_user_already_following(getApplication().getApplicationContext(), s.getData().getPost_likes().get(i).getUser_id())) {
                                    all_likes.get(i).setIs_follower(true);
                                } else {
                                    all_likes.get(i).setIs_follower(false);
                                }
                            }


                            postLikeAdapter = new PostLikeAdapter(PostLikes.this, all_likes);
                            user_likes_recycler_view.setLayoutManager(new LinearLayoutManager(PostLikes.this));
                            user_likes_recycler_view.setAdapter(postLikeAdapter);
                            postLikeAdapter.setOnLoadMoreListener(onLoadMoreListener);

                            if (s.getData().getNext_page() == 1) {
                                postLikeAdapter.setShouldLoadMore(true);
                                increase_page_number();
                            } else {
                                postLikeAdapter.setShouldLoadMore(false);
                            }
                        } else {


                        }
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @Subscribe
    public void on_user_follow_button_pressed(FollowUserEvent followUserEvent) {
        emit_follow_user_event(followUserEvent.getUser_id(), followUserEvent.getPosition());
    }

    @Subscribe
    public void on_user_unfollow_button_pressed(UnfollowUserEvent unfollowUserEvent) {
        emit_unfollow_user_event(unfollowUserEvent.getUser_id(), unfollowUserEvent.getPosition());
    }

    private void set_page_number(int page_number) {

        this.page_number = page_number;
    }

    private int getPage_number() {
        return this.page_number;
    }

    private void increase_page_number() {
        page_number = page_number + 1;
    }

    private void emit_follow_user_event(String user_id, int position) {
        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
                    @Override
                    public void onSuccess(FollowUserResponseModel s) {


                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }

    private void emit_unfollow_user_event(String user_id, int position) {
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
                    @Override
                    public void onSuccess(UnFollowUserResponseModel s) {
//                        postLikeAdapter.notifyDataSetChanged();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }
}

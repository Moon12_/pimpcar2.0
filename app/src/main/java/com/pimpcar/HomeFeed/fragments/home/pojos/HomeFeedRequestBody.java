package com.pimpcar.HomeFeed.fragments.home.pojos;

import com.google.gson.annotations.SerializedName;

public class HomeFeedRequestBody {
    /**
     * data : {"attributes":{"page_number":0}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"page_number":0}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * page_number : 0
             */

            @SerializedName("page_number")
            private int page_number;

            public int getPage_number() {
                return page_number;
            }

            public void setPage_number(int page_number) {
                this.page_number = page_number;
            }
        }
    }
}

package com.pimpcar.HomeFeed.fragments.profile.comments.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.profile.comments.adapters.PostCommentAdapter;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.CommentLike;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.PostCommentWithUserTag;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_subscriber.LikeOnCommentSubscriber;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentsBodyModel;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostCommentRequestBody;
import com.pimpcar.Network.model.posts.PostCommentResponse;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class PostComment extends AppCompatActivity {

    @BindView(R.id.user_profile_pic_rounded)
    CircleImageView user_profile_pic_rounded;

    @BindView(R.id.user_name_tv)
    AppCompatTextView user_name_tv;

    @BindView(R.id.user_post_description)
    AppCompatTextView user_post_description;

    @BindView(R.id.user_post_timestamp)
    AppCompatTextView user_post_timestamp;

    @BindView(R.id.user_post_comments_list)
    RecyclerView user_post_comments_list;

    @BindView(R.id.user_comment_profile)
    CircleImageView user_comment_profile;

    @BindView(R.id.post_button_tv)
    AppCompatImageView post_button_tv;

    @BindView(R.id.comment_post_edit_text)
    AppCompatEditText comment_post_edit_text;
    boolean is_comment_made = false;
    private UserPostBodyResponse.DataBean.PostsBean single_post_details = null;
    private HomeFeedResponse.DataBean.PostsBean single_posts_homefeed = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int page_number;
    private PostCommentAdapter postCommentAdapter;
    private List<PostAllCommentResponseModel.DataBean.CommentsBean> all_comments;
    private String post_id;
    private LikeOnCommentSubscriber likeOnCommentSubscriber;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    PostAllCommentsBodyModel user_post_all_comment = HelperMethods.create_post_comments_body(getPage_number(), getPost_id());
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getAllPostComments(HelperMethods.get_user_jwt_key(), user_post_all_comment)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<PostAllCommentResponseModel>() {
                                    @Override
                                    public void onSuccess(PostAllCommentResponseModel s) {

                                        Timber.d("#$%#$%#$%#$%#$%    " + s.toString());

                                        if (s.getData().getComments() != null && s.getData().getComments().size() > 0) {

                                            for (int i = 0; i < s.getData().getComments().size(); i++) {
                                                all_comments.add(s.getData().getComments().get(i));
                                                postCommentAdapter.notifyItemInserted(all_comments.size() - 1);
                                            }
                                            postCommentAdapter.moreDataLoaded(all_comments.size(), s.getData().getComments().size());

                                            if (s.getData().getNext_page() == 1) {
                                                increase_page_number();
                                                postCommentAdapter.setShouldLoadMore(true);
                                            } else {
                                                postCommentAdapter.setShouldLoadMore(false);
                                            }

                                        } else {
                                            postCommentAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };
    private KProgressHUD hud;
    private int REQUEST_ACTIVITY_RESULT_CALL_FEED = 1234;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comment_layout);
        ButterKnife.bind(this);
        apiService = ApiClient.getClient(getApplicationContext()).create(ApiService.class);

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);


        if (getIntent().getStringExtra(Constants.COMMENTS_COMING_FROM).contentEquals(Constants.PROFILE_COMING)) {
            single_post_details = getIntent().getParcelableExtra(Constants.SINGLE_POST_DATA);
            Log.d("checkingValue", "Home   " + single_post_details.getUser_name());
        } else {
            single_posts_homefeed = getIntent().getParcelableExtra(Constants.SINGLE_POST_DATA);
            Log.d("checkingValue", "ns   " + single_posts_homefeed.getUser_name());
        }


        Picasso.with(PostComment.this)
                .load(single_post_details == null ? single_posts_homefeed.getPost_image_url() : single_post_details.getPost_image_url())
                .fit()
                .into(user_profile_pic_rounded);


        if (HelperMethods.is_user_logged_in() && HelperMethods.get_saved_user_profile_image() != null && !HelperMethods.get_saved_user_profile_image().isEmpty()) {
            Picasso.with(PostComment.this)
                    .load(HelperMethods.get_saved_user_profile_image())
                    .fit()
                    .into(user_comment_profile);
        }

        // user_name_tv.setText(single_post_details == null ? single_posts_homefeed.getPost_user_name() + "" : single_post_details.getPost_user_name() + "");

//        if (single_post_details == null&&!single_posts_homefeed.getUser_name().equalsIgnoreCase("")){
//            user_name_tv.setText(single_posts_homefeed.getUser_name());
//        }else {
//            user_name_tv.setText(single_post_details.getUser_name());
//        }

        if (single_posts_homefeed != null) {
            if (!single_posts_homefeed.getUser_name().equalsIgnoreCase("")) {
                user_name_tv.setText(single_posts_homefeed.getUser_name());
            } else {
                user_name_tv.setText(single_posts_homefeed.getPost_user_name());
            }
        } else {
            if (!single_post_details.getUser_name().equalsIgnoreCase("")) {
                user_name_tv.setText(single_post_details.getUser_name());
            } else {
                user_name_tv.setText(single_post_details.getPost_user_name());
            }
        }
        user_post_description.setText(single_post_details == null ? single_posts_homefeed.getPost_description() : single_post_details.getPost_description());

        if (single_post_details != null && single_post_details.getPost_timestamp() != null) {
            user_post_timestamp.setText(HelperMethods.timeAgo(Long.parseLong(single_post_details.getPost_timestamp())));
        } else {
            user_post_timestamp.setVisibility(View.GONE);
//            user_post_timestamp.setText(HelperMethods.covertTimeToText(single_post_details.getPost_timestamp()));
        }


        if (single_post_details != null) {
            setPost_id(single_post_details.getPost_id());
            get_first_post_comment(getPost_id());
        } else {
            setPost_id(single_posts_homefeed.getPost_id());
            get_first_post_comment(getPost_id());
        }

    }

    private void get_first_post_comment(String post_id) {
        show_progress_dialog();

        PostAllCommentsBodyModel user_post_all_comment = HelperMethods.create_post_comments_body(getPage_number(), post_id);
        disposable.add(apiService.getAllPostComments(HelperMethods.get_user_jwt_key(), user_post_all_comment)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostAllCommentResponseModel>() {
                    @Override
                    public void onSuccess(PostAllCommentResponseModel s) {
                        dismiss_progress_dialog();
                        if (s.getData().getComments() != null && s.getData().getComments().size() > 0) {
                            if (all_comments != null)
                                all_comments.clear();
                            all_comments = s.getData().getComments();
                            postCommentAdapter = new PostCommentAdapter(PostComment.this, all_comments);
                            user_post_comments_list.setLayoutManager(new LinearLayoutManager(PostComment.this));
                            user_post_comments_list.setAdapter(postCommentAdapter);
                            postCommentAdapter.setOnLoadMoreListener(onLoadMoreListener);
                            postCommentAdapter.notifyDataSetChanged();

                            if (s.getData().getNext_page() == 1) {
                                postCommentAdapter.setShouldLoadMore(true);
                                increase_page_number();
                            } else {
                                postCommentAdapter.setShouldLoadMore(false);
                            }
                        }
                    }

                    @Override
                    protected void onStart() {
                        super.onStart();
                        Timber.d("method called[onStart()]");
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                Timber.d("#### Network call fucked");
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }


                }));


    }

    private void increase_page_number() {
        page_number = page_number + 1;
    }

    private int getPage_number() {
        return page_number;
    }

    public String getPost_id() {
        return this.post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    @OnClick(R.id.comment_post_edit_text)
    public void on_click_post_comment() {

        if (!comment_post_edit_text.getText().toString().isEmpty()) {


            make_comment_on_post(comment_post_edit_text.getText().toString());

        } else {
            HelperMethods.showToastbar(PostComment.this, "Please write input");
        }


    }

    private void make_comment_on_post(String comment_on_post) {

        show_progress_dialog();
        CompositeDisposable compositeDisposable = new CompositeDisposable();


        PostCommentRequestBody post_comments_body = HelperMethods.create_post_comment_body(getPost_id(), comment_on_post);
        if (disposable != null && apiService != null) {
            disposable.add(compositeDisposable);
            disposable.add(apiService.postCommentOnUserPost(HelperMethods.get_user_jwt_key(), post_comments_body)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<PostCommentResponse>() {
                        @Override
                        public void onSuccess(PostCommentResponse s) {
                            dismiss_progress_dialog();
                            get_first_post_comment(getPost_id());
                            is_comment_made = true;
                        }

                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onError(Throwable e) {
                            if (e instanceof HttpException) {
                                ResponseBody body = ((HttpException) e).response().errorBody();
                                try {
                                    String error_body = body.string();
                                    String error = Utils.get_error_from_response(error_body);
                                    dismiss_progress_dialog();
                                    if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                    }

                                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }));


        }

    }

    @OnClick(R.id.post_button_tv)
    public void make_comment() {
        if (HelperMethods.is_user_logged_in()) {
            if (!comment_post_edit_text.getText().toString().isEmpty()) {
                make_comment_on_post(comment_post_edit_text.getText().toString());
            }
        } else {
            Toasty.error(PostComment.this, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();
        }

    }

    @Subscribe
    public void like_post(CommentLike commentLike) {


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null)
            disposable.dispose();
    }

    @Subscribe
    public void comment_with_tag(PostCommentWithUserTag postCommentWithUserTag) {
        comment_post_edit_text.setText("@" + postCommentWithUserTag.getUser_name() + " ");
        comment_post_edit_text.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(comment_post_edit_text, InputMethodManager.SHOW_IMPLICIT);
        comment_post_edit_text.setSelection(comment_post_edit_text.getText().length());
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        if (is_comment_made) {
            returnIntent.putExtra("result", "yes updated");
        } else {
            returnIntent.putExtra("result", "no updated");
        }
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}

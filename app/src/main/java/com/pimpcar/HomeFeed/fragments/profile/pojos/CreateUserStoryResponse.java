package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateUserStoryResponse {
    /**
     * data : {"message":"stories updated to user","data":{"story_data":{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c08e3e98cb2288f51412b","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","story_text":"I am batman bitch","story_timestamp":1565264099,"location_location":"New Delhi","__v":0}}}
     */

    @SerializedName("data")
    private DataBeanX data;

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * message : stories updated to user
         * data : {"story_data":{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c08e3e98cb2288f51412b","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","story_text":"I am batman bitch","story_timestamp":1565264099,"location_location":"New Delhi","__v":0}}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("data")
        private DataBean data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * story_data : {"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c08e3e98cb2288f51412b","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","story_text":"I am batman bitch","story_timestamp":1565264099,"location_location":"New Delhi","__v":0}
             */

            @SerializedName("story_data")
            private StoryDataBean story_data;

            public StoryDataBean getStory_data() {
                return story_data;
            }

            public void setStory_data(StoryDataBean story_data) {
                this.story_data = story_data;
            }

            public static class StoryDataBean {
                /**
                 * story_image : ["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"]
                 * _id : 5d4c08e3e98cb2288f51412b
                 * uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * stories_id : $2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC
                 * story_text : I am batman bitch
                 * story_timestamp : 1565264099
                 * location_location : New Delhi
                 * __v : 0
                 */

                @SerializedName("_id")
                private String _id;
                @SerializedName("uid")
                private String uid;
                @SerializedName("stories_id")
                private String stories_id;
                @SerializedName("story_text")
                private String story_text;
                @SerializedName("story_timestamp")
                private int story_timestamp;
                @SerializedName("location_location")
                private String location_location;
                private int __v;
                private List<String> story_image;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getStories_id() {
                    return stories_id;
                }

                public void setStories_id(String stories_id) {
                    this.stories_id = stories_id;
                }

                public String getStory_text() {
                    return story_text;
                }

                public void setStory_text(String story_text) {
                    this.story_text = story_text;
                }

                public int getStory_timestamp() {
                    return story_timestamp;
                }

                public void setStory_timestamp(int story_timestamp) {
                    this.story_timestamp = story_timestamp;
                }

                public String getLocation_location() {
                    return location_location;
                }

                public void setLocation_location(String location_location) {
                    this.location_location = location_location;
                }

                public int get__v() {
                    return __v;
                }

                public void set__v(int __v) {
                    this.__v = __v;
                }

                public List<String> getStory_image() {
                    return story_image;
                }

                public void setStory_image(List<String> story_image) {
                    this.story_image = story_image;
                }
            }
        }
    }
}

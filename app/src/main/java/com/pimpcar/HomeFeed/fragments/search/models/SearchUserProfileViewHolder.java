package com.pimpcar.HomeFeed.fragments.search.models;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.FollowUser;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.UnfollowUser;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class SearchUserProfileViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_profile_pic)
    public CircleImageView user_profile_pic;

    @BindView(R.id.user_name_full_name)
    public AppCompatTextView user_name_full_name;

    @BindView(R.id.user_name)
    public AppCompatTextView user_name;

//    @BindView(R.id.user_follow_button_rl)
//    public RelativeLayout user_follow_button_rl;

    @BindView(R.id.user_follow_button)
    public AppCompatTextView user_follow_button;

    @BindView(R.id.user_un_follow_button)
    public AppCompatTextView user_un_follow_button;

//    @BindView(R.id.user_name_rl)
//    public RelativeLayout user_name_rl;
//

    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();


    public SearchUserProfileViewHolder(@NonNull View itemView, Context _mContext, List<UserSearchResponseBody.DataBean.SerachedDataBean> _mSerachedDataBeans) {
        super(itemView);

        ButterKnife.bind(this, itemView);


        apiService = ApiClient.getClient(_mContext.getApplicationContext()).create(ApiService.class);


        user_follow_button.setOnClickListener(v -> {


            user_follow_button.setVisibility(View.GONE);
            user_un_follow_button.setVisibility(View.VISIBLE);
           // follow_event(_mSerachedDataBeans.get(getAdapterPosition()).getUid());

            EventBus.getDefault().post(new FollowUser(_mSerachedDataBeans.get(getAdapterPosition()).getUid()));

        });

        user_un_follow_button.setOnClickListener(v -> {
            user_un_follow_button.setVisibility(View.GONE);
            user_follow_button.setVisibility(View.VISIBLE);
           // follow_event(_mSerachedDataBeans.get(getAdapterPosition()).getUid());
            EventBus.getDefault().post(new UnfollowUser(_mSerachedDataBeans.get(getAdapterPosition()).getUid()));

        });
    }


    private void follow_event(String user_id) {

        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
                    @Override
                    public void onSuccess(FollowUserResponseModel s) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }


    private void emit_unfollow_user_event(String user_id) {
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
                    @Override
                    public void onSuccess(UnFollowUserResponseModel s) {

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

}

package com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models;

public class PostCommentWithUserTag {
    private String user_name;


    public PostCommentWithUserTag(String user_name){

        this.user_name = user_name;

    }


    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}

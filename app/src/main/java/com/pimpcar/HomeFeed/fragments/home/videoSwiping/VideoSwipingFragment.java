package com.pimpcar.HomeFeed.fragments.home.videoSwiping;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

//import com.google.firebase.iid.FirebaseInstanceId;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.dialogs.ActionBottomDialogFragment;
import com.pimpcar.models.VideoDataModel;
import com.pimpcar.models.VideoListResponseModel;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.FollowUnFollowResponseModel;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoSwipingFragment extends Fragment implements VideoSwipingAdapter.Callback, ActionBottomDialogFragment.ItemClickListener {
    View view;
    private KProgressHUD hud;
    ViewPager2 videosViewPager;

    List<VideoItem> videoSeparate;
    List<VideoDataModel> videoItems;
    List<CommentsDataModel> commentsDataModels;
    OnActivityStateChanged onActivityStateChanged = null;
    VideoSwipingAdapter pagerAdapter;
    private static final int ACTIVITY_NUM = 0;
    private static final int CAMERA_PERMISSION_REQUEST_CODE = 88888;
    String url, surl;
    int launchcode, callcount = 0, modulecode;
    //   VideoView videoView;
    MediaController mediaController;
    String filePath;

    private int selectedPosition = 0;
    int playflag = 0;
    String history_date, user_token = "";
    int mid, pmode = 0;
    boolean isplaying = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_video_swiping, container, false);

        videosViewPager = view.findViewById(R.id.viewPagerVideos);
        videoItems = new ArrayList<>();
        videoSeparate = new ArrayList<>();
        commentsDataModels = new ArrayList<>();
        user_token = Utilities.getString(getActivity(), "user_token");
//        Toast.makeText(getActivity(), ""+user_token, Toast.LENGTH_SHORT).show();
        updateTokenApi(HelperMethods.get_underScoreId(), user_token);

//        user_token = Utilities.getString(getActivity(), "user_token");
//        getCommentsApi();
        getVideoApi();
        videosViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
//                displayMetaInfo(position);

//                Toast.makeText(getActivity(), "onPageSelected", Toast.LENGTH_SHORT).show();

            }
        });

//        videosViewPager.setAdapter(new VideoSwipingAdapter(getActivity(), videoItems));
        return view;
    }

    private void getVideoApi() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<VideoListResponseModel> call = getResponse.getVideolISTPosts();
        call.enqueue(new Callback<VideoListResponseModel>() {
            @Override
            public void onResponse(Call<VideoListResponseModel> call, Response<VideoListResponseModel> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
                        hud.dismiss();
                        Toast.makeText(getActivity(), "videos found successfully", Toast.LENGTH_SHORT).show();

                        videoItems = response.body().getPosts();
                        pagerAdapter = new VideoSwipingAdapter(getActivity(), videoItems, VideoSwipingFragment.this);

                        videosViewPager.setAdapter(pagerAdapter);

                    } else {
                        hud.dismiss();

                        Toast.makeText(getActivity(), "not found", Toast.LENGTH_SHORT).show();
                    }


                } else {
                    hud.dismiss();
                    Toast.makeText(getActivity(), "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                String tt = t.toString();
                Toast.makeText(getActivity(), "something went wrong" + tt, Toast.LENGTH_SHORT).show();

            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    //  initializeplayer();


    @Override
    public void onStart() {
        super.onStart();
//        pagerAdapter.startListening();
        videosViewPager.setAdapter(pagerAdapter);

    }

    @Override
    public void onStop() {
        super.onStop();
//        pagerAdapter.stopListening();

    }


    @Override
    public void onItemClick(int pos) {
        Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
        followUnFollowApi(videoItems.get(pos).getUnderScoreId());

    }

    private void followUnFollowApi(String followuserId) {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(70, 70);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", HelperMethods.get_underScoreId());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<FollowUnFollowResponseModel> call = getResponse.followUnfollowApi(followuserId, jsonObject);
        call.enqueue(new Callback<FollowUnFollowResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<FollowUnFollowResponseModel> call, Response<FollowUnFollowResponseModel> response) {
                int code = response.code();
                if (code == 200) {
                    getVideoApi();
//                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();

                } else {
                    hud.dismiss();
                    Toast.makeText(getActivity(), "not successfull response", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onItemClick(String item) {
        Toast.makeText(getActivity(), "dialog updated", Toast.LENGTH_SHORT).show();
    }

    private void updateTokenApi(String _id, String user_token) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_token", user_token);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<CommentsLikeResponseModel> call = getResponse.update_TokenApi(_id, jsonObject);
        call.enqueue(new Callback<CommentsLikeResponseModel>() {
            @Override
            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
                int code = response.code();

                if (code == 200) {
//                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();
//                    Toast.makeText(getActivity(), "Token Updated Successfully", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), "something is went wrong with fcm token", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("gttt", call.toString());
                Toast.makeText(getActivity(), "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

}
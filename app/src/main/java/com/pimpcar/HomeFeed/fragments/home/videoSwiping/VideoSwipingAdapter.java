package com.pimpcar.HomeFeed.fragments.home.videoSwiping;

import static android.media.session.PlaybackState.STATE_BUFFERING;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.google.android.exoplayer2.Player.STATE_ENDED;
import static com.google.android.exoplayer2.Player.STATE_READY;
import static com.pimpcar.utils.ShareprefConstantKeys.underScoreId;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.gson.JsonObject;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.Notification.NotificationDetailsPostActivity;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.dialogs.ActionBottomDialogFragment;
import com.pimpcar.models.PostFollowerModel;
import com.pimpcar.models.VideoDataModel;
import com.pimpcar.models.VideoListResponseModel;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.FollowUnFollowResponseModel;
import com.pimpcar.models.comments.models.CommentsLikesModel;
import com.pimpcar.models.comments.models.CommentsResponseModel;
import com.pimpcar.models.comments.models.GetFollowerFOLLOWINGResponseModel;
import com.pimpcar.models.comments.models.PostLikesModel;
import com.pimpcar.models.comments.models.UserAllFollowersModel;
import com.pimpcar.models.comments.models.UserAllFollowingModel;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VideoSwipingAdapter extends RecyclerView.Adapter<VideoSwipingAdapter.ViewHolder> {
    AppCompatTextView totalComments;
    AppCompatImageView post_like_button;
    private Context context;
    SimpleExoPlayer exoPlayer;
    //Initialize variable
    PlayerView playerView;
    RelativeLayout llParent;
    ProgressBar progressBar;
    private List<VideoDataModel> list;
    KProgressHUD hud;
    int category_id;
    List<CommentsDataModel> commentsDataModels;
    List<CommentsLikesModel> commentsLikesModels;
    List<PostFollowerModel> postFollowerModels;
    List<UserAllFollowersModel> userAllFollowersModels;
    List<UserAllFollowingModel> userAllFollowingModels;
    List<PostLikesModel> postLikesModels;
    private int post_total_number_of_likes = 0;
    private ApiService apiService;
    Callback callback;
    Uri video;
    VideoDataModel videoItem;
    private CompositeDisposable disposable;
    String cat_name, cat_image, userId = "", currentUserId = "";

    public VideoSwipingAdapter(Context context, List<VideoDataModel> list, Callback callback) {
        this.context = context;
        this.list = list;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_videos_container, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        videoItem = list.get(position);
//        ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        disposable = new CompositeDisposable();
//        holder.bind(position);
        commentsDataModels = new ArrayList<>();
        commentsLikesModels = new ArrayList<>();
        postFollowerModels = new ArrayList<>();
        userAllFollowersModels = new ArrayList<>();
        userAllFollowingModels = new ArrayList<>();
        postLikesModels = new ArrayList<>();
//        getCommentsApi(videoItem.getUnderScoreId());
        postFollowerModels = videoItem.getFollowerModels();
        postLikesModels = videoItem.getPostLikesModels();
        currentUserId = HelperMethods.get_user_id();
        String CurrentunderScoreId = HelperMethods.get_underScoreId();
        postFollowerModels = new ArrayList<>();
        postLikesModels = new ArrayList<>();
//        getCommentsApi(videoItem.getUnderScoreId());
        postLikesModels = videoItem.getPostLikesModels();
        postFollowerModels = videoItem.getFollowerModels();
//        if (postFollowerModels.size() == 0) {
//            holder.tvFollow.setText("Follow");
//        }
//
//        for (int i = 0; i <= postFollowerModels.size() - 1; i++) {
//
//            if (postFollowerModels.size() > 0) {
//                String underScoreIdd = videoItem.getFollowerModels().get(i).getUser_id();
//                if (underScoreIdd.equals(CurrentunderScoreId)) {
//                    holder.tvFollow.setText("UnFollow");
//                }
//            }
//
//
//        }
        if (videoItem.getTotal_post_likes() == 0) {
            Toast.makeText(context, "empty list", Toast.LENGTH_SHORT).show();
            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24_white);
        }
        for (int i = 0; i <= postLikesModels.size() - 1; i++) {


            if (postLikesModels.size() > 0) {
                String underScoreIdd = videoItem.getPostLikesModels().get(i).getUser_id();
//                Toast.makeText(context, HelperMethods.get_user_id(), Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, underScoreIdd, Toast.LENGTH_SHORT).show();

                if (underScoreIdd.equals(HelperMethods.get_user_id())) {
                    post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
//                    Toast.makeText(context, " macthed" + i, Toast.LENGTH_SHORT).show();


                }
//                else {
//                    Toast.makeText(context, "not macthed" + i, Toast.LENGTH_SHORT).show();
////                    post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24_white);
//
//                }
            }


        }


        holder.txtTitle.setText(videoItem.getPost_user_name());
        holder.txtDesc.setText(videoItem.getPost_description());
        holder.post_users_tag.setText("# " + videoItem.getPost_users_tag());
        holder.mVideoView.setVideoPath(videoItem.getPost_image_url());
        getAllFollowingFollwersApi(holder.tvFollow, videoItem.get_id_user_id());
        if (!videoItem.getUser_profile_pic_url().equals("")) {

            Picasso.with(context).load(videoItem.getUser_profile_pic_url()).fit().centerCrop().into(holder.image_view_profile_pic);

        } else {
            Picasso.with(context).load(R.drawable.user_profile_place_holder).fit().centerCrop().into(holder.image_view_profile_pic);

        }


        exoPlayer = new SimpleExoPlayer.Builder(context).build();

        playerView.setPlayer(exoPlayer);
        MediaItem mediaItem = MediaItem.fromUri(videoItem.getPost_image_url());
        exoPlayer.addMediaItem(mediaItem);

        exoPlayer.prepare();
        exoPlayer.play();
        totalComments.setText(videoItem.getPost_comments());
        holder.tvTotalLikes.setText(String.valueOf(videoItem.getTotal_post_likes()));
        holder.post_hash_tags.setText("# " + videoItem.getPost_hash_tags().toString());

        post_like_button.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onClick(View view) {
//                likeDislikePost(videoItem.post_id);
                if (post_like_button.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.ic_baseline_favorite_24_white).getConstantState()) {
                    likeDislikePost(videoItem.getPost_id(), holder.tvTotalLikes);
                    Toast.makeText(context, "yes", Toast.LENGTH_SHORT).show();

                } else {
//                    Toast.makeText(context, "unlike", Toast.LENGTH_SHORT).show();

                    likeDislikePost(videoItem.getPost_id(), holder.tvTotalLikes);

                }


            }
        });
        holder.image_view_option_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BottomSheetDialogFragment bottomSheetDialogFragment = new CommentsBottomSheetFragment();
//                bottomSheetDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                ActionBottomDialogFragment addPhotoBottomDialogFragment2 = ActionBottomDialogFragment.newInstance();
                addPhotoBottomDialogFragment2.setStyle(ActionBottomDialogFragment.STYLE_NORMAL, R.style
                        .CustomBottomSheetDialogTheme);
                String totalComments = videoItem.getPost_comments();
                String getUnderScoreId = videoItem.getUnderScoreId();
                Utilities.saveString(context, "postunderScoreId", getUnderScoreId);
                Utilities.saveString(context, "totalComments", totalComments);
                addPhotoBottomDialogFragment2.show(((FragmentActivity) context).getSupportFragmentManager(),

                        ActionBottomDialogFragment.TAG);


            }
        });

        holder.tvFollow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "under Developing", Toast.LENGTH_SHORT).show();

                if (holder.tvFollow.getText().toString().equals("Follow")) {
                followUnFollowApi(HelperMethods.get_underScoreId(), holder.tvFollow, videoItem.get_id_user_id());

                }

//                else {
//                    holder.tvFollow.setText("Follow");
//                    followUnFollowApi(HelperMethods.get_underScoreId(), holder.tvFollow, videoItem.get_id_user_id());
//
//                }
            }
        });

        holder.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                String fileType = getFileTypeFromURL(videoItem.getPost_image_url());
                if (fileType.equals("video")) {
//                    Toast.makeText(context, "video get", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(videoItem.getPost_image_url(), new HashMap<String, String>());
//                    retriever.setDataSource(videoItem.getPost_image_url().replaceAll(" ", "%20"));
//                    holder.mVideoView.setDataSource(videoItem.getPost_image_url().replaceAll(" ","%20"));


//                    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                    long timeInMillisec = Long.parseLong(time);
//                    retriever.release();
//                    String duration = convertMillieToHMmSs(timeInMillisec);
                    //use this duration
//                    Toast.makeText(context, "" + duration, Toast.LENGTH_SHORT).show();
                    float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                    float screenRatio = holder.mVideoView.getWidth() / (float) holder.mVideoView.getHeight();
                    float scale = videoRatio / screenRatio;
                    if (scale >= 1f) {
                        holder.mVideoView.setScaleX(scale);
                    } else {
                        holder.mVideoView.setScaleY(1f / scale);
                    }
                    mp.start();
                }

            }
        });
        holder.toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (holder.toggleButton1.isChecked()) {
                    Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                    holder.mVideoView.canPause();

                } else {
                    Toast.makeText(context, "DisLiked", Toast.LENGTH_SHORT).show();

                }
            }
        });
        holder.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        holder.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(context, "Cant Play this Video" + what, Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                Toast.makeText(context, "" + extra, Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
//                mp.prepareAsync();

                return true;
            }
        });
        holder.image_view_option_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, videoItem.getPost_image_url());
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                context.startActivity(shareIntent);
//                Intent share = new Intent(Intent.ACTION_SEND);
//                share.setType("video/*");
//                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(videoItem.videoURL));
//                context.startActivity(Intent.createChooser(share, "Share Video"));


            }
        });

    }

    //
    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        getAllFollowingFollwersApi(holder.tvFollow, videoItem.getPost_image_url());

    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        getAllFollowingFollwersApi(holder.tvFollow, videoItem.getPost_image_url());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface Callback {
        void onItemClick(int pos);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView tvTotalLikes;
        VideoView mVideoView;
        TextView txtTitle, txtDesc, post_users_tag, post_hash_tags;
        LikeButton androidLikeButton;
        ToggleButton toggleButton1;
        TextView tvFollow;
        ShapeableImageView image_view_profile_pic;
        AppCompatImageView image_view_option_share, image_view_option_comment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            androidLikeButton = itemView.findViewById(R.id.likeBtn);
            playerView = itemView.findViewById(R.id.playerview);
            tvFollow = itemView.findViewById(R.id.tvFollow);
            image_view_profile_pic = itemView.findViewById(R.id.image_view_profile_pic);
            post_like_button = itemView.findViewById(R.id.post_like_button);
            toggleButton1 = itemView.findViewById(R.id.toggleButton1);
            llParent = itemView.findViewById(R.id.llParent);
            tvTotalLikes = itemView.findViewById(R.id.post_number_of_likes);
            post_users_tag = itemView.findViewById(R.id.post_users_tag);
            totalComments = itemView.findViewById(R.id.totalComments);
            post_hash_tags = itemView.findViewById(R.id.post_hash_tags);
            mVideoView = itemView.findViewById(R.id.videoView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDesc = itemView.findViewById(R.id.txtDesc);
            progressBar = itemView.findViewById(R.id.progressBar);
            image_view_option_share = itemView.findViewById(R.id.image_view_option_share);
            image_view_option_comment = itemView.findViewById(R.id.image_view_option_comment);
//            cat_names =itemView.findViewById(R.id.text_cat);
            post_hash_tags.setSelected(true);
            post_users_tag.setSelected(true);

        }

        private void bind(int pos) {
            initClickListener();
        }

        private void initClickListener() {
//            tvFollow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    callback.onItemClick(getAdapterPosition());
//                }
//            });
        }
    }

    public void startPlayer() {
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.getPlaybackState();
    }

    public void pausePlayer() {
        exoPlayer.setPlayWhenReady(false);
        exoPlayer.getPlaybackState();
    }

    public void releasePlayer() {
        exoPlayer.setPlayWhenReady(false);
        exoPlayer.release();
        exoPlayer.stop(true);
    }

//============================METHODS HERE ==============================================================================





    private void likeDislikePost(String postId, AppCompatTextView tvTotalLikes) {

        JsonObject jsonObject = new JsonObject();
        JsonObject data = new JsonObject();
        JsonObject attributes = new JsonObject();
        data.add("attributes", attributes);
        attributes.addProperty("post_id", postId);
        jsonObject.add("data", data);


        KProgressHUD hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setSize(50, 50)
                .show();
        ApiService service = ApiClient.getClient(context).create(ApiService.class);
        Call<PostLikeDislikeResponse> call = service.likeDislikeApi(jsonObject, HelperMethods.get_user_jwt_key());
        call.enqueue(new retrofit2.Callback<PostLikeDislikeResponse>() {
            @Override
            public void onResponse(Call<PostLikeDislikeResponse> call, retrofit2.Response<PostLikeDislikeResponse> response) {

                assert response.body() != null;
                if (response.isSuccessful()) {

                    hud.dismiss();


                    if (response.body().getData().getMessage().equals("post liked successfully")) {
                        int totallikesIncremnt = Integer.parseInt(tvTotalLikes.getText().toString()) + 1;
                        tvTotalLikes.setText(String.valueOf(totallikesIncremnt));
                        post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24);
                        Toast.makeText(context, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        if (tvTotalLikes.getText().toString().equals("1")) {
                            tvTotalLikes.setText("0");
//                            Toast.makeText(ctx, "call", Toast.LENGTH_SHORT).show();
                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24_white);
                            Toast.makeText(context, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            int totallikesIncremnt = Integer.parseInt(tvTotalLikes.getText().toString()) - 1;
                            tvTotalLikes.setText(String.valueOf(totallikesIncremnt));
                            post_like_button.setImageResource(R.drawable.ic_baseline_favorite_24_white);
                            Toast.makeText(context, response.body().getData().getMessage(), Toast.LENGTH_SHORT).show();

                        }


                    }
//                    Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

                } else {
                    hud.dismiss();
                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostLikeDislikeResponse> call, Throwable t) {
                hud.dismiss();
//                t.printStackTrace();
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void followUnFollowApi(String followuserId, TextView folowUnfOLLOW, String postUnderScoreid) {
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(70, 70);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", postUnderScoreid);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<FollowUnFollowResponseModel> call = getResponse.followUnfollowApi(followuserId, jsonObject);
        call.enqueue(new retrofit2.Callback<FollowUnFollowResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<FollowUnFollowResponseModel> call, Response<FollowUnFollowResponseModel> response) {
                int code = response.code();
                if (code == 200) {
                    assert response.body() != null;
                    if (response.body().getMessage().equals("User followed")) {
                        folowUnfOLLOW.setText("Following");
                        hud.dismiss();
                    } else {
                        folowUnfOLLOW.setText("Follow");
                        hud.dismiss();


                    }
                    Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    //                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();

                } else {
                    hud.dismiss();
                    Toast.makeText(context, "not successfull response", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getAllFollowingFollwersApi(TextView tvFollow, String user_post_id) {
//        hud = KProgressHUD.create(context)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path
        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<GetFollowerFOLLOWINGResponseModel> call = getResponse.get_follower_following(HelperMethods.get_underScoreId());
        call.enqueue(new retrofit2.Callback<GetFollowerFOLLOWINGResponseModel>() {
            @Override
            public void onResponse(Call<GetFollowerFOLLOWINGResponseModel> call, Response<GetFollowerFOLLOWINGResponseModel> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
//                        hud.dismiss();
                        userAllFollowersModels = response.body().getUser().getUser_followers();
                        userAllFollowingModels = response.body().getUser().getUser_following();
//                        Toast.makeText(context, "found" + userAllFollowersModels, Toast.LENGTH_SHORT).show();

                        if (userAllFollowingModels.size() == 0) {
                            tvFollow.setText("Follow");
                        }

                        for (int i = 0; i <= userAllFollowingModels.size() - 1; i++) {
                            String user_iddd = userAllFollowingModels.get(i).getUser_id();
                            if (userAllFollowingModels.size() > 0) {

                                if (user_iddd.equals(user_post_id)) {
                                    tvFollow.setText("Following");
                                }
                            }


                        }
                    } else {
//                        hud.dismiss();

                        Toast.makeText(context, "not found", Toast.LENGTH_SHORT).show();
                    }


                } else {
//                    hud.dismiss();
                    Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
                Log.d("gttt", call.toString());
                String tt = t.toString();
                Toast.makeText(context, "something went wrong" + tt, Toast.LENGTH_SHORT).show();

            }
        });
    }

//==============================================================================================================












    public void shareVideo(final String title, String path) {

        MediaScannerConnection.scanFile(getApplicationContext(), new String[]{path},

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(
                                android.content.Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_SUBJECT, title);
                        shareIntent.putExtra(
                                android.content.Intent.EXTRA_TITLE, title);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        context.startActivity(Intent.createChooser(shareIntent,
                                "Share This Video"));

                    }
                });
    }

    public static String convertMillieToHMmSs(long millie) {
        long seconds = (millie / 1000);
        long second = seconds % 60;
        long minute = (seconds / 60) % 60;
        long hour = (seconds / (60 * 60)) % 24;

        String result = "";
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        } else {
            return String.format("%02d:%02d", minute, second);
        }

    }

    private String getFileTypeFromURL(String url) {
        String[] splitedArray = url.split("\\.");
        String lastValueOfArray = splitedArray[splitedArray.length - 1];
        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
            return "video";
        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
            return "audio";
        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
            return "photo";
        } else {
            return "";
        }
    }

    private int increase_number_of_likes_by_one() {
        return post_total_number_of_likes = post_total_number_of_likes + 1;
    }

    private int decrease_number_of_likes_by_one() {

        return post_total_number_of_likes = post_total_number_of_likes - 1;
    }


//        if (postFollowerModels.size() == 0) {
//            tvFollow.setVisibility(View.VISIBLE);
//            tvUnFollow.setVisibility(View.GONE);
//        }
//        if (postLikesModels.size() == 0) {
//            post_like_button.setVisibility(View.VISIBLE);
//            postUnLIKE.setVisibility(View.GONE);
//        }
//        for (int i = 0; i <= postFollowerModels.size() - 1; i++) {
//
//            if (postFollowerModels.size() > 0) {
//                String underScoreIdd = videoItem.getFollowerModels().get(i).getUser_id();
//                if (underScoreIdd.equals(CurrentunderScoreId)) {
//                    tvFollow.setVisibility(View.GONE);
//                    tvUnFollow.setVisibility(View.VISIBLE);
//                } else {
//                    tvFollow.setVisibility(View.VISIBLE);
//                    tvUnFollow.setVisibility(View.GONE);
//                }
//            }
//
//
//        }
//
//        for (int i = 0; i <= postLikesModels.size() - 1; i++) {
//
//            if (postLikesModels.size() > 0) {
//                String underScoreIdd = videoItem.getPostLikesModels().get(i).getUser_id();
//                if (underScoreIdd.equals(HelperMethods.get_user_id())) {
//                    post_like_button.setVisibility(View.GONE);
//                    postUnLIKE.setVisibility(View.VISIBLE);
//                } else {
//                    post_like_button.setVisibility(View.VISIBLE);
//                    postUnLIKE.setVisibility(View.GONE);
//                }
//            }
//
//
//        }
//        holder.mVideoView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
//            @Override
//            public void onViewAttachedToWindow(View view) {
//
//                postFollowerModels = new ArrayList<>();
//                postLikesModels = new ArrayList<>();
////        getCommentsApi(videoItem.getUnderScoreId());
//                postFollowerModels = videoItem.getFollowerModels();
//                postLikesModels = videoItem.getPostLikesModels();
//                if (postFollowerModels.size() == 0) {
//                    holder.tvFollow.setText("Follow");
//                }
//                if (postLikesModels.size() == 0) {
//                    post_like_button.setVisibility(View.VISIBLE);
//                    postUnLIKE.setVisibility(View.GONE);
//                }
//                for (int i = 0; i <= postFollowerModels.size() - 1; i++) {
//
//                    if (postFollowerModels.size() > 0) {
//                        String underScoreIdd = videoItem.getFollowerModels().get(i).getUser_id();
//                        if (underScoreIdd.equals(CurrentunderScoreId)) {
//                            holder.tvFollow.setText("UnFollow");
//
//                        } else {
//                            holder.tvFollow.setText("Follow");
//
//                        }
//                    }
//
//
//                }
//                for (int i = 0; i <= postLikesModels.size() - 1; i++) {
//
//                    if (postLikesModels.size() > 0) {
//                        String underScoreIdd = videoItem.getPostLikesModels().get(i).getUser_id();
//                        if (underScoreIdd.equals(HelperMethods.get_user_id())) {
//                            post_like_button.setVisibility(View.GONE);
//                            postUnLIKE.setVisibility(View.VISIBLE);
//                        } else {
//                            post_like_button.setVisibility(View.VISIBLE);
//                            postUnLIKE.setVisibility(View.GONE);
//                        }
//                    }
//
//
//                }
//
//
//            }
//
//            @Override
//            public void onViewDetachedFromWindow(View view) {
//                postFollowerModels = new ArrayList<>();
//                postLikesModels = new ArrayList<>();
////        getCommentsApi(videoItem.getUnderScoreId());
//                postFollowerModels = videoItem.getFollowerModels();
//                postLikesModels = videoItem.getPostLikesModels();
//                if (postFollowerModels.size() == 0) {
//                    holder.tvFollow.setText("Follow");
//
//                }
//                if (postLikesModels.size() == 0) {
//                    post_like_button.setVisibility(View.VISIBLE);
//                    postUnLIKE.setVisibility(View.GONE);
//                }
//                for (int i = 0; i <= postFollowerModels.size() - 1; i++) {
//
//                    if (postFollowerModels.size() > 0) {
//                        String underScoreIdd = videoItem.getFollowerModels().get(i).getUser_id();
//                        if (underScoreIdd.equals(CurrentunderScoreId)) {
//                            holder.tvFollow.setText("UnFollow");
//
//                        } else {
//                            holder.tvFollow.setText("Follow");
//
//                        }
//                    }
//
//
//                }
//                for (int i = 0; i <= postLikesModels.size() - 1; i++) {
//
//                    if (postLikesModels.size() > 0) {
//                        String underScoreIdd = videoItem.getPostLikesModels().get(i).getUser_id();
//                        if (underScoreIdd.equals(HelperMethods.get_user_id())) {
//                            post_like_button.setVisibility(View.GONE);
//                            postUnLIKE.setVisibility(View.VISIBLE);
//                        } else {
//                            post_like_button.setVisibility(View.VISIBLE);
//                            postUnLIKE.setVisibility(View.GONE);
//                        }
//                    }
//
//
//                }
//
//            }
//        });


//        if (userId.equals(currentUserId)) {
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
//        } else {
//            holder.androidLikeButton.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
//
//        }
}

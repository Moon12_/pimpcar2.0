package com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_subscriber;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.CommentOnPostEvent;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostCommentRequestBody;
import com.pimpcar.Network.model.posts.PostCommentResponse;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class CommentOnPostSubscriber {

    private Context mContext;
    private CommentOnPostEvent commentOnPostEvent;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    public CommentOnPostSubscriber(ApiService apiService, Context context) {
        this.apiService = apiService;
        this.mContext = context;
    }


    @Subscribe
    public void onCommentOnPostEvent(CommentOnPostEvent commentOnPostEvent) {
        comment_on_post(commentOnPostEvent.getPost_id(), commentOnPostEvent.getComment());
    }

    private void comment_on_post(String post_id, String comment) {
        PostCommentRequestBody postCommentRequestBody = HelperMethods.create_post_comment_body(post_id, comment);
        disposable.add(apiService.postCommentOnUserPost(HelperMethods.get_user_jwt_key(), postCommentRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostCommentResponse>() {
                    @Override
                    public void onSuccess(PostCommentResponse s) {
                        Timber.d("comment done message " + s.getData().getMessage());

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                }

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));


    }


    public void registerPostCommentEvent() {
        if (!EventBus.getDefault().isRegistered(this)) {
            Timber.d("---> :: Registered Successful");
            EventBus.getDefault().register(this);
        }
    }

    public void unRegisterPostCommentEvent() {
        if (EventBus.getDefault().isRegistered(this)) {
            Timber.d("---> :: Unregistered Successful");
            EventBus.getDefault().unregister(this);
        }
    }

}

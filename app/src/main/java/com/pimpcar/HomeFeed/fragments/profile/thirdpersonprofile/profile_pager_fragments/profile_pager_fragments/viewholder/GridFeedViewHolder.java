package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.profile_pager_fragments.profile_pager_fragments.viewholder;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.pimpcar.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridFeedViewHolder extends RecyclerView.ViewHolder {


    @BindView(R.id.grid_view_image_view)
    public AppCompatImageView grid_view_image_view;

    public GridFeedViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

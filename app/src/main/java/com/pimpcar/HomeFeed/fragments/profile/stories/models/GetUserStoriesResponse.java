package com.pimpcar.HomeFeed.fragments.profile.stories.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GetUserStoriesResponse implements Parcelable{
    /**
     * data : {"message":"users stories","stories":[{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c08e3e98cb2288f51412b","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","story_text":"I am batman bitch","story_timestamp":1565264099,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264551-RU0Fn7zfxMj5duwDwCHPaQdxxa4FUXTU_$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C_1565264550_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264551-lNYFRtfaLepy0XSNrpx2vkLTV9CNJN4g_$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C_1565264550_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0aa6e98cb2288f51412c","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","story_text":"I am batman bitch","story_timestamp":1565264550,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264554-kdRSyQg2TEI4BQvhTUQcvM5zxVumYW2t_$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW_1565264554_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264554-5SwenUcAfaU34VYQA7xvHutt055KCY8e_$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW_1565264554_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0aaae98cb2288f51412d","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","story_text":"I am batman bitch","story_timestamp":1565264554,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264588-VHkCcqXorSXrOtcTFzjSkQdHsEd8oHg7_$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG_1565264588_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264588-z98K5ML2t10TNQ7PlPYX8oQoRJpg2D8f_$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG_1565264588_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0acce98cb2288f51412e","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","story_text":"I am batman bitch","story_timestamp":1565264588,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264591-ftc9mNMcVvq2Ca9zrEOpw09obDhLBSPG_$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq_1565264591_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264591-eyP2FDcYdRYkMOjYsEdt0NU0N2YG49IU_$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq_1565264591_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0acfe98cb2288f51412f","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","story_text":"I am batman bitch","story_timestamp":1565264591,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264594-dgFKqLS8isBvDJW2z9U1fNqIoFNFtWIB_$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u_1565264593_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264594-7WZBSkyNK8I3pWKMcziTFiKAEkcARgSU_$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u_1565264593_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad1e98cb2288f514130","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","story_text":"I am batman bitch","story_timestamp":1565264593,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264596-GsyVFx3Y2AAUAFU50wLUlmOHz5f3tXir_$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy_1565264596_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264596-ZjNrE3slmRbWNp5I7lNudDvNXkvtgLh7_$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy_1565264596_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad4e98cb2288f514131","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","story_text":"I am batman bitch","story_timestamp":1565264596,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264598-whKmfrmswHdS0UhPmrPCaB8Vyye38oek_$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6_1565264598_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264599-pTRiR8p0A54NidAtm2EczVt3R8P6QOrT_$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6_1565264598_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad6e98cb2288f514132","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","story_text":"I am batman bitch","story_timestamp":1565264598,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264601-xNDVxkQEgd4KxKp4TiMTOO2hwZoqfNiR_$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN._1565264600_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264601-knurWXVmsVHrGZennaHe6qZiPO74zgqR_$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN._1565264600_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad8e98cb2288f514133","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN.","story_text":"I am batman bitch","story_timestamp":1565264600,"location_location":"New Delhi","__v":0}],"next_page":0}
     */

    @SerializedName("data")
    private DataBean data;

    protected GetUserStoriesResponse(Parcel in) {
        data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Creator<GetUserStoriesResponse> CREATOR = new Creator<GetUserStoriesResponse>() {
        @Override
        public GetUserStoriesResponse createFromParcel(Parcel in) {
            return new GetUserStoriesResponse(in);
        }

        @Override
        public GetUserStoriesResponse[] newArray(int size) {
            return new GetUserStoriesResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
    }

    public static class DataBean implements Parcelable{
        /**
         * message : users stories
         * stories : [{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c08e3e98cb2288f51412b","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","story_text":"I am batman bitch","story_timestamp":1565264099,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264551-RU0Fn7zfxMj5duwDwCHPaQdxxa4FUXTU_$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C_1565264550_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264551-lNYFRtfaLepy0XSNrpx2vkLTV9CNJN4g_$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C_1565264550_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0aa6e98cb2288f51412c","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","story_text":"I am batman bitch","story_timestamp":1565264550,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264554-kdRSyQg2TEI4BQvhTUQcvM5zxVumYW2t_$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW_1565264554_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264554-5SwenUcAfaU34VYQA7xvHutt055KCY8e_$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW_1565264554_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0aaae98cb2288f51412d","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","story_text":"I am batman bitch","story_timestamp":1565264554,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264588-VHkCcqXorSXrOtcTFzjSkQdHsEd8oHg7_$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG_1565264588_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264588-z98K5ML2t10TNQ7PlPYX8oQoRJpg2D8f_$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG_1565264588_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0acce98cb2288f51412e","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","story_text":"I am batman bitch","story_timestamp":1565264588,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264591-ftc9mNMcVvq2Ca9zrEOpw09obDhLBSPG_$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq_1565264591_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264591-eyP2FDcYdRYkMOjYsEdt0NU0N2YG49IU_$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq_1565264591_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0acfe98cb2288f51412f","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","story_text":"I am batman bitch","story_timestamp":1565264591,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264594-dgFKqLS8isBvDJW2z9U1fNqIoFNFtWIB_$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u_1565264593_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264594-7WZBSkyNK8I3pWKMcziTFiKAEkcARgSU_$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u_1565264593_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad1e98cb2288f514130","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","story_text":"I am batman bitch","story_timestamp":1565264593,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264596-GsyVFx3Y2AAUAFU50wLUlmOHz5f3tXir_$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy_1565264596_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264596-ZjNrE3slmRbWNp5I7lNudDvNXkvtgLh7_$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy_1565264596_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad4e98cb2288f514131","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","story_text":"I am batman bitch","story_timestamp":1565264596,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264598-whKmfrmswHdS0UhPmrPCaB8Vyye38oek_$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6_1565264598_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264599-pTRiR8p0A54NidAtm2EczVt3R8P6QOrT_$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6_1565264598_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad6e98cb2288f514132","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","story_text":"I am batman bitch","story_timestamp":1565264598,"location_location":"New Delhi","__v":0},{"story_image":["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264601-xNDVxkQEgd4KxKp4TiMTOO2hwZoqfNiR_$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN._1565264600_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264601-knurWXVmsVHrGZennaHe6qZiPO74zgqR_$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN._1565264600_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"],"_id":"5d4c0ad8e98cb2288f514133","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","stories_id":"$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN.","story_text":"I am batman bitch","story_timestamp":1565264600,"location_location":"New Delhi","__v":0}]
         * next_page : 0
         */

        @SerializedName("message")
        private String message;
        @SerializedName("next_page")
        private int next_page;
        @SerializedName("stories")
        private ArrayList<StoriesBean> stories;

        protected DataBean(Parcel in) {
            message = in.readString();
            next_page = in.readInt();
            stories = in.createTypedArrayList(StoriesBean.CREATOR);
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getNext_page() {
            return next_page;
        }

        public void setNext_page(int next_page) {
            this.next_page = next_page;
        }

        public ArrayList<StoriesBean> getStories() {
            return stories;
        }

        public void setStories(ArrayList<StoriesBean> stories) {
            this.stories = stories;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(message);
            dest.writeInt(next_page);
            dest.writeTypedList(stories);
        }

        public static class StoriesBean implements Parcelable {
            /**
             * story_image : ["https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-hkjxhyZc4V1dHBLwxZoyv1z4SOv2gWhA_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg","https://s3.us-east-2.amazonaws.com/pimpcarstories/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565264100-d8loC8cDLlCuGipgwnzPGGAdgGMjm40d_$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC_1565264099_22855eb3-fe40-5832-b54e-6f8042ba0f6d.jpg"]
             * _id : 5d4c08e3e98cb2288f51412b
             * uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
             * stories_id : $2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC
             * story_text : I am batman bitch
             * story_timestamp : 1565264099
             * location_location : New Delhi
             */

            @SerializedName("_id")
            private String _id;
            @SerializedName("uid")
            private String uid;
            @SerializedName("stories_id")
            private String stories_id;
            @SerializedName("story_text")
            private String story_text;
            @SerializedName("story_timestamp")
            private int story_timestamp;
            @SerializedName("location_location")
            private String location_location;
            @SerializedName("story_image")
            private ArrayList<String> story_image;

            protected StoriesBean(Parcel in) {
                _id = in.readString();
                uid = in.readString();
                stories_id = in.readString();
                story_text = in.readString();
                story_timestamp = in.readInt();
                location_location = in.readString();
                story_image = in.createStringArrayList();
            }

            public static final Creator<StoriesBean> CREATOR = new Creator<StoriesBean>() {
                @Override
                public StoriesBean createFromParcel(Parcel in) {
                    return new StoriesBean(in);
                }

                @Override
                public StoriesBean[] newArray(int size) {
                    return new StoriesBean[size];
                }
            };

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getStories_id() {
                return stories_id;
            }

            public void setStories_id(String stories_id) {
                this.stories_id = stories_id;
            }

            public String getStory_text() {
                return story_text;
            }

            public void setStory_text(String story_text) {
                this.story_text = story_text;
            }

            public int getStory_timestamp() {
                return story_timestamp;
            }

            public void setStory_timestamp(int story_timestamp) {
                this.story_timestamp = story_timestamp;
            }

            public String getLocation_location() {
                return location_location;
            }

            public void setLocation_location(String location_location) {
                this.location_location = location_location;
            }

            public ArrayList<String> getStory_image() {
                return story_image;
            }

            public void setStory_image(ArrayList<String> story_image) {
                this.story_image = story_image;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(_id);
                dest.writeString(uid);
                dest.writeString(stories_id);
                dest.writeString(story_text);
                dest.writeInt(story_timestamp);
                dest.writeString(location_location);
                dest.writeStringList(story_image);
            }
        }
    }
}

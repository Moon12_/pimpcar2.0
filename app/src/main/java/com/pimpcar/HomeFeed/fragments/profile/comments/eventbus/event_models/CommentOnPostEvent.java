package com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models;

public class CommentOnPostEvent {

    private String post_id;
    private String comment;


    public CommentOnPostEvent(String postid, String cmnt) {
        this.post_id = postid;
        this.comment = cmnt;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}

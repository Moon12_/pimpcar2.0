package com.pimpcar.HomeFeed.fragments.home.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllPostImageCommentsModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("user_id")
    private String user_id;
   @SerializedName("comment_timestamp")
    private String comment_timestamp;
   @SerializedName("comment_likes")
    private List<AllPostImageCommentsLikeModel> allPostImageCommentsLikeModels;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment_timestamp() {
        return comment_timestamp;
    }

    public void setComment_timestamp(String comment_timestamp) {
        this.comment_timestamp = comment_timestamp;
    }


    public List<AllPostImageCommentsLikeModel> getAllPostImageCommentsLikeModels() {
        return allPostImageCommentsLikeModels;
    }

    public void setAllPostImageCommentsLikeModels(List<AllPostImageCommentsLikeModel> allPostImageCommentsLikeModels) {
        this.allPostImageCommentsLikeModels = allPostImageCommentsLikeModels;
    }
}

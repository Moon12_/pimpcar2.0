//package com.pimpcar.HomeFeed.fragments.notification;
//
//import android.graphics.Canvas;
//import android.os.Build;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.RelativeLayout;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.widget.AppCompatImageView;
//import androidx.appcompat.widget.AppCompatTextView;
//import androidx.fragment.app.Fragment;
//import androidx.recyclerview.widget.ItemTouchHelper;
//import androidx.recyclerview.widget.LinearLayoutManager;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
//import com.pimpcar.HomeFeed.fragments.notification.adapters.NotificationAdapter;
//import com.pimpcar.HomeFeed.fragments.notification.models.NotificationRequestBody;
//import com.pimpcar.HomeFeed.fragments.notification.models.NotificationResponseBody;
//import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.FollowUser;
//import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.UnfollowUser;
//import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
//import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
//import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
//import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
//import com.pimpcar.Network.ApiClient;
//import com.pimpcar.Network.ApiService;
//import com.pimpcar.R;
//import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
//import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
//import com.pimpcar.Widgets.progressbar.KProgressHUD;
//import com.pimpcar.utils.Constants;
//import com.pimpcar.utils.HelperMethods;
//import com.pimpcar.utils.Utils;
//
//import org.greenrobot.eventbus.EventBus;
//import org.greenrobot.eventbus.Subscribe;
//
//import java.io.IOException;
//import java.util.List;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.disposables.CompositeDisposable;
//import io.reactivex.observers.DisposableSingleObserver;
//import io.reactivex.schedulers.Schedulers;
//import okhttp3.ResponseBody;
//
//public class NotificationFragment11 extends Fragment {
//
//
//    @BindView(R.id.notification_RV)
//    RecyclerView notification_RV;
//    @BindView(R.id.drawer_layout_open_close_button)
//    AppCompatImageView drawer_layout_open_close_button;
//    @BindView(R.id.search_nothing_result)
//    RelativeLayout search_nothing_result;
//    @BindView(R.id.search_ic_empty)
//    AppCompatImageView search_ic_empty;
//    @BindView(R.id.search_result_foundanything_tv)
//    AppCompatTextView search_result_foundanything_tv;
//    private Unbinder unbinder;
//    private ApiService apiService;
//    private CompositeDisposable disposable = new CompositeDisposable();
//    private int first_next_page;
//    private int page_number = 0;
//    private NotificationAdapter notificationAdapter;
//    private List<NotificationResponseBody.DataBean.NotificationBean> all_notification;
//
//    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
//            new InfiniteAdapter.OnLoadMoreListener() {
//                @Override
//                public void onLoadMore() {
//                    NotificationRequestBody notificationRequestBody = HelperMethods.create_request_body_for_notification(getPage_number());
//                    if (disposable != null && apiService != null) {
//                        disposable.add(apiService.getUserNotification(HelperMethods.get_user_jwt_key(), notificationRequestBody)
//                                .subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribeWith(new DisposableSingleObserver<NotificationResponseBody>() {
//                                    @Override
//                                    public void onSuccess(NotificationResponseBody s) {
//
//                                        if (s.getData().getNotification() != null && s.getData().getNotification().size() > 0) {
//                                            for (int i = 0; i < s.getData().getNotification().size(); i++) {
//                                                all_notification.add(s.getData().getNotification().get(i));
//                                                notificationAdapter.notifyItemInserted(all_notification.size() - 1);
//                                            }
//                                            notificationAdapter.moreDataLoaded(all_notification.size(), s.getData().getNotification().size());
//
//
//                                            if (s.getData().getNext_page() == 1) {
//                                                first_next_page = first_next_page + 1;
//                                                notificationAdapter.setShouldLoadMore(true);
//                                            } else {
//                                                notificationAdapter.setShouldLoadMore(false);
//                                            }
//
//                                        } else {
//                                            notificationAdapter.setShouldLoadMore(false);
//                                        }
//                                    }
//
//                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                                    @Override
//                                    public void onError(Throwable e) {
//                                        if (e instanceof HttpException) {
//                                            ResponseBody body = ((HttpException) e).response().errorBody();
//                                            try {
//                                                String error_body = body.string();
//                                                String error = Utils.get_error_from_response(error_body);
//                                            } catch (IOException e1) {
//                                                e1.printStackTrace();
//                                            }
//                                        }
//                                    }
//                                }));
//
//
//                    }
//                }
//            };
//    private KProgressHUD hud;
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (getActivity() != null) {
//            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
//                @Override
//                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
//                    return false;
//                }
//
//                @Override
//                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
//                    // Row is swiped from recycler view
//                    // remove it from adapter
//                }
//
//                @Override
//                public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
//                    // view the background view
//                }
//            };
//
//// attaching the touch helper to recycler view
//            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(notification_RV);
//            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
//            get_all_user_notification();
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        if (!EventBus.getDefault().isRegistered(this)) {
//            EventBus.getDefault().register(this);
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View root_view = inflater.inflate(R.layout.notification_layout, container, false);
//        unbinder = ButterKnife.bind(this, root_view);
//
//        return root_view;
//    }
//
//    private void get_all_user_notification() {
//
//        show_progress_dialog();
//
//        NotificationRequestBody notificationRequestBody = HelperMethods.create_request_body_for_notification(getPage_number());
//        disposable.add(apiService.getUserNotification(HelperMethods.get_user_jwt_key(), notificationRequestBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<NotificationResponseBody>() {
//                    @Override
//                    public void onSuccess(NotificationResponseBody s) {
//                        dismiss_progress_dialog();
//                        if (s != null && s.getData().getNotification() != null && s.getData().getNotification().size() > 0) {
//
//                            search_nothing_result.setVisibility(View.GONE);
//                            search_ic_empty.setVisibility(View.GONE);
//                            search_result_foundanything_tv.setVisibility(View.GONE);
//                            notification_RV.setVisibility(View.VISIBLE);
//
//                            all_notification = s.getData().getNotification();
//
//                            notificationAdapter = new NotificationAdapter(getActivity(), s.getData().getNotification());
//                            notification_RV.setLayoutManager(new LinearLayoutManager(getActivity()));
//                            notification_RV.setAdapter(notificationAdapter);
//
//                            notificationAdapter.setOnLoadMoreListener(onLoadMoreListener);
//
//                            if (s.getData().getNext_page() == 1) {
//                                increase_page_number();
//                                notificationAdapter.setShouldLoadMore(true);
//                            } else {
//                                notificationAdapter.setShouldLoadMore(false);
//                            }
//                        } else {
//
//                            notification_RV.setVisibility(View.GONE);
//
//                            search_nothing_result.setVisibility(View.VISIBLE);
//                            search_ic_empty.setVisibility(View.VISIBLE);
//                            search_result_foundanything_tv.setVisibility(View.VISIBLE);
//
//
////                            HelperMethods.showToastbar(getActivity(), s.getData().getMessage().toUpperCase());
//                        }
//                    }
//
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onError(Throwable e) {
//                        dismiss_progress_dialog();
//                        if (e instanceof HttpException) {
//                            ResponseBody body = ((HttpException) e).response().errorBody();
//                            try {
//                                String error_body = body.string();
//                                String error = Utils.get_error_from_response(error_body);
//
//                                notification_RV.setVisibility(View.GONE);
//
//                                search_nothing_result.setVisibility(View.VISIBLE);
//                                search_ic_empty.setVisibility(View.VISIBLE);
//                                search_result_foundanything_tv.setVisibility(View.VISIBLE);
//
////                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
//                            } catch (IOException e1) {
//                                e1.printStackTrace();
//                            }
//                        }
//                    }
//                }));
//    }
//
//    private void increase_page_number() {
//        first_next_page = first_next_page + 1;
//    }
//
//    @Subscribe
//    public void follow_user(FollowUser followUser) {
//        emit_unfollow_user_event(followUser.getUser_id_to_follow());
//    }
//
//    @Subscribe
//    public void unfollow_user(UnfollowUser unfollowUser) {
//
//        emit_unfollow_user_event(unfollowUser.getUser_id_to_follow());
//    }
//
//    private void emit_unfollow_user_event(String user_id) {
//        show_progress_dialog();
//        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
//        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
//                    @Override
//                    public void onSuccess(UnFollowUserResponseModel s) {
//                        dismiss_progress_dialog();
//                        //get_user_profile(getUser_id());
//                    }
//
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onError(Throwable e) {
//                        dismiss_progress_dialog();
//                        if (e instanceof HttpException) {
//                            ResponseBody body = ((HttpException) e).response().errorBody();
//                            try {
//                                String error_body = body.string();
//                                String error = Utils.get_error_from_response(error_body);
//                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
//                            } catch (IOException e1) {
//                                e1.printStackTrace();
//                            }
//                        }
//                    }
//                }));
//    }
//
//    private void follow_event(String user_id) {
//        show_progress_dialog();
//        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
//        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
//                    @Override
//                    public void onSuccess(FollowUserResponseModel s) {
//                        dismiss_progress_dialog();
//                        // get_user_profile(getUser_id());
//                    }
//
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onError(Throwable e) {
//                        dismiss_progress_dialog();
//                        if (e instanceof HttpException) {
//                            ResponseBody body = ((HttpException) e).response().errorBody();
//                            try {
//                                String error_body = body.string();
//                                String error = Utils.get_error_from_response(error_body);
//                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
//                            } catch (IOException e1) {
//                                e1.printStackTrace();
//                            }
//                        }
//                    }
//                }));
//    }
//
//    public void show_progress_dialog() {
//        hud = KProgressHUD.create(getActivity())
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();
//    }
//
//    public void dismiss_progress_dialog() {
//        if (hud != null && hud.isShowing())
//            hud.dismiss();
//    }
//
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        if (EventBus.getDefault().isRegistered(this))
//            EventBus.getDefault().unregister(this);
//    }
//
//    public int getPage_number() {
//        return page_number;
//    }
//
//    public void setPage_number(int page_number) {
//        this.page_number = page_number;
//    }
//}

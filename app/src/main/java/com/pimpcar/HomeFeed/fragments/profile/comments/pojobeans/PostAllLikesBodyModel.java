package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PostAllLikesBodyModel {
    /**
     * data : {"attributes":{"post_id":"RZEJX2MwH1564054176241","page_no":1}}
     */

    public PostAllLikesBodyModel() {

    }

    @SerializedName("data")
    private DataBean data;


    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }


    public static class DataBean {
        /**
         * attributes : {"post_id":"RZEJX2MwH1564054176241","page_no":1}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * post_id : RZEJX2MwH1564054176241
             * page_no : 1
             */

            public AttributesBean() {

            }

            @SerializedName("post_id")
            private String post_id;
            @SerializedName("page_no")
            private int page_no;


            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public int getPage_no() {
                return page_no;
            }

            public void setPage_no(int page_no) {
                this.page_no = page_no;
            }


        }
    }
}

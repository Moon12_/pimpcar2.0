package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.UserStoriesViewHolder;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesResponse;
import com.pimpcar.HomeFeed.fragments.profile.stories.view.StoryModel;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.profile_pager_fragments.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Utils;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class UserStoriesAdapter extends InfiniteAdapter<UserStoriesViewHolder> {


    private Context _mContext;
    private ArrayList<GetUserStoriesResponse.DataBean.StoriesBean> storiesBeans;
    private LayoutInflater _mInfiniteLayoutInfilator, _mInfiniteLayoutItemInfilator;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private UserStoriesViewHolder feedViewHolder;

    public UserStoriesAdapter(Context context, ArrayList<GetUserStoriesResponse.DataBean.StoriesBean> stories) {

        this._mContext = context;
        this.storiesBeans = stories;
        _mInfiniteLayoutInfilator = LayoutInflater.from(_mContext);
        _mInfiniteLayoutItemInfilator = LayoutInflater.from(_mContext);

    }


    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mInfiniteLayoutInfilator.inflate(R.layout.infinite_loading_nothing, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (storiesBeans != null && storiesBeans.size() > 0)
            return storiesBeans.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public UserStoriesViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mInfiniteLayoutItemInfilator.inflate(R.layout.user_stories_layout, parent, false);
        ButterKnife.bind(this, itemView);
        feedViewHolder = new UserStoriesViewHolder(itemView);
        return feedViewHolder;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {

        } else {
            GetUserStoriesResponse.DataBean.StoriesBean storiesBean = storiesBeans.get(position);
            feedViewHolder.user_stories_caption.setText(storiesBean.getStory_text() != null && !storiesBean.getStory_text().isEmpty() ? storiesBean.getStory_text() : "");

            if (storiesBean.getStory_image() != null && storiesBean.getStory_image().size() > 0) {
                ArrayList<StoryModel> uris = new ArrayList<>();
                for (int i = 0; i < storiesBean.getStory_image().size(); i++) {
                    uris.add(new StoryModel(storiesBean.getStory_image().get(i), storiesBean.getStory_text(), Utils.toDuration(storiesBean.getStory_timestamp())));
                }
                feedViewHolder.user_stories_portfolio.setImageUris(uris);
            }

        }
        super.onBindViewHolder(holder, position);


    }
}

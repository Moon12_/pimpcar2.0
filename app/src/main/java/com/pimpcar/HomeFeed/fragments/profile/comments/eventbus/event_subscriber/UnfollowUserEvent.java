package com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_subscriber;

public class UnfollowUserEvent {
    private String user_id;
    private int position;

    public UnfollowUserEvent(String user_id, int position) {
        this.user_id = user_id;
        this.position = position;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}

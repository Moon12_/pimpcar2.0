package com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.Fragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.FollowerFollowing.adapter.UserFollowingAdapter;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountResponse;
import com.pimpcar.HomeFeed.fragments.search.Interface.IFragmentListener;
import com.pimpcar.HomeFeed.fragments.search.Interface.ISearch;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowFragment extends Fragment implements ISearch {
    private static final String ARG_SEARCHTERM = "user_id";
    @BindView(R.id.user_followers_recyclerview)
    RecyclerView user_followers_recyclerview;
    @BindView(R.id.progressBar3)
    ProgressBar progressBar;
    private IFragmentListener mIFragmentListener = null;
    private String mSearchTerm = null;
    private Unbinder unbinder;
    private UserFollowingAdapter userFollowsAdapter;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int first_next_page;
    private ArrayList<FollowingAccountResponse.DataBean.FollowAccount> postsBeans;
    private ApiService apiService;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    FollowingAccountRequestBody followedAccountRequestBody = HelperMethods.following_list(first_next_page, "follows");
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getFollowingList(HelperMethods.get_user_jwt_key(), followedAccountRequestBody)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<FollowingAccountResponse>() {
                                    @Override
                                    public void onSuccess(FollowingAccountResponse s) {
                                        Log.d("checkingData", " " + s.getData().followed_accounts.size());
                                        if (s.getData().getFollowed_accounts() != null && s.getData().getFollowed_accounts().size() > 0) {
                                            for (int i = 0; i < s.getData().getFollowed_accounts().size(); i++) {
                                                postsBeans.add(s.getData().getFollowed_accounts().get(i));
                                                userFollowsAdapter.notifyItemInserted(postsBeans.size() - 1);
                                            }

                                            userFollowsAdapter.moreDataLoaded(postsBeans.size(), s.getData().getFollowed_accounts().size());
//
//
                                            if (s.getData().getNext_page() == 1) {
                                                first_next_page = first_next_page + 1;
                                                userFollowsAdapter.setShouldLoadMore(true);
                                            } else {
                                                userFollowsAdapter.setShouldLoadMore(false);
                                            }

                                        } else {
                                            userFollowsAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };

    public static FollowFragment newInstance(String user_id) {

        FollowFragment fragment = new FollowFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SEARCHTERM, user_id);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_follow, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            mSearchTerm = (String) getArguments().get(ARG_SEARCHTERM);
        }
        unbinder = ButterKnife.bind(this, view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mSearchTerm) {
            onTextQuery(mSearchTerm);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
            get_first_page_data_for_Following_Frag();
        }
    }

    private void get_first_page_data_for_Following_Frag() {
        showProgress();
        FollowingAccountRequestBody followedAccountRequestBody = HelperMethods.following_list(first_next_page, "following");

        disposable.add(apiService.getFollowingList(HelperMethods.get_user_jwt_key(), followedAccountRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowingAccountResponse>() {
                    @Override
                    public void onSuccess(FollowingAccountResponse s) {
                        hideProgress();
                        Log.d("checkingVVV", " " + s.getData().followed_accounts.size());
                        if (s.getData().getFollowed_accounts() != null && s.getData().getFollowed_accounts().size() > 0) {
                            postsBeans = s.getData().getFollowed_accounts();

                            userFollowsAdapter = new UserFollowingAdapter(getContext(), postsBeans);
                            user_followers_recyclerview.setAdapter(userFollowsAdapter);
                            user_followers_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
                            userFollowsAdapter.setOnLoadMoreListener(onLoadMoreListener);
                            if (s.getData().getNext_page() == 1) {
                                increase_page_number();
                                userFollowsAdapter.setShouldLoadMore(true);
                            } else {
                                userFollowsAdapter.setShouldLoadMore(false);
                            }
                        }

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        hideProgress();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @Override
    public void onTextQuery(String text) {

        Log.d("checkingFollowFragment", " " + text);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mIFragmentListener = (IFragmentListener) context;
        mIFragmentListener.addiSearch(FollowFragment.this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (null != mIFragmentListener)
            mIFragmentListener.removeISearch(FollowFragment.this);
    }

    private void increase_page_number() {
        first_next_page = first_next_page + 1;
    }

    private void showProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }

    }

    private void hideProgress() {
        if (progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }
}

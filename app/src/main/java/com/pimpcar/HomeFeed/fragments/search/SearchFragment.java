package com.pimpcar.HomeFeed.fragments.search;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.FollowUser;
import com.pimpcar.HomeFeed.fragments.notification.models.eventbus.UnfollowUser;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.search.adapter.SearchCategoryAdapter;
import com.pimpcar.HomeFeed.fragments.search.adapter.SearchFamoustAdapter;
import com.pimpcar.HomeFeed.fragments.search.adapter.SpannableGridLayoutManager;
import com.pimpcar.HomeFeed.fragments.search.models.CategoryPOJO;
import com.pimpcar.HomeFeed.pojos.SearchFamousRequestBody;
import com.pimpcar.HomeFeed.pojos.SearchFamousResponseBody;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class SearchFragment extends Fragment implements View.OnClickListener {


    @BindView(R.id.asymmetric_gridview_search_famous_result)
    RecyclerView asymmetric_gridview_search_famous_result;
    @BindView(R.id.all)
    RelativeLayout all;
    @BindView(R.id.pictures)
    RelativeLayout picture;
    @BindView(R.id.video)
    RelativeLayout video;
    @BindView(R.id.textView4)
    TextView textView4;
    SearchCategoryAdapter searchCategoryAdapter;
    private Unbinder unbinder;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private List<CategoryPOJO> CategoryPOJOList = new ArrayList<>();
    private int next_page_number = 0;
    private SearchFamoustAdapter searchFamoustAdapter;
    private List<SearchFamousResponseBody.DataBean.PostsBean> all_data_post_anything;
    // also update next page number in fragment
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    SearchFamousRequestBody searchFamousRequestBody = HelperMethods.create_famous_search_match(next_page_number);
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getSearchFamousPost(HelperMethods.get_user_jwt_key(), searchFamousRequestBody)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<SearchFamousResponseBody>() {
                                    @Override
                                    public void onSuccess(SearchFamousResponseBody s) {


                                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {
                                            for (int i = 0; i < s.getData().getPosts().size(); i++) {
                                                all_data_post_anything.add(s.getData().getPosts().get(i));
                                                searchFamoustAdapter.notifyItemChanged(all_data_post_anything.size() - 1);
                                            }
                                            searchFamoustAdapter.moreDataLoaded(all_data_post_anything.size(), s.getData().getPosts().size());

                                            if (s.getData().getNext_page() == 1) {
                                                next_page_number = next_page_number + 1;
                                                searchFamoustAdapter.setShouldLoadMore(true);
                                            } else {
                                                searchFamoustAdapter.setShouldLoadMore(false);
                                            }


                                        } else {
                                            searchFamoustAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));

                    }
                }
            };

    @Override
    public void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.search_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);

        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        search_famous_gridview();

        prepareData();
        textView4.setOnClickListener(this);

        all.setOnClickListener(this);
        picture.setOnClickListener(this);
        video.setOnClickListener(this);
        showData("all");
        return root_view;
    }

    private void prepareData() {
        CategoryPOJO CategoryPOJO = new CategoryPOJO("All", R.drawable.wheel);
        CategoryPOJOList.add(CategoryPOJO);

        CategoryPOJO = new CategoryPOJO("Pictures", R.drawable.wheel);
        CategoryPOJOList.add(CategoryPOJO);

        CategoryPOJO = new CategoryPOJO("Video", R.drawable.wheel);
        CategoryPOJOList.add(CategoryPOJO);
    }

    private void search_famous_gridview() {
        SearchFamousRequestBody searchFamousRequestBody = HelperMethods.create_famous_search_match(getNext_page_number());


        disposable.add(apiService.getSearchFamousPost(HelperMethods.get_user_jwt_key(), searchFamousRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SearchFamousResponseBody>() {
                    @Override
                    public void onSuccess(SearchFamousResponseBody s) {
                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {

                            if (asymmetric_gridview_search_famous_result != null) {
                                all_data_post_anything = s.getData().getPosts();

                                SpannableGridLayoutManager gridLayoutManager = new
                                        SpannableGridLayoutManager(new SpannableGridLayoutManager.GridSpanLookup() {
                                    @Override
                                    public SpannableGridLayoutManager.SpanInfo getSpanInfo(int position) {
                                        if (position % 12 == 0 || position % 12 == 7) {
                                            return new SpannableGridLayoutManager.SpanInfo(2, 2);
                                        } else {
                                            return new SpannableGridLayoutManager.SpanInfo(1, 1);
                                        }
                                    }
                                }, 3, 1.3f); // 3 is the number of coloumn , how nay to display is 1f

                                asymmetric_gridview_search_famous_result.setLayoutManager(gridLayoutManager);
                                searchFamoustAdapter = new SearchFamoustAdapter(getActivity(), all_data_post_anything, 3);

                                asymmetric_gridview_search_famous_result.setAdapter(searchFamoustAdapter);
                                // set Horizontal Orientation
                                searchFamoustAdapter.setOnLoadMoreListener(onLoadMoreListener);
                                if (s.getData().getNext_page() == 1) {
                                    increase_page_number();
                                    searchFamoustAdapter.setShouldLoadMore(true);
                                } else {
                                    searchFamoustAdapter.setShouldLoadMore(false);
                                }
                            }

                        } else {

                        }
                    }
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                                Timber.e(e1.getMessage());
                            }
                        }
                    }
                }));

    }

    private void increase_page_number() {
        next_page_number = next_page_number + 1;
    }

    public int getNext_page_number() {
        return next_page_number;
    }

    public void setNext_page_number(int next_page_number) {
        this.next_page_number = next_page_number;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textView4: {
                startActivity(new Intent(getActivity(), InSearchActivity.class));
                break;
            }
            case R.id.all: {
                showData("all");
                break;
            }
            case R.id.pictures: {
                showData("picture");
                break;
            }
            case R.id.video: {
                showData("video");
                break;
            }
        }
    }

    private void showData(String video) {
        if (video.equals("all") || video.equals("picture")) {
            if (asymmetric_gridview_search_famous_result.getVisibility() == View.GONE) {
                asymmetric_gridview_search_famous_result.setVisibility(View.VISIBLE);
            }
        } else {
            if (asymmetric_gridview_search_famous_result.getVisibility() == View.VISIBLE) {
                asymmetric_gridview_search_famous_result.setVisibility(View.GONE);
            }
        }
    }


    @Subscribe
    public void follow_user(FollowUser followUser) {
        follow_event(followUser.getUser_id_to_follow());
    }

    @Subscribe
    public void unfollow_user(UnfollowUser unfollowUser) {

        emit_unfollow_user_event(unfollowUser.getUser_id_to_follow());
    }


    private void follow_event(String user_id) {
        show_progress_dialog();
        FollowUserRequestBodyModel followUserRequestBodyModel = HelperMethods.create_follow_user_req_body(user_id);
        disposable.add(apiService.followUser(HelperMethods.get_user_jwt_key(), followUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<FollowUserResponseModel>() {
                    @Override
                    public void onSuccess(FollowUserResponseModel s) {
                        dismiss_progress_dialog();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }


    private void emit_unfollow_user_event(String user_id) {
        show_progress_dialog();
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = HelperMethods.create_unfollow_user_req_body(user_id);
        disposable.add(apiService.unfollowUser(HelperMethods.get_user_jwt_key(), unfollowUserRequestBodyModel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnFollowUserResponseModel>() {
                    @Override
                    public void onSuccess(UnFollowUserResponseModel s) {
                        dismiss_progress_dialog();
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    private KProgressHUD hud;

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


}

package com.pimpcar.HomeFeed.fragments.search.SearchFragments;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.search.adapter.SearchUserProfileAdapter;
import com.pimpcar.HomeFeed.fragments.search.Interface.IFragmentListener;
import com.pimpcar.HomeFeed.fragments.search.Interface.ISearch;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_search.UserSearchRequestBody;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class TopFragment extends Fragment implements ISearch {
    private static final String ARG_SEARCHTERM = "search_term";
    @BindView(R.id.user_search_list)
    RecyclerView user_search_list;
    private String mSearchTerm = null;
    private IFragmentListener mIFragmentListener = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private SearchUserProfileAdapter searchUserAdapter;
    private Unbinder unbinder;



    public static TopFragment newInstance(String searchTerm) {
        TopFragment fragment = new TopFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SEARCHTERM, searchTerm);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top, container, false);
        unbinder = ButterKnife.bind(this, view);


        if (getArguments() != null) {
            mSearchTerm = (String) getArguments().get(ARG_SEARCHTERM);
        }


        return view;
    }


    private void search_user_text_regex(CharSequence s) {
        if (s.length() > 1) {

            String search_text_change = s.toString();
            UserSearchRequestBody userSearchRequestBody = Utils.create_user_search_body(search_text_change);
            disposable.add(apiService.getSearchUsers(HelperMethods.get_user_jwt_key(), userSearchRequestBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<UserSearchResponseBody>() {
                        @Override
                        public void onSuccess(UserSearchResponseBody s) {

                            if (s.getData().getSerached_data().size() > 0) {
                                user_search_list.setVisibility(View.VISIBLE);
                                searchUserAdapter = new SearchUserProfileAdapter(getActivity(), s.getData().getSerached_data());
                                user_search_list.setLayoutManager(new LinearLayoutManager(getActivity()));
                                user_search_list.setItemAnimator(new DefaultItemAnimator());
                                user_search_list.setAdapter(searchUserAdapter);
                            } else {
                                user_search_list.setVisibility(View.GONE);
                            }


                        }


                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onError(Throwable e) {

                            if (e instanceof HttpException) {
                                ResponseBody body = ((HttpException) e).response().errorBody();
                                try {
                                    String error_body = body.string();
                                    String error = Utils.get_error_from_response(error_body);
                                    user_search_list.setVisibility(View.GONE);
                                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }));


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != mSearchTerm) {
            onTextQuery(mSearchTerm);
        }
    }


    @Override
    public void onTextQuery(String text) {
        if (text.length() > 1) {
            search_user_text_regex(text);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mIFragmentListener = (IFragmentListener) context;
        mIFragmentListener.addiSearch(TopFragment.this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (null != mIFragmentListener)
            mIFragmentListener.removeISearch(TopFragment.this);
    }
}

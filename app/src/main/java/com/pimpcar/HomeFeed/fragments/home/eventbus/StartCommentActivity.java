package com.pimpcar.HomeFeed.fragments.home.eventbus;

import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;

public class StartCommentActivity {

    private HomeFeedResponse.DataBean.PostsBean postsBean;

    public StartCommentActivity(HomeFeedResponse.DataBean.PostsBean single_post_details) {
        this.postsBean = single_post_details;
    }

    public HomeFeedResponse.DataBean.PostsBean getPostsBean() {
        return postsBean;
    }

    public void setPostsBean(HomeFeedResponse.DataBean.PostsBean postsBean) {
        this.postsBean = postsBean;
    }
}

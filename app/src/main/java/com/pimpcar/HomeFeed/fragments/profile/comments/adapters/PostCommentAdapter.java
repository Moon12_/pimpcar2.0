package com.pimpcar.HomeFeed.fragments.profile.comments.adapters;

import android.content.Context;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.PostCommentWithUserTag;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.viewholder.PostCommentViewHolder;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import timber.log.Timber;

public class PostCommentAdapter extends InfiniteAdapter<PostCommentViewHolder> {


    private Context mContext;
    private List<PostAllCommentResponseModel.DataBean.CommentsBean> _mAllComments;
    private LayoutInflater _mLayoutInflater_item = null, _mLayoutInflater_loading = null;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private PostCommentViewHolder postCommentViewHolder;


    public PostCommentAdapter(Context context, List<PostAllCommentResponseModel.DataBean.CommentsBean> all_comments) {
        this.mContext = context;
        this._mAllComments = all_comments;
        if (mContext != null) {
            _mLayoutInflater_item = LayoutInflater.from(mContext);
            _mLayoutInflater_loading = LayoutInflater.from(mContext);
        }
    }


    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mLayoutInflater_loading.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mAllComments != null && _mAllComments.size() > 0)
            return _mAllComments.size();
        Timber.d("size of of the list" + _mAllComments.size());
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }

    @Override
    public PostCommentViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayoutInflater_item.inflate(R.layout.comment_item_list_layout, parent, false);
        ButterKnife.bind(this, itemView);
        postCommentViewHolder = new PostCommentViewHolder(itemView);
        return postCommentViewHolder;
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {

        } else {

            postCommentViewHolder = (PostCommentViewHolder) holder;
            PostAllCommentResponseModel.DataBean.CommentsBean commentsBean = _mAllComments.get(position);
            postCommentViewHolder.user_name_tv.setText(commentsBean.getUser_name());
            postCommentViewHolder.user_post_description.setText(commentsBean.getComment_statement());
            postCommentViewHolder.comment_time.setText(Utils.toDuration(Long.parseLong(commentsBean.getComment_timestamp())) + " ago");
            try {
                Picasso.with(mContext)
                        .load(commentsBean.getComment_user_profile_pic())
                        .fit()
                        .into(postCommentViewHolder.user_profile_pic_rounded);
            }catch (Exception e){
                e.printStackTrace();
            }



            postCommentViewHolder.comment_likes.setText(commentsBean.getComment_likes().size() + " Likes");

            postCommentViewHolder.comment_like_icon.setOnClickListener(v -> {
                if (!HelperMethods.is_user_logged_in()) {
                    Toasty.error(mContext, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();
                }

            });

            postCommentViewHolder.kcomment_reply.setOnClickListener(v -> {
                if (HelperMethods.is_user_logged_in()) {
                    EventBus.getDefault().post(new PostCommentWithUserTag(commentsBean.getUser_name()));
                } else {
                    Toasty.error(mContext, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();

                }

            });

            if (commentsBean != null && commentsBean.getComment_timestamp() != null) {
                postCommentViewHolder.comment_time.setText(HelperMethods.timeAgo(Long.parseLong(commentsBean.getComment_timestamp())) + "");
            }

        }

        super.onBindViewHolder(holder, position);
    }
}

package com.pimpcar.HomeFeed.fragments.home;


import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.clockbyte.admobadapter.AdmobRecyclerAdapterWrapper;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.ads.MobileAds;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.home.adapters.HomeFeedAdapter;
import com.pimpcar.HomeFeed.fragments.home.eventbus.OpenUserProfileEvent;
import com.pimpcar.HomeFeed.fragments.home.eventbus.StartCommentActivity;
import com.pimpcar.HomeFeed.fragments.home.eventbus.TakeUserToHisOwnProfileEvent;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedPictureResponse;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedRequestBody;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.profile.comments.activity.PostComment;
import com.pimpcar.HomeFeed.fragments.profile.comments.eventbus.event_models.LikeOnPostEvent;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.OtherUserProfileActivity;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostLikeDislikeRequestBody;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllPostFragment extends Fragment {
    @BindView(R.id.recycler_view_post_paging)
    RecyclerView recycler_view_post_paging;
    @BindView(R.id.shimmer_view_container)
    ShimmerFrameLayout mShimmerViewContainer;
    @BindView(R.id.swipe_to_refresh_data)
    SwipeRefreshLayout swipe_to_refresh_data;
    private AdmobRecyclerAdapterWrapper adapterWrapper;
    private Unbinder unbinder = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private int first_next_page;
    private ArrayList<HomeFeedResponse.DataBean.PostsBean> postsBeans;
    private ArrayList<HomeFeedResponse.DataBean.PostsBean> postsBeansPictures;
    private HomeFeedAdapter homeFeedAdapter = null;
    private int REQUEST_ACTIVITY_RESULT_CALL_FEED = 1234;
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    HomeFeedRequestBody homeFeedRequestBody = HelperMethods.create_home_feed_request_body(first_next_page);
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getHomeFeedPosts(homeFeedRequestBody)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<HomeFeedResponse>() {
                                    @Override
                                    public void onSuccess(HomeFeedResponse s) {
                                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {
                                            for (int i = 0; i < s.getData().getPosts().size() - 1; i++) {
                                                postsBeans.add(s.getData().getPosts().get(i));
                                                homeFeedAdapter.notifyItemInserted(postsBeans.size() - 1);
                                            }
                                            homeFeedAdapter.moreDataLoaded(postsBeans.size(), s.getData().getPosts().size());


                                            if (s.getData().getNext_page() == 1) {
                                                first_next_page = first_next_page + 1;
                                                homeFeedAdapter.setShouldLoadMore(true);
                                            } else {
                                                homeFeedAdapter.setShouldLoadMore(false);
                                            }

                                        } else {
                                            homeFeedAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);
                                            } catch (IOException e1) {
                                                e1.printStackTrace();
                                            }
                                        }
                                    }
                                }));


                    }
                }
            };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_all_post, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        //     MobileAds.initialize(getContext(), getString(R.string.every_three_button_key));
        setrefrest_layout();
        return root_view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
            get_first_page_data_for_home_feed();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        setFirst_next_page(0);
        super.onPause();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }


    private void get_first_page_data_for_home_feed() {
        HomeFeedRequestBody homeFeedRequestBody = HelperMethods.create_home_feed_request_body(first_next_page);
        postsBeansPictures = new ArrayList<>();
        disposable.add(apiService.getHomeFeedPosts(homeFeedRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<HomeFeedResponse>() {
                    @Override
                    public void onSuccess(HomeFeedResponse s) {
                        if (swipe_to_refresh_data != null && swipe_to_refresh_data.isRefreshing())
                            swipe_to_refresh_data.setRefreshing(false);
//                        Toast.makeText(getActivity(), "first" + s.getData().getPosts(), Toast.LENGTH_SHORT).show();

                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {

                            if (mShimmerViewContainer != null && recycler_view_post_paging != null) {
//                                Toast.makeText(getActivity(), "" + s.getData().getPosts(), Toast.LENGTH_SHORT).show();
                                mShimmerViewContainer.setVisibility(View.GONE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                recycler_view_post_paging.setVisibility(View.VISIBLE);


                                postsBeans = s.getData().getPosts();
                                for (int i = 0; i <= postsBeans.size() - 1; i++) {
                                    String type = getFileTypeFromURL(s.getData().getPosts().get(i).getPost_image_url());
                                    if (type.equals("photo")) {

                                        postsBeansPictures.add(postsBeans.get(i));

                                        Toast.makeText(getActivity(), "picture found"+postsBeansPictures, Toast.LENGTH_SHORT).show();
                                    } else {
                                        postsBeansPictures.add(postsBeans.get(i));
                                        Toast.makeText(getActivity(), "video found"+postsBeansPictures, Toast.LENGTH_SHORT).show();
                                    }

                                }
                                homeFeedAdapter = new HomeFeedAdapter(getActivity(), postsBeans);
//                                adapterWrapper = new AdmobRecyclerAdapterWrapper(getContext(), getString(R.string.every_three_button_key));
//
//
//                                adapterWrapper.setAdapter(homeFeedAdapter);
//                                adapterWrapper.setLimitOfAds(20);
//                                adapterWrapper.getContentAdsLayoutContext().setAdLayoutId(R.layout.ad_unified);
//                                adapterWrapper.getInstallAdsLayoutContext().setAdLayoutId(R.layout.ad_unified);
//                                adapterWrapper.setNoOfDataBetweenAds(4);
//                                adapterWrapper.setFirstAdIndex(2);
//                                adapterWrapper.setViewTypeBiggestSource(100);

                                recycler_view_post_paging.setAdapter(homeFeedAdapter);
                                recycler_view_post_paging.setLayoutManager(new LinearLayoutManager(getActivity()));
                                homeFeedAdapter.setOnLoadMoreListener(onLoadMoreListener);
                                if (s.getData().getNext_page() == 1) {
                                    increase_page_number();
                                    homeFeedAdapter.setShouldLoadMore(true);
                                } else {
                                    homeFeedAdapter.setShouldLoadMore(false);
                                }
                            } else {
//                                Toast.makeText(getActivity(), "shimming", Toast.LENGTH_SHORT).show();
                            }

                        } else {

                            if (mShimmerViewContainer != null && recycler_view_post_paging != null) {
//                                Toast.makeText(getActivity(), "shimming11", Toast.LENGTH_SHORT).show();

                                recycler_view_post_paging.setVisibility(View.GONE);
                                mShimmerViewContainer.setVisibility(View.VISIBLE);
                                mShimmerViewContainer.startShimmerAnimation();
                            }

                        }
                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(getActivity(), "throwable" + e.toString(), Toast.LENGTH_SHORT).show();

                        if (swipe_to_refresh_data != null && swipe_to_refresh_data.isRefreshing())
                            swipe_to_refresh_data.setRefreshing(false);

                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                Toast.makeText(getActivity(), "error" + error.toString(), Toast.LENGTH_SHORT).show();

                            } catch (IOException e1) {
                                e1.printStackTrace();
                                Toast.makeText(getActivity(), "e1" + e1, Toast.LENGTH_SHORT).show();

                            }
                        }
                    }
                }));
    }

    private void increase_page_number() {
        first_next_page = first_next_page + 1;
    }


    private void setrefrest_layout() {
        swipe_to_refresh_data.setOnRefreshListener(() -> {
            setFirst_next_page(0);
            get_first_page_data_for_home_feed();
        });
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }

        if (adapterWrapper != null) {
            adapterWrapper.destroyAds();
        }

        super.onDestroyView();

    }


    public int getFirst_next_page() {
        return first_next_page;
    }

    public void setFirst_next_page(int first_next_page) {
        this.first_next_page = first_next_page;
    }


    @Subscribe
    public void start_comment_activity(StartCommentActivity startCommentActivity) {
        Intent intent = new Intent(getActivity(), PostComment.class);
        intent.putExtra(Constants.COMMENTS_COMING_FROM, Constants.FEED_COMMENTS);
        intent.putExtra(Constants.SINGLE_POST_DATA, startCommentActivity.getPostsBean());
        startActivityForResult(intent, REQUEST_ACTIVITY_RESULT_CALL_FEED);
    }


    @Subscribe
    public void open_user_profile(OpenUserProfileEvent openUserProfileEvent) {


        if (openUserProfileEvent.getUser_id().equalsIgnoreCase(HelperMethods.get_user_id())) {
            EventBus.getDefault().post(new TakeUserToHisOwnProfileEvent());
        } else {
            Intent intent = new Intent(getActivity(), OtherUserProfileActivity.class);
            intent.putExtra(Constants.THIRD_USER_ID, openUserProfileEvent.getUser_id());
            startActivity(intent);
        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ACTIVITY_RESULT_CALL_FEED) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle mbundle = data.getExtras();
                String result_data = mbundle.getString("result");
                Timber.d("result data : " + result_data);
                if (result_data.contains("yes updated")) {
                    get_first_page_data_for_home_feed();
                }
            }
        }

    }


    @Subscribe
    public void likeDislikePost(LikeOnPostEvent postLike) {
        PostLikeDislikeRequestBody post_like_dislike_body = HelperMethods.create_post_like_dislike_body(postLike.getPost_id());
        disposable.add(apiService.postLikeDislikeOnPost(HelperMethods.get_user_jwt_key(), post_like_dislike_body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PostLikeDislikeResponse>() {
                    @Override
                    public void onSuccess(PostLikeDislikeResponse s) {
                        Timber.d("post like dislike done message " + s.getData().getMessage());

                    }

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                }

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    private String getFileTypeFromURL(String url) {
        String[] splitedArray = url.split("\\.");
        String lastValueOfArray = splitedArray[splitedArray.length - 1];
        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
            return "video";
        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
            return "audio";
        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
            return "photo";
        } else {
            return "";
        }
    }
}

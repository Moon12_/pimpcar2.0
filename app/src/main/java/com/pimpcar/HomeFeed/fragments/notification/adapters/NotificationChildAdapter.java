package com.pimpcar.HomeFeed.fragments.notification.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsReplyLikesModel;
import com.pimpcar.models.comments.models.CommentsReplyTextModel;
import com.pimpcar.models.comments.models.FollowUnFollowResponseModel;
import com.pimpcar.models.comments.models.GetFollowerFOLLOWINGResponseModel;
import com.pimpcar.models.comments.models.UserAllFollowersModel;
import com.pimpcar.models.comments.models.UserAllFollowingModel;
import com.pimpcar.models.notifications.NotificationDataaaModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotificationChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context ctx;

    private List<NotificationDataaaModel> childData;
    private List<NotificationDataaaModel> childDataBk;
    KProgressHUD hud;
    String sizeOfLikes = "", user_token = "";
    List<UserAllFollowersModel> userAllFollowersModels;
    List<UserAllFollowingModel> userAllFollowingModels;

    public NotificationChildAdapter(Context ctx, List<NotificationDataaaModel> childData) {
        this.ctx = ctx;
        this.childData = childData;
        childDataBk = new ArrayList<>(childData);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView tvExpandCollapseToggle;
        RelativeLayout rl;
        CircleImageView profile_image;
        AppCompatTextView user_name, user_action_text, user_follow_button, dateTime;

        public ViewHolder(View itemView) {
            super(itemView);
            profile_image = itemView.findViewById(R.id.not_person_pic);
            user_name = itemView.findViewById(R.id.user_name);
            tvExpandCollapseToggle = itemView.findViewById(R.id.iv_expand_collapse_toggle);
            user_action_text = itemView.findViewById(R.id.user_action_text);
            user_follow_button = itemView.findViewById(R.id.user_follow_button);
            rl = itemView.findViewById(R.id.rl);
            dateTime = itemView.findViewById(R.id.dateTime);
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item_layout, parent, false);

        ViewHolder cavh = new ViewHolder(itemLayoutView);
        return cavh;
    }


    final Handler handler = new Handler();
    Runnable collapseList = new Runnable() {
        @Override
        public void run() {
            if (getItemCount() > 1) {
                childData.remove(1);
                notifyDataSetChanged();
                handler.postDelayed(collapseList, 50);
            }
        }
    };

    Runnable expandList = new Runnable() {
        @Override
        public void run() {
            int currSize = childData.size();
            if (currSize == childDataBk.size()) return;

            if (currSize == 0) {
                childData.add(childDataBk.get(currSize));
            } else {
                childData.add(childDataBk.get(currSize));
            }
            notifyDataSetChanged();

            handler.postDelayed(expandList, 50);
        }
    };


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        NotificationDataaaModel p = childData.get(position);

        user_token = Utilities.getString(ctx, "user_token");
        userAllFollowersModels = new ArrayList<>();
        userAllFollowingModels = new ArrayList<>();

        if (position == 0 && getItemCount() == 1) {
            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_baseline_expand_more_24_white);
            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
        } else if (position == childDataBk.size() - 1) {
            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_baseline_expand_less_24_white);
            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
        } else {
            vh.tvExpandCollapseToggle.setVisibility(View.GONE);
        }

        vh.user_name.setText(p.getUser_name());
        getAllFollowingFollwersApi(vh.user_follow_button, p.getUser_id());
//        Toast.makeText(ctx, "" + p.getNotification_description().toString(), Toast.LENGTH_SHORT).show();
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,yyyy");
//        String dateString = formatter.format(new Date(Long.parseLong(p.getDate_time())));
        String MyFinalValue = covertTimeToText(p.getDate_time());


        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("HH:mm:ss");

        Date d = null;
        try {
            d = input.parse(p.getDate_time());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formatted = output.format(d);
        Log.i("DATE", "" + formatted);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            Date dt = sdf.parse(formatted);

            SimpleDateFormat sdfs = new SimpleDateFormat("hh:mm a");
            String formatedTime = sdfs.format(dt);
            vh.dateTime.setText(formatedTime);
            Log.v("parseTime", formatedTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (p.getNotification_of().equals("comment_like") && childData.size() > 1) {
            vh.user_action_text.setText(p.getNotification_description() + " & " + String.valueOf(childData.size() - 1 + " other liked your post"));
        } else if (p.getNotification_of().equals("comment_like")) {
            vh.rl.setVisibility(View.VISIBLE);
        } else {
            vh.user_action_text.setText(p.getNotification_description());

        }

        if (!TextUtils.isEmpty(p.getProfile_pic())) {
            Picasso.with(ctx).load(p.getProfile_pic()).placeholder(R.drawable.profile_icon).into(vh.profile_image);

        } else {
//            Picasso.with(ctx).load(R.drawable.user_profile_place_holder).placeholder(R.drawable.profile_icon).into(vh.profile_image);

        }
//        vh.profile_image.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(ctx, OtherUserProfileActivity.class);
//                i.putExtra(Constants.THIRD_USER_ID, p.getUser_id());
//                ctx.startActivity(i);
//            }
//        });
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItemCount() > 1) {
                    handler.post(collapseList);
                } else {
                    handler.post(expandList);
                }
            }
        });

        vh.user_follow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                followUnFollowApi(HelperMethods.get_underScoreId(), vh.user_follow_button, p.get_id_user_id());

            }
        });

    }

    @Override
    public int getItemCount() {
        return childData.size();
    }

    public String covertTimeToText(String dataDate) {

        String convTime = null;

        String prefix = "";
        String suffix = "Ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date pasTime = dateFormat.parse(dataDate);

            Date nowTime = new Date();

            long dateDiff = nowTime.getTime() - pasTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(dateDiff);
            long minute = TimeUnit.MILLISECONDS.toMinutes(dateDiff);
            long hour = TimeUnit.MILLISECONDS.toHours(dateDiff);
            long day = TimeUnit.MILLISECONDS.toDays(dateDiff);

            if (second < 60) {
                convTime = second + " Sec " + suffix;
            } else if (minute < 60) {
                convTime = minute + " Min " + suffix;
            } else if (hour < 24) {
                convTime = hour + " Hrs " + suffix;
            } else if (day >= 7) {
                if (day > 360) {
                    convTime = (day / 360) + " Yrs " + suffix;
                } else if (day > 30) {
                    convTime = (day / 30) + " Months " + suffix;
                } else {
                    convTime = (day / 7) + " Week " + suffix;
                }
            } else if (day < 7) {
                convTime = day + " d " + suffix;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e("ConvTimeE", e.getMessage());
        }

        return convTime;
    }

    private void followUnFollowApi(String followuserId, TextView folowUnfOLLOW, String postUnderScoreid) {
        hud = KProgressHUD.create(ctx)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(70, 70);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", postUnderScoreid);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<FollowUnFollowResponseModel> call = getResponse.followUnfollowApi(followuserId, jsonObject);
        call.enqueue(new retrofit2.Callback<FollowUnFollowResponseModel>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<FollowUnFollowResponseModel> call, Response<FollowUnFollowResponseModel> response) {
                int code = response.code();
                if (code == 200) {
                    assert response.body() != null;
                    if (response.body().getMessage().equals("User followed")) {
                        folowUnfOLLOW.setText("Following");
                        hud.dismiss();
                    } else {
                        folowUnfOLLOW.setText("Follow Back");
                        hud.dismiss();


                    }
                    Toast.makeText(ctx, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    //                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();

                } else {
                    hud.dismiss();
                    Toast.makeText(ctx, "not successfull response", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getAllFollowingFollwersApi(TextView tvFollow, String user_post_id) {
//        hud = KProgressHUD.create(context)
//                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
//        hud.setSize(50, 50);
//        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path
        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<GetFollowerFOLLOWINGResponseModel> call = getResponse.get_follower_following(HelperMethods.get_underScoreId());
        call.enqueue(new retrofit2.Callback<GetFollowerFOLLOWINGResponseModel>() {
            @Override
            public void onResponse(Call<GetFollowerFOLLOWINGResponseModel> call, Response<GetFollowerFOLLOWINGResponseModel> response) {
                if (response.code() == 200) {
                    if (response.isSuccessful()) {
//                        hud.dismiss();
                        userAllFollowersModels = response.body().getUser().getUser_followers();
                        userAllFollowingModels = response.body().getUser().getUser_following();
//                        Toast.makeText(context, "found" + userAllFollowersModels, Toast.LENGTH_SHORT).show();

                        if (userAllFollowingModels.size() == 0) {
                            tvFollow.setText("Follow Back");
                        }

                        for (int i = 0; i <= userAllFollowingModels.size() - 1; i++) {
                            String user_iddd = userAllFollowingModels.get(i).getUser_id();
                            if (userAllFollowingModels.size() > 0) {

                                if (user_iddd.equals(user_post_id)) {
                                    tvFollow.setText("Following");
                                }
                            }


                        }
                    } else {
//                        hud.dismiss();

                        Toast.makeText(ctx, "not found", Toast.LENGTH_SHORT).show();
                    }


                } else {
//                    hud.dismiss();
                    Toast.makeText(ctx, "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
//                hud.dismiss();
                Log.d("gttt", call.toString());
                String tt = t.toString();
                Toast.makeText(ctx, "something went wrong" + tt, Toast.LENGTH_SHORT).show();

            }
        });
    }
}
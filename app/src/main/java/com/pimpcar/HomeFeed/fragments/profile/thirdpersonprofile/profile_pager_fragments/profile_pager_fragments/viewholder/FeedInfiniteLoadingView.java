package com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.profile_pager_fragments.profile_pager_fragments.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.SpinView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedInfiniteLoadingView extends RecyclerView.ViewHolder {

    @BindView(R.id.progress_bar)
    SpinView progress_bar;


    public FeedInfiniteLoadingView(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

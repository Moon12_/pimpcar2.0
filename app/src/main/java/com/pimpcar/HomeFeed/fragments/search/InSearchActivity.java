package com.pimpcar.HomeFeed.fragments.search;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.pimpcar.HomeFeed.fragments.search.Interface.IDataCallback;
import com.pimpcar.HomeFeed.fragments.search.Interface.IFragmentListener;
import com.pimpcar.HomeFeed.fragments.search.Interface.ISearch;
import com.pimpcar.HomeFeed.fragments.search.adapter.SearchAdapter;
import com.pimpcar.R;
import com.pimpcar.models.SearchDataModel;

import java.util.ArrayList;
import java.util.List;

public class InSearchActivity extends AppCompatActivity implements IFragmentListener, View.OnClickListener {
    TabLayout tabLayout;
    ViewPager viewPager;
    EditText search_editText;
    List<String> forsearch = new ArrayList<>();
    private String selection = "";
    String search_text = "";
    private static final int SPEECH_REQUEST_CODE = 0;
    ArrayList<ISearch> iSearch = new ArrayList<>();
    AutoCompleteTextView etSearch;
    ImageView ivClear, ivVoice, ivSearch;
    ArrayList<String> listData = null;

    IDataCallback iDataCallback = null;

    private PageAdapter adapter;
    private static final String[] COUNTRIES = new String[] {
            "Email", "Info", "Delete", "Dialer", "Alert" ,"Map", "Email", "Info", "Dialer", "Alert"
    };

    public void setiDataCallback(IDataCallback iDataCallback) {
        this.iDataCallback = iDataCallback;
        iDataCallback.onFragmentCreated(listData);
    }

    @Override
    public void addiSearch(ISearch iSearch) {
        this.iSearch.add(iSearch);
    }

    @Override
    public void removeISearch(ISearch iSearch) {
        this.iSearch.remove(iSearch);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insearch);

        listData = new ArrayList<>();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        ivClear = findViewById(R.id.ivClear);
        ivVoice = findViewById(R.id.ivVoice);
        ivSearch = findViewById(R.id.ivSearch);
        tabLayout.addTab(tabLayout.newTab().setText("Top"));
        tabLayout.addTab(tabLayout.newTab().setText("Accounts"));
        tabLayout.addTab(tabLayout.newTab().setText("Tag"));
        tabLayout.addTab(tabLayout.newTab().setText("Places"));


        ImageView back_button = findViewById(R.id.back_button);
        back_button.setOnClickListener(this);

        etSearch = findViewById(R.id.etSearch);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(InSearchActivity.this, android.R.layout.simple_list_item_1, COUNTRIES);
        etSearch.setAdapter(arrayAdapter);
        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
                ivClear.setVisibility(View.GONE);
            }
        });

        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long rowId) {
                selection = etSearch.getText().toString();
                selection = (String) parent.getItemAtPosition(position);

            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    selection = etSearch.getText().toString();
                    if (selection.equals("")) {

                        Toast.makeText(InSearchActivity.this, "Please Enter Something for Search", Toast.LENGTH_SHORT).show();
                    } else {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
                        ivClear.setVisibility(View.VISIBLE);
//                        getSearchApi(selection);
                    }

                    return true;
                }
                return false;
            }
        });
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selection = etSearch.getText().toString();
                if (!selection.equals("")) {
                    ivClear.setVisibility(View.VISIBLE);
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
//                    getSearchApi(selection);

                } else {
                    Toast.makeText(InSearchActivity.this, "Please Enter Something for Search", Toast.LENGTH_SHORT).show();
                }

            }
        });
        ivVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displaySpeechRecognizer();
//                getSearchApi(selection);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        break;
                    case 1:

                        break;
                    case 2:

                        break;
                }
                Toast.makeText(InSearchActivity.this, "" + tab.getText().toString(), Toast.LENGTH_SHORT).show();

                setData();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


//        adapter = new PageAdapter(getSupportFragmentManager(), search_text);
//        viewPager.setAdapter(adapter);

//        tabLayout.setupWithViewPager(viewPager);
        setData();
    }

    private void setData() {

        SearchDataModel[] searchDataModels = new SearchDataModel[]{
                new SearchDataModel("Email", android.R.drawable.ic_dialog_email),
                new SearchDataModel("Info", android.R.drawable.ic_dialog_info),
                new SearchDataModel("Delete", android.R.drawable.ic_delete),
                new SearchDataModel("Dialer", android.R.drawable.ic_dialog_dialer),
                new SearchDataModel("Alert", android.R.drawable.ic_dialog_alert),
                new SearchDataModel("Map", android.R.drawable.ic_dialog_map),
                new SearchDataModel("Email", android.R.drawable.ic_dialog_email),
                new SearchDataModel("Info", android.R.drawable.ic_dialog_info),
                new SearchDataModel("Delete", android.R.drawable.ic_delete),
                new SearchDataModel("Dialer", android.R.drawable.ic_dialog_dialer),
                new SearchDataModel("Alert", android.R.drawable.ic_dialog_alert),
                new SearchDataModel("Map", android.R.drawable.ic_dialog_map),
        };

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        SearchAdapter adapter = new SearchAdapter(searchDataModels);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_button: {
                onBackPressed();
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void displaySpeechRecognizer() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
// Start the activity, the intent will be populated with the speech text
        startActivityForResult(intent, SPEECH_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {
        if (requestCode == SPEECH_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
            etSearch.setText(spokenText);
            ivClear.setVisibility(View.VISIBLE);
//            getSearchApi(spokenText);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

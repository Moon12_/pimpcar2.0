package com.pimpcar.HomeFeed.fragments.search;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.pimpcar.HomeFeed.fragments.search.SearchFragments.AccountFragment;
import com.pimpcar.HomeFeed.fragments.search.SearchFragments.PlacesFragment;
import com.pimpcar.HomeFeed.fragments.search.SearchFragments.TagsFragment;
import com.pimpcar.HomeFeed.fragments.search.SearchFragments.TopFragment;

public class PageAdapter extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;
    private String mSearchTerm;

    //Constructor to the class
    public PageAdapter(FragmentManager fm, String searchTerm) {
        super(fm);
        //Initializing tab count
        this.mSearchTerm = searchTerm;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 3:
                PlacesFragment tab4 = PlacesFragment.newInstance(mSearchTerm);
                return tab4;
            case 2:
                TagsFragment tab3 = TagsFragment.newInstance(mSearchTerm);
                return tab3;
            case 1:
                AccountFragment tab2 = AccountFragment.newInstance(mSearchTerm);
                return tab2;
            case 0:
                TopFragment tab1 = TopFragment.newInstance(mSearchTerm);
                return tab1;
            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Top";
        } else if (position == 1) {
            title = "Accounts";
        } else if (position == 2) {
            title = "Tags";
        } else if (position == 3) {
            title = "Places";
        }
        return title;
    }

    public void setTextQueryChanged(String newText) {
        mSearchTerm = newText;
    }
}
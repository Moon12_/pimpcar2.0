package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;


import com.google.gson.annotations.SerializedName;

public class FollowUserResponseModel {

    /**
     * data : {"message":"user follow updated","data":{"updated_data":{"_id":"5d5bc8c28c3813258751c1b2","n_id":"hlLk_s1iM1566296258531","timestamp":1566296258,"notification_action":"Now Following you","user_caused_notification_name":"Zack Snyder","user_caused_notification_uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_caused_notification_profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_id":"","post_pic":"","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","__v":0}}}
     */

    @SerializedName("data")
    private DataBeanX data;

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * message : user follow updated
         * data : {"updated_data":{"_id":"5d5bc8c28c3813258751c1b2","n_id":"hlLk_s1iM1566296258531","timestamp":1566296258,"notification_action":"Now Following you","user_caused_notification_name":"Zack Snyder","user_caused_notification_uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_caused_notification_profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_id":"","post_pic":"","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","__v":0}}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("data")
        private DataBean data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * updated_data : {"_id":"5d5bc8c28c3813258751c1b2","n_id":"hlLk_s1iM1566296258531","timestamp":1566296258,"notification_action":"Now Following you","user_caused_notification_name":"Zack Snyder","user_caused_notification_uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_caused_notification_profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_id":"","post_pic":"","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","__v":0}
             */

            @SerializedName("updated_data")
            private UpdatedDataBean updated_data;

            public UpdatedDataBean getUpdated_data() {
                return updated_data;
            }

            public void setUpdated_data(UpdatedDataBean updated_data) {
                this.updated_data = updated_data;
            }

            public static class UpdatedDataBean {
                /**
                 * _id : 5d5bc8c28c3813258751c1b2
                 * n_id : hlLk_s1iM1566296258531
                 * timestamp : 1566296258
                 * notification_action : Now Following you
                 * user_caused_notification_name : Zack Snyder
                 * user_caused_notification_uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * user_caused_notification_profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1565958853-LBffv9NdUGyJ9cqbRR94ruLF8sXoxk9v_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                 * post_id :
                 * post_pic :
                 * user_id : 3d248ae7-91c3-5d6e-898a-665435049dad
                 * __v : 0
                 */

                @SerializedName("_id")
                private String _id;
                @SerializedName("n_id")
                private String n_id;
                @SerializedName("timestamp")
                private int timestamp;
                @SerializedName("notification_action")
                private String notification_action;
                @SerializedName("user_caused_notification_name")
                private String user_caused_notification_name;
                @SerializedName("user_caused_notification_uid")
                private String user_caused_notification_uid;
                @SerializedName("user_caused_notification_profile_pic")
                private String user_caused_notification_profile_pic;
                @SerializedName("post_id")
                private String post_id;
                @SerializedName("post_pic")
                private String post_pic;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("__v")
                private int __v;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getN_id() {
                    return n_id;
                }

                public void setN_id(String n_id) {
                    this.n_id = n_id;
                }

                public int getTimestamp() {
                    return timestamp;
                }

                public void setTimestamp(int timestamp) {
                    this.timestamp = timestamp;
                }

                public String getNotification_action() {
                    return notification_action;
                }

                public void setNotification_action(String notification_action) {
                    this.notification_action = notification_action;
                }

                public String getUser_caused_notification_name() {
                    return user_caused_notification_name;
                }

                public void setUser_caused_notification_name(String user_caused_notification_name) {
                    this.user_caused_notification_name = user_caused_notification_name;
                }

                public String getUser_caused_notification_uid() {
                    return user_caused_notification_uid;
                }

                public void setUser_caused_notification_uid(String user_caused_notification_uid) {
                    this.user_caused_notification_uid = user_caused_notification_uid;
                }

                public String getUser_caused_notification_profile_pic() {
                    return user_caused_notification_profile_pic;
                }

                public void setUser_caused_notification_profile_pic(String user_caused_notification_profile_pic) {
                    this.user_caused_notification_profile_pic = user_caused_notification_profile_pic;
                }

                public String getPost_id() {
                    return post_id;
                }

                public void setPost_id(String post_id) {
                    this.post_id = post_id;
                }

                public String getPost_pic() {
                    return post_pic;
                }

                public void setPost_pic(String post_pic) {
                    this.post_pic = post_pic;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public int get__v() {
                    return __v;
                }

                public void set__v(int __v) {
                    this.__v = __v;
                }
            }
        }
    }
}

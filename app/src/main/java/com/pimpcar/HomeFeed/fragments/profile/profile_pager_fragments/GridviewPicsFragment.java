package com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.adapters.UserPicsGridViewAdapter;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.posts.UserPostsBodyRequest;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class GridviewPicsFragment extends Fragment {

    @BindView(R.id.recycler_view_grid_view)
    RecyclerView recycler_view_grid_view;
    @BindView(R.id.user_post_nothing_tv)
    AppCompatTextView user_post_nothing_tv;
    private Unbinder unbinder;
    private ArrayList<UserPostBodyResponse.DataBean.PostsBean> user_post_data;

    private UserPicsGridViewAdapter userPicsGridViewAdapter;
    private int first_next_page;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();

    // also update next page number in fragment
    private InfiniteAdapter.OnLoadMoreListener onLoadMoreListener =
            new InfiniteAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    UserPostsBodyRequest userPostsBodyRequest = HelperMethods.create_user_profile_post_get(first_next_page);
                    if (disposable != null && apiService != null) {
                        disposable.add(apiService.getUserPosts(HelperMethods.get_user_jwt_key(), userPostsBodyRequest)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeWith(new DisposableSingleObserver<UserPostBodyResponse>() {
                                    @Override
                                    public void onSuccess(UserPostBodyResponse s) {
                                        if (s.getData().getPosts() != null && s.getData().getPosts().size() > 0) {
                                            for (int i = 0; i < s.getData().getPosts().size(); i++) {
                                                user_post_data.add(s.getData().getPosts().get(i));
                                                userPicsGridViewAdapter.notifyItemChanged(user_post_data.size() - 1);
                                            }
                                            userPicsGridViewAdapter.moreDataLoaded(user_post_data.size(), s.getData().getPosts().size());

                                            if (s.getData().getNext_page() == 1) {
                                                first_next_page = first_next_page + 1;
                                                userPicsGridViewAdapter.setShouldLoadMore(true);
                                            } else {
                                                userPicsGridViewAdapter.setShouldLoadMore(false);
                                            }


                                        } else {
                                            userPicsGridViewAdapter.setShouldLoadMore(false);
                                        }
                                    }

                                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                                    @Override
                                    public void onError(Throwable e) {
                                        Toast.makeText(getActivity(), "Throwable e"+e.toString(), Toast.LENGTH_SHORT).show();
                                        if (e instanceof HttpException) {
                                            ResponseBody body = ((HttpException) e).response().errorBody();
                                            try {
                                                String error_body = body.string();
                                                String error = Utils.get_error_from_response(error_body);
                                                Toast.makeText(getActivity(), " error"+error_body.toString(), Toast.LENGTH_SHORT).show();

                                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {

                                                }
                                                Toast.makeText(getActivity(), " error55", Toast.LENGTH_SHORT).show();

                                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                            } catch (IOException e1) {
                                                e1.printStackTrace();

                                            }
                                        }
                                    }
                                }));

                    }
                }
            };

//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        if (getActivity() != null) {
//
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.user_pics_grid_layout, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        user_post_data = getArguments().getParcelableArrayList(Constants.USER_POST_LIST);
        first_next_page = getArguments().getInt(Constants.USER_NEXT_PAGE_DETAILS);

        Toast.makeText(getActivity(), " data"+user_post_data.toString(), Toast.LENGTH_SHORT).show();

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 3);
        userPicsGridViewAdapter = new UserPicsGridViewAdapter(getActivity(), user_post_data);
        recycler_view_grid_view.setLayoutManager(gridLayoutManager);
        recycler_view_grid_view.setAdapter(userPicsGridViewAdapter);
        userPicsGridViewAdapter.setOnLoadMoreListener(onLoadMoreListener);

        try {
            if (user_post_data != null) {
                if (user_post_data.size() == 0 || user_post_data == null) {
                    recycler_view_grid_view.setVisibility(View.GONE);
                    user_post_nothing_tv.setVisibility(View.VISIBLE);
                } else {
                    user_post_nothing_tv.setVisibility(View.GONE);
                    recycler_view_grid_view.setVisibility(View.VISIBLE);
                }

            }

            if (first_next_page == 0) {
                userPicsGridViewAdapter.setShouldLoadMore(false);
            } else {
                userPicsGridViewAdapter.setShouldLoadMore(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return root_view;
    }
}

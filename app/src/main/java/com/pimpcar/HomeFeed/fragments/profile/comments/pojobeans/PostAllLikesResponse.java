package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostAllLikesResponse {


    /**
     * data : {"message":"post likes","post_likes":[{"_id":"5d3eb3ea0b9de120544d940e","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_id":"oBMrgd6fZ1564389937251","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1564390378","__v":0},{"_id":"5d3eb4210b9de120544d9410","user_id":"27f56235-371b-5106-a169-4cb86ff78d3e","post_id":"oBMrgd6fZ1564389937251","user_name":"Shakti Pandit","user_profile_pic_url":"https://auraleewallace.files.wordpress.com/2014/11/batman.jpeg","like_timestamp":"1564390433","__v":0},{"_id":"5d3eb6090b9de120544d9414","user_id":"37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","post_id":"oBMrgd6fZ1564389937251","user_name":"Sidharth Kushwaha","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78/1564141161-Ypo7hGMF2mPEQEtL5c8EdNNjifXC3BrF_37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78_37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78_Sidharth.jpg","like_timestamp":"1564390921","__v":0},{"_id":"5d3eb6460b9de120544d9416","user_id":"e47662af-cd07-50ab-b41d-6911662a37f2","post_id":"oBMrgd6fZ1564389937251","user_name":"Sailesh Sharma","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/e47662af-cd07-50ab-b41d-6911662a37f2/1564141303-1OkIjW7fEFRKO8F7KBXhdYfifBi5DdkQ_e47662af-cd07-50ab-b41d-6911662a37f2_e47662af-cd07-50ab-b41d-6911662a37f2_Sailesh.jpg","like_timestamp":"1564390982","__v":0},{"_id":"5d3eb6650b9de120544d9418","user_id":"c016cb14-10c2-500b-aaf6-6e2a629de04d","post_id":"oBMrgd6fZ1564389937251","user_name":"Monika Fatty","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/c016cb14-10c2-500b-aaf6-6e2a629de04d/1564141368-jGJ5zPIlJBxQwv249tKwCJe3t2exsvkH_c016cb14-10c2-500b-aaf6-6e2a629de04d_c016cb14-10c2-500b-aaf6-6e2a629de04d_Monika.jpg","like_timestamp":"1564391013","__v":0},{"_id":"5d3eb6940b9de120544d941a","user_id":"fea89666-b728-52e7-84ae-3e95bf8ffd00","post_id":"oBMrgd6fZ1564389937251","user_name":"Vipul Baccha","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/fea89666-b728-52e7-84ae-3e95bf8ffd00/1564141565-E7CuFj7VAdAShc2RWKxTnCBfzmy5IE2U_fea89666-b728-52e7-84ae-3e95bf8ffd00_fea89666-b728-52e7-84ae-3e95bf8ffd00_Vipul.jpg","like_timestamp":"1564391060","__v":0},{"_id":"5d3eb6c00b9de120544d941c","user_id":"c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","post_id":"oBMrgd6fZ1564389937251","user_name":"Salim Sulemaan","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd/1564141605-ERIBnJos1hndWtB62tsbzFvMKaX98YSR_c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd_c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd_Salim.jpg","like_timestamp":"1564391104","__v":0},{"_id":"5d3eb6fc0b9de120544d941e","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_id":"oBMrgd6fZ1564389937251","user_name":"Henry Cavil","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/3d248ae7-91c3-5d6e-898a-665435049dad/1564141645-ausUdSPRWqBRS1qAiXSJytDBs8UNwJnl_3d248ae7-91c3-5d6e-898a-665435049dad_3d248ae7-91c3-5d6e-898a-665435049dad_Henry.jpg","like_timestamp":"1564391164","__v":0},{"_id":"5d3eb73b0b9de120544d9420","user_id":"fe70bb50-26d8-555c-b84e-85cdaa47e800","post_id":"oBMrgd6fZ1564389937251","user_name":"Ben Afleck","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/fe70bb50-26d8-555c-b84e-85cdaa47e800/1564141680-0ov4yHnXyyY9HteZ8aKyUEJTdQspGSdy_fe70bb50-26d8-555c-b84e-85cdaa47e800_fe70bb50-26d8-555c-b84e-85cdaa47e800_Ben.jpg","like_timestamp":"1564391227","__v":0},{"_id":"5d3eb7680b9de120544d9422","user_id":"d577f9e5-4ef9-518c-89b9-3be0dd5d4221","post_id":"oBMrgd6fZ1564389937251","user_name":"Clark Kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/d577f9e5-4ef9-518c-89b9-3be0dd5d4221/1564141778-x4i28MdMgfE8oaGCsKU7XAGJzE7p5fT4_d577f9e5-4ef9-518c-89b9-3be0dd5d4221_d577f9e5-4ef9-518c-89b9-3be0dd5d4221_Clark.jpg","like_timestamp":"1564391272","__v":0}],"next_page":1}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : post likes
         * post_likes : [{"_id":"5d3eb3ea0b9de120544d940e","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_id":"oBMrgd6fZ1564389937251","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1564390378","__v":0},{"_id":"5d3eb4210b9de120544d9410","user_id":"27f56235-371b-5106-a169-4cb86ff78d3e","post_id":"oBMrgd6fZ1564389937251","user_name":"Shakti Pandit","user_profile_pic_url":"https://auraleewallace.files.wordpress.com/2014/11/batman.jpeg","like_timestamp":"1564390433","__v":0},{"_id":"5d3eb6090b9de120544d9414","user_id":"37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","post_id":"oBMrgd6fZ1564389937251","user_name":"Sidharth Kushwaha","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78/1564141161-Ypo7hGMF2mPEQEtL5c8EdNNjifXC3BrF_37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78_37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78_Sidharth.jpg","like_timestamp":"1564390921","__v":0},{"_id":"5d3eb6460b9de120544d9416","user_id":"e47662af-cd07-50ab-b41d-6911662a37f2","post_id":"oBMrgd6fZ1564389937251","user_name":"Sailesh Sharma","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/e47662af-cd07-50ab-b41d-6911662a37f2/1564141303-1OkIjW7fEFRKO8F7KBXhdYfifBi5DdkQ_e47662af-cd07-50ab-b41d-6911662a37f2_e47662af-cd07-50ab-b41d-6911662a37f2_Sailesh.jpg","like_timestamp":"1564390982","__v":0},{"_id":"5d3eb6650b9de120544d9418","user_id":"c016cb14-10c2-500b-aaf6-6e2a629de04d","post_id":"oBMrgd6fZ1564389937251","user_name":"Monika Fatty","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/c016cb14-10c2-500b-aaf6-6e2a629de04d/1564141368-jGJ5zPIlJBxQwv249tKwCJe3t2exsvkH_c016cb14-10c2-500b-aaf6-6e2a629de04d_c016cb14-10c2-500b-aaf6-6e2a629de04d_Monika.jpg","like_timestamp":"1564391013","__v":0},{"_id":"5d3eb6940b9de120544d941a","user_id":"fea89666-b728-52e7-84ae-3e95bf8ffd00","post_id":"oBMrgd6fZ1564389937251","user_name":"Vipul Baccha","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/fea89666-b728-52e7-84ae-3e95bf8ffd00/1564141565-E7CuFj7VAdAShc2RWKxTnCBfzmy5IE2U_fea89666-b728-52e7-84ae-3e95bf8ffd00_fea89666-b728-52e7-84ae-3e95bf8ffd00_Vipul.jpg","like_timestamp":"1564391060","__v":0},{"_id":"5d3eb6c00b9de120544d941c","user_id":"c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","post_id":"oBMrgd6fZ1564389937251","user_name":"Salim Sulemaan","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd/1564141605-ERIBnJos1hndWtB62tsbzFvMKaX98YSR_c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd_c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd_Salim.jpg","like_timestamp":"1564391104","__v":0},{"_id":"5d3eb6fc0b9de120544d941e","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_id":"oBMrgd6fZ1564389937251","user_name":"Henry Cavil","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/3d248ae7-91c3-5d6e-898a-665435049dad/1564141645-ausUdSPRWqBRS1qAiXSJytDBs8UNwJnl_3d248ae7-91c3-5d6e-898a-665435049dad_3d248ae7-91c3-5d6e-898a-665435049dad_Henry.jpg","like_timestamp":"1564391164","__v":0},{"_id":"5d3eb73b0b9de120544d9420","user_id":"fe70bb50-26d8-555c-b84e-85cdaa47e800","post_id":"oBMrgd6fZ1564389937251","user_name":"Ben Afleck","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/fe70bb50-26d8-555c-b84e-85cdaa47e800/1564141680-0ov4yHnXyyY9HteZ8aKyUEJTdQspGSdy_fe70bb50-26d8-555c-b84e-85cdaa47e800_fe70bb50-26d8-555c-b84e-85cdaa47e800_Ben.jpg","like_timestamp":"1564391227","__v":0},{"_id":"5d3eb7680b9de120544d9422","user_id":"d577f9e5-4ef9-518c-89b9-3be0dd5d4221","post_id":"oBMrgd6fZ1564389937251","user_name":"Clark Kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/d577f9e5-4ef9-518c-89b9-3be0dd5d4221/1564141778-x4i28MdMgfE8oaGCsKU7XAGJzE7p5fT4_d577f9e5-4ef9-518c-89b9-3be0dd5d4221_d577f9e5-4ef9-518c-89b9-3be0dd5d4221_Clark.jpg","like_timestamp":"1564391272","__v":0}]
         * next_page : 1
         */

        @SerializedName("message")
        private String message;
        @SerializedName("next_page")
        private int next_page;
        @SerializedName("post_likes")
        private List<PostLikesBean> post_likes;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getNext_page() {
            return next_page;
        }

        public void setNext_page(int next_page) {
            this.next_page = next_page;
        }

        public List<PostLikesBean> getPost_likes() {
            return post_likes;
        }

        public void setPost_likes(List<PostLikesBean> post_likes) {
            this.post_likes = post_likes;
        }

        public static class PostLikesBean {
            /**
             * _id : 5d3eb3ea0b9de120544d940e
             * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
             * post_id : oBMrgd6fZ1564389937251
             * user_name : Zack Snyder
             * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
             * like_timestamp : 1564390378
             * __v : 0
             */

            @SerializedName("_id")
            private String _id;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("user_profile_pic_url")
            private String user_profile_pic_url;
            @SerializedName("like_timestamp")
            private String like_timestamp;
            @SerializedName("username")
            private String username;
            @SerializedName("__v")
            private int __v;
            private boolean is_follower = false;

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getUser_profile_pic_url() {
                return user_profile_pic_url;
            }

            public void setUser_profile_pic_url(String user_profile_pic_url) {
                this.user_profile_pic_url = user_profile_pic_url;
            }

            public String getLike_timestamp() {
                return like_timestamp;
            }

            public void setLike_timestamp(String like_timestamp) {
                this.like_timestamp = like_timestamp;
            }

            public int get__v() {
                return __v;
            }

            public void set__v(int __v) {
                this.__v = __v;
            }

            public boolean isIs_follower() {
                return is_follower;
            }

            public void setIs_follower(boolean is_follower) {
                this.is_follower = is_follower;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }
        }
    }
}

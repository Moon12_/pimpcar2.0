package com.pimpcar.HomeFeed.fragments.home.pojos;

import com.google.gson.annotations.SerializedName;

public class AllPostImageCommentsLikeModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("user_id")
    private String user_id;
   @SerializedName("timestamp")
    private String timestamp;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FollowingAccountResponse {

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("message")
        private String message;
        @SerializedName("next_page")
        private int next_page;
        @SerializedName("following_accounts")
        public ArrayList<FollowAccount> followed_accounts;

        @SerializedName("user_name")
        private String user_name;

        public String getUser_name() {
            return user_name;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getNext_page() {
            return next_page;
        }

        public void setNext_page(int next_page) {
            this.next_page = next_page;
        }

        public ArrayList<FollowAccount> getFollowed_accounts() {
            return followed_accounts;
        }

        public void setFollowed_accounts(ArrayList<FollowAccount> notification) {
            this.followed_accounts = notification;
        }

        public static class FollowAccount {

            @SerializedName("email")
            private EmailBean email;
            @SerializedName("profile_pic")
            private String profilePic;
            @SerializedName("_id")
            private String id;
            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("user_id")
            private String userId;

            public String getUserId() {
                return userId;
            }

            public void setUserId(String userId) {
                this.userId = userId;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public EmailBean getEmail() {
                return email;
            }

            public void setEmail(EmailBean email) {
                this.email = email;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public String getProfilePic() {
                return profilePic;
            }

            public void setProfilePic(String profilePic) {
                this.profilePic = profilePic;
            }

            public class EmailBean {

                @SerializedName("email_status")
                private Integer emailStatus;
                @SerializedName("email_id")
                private String emailId;

                public Integer getEmailStatus() {
                    return emailStatus;
                }

                public void setEmailStatus(Integer emailStatus) {
                    this.emailStatus = emailStatus;
                }

                public String getEmailId() {
                    return emailId;
                }

                public void setEmailId(String emailId) {
                    this.emailId = emailId;
                }
            }


        }
    }
}

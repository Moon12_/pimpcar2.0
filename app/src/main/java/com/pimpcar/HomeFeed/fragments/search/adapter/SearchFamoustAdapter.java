package com.pimpcar.HomeFeed.fragments.search.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaMetadataRetriever;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
import com.pimpcar.HomeFeed.fragments.search.models.SearchFamousAdapaterViewHolder;
import com.pimpcar.HomeFeed.pojos.SearchFamousResponseBody;
import com.pimpcar.Notification.NotificationDetailsPostActivity;
import com.pimpcar.R;
import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
import com.pimpcar.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

public class SearchFamoustAdapter extends InfiniteAdapter<SearchFamousAdapaterViewHolder> {

    private Context _mContext;
    private LayoutInflater _mInfinite_layout_inflater;
    private LayoutInflater _mLayout_item_inflater;
    private FeedInfiniteLoadingView viewHolderForLoadingView;
    private SearchFamousAdapaterViewHolder searchFamousAdapaterViewHolder;
    private List<SearchFamousResponseBody.DataBean.PostsBean> _mSearchFamousList;
    private int span_count;


    public SearchFamoustAdapter(Context context, List<SearchFamousResponseBody.DataBean.PostsBean> postsBeans, int spancount) {
        this._mContext = context;
        this._mSearchFamousList = postsBeans;
        this._mLayout_item_inflater = LayoutInflater.from(_mContext);
        this._mInfinite_layout_inflater = LayoutInflater.from(_mContext);
        this.span_count = spancount;

    }

    @Override
    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
        View itemView = _mInfinite_layout_inflater.inflate(R.layout.infinite_loading_view, parent, false);
        ButterKnife.bind(this, itemView);
        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
        return viewHolderForLoadingView;
    }

    @Override
    public int getCount() {
        if (_mSearchFamousList != null && _mSearchFamousList.size() > 0)
            return _mSearchFamousList.size();
        return 0;
    }

    @Override
    public int getViewType(int position) {
        return 1;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FeedInfiniteLoadingView) {


        } else {

            searchFamousAdapaterViewHolder = (SearchFamousAdapaterViewHolder) holder;

            SearchFamousResponseBody.DataBean.PostsBean single_post_details = _mSearchFamousList.get(position);
            if (single_post_details != null && single_post_details.getPost_image_url() != null && !single_post_details.getPost_image_url().isEmpty()) {

                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) _mContext).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;


                Transformation transformation = new Transformation() {

                    @Override
                    public Bitmap transform(Bitmap source) {


                        int targetWidth = width;

                        double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                        int targetHeight = (int) (targetWidth * aspectRatio);
                        Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                        if (result != source) {
                            // Same bitmap is returned if sizes are the same
                            source.recycle();
                        }
                        return result;
                    }

                    @Override
                    public String key() {
                        return "transformation" + " desiredWidth";
                    }
                };


                String type = getFileTypeFromURL(single_post_details.getPost_image_url());
                if (type.equals("video")) {
                    try {
//                        Bitmap bitmap=retriveVideoFrameFromVideo(single_post_details.getPost_image_url());
                        Toast.makeText(_mContext, "found", Toast.LENGTH_SHORT).show();
                        searchFamousAdapaterViewHolder.rlVideoItem.setVisibility(View.VISIBLE);

                        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
// Set video url as data source
                        retriever.setDataSource(single_post_details.getPost_image_url(), new HashMap<String, String>());
// Get frame at 2nd second as Bitmap image
                        Bitmap bitmap = retriever.getFrameAtTime(2000000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
//                        Toast.makeText(_mContext, "" + bitmap, Toast.LENGTH_SHORT).show();

// Display the Bitmap image in an ImageView
                        searchFamousAdapaterViewHolder.grid_view_image_view.setImageBitmap(bitmap);
//                        Glide.with(_mContext)
//                                .asBitmap()
//                                .load(bitmap)
//                                .diskCacheStrategy(DiskCacheStrategy.DATA)
//                                .into(homeFeedViewHolder.user_post_image);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                } else {
                    Picasso.with(_mContext)
                            .load(single_post_details.getPost_image_url())
                            .transform(transformation)
                            .into(searchFamousAdapaterViewHolder.grid_view_image_view, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {

                                }
                            });
                }


                searchFamousAdapaterViewHolder.itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(_mContext, NotificationDetailsPostActivity.class);
                    intent.putExtra(Constants.POST_ID_SENT, _mSearchFamousList.get(position).getPost_id());
                    intent.putExtra("USER_ID", _mSearchFamousList.get(position).getUser_id());
                    _mContext.startActivity(intent);
                });
            }
        }

        super.onBindViewHolder(holder, position);
    }

    @Override
    public SearchFamousAdapaterViewHolder onCreateView(ViewGroup parent, int viewType) {
        View itemView = _mLayout_item_inflater.inflate(R.layout.sing_data_grid_view, parent, false);
        ButterKnife.bind(this, itemView);

        searchFamousAdapaterViewHolder = new SearchFamousAdapaterViewHolder(itemView);
        return searchFamousAdapaterViewHolder;
    }

    private String getFileTypeFromURL(String url) {
        String[] splitedArray = url.split("\\.");
        String lastValueOfArray = splitedArray[splitedArray.length - 1];
        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
            return "video";
        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
            return "audio";
        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
            return "photo";
        } else {
            return "";
        }
    }
}

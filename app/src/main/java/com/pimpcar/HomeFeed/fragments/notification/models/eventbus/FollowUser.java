package com.pimpcar.HomeFeed.fragments.notification.models.eventbus;

public class FollowUser {


    private String user_id_to_follow;

    public FollowUser(String user_id) {
        this.user_id_to_follow = user_id;
    }

    public String getUser_id_to_follow() {
        return user_id_to_follow;
    }

    public void setUser_id_to_follow(String user_id_to_follow) {
        this.user_id_to_follow = user_id_to_follow;
    }
}

package com.pimpcar.HomeFeed.fragments.profile.pojos;

import com.google.gson.annotations.SerializedName;

public class ChangeUserStatusRequestBody {
    /**
     * data : {"attributes":{"user_status":"If I advane you follow me, If i Retreat kill me and if i die avenge me"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"user_status":"If I advane you follow me, If i Retreat kill me and if i die avenge me"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * user_status : If I advane you follow me, If i Retreat kill me and if i die avenge me
             */

            @SerializedName("user_status")
            private String user_status;

            public String getUser_status() {
                return user_status;
            }

            public void setUser_status(String user_status) {
                this.user_status = user_status;
            }
        }
    }
}

package com.pimpcar.HomeFeed.fragments.search.SearchFragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.R;

public class PlacesFragment extends Fragment {
    private static final String ARG_SEARCHTERM = "search_term";

    public static PlacesFragment newInstance(String searchTerm) {
        PlacesFragment fragment = new PlacesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SEARCHTERM, searchTerm);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_places, container, false);


        return view;
    }


}

package com.pimpcar.HomeFeed.fragments.search.Interface;

import java.util.ArrayList;

public interface IDataCallback {
    void onFragmentCreated(ArrayList<String> listData);
}

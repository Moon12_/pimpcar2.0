package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import com.google.gson.annotations.SerializedName;

public class FollowUserRequestBodyModel {

    /**
     * data : {"attributes":{"to_follow":"27f56235-371b-5106-a169-4cb86ff78d3e"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"to_follow":"27f56235-371b-5106-a169-4cb86ff78d3e"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * to_follow : 27f56235-371b-5106-a169-4cb86ff78d3e
             */

            @SerializedName("to_follow")
            private String to_follow;

            public String getTo_follow() {
                return to_follow;
            }

            public void setTo_follow(String to_follow) {
                this.to_follow = to_follow;
            }
        }
    }
}

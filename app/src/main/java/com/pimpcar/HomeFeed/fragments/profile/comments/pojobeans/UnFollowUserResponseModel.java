package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UnFollowUserResponseModel {

    /**
     * data : {"message":"user unfollow updated","data":{"updated_data":[{"email":{"email_status":1,"email_id":"wick@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","user_total_posts":2,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"_id":"5d397810bfc04f62e8393363","uid":"147b9033-7292-5d82-a50e-7927a7b7eafd","fname":"John","lname":"Wick","login_type":"email","__v":0,"hasshed_password":"$2a$13$fLUXQc7XIPM3fPHnRWSku.V0WOLoBTZhMNoECBxaNHlaA9i1A9Ckq","gender":"male"},{"email":{"email_status":1,"email_id":"shakti@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/27f56235-371b-5106-a169-4cb86ff78d3e/1564141041-vbd7jaoZ9c8qPY2tOEbZdL01BnNUif4z_27f56235-371b-5106-a169-4cb86ff78d3e_27f56235-371b-5106-a169-4cb86ff78d3e_Shakti.jpg","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"_id":"5d397babbfc04f62e8393364","uid":"27f56235-371b-5106-a169-4cb86ff78d3e","fname":"Shakti","lname":"Pandit","login_type":"email","__v":0,"hasshed_password":"$2a$13$8d9rcx63xJhEGq4t8lFDreZ7Ib6usYB8E8ZqoIOBV17WReLM3mG.C","gender":"male"}]}}
     */

    @SerializedName("data")
    private DataBeanX data;

    public DataBeanX getData() {
        return data;
    }

    public void setData(DataBeanX data) {
        this.data = data;
    }

    public static class DataBeanX {
        /**
         * message : user unfollow updated
         * data : {"updated_data":[{"email":{"email_status":1,"email_id":"wick@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","user_total_posts":2,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"_id":"5d397810bfc04f62e8393363","uid":"147b9033-7292-5d82-a50e-7927a7b7eafd","fname":"John","lname":"Wick","login_type":"email","__v":0,"hasshed_password":"$2a$13$fLUXQc7XIPM3fPHnRWSku.V0WOLoBTZhMNoECBxaNHlaA9i1A9Ckq","gender":"male"},{"email":{"email_status":1,"email_id":"shakti@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/27f56235-371b-5106-a169-4cb86ff78d3e/1564141041-vbd7jaoZ9c8qPY2tOEbZdL01BnNUif4z_27f56235-371b-5106-a169-4cb86ff78d3e_27f56235-371b-5106-a169-4cb86ff78d3e_Shakti.jpg","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"_id":"5d397babbfc04f62e8393364","uid":"27f56235-371b-5106-a169-4cb86ff78d3e","fname":"Shakti","lname":"Pandit","login_type":"email","__v":0,"hasshed_password":"$2a$13$8d9rcx63xJhEGq4t8lFDreZ7Ib6usYB8E8ZqoIOBV17WReLM3mG.C","gender":"male"}]}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("data")
        private DataBean data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            private List<UpdatedDataBean> updated_data;

            public List<UpdatedDataBean> getUpdated_data() {
                return updated_data;
            }

            public void setUpdated_data(List<UpdatedDataBean> updated_data) {
                this.updated_data = updated_data;
            }

            public static class UpdatedDataBean {
                /**
                 * email : {"email_status":1,"email_id":"wick@gmail.com"}
                 * contact_number : {"contact_no_status":-1}
                 * user_status : {"current_status":""}
                 * counts : {"failed_login":0}
                 * meta_data : {"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0}
                 * user_blocked_list : []
                 * user_reports : []
                 * profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg
                 * user_total_posts : 2
                 * user_total_followers : 0
                 * user_total_following : 0
                 * user_followers : []
                 * user_following : []
                 * _id : 5d397810bfc04f62e8393363
                 * uid : 147b9033-7292-5d82-a50e-7927a7b7eafd
                 * fname : John
                 * lname : Wick
                 * login_type : email
                 * __v : 0
                 * gender : male
                 */

                @SerializedName("email")
                private EmailBean email;
                @SerializedName("contact_number")
                private ContactNumberBean contact_number;
                @SerializedName("user_status")
                private UserStatusBean user_status;
                @SerializedName("counts")
                private CountsBean counts;
                @SerializedName("meta_data")
                private MetaDataBean meta_data;
                @SerializedName("profile_pic")
                private String profile_pic;
                @SerializedName("user_total_posts")
                private int user_total_posts;
                @SerializedName("user_total_followers")
                private int user_total_followers;
                @SerializedName("user_total_following")
                private int user_total_following;
                @SerializedName("_id")
                private String _id;
                @SerializedName("uid")
                private String uid;
                @SerializedName("fname")
                private String fname;
                @SerializedName("lname")
                private String lname;
                @SerializedName("login_type")
                private String login_type;
                @SerializedName("__v")
                private int __v;
                @SerializedName("gender")
                private String gender;
                private List<String> user_blocked_list;
                private List<String> user_reports;
                private List<String> user_followers;
                private List<String> user_following;

                public EmailBean getEmail() {
                    return email;
                }

                public void setEmail(EmailBean email) {
                    this.email = email;
                }

                public ContactNumberBean getContact_number() {
                    return contact_number;
                }

                public void setContact_number(ContactNumberBean contact_number) {
                    this.contact_number = contact_number;
                }

                public UserStatusBean getUser_status() {
                    return user_status;
                }

                public void setUser_status(UserStatusBean user_status) {
                    this.user_status = user_status;
                }

                public CountsBean getCounts() {
                    return counts;
                }

                public void setCounts(CountsBean counts) {
                    this.counts = counts;
                }

                public MetaDataBean getMeta_data() {
                    return meta_data;
                }

                public void setMeta_data(MetaDataBean meta_data) {
                    this.meta_data = meta_data;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }

                public int getUser_total_posts() {
                    return user_total_posts;
                }

                public void setUser_total_posts(int user_total_posts) {
                    this.user_total_posts = user_total_posts;
                }

                public int getUser_total_followers() {
                    return user_total_followers;
                }

                public void setUser_total_followers(int user_total_followers) {
                    this.user_total_followers = user_total_followers;
                }

                public int getUser_total_following() {
                    return user_total_following;
                }

                public void setUser_total_following(int user_total_following) {
                    this.user_total_following = user_total_following;
                }

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getFname() {
                    return fname;
                }

                public void setFname(String fname) {
                    this.fname = fname;
                }

                public String getLname() {
                    return lname;
                }

                public void setLname(String lname) {
                    this.lname = lname;
                }

                public String getLogin_type() {
                    return login_type;
                }

                public void setLogin_type(String login_type) {
                    this.login_type = login_type;
                }

                public int get__v() {
                    return __v;
                }

                public void set__v(int __v) {
                    this.__v = __v;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public List<String> getUser_blocked_list() {
                    return user_blocked_list;
                }

                public void setUser_blocked_list(List<String> user_blocked_list) {
                    this.user_blocked_list = user_blocked_list;
                }

                public List<String> getUser_reports() {
                    return user_reports;
                }

                public void setUser_reports(List<String> user_reports) {
                    this.user_reports = user_reports;
                }

                public List<String> getUser_followers() {
                    return user_followers;
                }

                public void setUser_followers(List<String> user_followers) {
                    this.user_followers = user_followers;
                }

                public List<String> getUser_following() {
                    return user_following;
                }

                public void setUser_following(List<String> user_following) {
                    this.user_following = user_following;
                }

                public static class EmailBean {
                    /**
                     * email_status : 1
                     * email_id : wick@gmail.com
                     */

                    @SerializedName("email_status")
                    private int email_status;
                    @SerializedName("email_id")
                    private String email_id;

                    public int getEmail_status() {
                        return email_status;
                    }

                    public void setEmail_status(int email_status) {
                        this.email_status = email_status;
                    }

                    public String getEmail_id() {
                        return email_id;
                    }

                    public void setEmail_id(String email_id) {
                        this.email_id = email_id;
                    }
                }

                public static class ContactNumberBean {
                    /**
                     * contact_no_status : -1
                     */

                    @SerializedName("contact_no_status")
                    private int contact_no_status;

                    public int getContact_no_status() {
                        return contact_no_status;
                    }

                    public void setContact_no_status(int contact_no_status) {
                        this.contact_no_status = contact_no_status;
                    }
                }

                public static class UserStatusBean {
                    /**
                     * current_status :
                     */

                    @SerializedName("current_status")
                    private String current_status;

                    public String getCurrent_status() {
                        return current_status;
                    }

                    public void setCurrent_status(String current_status) {
                        this.current_status = current_status;
                    }
                }

                public static class CountsBean {
                    /**
                     * failed_login : 0
                     */

                    @SerializedName("failed_login")
                    private int failed_login;

                    public int getFailed_login() {
                        return failed_login;
                    }

                    public void setFailed_login(int failed_login) {
                        this.failed_login = failed_login;
                    }
                }

                public static class MetaDataBean {
                    /**
                     * password_change_otp : 0
                     * contact_number_verification_otp : 0
                     * email_verification_otp : 0
                     */

                    @SerializedName("password_change_otp")
                    private int password_change_otp;
                    @SerializedName("contact_number_verification_otp")
                    private int contact_number_verification_otp;
                    @SerializedName("email_verification_otp")
                    private int email_verification_otp;

                    public int getPassword_change_otp() {
                        return password_change_otp;
                    }

                    public void setPassword_change_otp(int password_change_otp) {
                        this.password_change_otp = password_change_otp;
                    }

                    public int getContact_number_verification_otp() {
                        return contact_number_verification_otp;
                    }

                    public void setContact_number_verification_otp(int contact_number_verification_otp) {
                        this.contact_number_verification_otp = contact_number_verification_otp;
                    }

                    public int getEmail_verification_otp() {
                        return email_verification_otp;
                    }

                    public void setEmail_verification_otp(int email_verification_otp) {
                        this.email_verification_otp = email_verification_otp;
                    }
                }
            }
        }
    }
}

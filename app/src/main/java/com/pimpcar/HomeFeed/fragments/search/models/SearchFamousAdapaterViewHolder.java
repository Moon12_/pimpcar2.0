package com.pimpcar.HomeFeed.fragments.search.models;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.pimpcar.R;
import com.pimpcar.Widgets.ProportionalImageView;
import com.pimpcar.utils.HelperMethods;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFamousAdapaterViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.grid_view_image_view)
    public ProportionalImageView grid_view_image_view;

    @BindView(R.id.rlVideoItem)
    public RelativeLayout rlVideoItem;

    public SearchFamousAdapaterViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        GridLayoutManager.LayoutParams layoutParams = new
                GridLayoutManager.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        float margin = HelperMethods.dpToPx(2);
        layoutParams.setMargins((int) margin, (int) margin, (int) margin,
                (int) margin);
        itemView.setLayoutParams(layoutParams);
    }
}

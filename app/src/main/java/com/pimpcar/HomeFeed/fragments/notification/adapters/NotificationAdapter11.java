//package com.pimpcar.HomeFeed.fragments.notification.adapters;
//
//import android.content.Context;
//import android.content.Intent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.pimpcar.HomeFeed.fragments.notification.adapters.viewholders.NotificationViewHolder;
//import com.pimpcar.HomeFeed.fragments.notification.models.NotificationResponseBody;
//import com.pimpcar.HomeFeed.fragments.profile.profile_pager_fragments.viewholder.FeedInfiniteLoadingView;
//import com.pimpcar.Notification.NotificationDetailsPostActivity;
//import com.pimpcar.R;
//import com.pimpcar.Widgets.InfiniteAdapter.InfiniteAdapter;
//import com.pimpcar.utils.Constants;
//import com.squareup.picasso.Picasso;
//
//import java.util.List;
//
//import butterknife.ButterKnife;
//
//public class NotificationAdapter11 extends InfiniteAdapter<NotificationViewHolder> {
//
//
//    int pos = 0;
//    private Context _mContext;
//    private LayoutInflater layoutInflater;
//    private LayoutInflater infinite_layout_inflator;
//    private List<NotificationResponseBody.DataBean.NotificationBean> _mNotificationBeans;
//    private FeedInfiniteLoadingView viewHolderForLoadingView;
//    private NotificationViewHolder notificationViewHolder;
//
//    public NotificationAdapter11(Context context, List<NotificationResponseBody.DataBean.NotificationBean> notificationBeans) {
//
//        this._mContext = context;
//        this.layoutInflater = LayoutInflater.from(_mContext);
//        this.infinite_layout_inflator = LayoutInflater.from(_mContext);
//        this._mNotificationBeans = notificationBeans;
//
//
//    }
//
//
//    @Override
//    public RecyclerView.ViewHolder getLoadingViewHolder(ViewGroup parent) {
//        View itemView = infinite_layout_inflator.inflate(R.layout.infinite_loading_view, parent, false);
//        ButterKnife.bind(this, itemView);
//        viewHolderForLoadingView = new FeedInfiniteLoadingView(itemView);
//        return viewHolderForLoadingView;
//    }
//
//    @Override
//    public int getCount() {
//        return _mNotificationBeans.size();
//    }
//
//    @Override
//    public int getViewType(int position) {
//        return 1;
//    }
//
//    @Override
//    public NotificationViewHolder onCreateView(ViewGroup parent, int viewType) {
//        View itemView = layoutInflater.inflate(R.layout.notification_item_layout, parent, false);
//        ButterKnife.bind(this, itemView);
//        notificationViewHolder = new NotificationViewHolder(itemView, _mNotificationBeans, _mContext);
//        return notificationViewHolder;
//    }
//
//
//    @Override
//    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
//
//        if (holder instanceof FeedInfiniteLoadingView) {
//
//        } else {
//
//            notificationViewHolder = (NotificationViewHolder) holder;
//
//            boolean is_user_follow = _mNotificationBeans.get(position).isIs_user_already_follow();
//
//            String name = _mNotificationBeans.get(position).getUser_caused_notification_name() != null
//                    || !_mNotificationBeans.get(position).getUser_caused_notification_name().isEmpty() ?
//                    _mNotificationBeans.get(position).getUser_caused_notification_name() : " ";
//            notificationViewHolder.user_name.setText(name);
//
//
//            String user_action = _mNotificationBeans.get(position).getNotification_action() != null ||
//                    !_mNotificationBeans.get(position).getNotification_action().isEmpty() ?
//                    _mNotificationBeans.get(position).getNotification_action() : "";
//
//            notificationViewHolder.user_action_text.setText(user_action);
//
//            if (is_user_follow) {
//                notificationViewHolder.user_follow_button_rl.setVisibility(View.GONE);
//                notificationViewHolder.notification_post_image.setVisibility(View.VISIBLE);
//                Picasso.with(_mContext)
//                        .load(_mNotificationBeans.get(position).getPost_pic())
//                        .fit()
//                        .into(notificationViewHolder.notification_post_image);
//
//            } else {
//                notificationViewHolder.notification_post_image.setVisibility(View.GONE);
//                notificationViewHolder.user_follow_button_rl.setVisibility(View.VISIBLE);
//
//            }
//
//            if (_mNotificationBeans.get(position).getUser_caused_notification_profile_pic() != null ||
//                    !_mNotificationBeans.get(position).getUser_caused_notification_profile_pic().isEmpty()) {
//
//                Picasso.with(_mContext)
//                        .load(_mNotificationBeans.get(position).getUser_caused_notification_profile_pic())
//                        .fit()
//                        .into(notificationViewHolder.not_person_pic);
//            }
//
//            holder.itemView.setOnClickListener(v -> {
//                Intent intent = new Intent(_mContext, NotificationDetailsPostActivity.class);
//                intent.putExtra(Constants.POST_ID_SENT, _mNotificationBeans.get(position).getPost_id());
//                _mContext.startActivity(intent);
//
//            });
//
//
//        }
//
//        super.onBindViewHolder(holder, position);
//    }
//}

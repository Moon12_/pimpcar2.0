package com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CommentLikeDislikeBodyModel implements Parcelable {
    /**
     * data : {"attributes":{"comment_id":"cpZLL1T9V1563440154669"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected CommentLikeDislikeBodyModel(Parcel in) {
        data = in.readParcelable(DataBean.class.getClassLoader());
    }

    public static final Creator<CommentLikeDislikeBodyModel> CREATOR = new Creator<CommentLikeDislikeBodyModel>() {
        @Override
        public CommentLikeDislikeBodyModel createFromParcel(Parcel in) {
            return new CommentLikeDislikeBodyModel(in);
        }

        @Override
        public CommentLikeDislikeBodyModel[] newArray(int size) {
            return new CommentLikeDislikeBodyModel[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(data, flags);
    }

    public static class DataBean implements Parcelable {
        /**
         * attributes : {"comment_id":"cpZLL1T9V1563440154669"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        protected DataBean(Parcel in) {
        }

        public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel in) {
                return new DataBean(in);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
        }

        public static class AttributesBean {
            /**
             * comment_id : cpZLL1T9V1563440154669
             */

            @SerializedName("comment_id")
            private String comment_id;

            public String getComment_id() {
                return comment_id;
            }

            public void setComment_id(String comment_id) {
                this.comment_id = comment_id;
            }
        }
    }
}

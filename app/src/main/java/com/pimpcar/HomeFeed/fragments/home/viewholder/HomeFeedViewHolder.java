package com.pimpcar.HomeFeed.fragments.home.viewholder;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.pimpcar.HomeFeed.fragments.home.eventbus.LikeOnPostHomeEvent;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.R;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

//import com.google.android.gms.ads.AdView;

public class HomeFeedViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.user_profile_pic_rounded)
    public CircleImageView user_profile_pic_rounded;

    @BindView(R.id.user_name_tv)
    public AppCompatTextView user_name_tv;

    @BindView(R.id.user_location)
    public AppCompatTextView user_location;

    @BindView(R.id.three_vertical_dots)
    public AppCompatImageView three_vertical_dots;

    @BindView(R.id.user_post_image)
    public AppCompatImageView user_post_image;

    @BindView(R.id.post_description)
    public AppCompatTextView post_description;

    @BindView(R.id.post_time)
    public AppCompatTextView post_time;

    @BindView(R.id.post_like_button)
    public AppCompatImageView post_like_button;

    @BindView(R.id.post_comment)
    public AppCompatImageView post_comment;

    @BindView(R.id.post_number_of_likes)
    public AppCompatTextView post_number_of_likes;

    @BindView(R.id.user_comments_see)
    public AppCompatTextView user_comments_see;

    @BindView(R.id.first_person_image)
    public AppCompatImageView first_person_image;

    @BindView(R.id.second_person_dp)
    public AppCompatImageView second_person_dp;

    @BindView(R.id.third_person_dp)
    public AppCompatImageView third_person_dp;

    @BindView(R.id.fourth_person_dp)
    public AppCompatImageView fourth_person_dp;

    @BindView(R.id.comment_made_rl)
    public RelativeLayout comment_made_rl;

    @BindView(R.id.r1)
    public LinearLayout r1;

    public HomeFeedViewHolder(@NonNull View itemView, ArrayList<HomeFeedResponse.DataBean.PostsBean> _mPostsBeans, Context _mContext) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        post_like_button.setOnClickListener(v -> {


            if (HelperMethods.is_user_logged_in()) {
                EventBus.getDefault().post(new LikeOnPostHomeEvent(_mPostsBeans.get(getAdapterPosition()).getPost_id()));


                if (v.getTag().toString().contains("user_not_liked")) {
                    post_like_button.setTag("user_liked");
                    post_like_button.setBackgroundResource(R.drawable.liked_pressed);
                    post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                    post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                    ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
                    marginParams.setMargins(20, 20, 20, 20);
                    int post_like = _mPostsBeans.get(getAdapterPosition()).getPost_likes().size() + 1;
                    post_number_of_likes.setText(post_like + " likes");
                    if (post_like == 1) {
                        post_number_of_likes.setVisibility(View.VISIBLE);
                    }

                    make_view_likes_profile_visible(post_like, _mPostsBeans, _mContext);

                } else {
                    post_like_button.setTag("user_not_liked");
                    post_like_button.setBackgroundResource(R.drawable.like_unpressed);
                    post_like_button.getLayoutParams().height = Utils.dpToPx(25);
                    post_like_button.getLayoutParams().width = Utils.dpToPx(25);
                    ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) post_like_button.getLayoutParams();
                    marginParams.setMargins(20, 20, 20, 20);
                    int post_like = _mPostsBeans.get(getAdapterPosition()).getPost_likes().size() - 1;
                    post_number_of_likes.setText(post_like + " likes");

                    if (post_like == 0) {
                        post_number_of_likes.setVisibility(View.GONE);
                        first_person_image.setVisibility(View.GONE);
                    } else if (post_like == -1) {
                        post_number_of_likes.setVisibility(View.GONE);
                        first_person_image.setVisibility(View.GONE);
                    } else {
                        post_number_of_likes.setVisibility(View.VISIBLE);
                    }


                    make_view_likes_profile_visible(post_like, _mPostsBeans, _mContext);


                }
            } else {
                Toasty.error(_mContext, ResourcesUtil.getString(R.string.you_have_to_login), Toast.LENGTH_SHORT, true).show();

            }


        });


    }


    private void make_view_likes_profile_visible(int post_like, ArrayList<HomeFeedResponse.DataBean.PostsBean> mPostsBeans, Context mContext) {
        if (post_like == 3) {
            third_person_dp.setVisibility(View.VISIBLE);
            fourth_person_dp.setVisibility(View.GONE);
            if (HelperMethods.get_saved_user_profile_image() != null && !HelperMethods.get_saved_user_profile_image().isEmpty()) {
                Picasso.with(mContext)
                        .load(HelperMethods.get_saved_user_profile_image())
                        .fit()
                        .into(third_person_dp);
            }
        }

        if (post_like == 1) {
            first_person_image.setVisibility(View.VISIBLE);
            second_person_dp.setVisibility(View.GONE);
            third_person_dp.setVisibility(View.GONE);
            fourth_person_dp.setVisibility(View.GONE);

            if (HelperMethods.get_saved_user_profile_image() != null && !HelperMethods.get_saved_user_profile_image().isEmpty()) {
                Picasso.with(mContext)
                        .load(HelperMethods.get_saved_user_profile_image())
                        .fit()
                        .into(first_person_image);
            }


        }
        if (post_like == 2) {
            second_person_dp.setVisibility(View.VISIBLE);
            third_person_dp.setVisibility(View.GONE);
            fourth_person_dp.setVisibility(View.GONE);

            if (HelperMethods.get_saved_user_profile_image() != null && !HelperMethods.get_saved_user_profile_image().isEmpty()) {
                Picasso.with(mContext)
                        .load(HelperMethods.get_saved_user_profile_image())
                        .fit()
                        .into(second_person_dp);
            }

        }
    }
}

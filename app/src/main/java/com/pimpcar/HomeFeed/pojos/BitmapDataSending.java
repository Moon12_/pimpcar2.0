package com.pimpcar.HomeFeed.pojos;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class BitmapDataSending implements Parcelable {


    private Bitmap bitmap_data;


    public BitmapDataSending(Bitmap _bitmap) {

        this.bitmap_data = _bitmap;
    }


    protected BitmapDataSending(Parcel in) {
        bitmap_data = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Creator<BitmapDataSending> CREATOR = new Creator<BitmapDataSending>() {
        @Override
        public BitmapDataSending createFromParcel(Parcel in) {
            return new BitmapDataSending(in);
        }

        @Override
        public BitmapDataSending[] newArray(int size) {
            return new BitmapDataSending[size];
        }
    };



    public Bitmap getBitmap_data() {
        return bitmap_data;
    }

    public void setBitmap_data(Bitmap bitmap_data) {
        this.bitmap_data = bitmap_data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(bitmap_data, flags);
    }
}

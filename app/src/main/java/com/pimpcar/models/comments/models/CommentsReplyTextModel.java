package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentsReplyTextModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("profile_pic")
    private String profile_pic;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("comment_id")
    private String comment_id;

    @SerializedName("user_name")
    private String user_name;
    @SerializedName("timestamp")
    private String timestamp;
    @SerializedName("comment_text")
    private String comment_text;
    @SerializedName("comment_likes")
    private List<CommentsReplyLikesModel> commentsReplyLikesModels;

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public List<CommentsReplyLikesModel> getCommentsReplyLikesModels() {
        return commentsReplyLikesModels;
    }

    public void setCommentsReplyLikesModels(List<CommentsReplyLikesModel> commentsReplyLikesModels) {
        this.commentsReplyLikesModels = commentsReplyLikesModels;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

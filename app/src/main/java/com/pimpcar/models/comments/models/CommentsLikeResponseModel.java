package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

public class CommentsLikeResponseModel {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

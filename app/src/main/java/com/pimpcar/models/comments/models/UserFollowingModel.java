package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.models.PostFollowerModel;

import java.util.List;

public class UserFollowingModel {
    @SerializedName("followers")
    private String followers;

    @SerializedName("following")
    private String following;
    @SerializedName("user_followers")
    private List<UserAllFollowersModel> user_followers;

      @SerializedName("user_following")
    private List<UserAllFollowingModel> user_following;

    public String getFollowers() {
        return followers;
    }

    public void setFollowers(String followers) {
        this.followers = followers;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public List<UserAllFollowersModel> getUser_followers() {
        return user_followers;
    }

    public void setUser_followers(List<UserAllFollowersModel> user_followers) {
        this.user_followers = user_followers;
    }

    public List<UserAllFollowingModel> getUser_following() {
        return user_following;
    }

    public void setUser_following(List<UserAllFollowingModel> user_following) {
        this.user_following = user_following;
    }
}
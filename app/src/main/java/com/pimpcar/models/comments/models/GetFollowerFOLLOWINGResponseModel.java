package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.models.VideoDataModel;

import java.util.List;

public class GetFollowerFOLLOWINGResponseModel {
    @SerializedName("user")
    private UserFollowingModel user;

    public UserFollowingModel getUser() {
        return user;
    }

    public void setUser(UserFollowingModel user) {
        this.user = user;
    }
}

package com.pimpcar.models.comments.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserAllFollowersModel {
    @SerializedName("_id")
    private String _id;

    @SerializedName("user_id")
    private String user_id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
package com.pimpcar.models.comments.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.OtherUserProfileActivity;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsReplyLikesModel;
import com.pimpcar.models.comments.models.CommentsReplyTextModel;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChildAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context ctx;
    String postid = "";
    private List<CommentsReplyTextModel> childData;
    private List<CommentsReplyTextModel> childDataBk;
    private List<CommentsReplyLikesModel> commentsReplyLikesModels;
    KProgressHUD hud;
    String sizeOfLikes = "", user_token = "";

    public ChildAdapter(Context ctx, List<CommentsReplyTextModel> childData) {
        this.ctx = ctx;
        this.childData = childData;
        childDataBk = new ArrayList<>(childData);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvUserName, timstamp;
        TextView tvCommentReply;
        TextView tvReply;
        AppCompatImageView postLikeBtn, deleteComment;
        ImageView tvExpandCollapseToggle;
        ImageView profile_image;
        TextView totalLikes;

        public ViewHolder(View itemView) {
            super(itemView);
            tvCommentReply = itemView.findViewById(R.id.tvCommentReply);
            deleteComment = itemView.findViewById(R.id.deleteComment);
            profile_image = itemView.findViewById(R.id.profile_image);
            tvExpandCollapseToggle = itemView.findViewById(R.id.iv_expand_collapse_toggle);
            postLikeBtn = itemView.findViewById(R.id.postLikeBtn);
            totalLikes = itemView.findViewById(R.id.totalLikes);
            timstamp = itemView.findViewById(R.id.timstamp);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvReply = itemView.findViewById(R.id.tvReply);
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.nested_recycler_item_child, parent, false);

        ViewHolder cavh = new ViewHolder(itemLayoutView);
        return cavh;
    }


    final Handler handler = new Handler();
    Runnable collapseList = new Runnable() {
        @Override
        public void run() {
            if (getItemCount() > 1) {
                childData.remove(1);
                notifyDataSetChanged();
                handler.postDelayed(collapseList, 50);
            }
        }
    };

    Runnable expandList = new Runnable() {
        @Override
        public void run() {
            int currSize = childData.size();
            if (currSize == childDataBk.size()) return;

            if (currSize == 0) {
                childData.add(childDataBk.get(currSize));
            } else {
                childData.add(childDataBk.get(currSize));
            }
            notifyDataSetChanged();

            handler.postDelayed(expandList, 50);
        }
    };


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        user_token = Utilities.getString(ctx, "user_token");

        if (position == 0 && getItemCount() == 1) {
            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_baseline_expand_more_24);
            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
        } else if (position == childDataBk.size() - 1) {
            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_baseline_expand_less_24);
            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
        } else {
            vh.tvExpandCollapseToggle.setVisibility(View.GONE);
        }

        CommentsReplyTextModel c = childData.get(position);
        commentsReplyLikesModels = new ArrayList<>();
        commentsReplyLikesModels = c.getCommentsReplyLikesModels();
        if (commentsReplyLikesModels.size() == 0) {
            vh.postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_border_24);

        }
        for (int i = 0; i <= commentsReplyLikesModels.size() - 1; i++) {

            if (commentsReplyLikesModels.size() > 0) {
                String underScoreIdd = c.getCommentsReplyLikesModels().get(i).getUser_id();
                if (underScoreIdd.equals(HelperMethods.get_underScoreId())) {
                    vh.postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_24);

//                    postLikeBtn.setVisibility(View.GONE);
//                    postUnLike.setVisibility(View.VISIBLE);
                } else {
                    vh.postLikeBtn.setImageResource(R.drawable.ic_baseline_favorite_border_24);


                }
            }


        }
        vh.profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, OtherUserProfileActivity.class);
                i.putExtra(Constants.THIRD_USER_ID, c.getUser_id());
                ctx.startActivity(i);
            }
        });
//        commentsReplyLikesModels = new ArrayList<>();
////        getCommentsApi(videoItem.getUnderScoreId());
//        for (int j = 0; j <= childData.size() - 1; j++) {
//            commentsReplyLikesModels = childData.get(j).getCommentsReplyLikesModels();
//            if (commentsReplyLikesModels.size() == 0) {
//                postLikeBtn.setVisibility(View.VISIBLE);
//                postUnLike.setVisibility(View.GONE);
//            }
//            for (int i = 0; i <= commentsReplyLikesModels.size() - 1; i++) {
//
//                if (commentsReplyLikesModels.size() > 0) {
//                    String underScoreIdd = commentsReplyLikesModels.get(i).getUser_id();
//                    if (underScoreIdd.equals(HelperMethods.get_underScoreId())) {
//                        postLikeBtn.setVisibility(View.GONE);
//                        postUnLike.setVisibility(View.VISIBLE);
//                    } else {
//                        postLikeBtn.setVisibility(View.VISIBLE);
//                        postUnLike.setVisibility(View.GONE);
//
//                    }
//                } else {
//                    Toast.makeText(ctx, "else case", Toast.LENGTH_SHORT).show();
//                    postLikeBtn.setVisibility(View.VISIBLE);
//                    postUnLike.setVisibility(View.GONE);
//                }
//            }
//
//
//        }


        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
        postid = Utilities.getString(ctx, "postunderScoreId");
        sizeOfLikes = String.valueOf(c.getCommentsReplyLikesModels().size());
//        Toast.makeText(ctx, ""+sizeOfLikes, Toast.LENGTH_SHORT).show();
        vh.totalLikes.setText(sizeOfLikes);
        vh.tvCommentReply.setText(c.getComment_text());
        vh.tvUserName.setText(c.getUser_name());
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM,yyyy");
        String dateString = formatter.format(new Date(Long.parseLong(c.getTimestamp())));
        String MyFinalValue = getlongtoago(Long.parseLong(c.getTimestamp()));
        vh.timstamp.setText(MyFinalValue);
//        vh.timstamp.setText(dateString);
//        Picasso.with(ctx).load(c.getProfile_pic()).placeholder(R.drawable.profile_icon).into(vh. vh.tvUserName.setText(p.getUser_name());
        Picasso.with(ctx).load(c.getProfile_pic()).placeholder(R.drawable.profile_icon).into(vh.profile_image);
//    );
        vh.postLikeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vh.postLikeBtn.getDrawable().getConstantState() == ctx.getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24).getConstantState()) {
                    commentsReplyLikeApi(c.getComment_id(), vh.totalLikes, c.get_id(), HelperMethods.get_underScoreId(),
                            timeStamp, c.getCommentsReplyLikesModels().size(), "dolike", vh.postLikeBtn);

                } else {
                    commentsReplyLikeApi(c.getComment_id(), vh.totalLikes, c.get_id(), HelperMethods.get_underScoreId(),
                            timeStamp, c.getCommentsReplyLikesModels().size(), "unlike", vh.postLikeBtn);

                }
            }
        });
//
//        if (HelperMethods.get_user_id().equals(c.getUser_id())) {
//            vh.deleteComment.setVisibility(View.VISIBLE);
//
//        }
        vh.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (HelperMethods.get_user_id().equals(c.getUser_id())) {
//                    vh.deleteComment.setVisibility(View.VISIBLE);
                    showCustomDialogForDeleteComment(c.get_id(), vh.getAdapterPosition());
                } else {
                    Toast.makeText(ctx, "you can delete your own comment ", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
        vh.deleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomDialogForDeleteComment(c.get_id(), vh.getAdapterPosition());

            }
        });
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItemCount() > 1) {
                    handler.post(collapseList);
                } else {
                    handler.post(expandList);
                }
            }
        });

        vh.tvReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout mRlayout = (RelativeLayout) v.findViewById(R.id.rlTop);
                RelativeLayout.LayoutParams mRparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                EditText myEditText = new EditText(ctx);
                myEditText.setLayoutParams(mRparams);
                mRlayout.addView(myEditText);
            }
        });
    }

    @Override
    public int getItemCount() {
        return childData.size();
    }

    private void commentsReplyLikeApi(String commentid, TextView tvTotalLikes, String commentReplyId, String userid, String timestamp, int totalSize, String status, AppCompatImageView imageLike) {
        hud = KProgressHUD.create(ctx)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
        JsonObject jsonObject = new JsonObject();
        JsonObject attributes = new JsonObject();
        jsonObject.addProperty("comment_id", commentid);
        jsonObject.add("comment_likes", attributes);
        attributes.addProperty("user_id", userid);
        attributes.addProperty("timestamp", timestamp);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<CommentsLikeResponseModel> call = getResponse.getCommetnsReplyLikeApi(commentReplyId, jsonObject);
        call.enqueue(new Callback<CommentsLikeResponseModel>() {
            @Override
            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
                if (response.code() == 200) {
                    hud.dismiss();
                    if (status.equals("dolike")) {
                        int totallikesIncremnt = Integer.parseInt(tvTotalLikes.getText().toString()) + 1;
                        tvTotalLikes.setText(String.valueOf(totallikesIncremnt));
                        imageLike.setImageResource(R.drawable.ic_baseline_favorite_24);

                        Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        if (tvTotalLikes.getText().toString().equals("1")) {
                            tvTotalLikes.setText("0");
//                            Toast.makeText(ctx, "call", Toast.LENGTH_SHORT).show();
                            imageLike.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        } else {
                            int totallikesIncremnt = Integer.parseInt(tvTotalLikes.getText().toString()) - 1;
                            tvTotalLikes.setText(String.valueOf(totallikesIncremnt));
                            imageLike.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                            Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    }


                } else {
                    Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public static String getlongtoago(long createdAt) {
        DateFormat userDateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        DateFormat dateFormatNeeded = new SimpleDateFormat("MM/dd/yyyy HH:MM:SS");
        Date date = null;
        date = new Date(createdAt);
        String crdate1 = dateFormatNeeded.format(date);

        // Date Calculation
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        crdate1 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(date);

        // get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        String currenttime = dateFormat.format(cal.getTime());

        Date CreatedAt = null;
        Date current = null;
        try {
            CreatedAt = dateFormat.parse(crdate1);
            current = dateFormat.parse(currenttime);
        } catch (java.text.ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // Get msec from each, and subtract.
        long diff = current.getTime() - CreatedAt.getTime();
        long diffSeconds = diff / 1000;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        String time = null;
        if (diffDays > 0) {
            if (diffDays == 1) {
                time = diffDays + " day ago ";
            } else {
                time = diffDays + " days ago ";
            }
        } else {
            if (diffHours > 0) {
                if (diffHours == 1) {
                    time = diffHours + " hr ago";
                } else {
                    time = diffHours + " hrs ago";
                }
            } else {
                if (diffMinutes > 0) {
                    if (diffMinutes == 1) {
                        time = diffMinutes + " min ago";
                    } else {
                        time = diffMinutes + " mins ago";
                    }
                } else {
                    if (diffSeconds > 0) {
                        time = diffSeconds + " secs ago";
                    }
                }

            }

        }
        return time;
    }

    private void deleteCommentsApi(String commentId, int position) {
        hud = KProgressHUD.create(ctx)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("comment_reply_id", commentId);
        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
        Call<CommentsLikeResponseModel> call = getResponse.delete_commentReplyApi(postid, jsonObject);
        call.enqueue(new Callback<CommentsLikeResponseModel>() {
            @Override
            public void onResponse(Call<CommentsLikeResponseModel> call, Response<CommentsLikeResponseModel> response) {
                if (response.code() == 200) {
                    hud.dismiss();
//                    commentsDataModels.add(new CommentsDataModel(HelperMethods.get_user_id(),commentText,timestamp));
//                    parentAdapter.notifyDataSetChanged();
//                    dismiss();
                    childData.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, childData.size());
                    Toast.makeText(ctx, "comment Deleted Successfully", Toast.LENGTH_SHORT).show();


                } else {
                    hud.dismiss();
                    Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(ctx, "failed", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void showCustomDialogForDeleteComment(String commentId, int pos) {
        final PrettyDialog pDialog = new PrettyDialog(ctx);
        pDialog
                .setTitle("Message")
                .setMessage("Are you sure you want to Delete Comment?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                deleteCommentsApi(commentId, pos);
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_white,
                        R.color.pdlg_color_red,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();

    }
}
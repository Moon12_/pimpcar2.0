package com.pimpcar.models.comments.adapter;

import static com.facebook.FacebookSdk.getApplicationContext;

import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.google.gson.JsonObject;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.R;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.dialogs.ActionBottomDialogFragment;
import com.pimpcar.models.VideoDataModel;
import com.pimpcar.models.comments.models.CommentsDataModel;
import com.pimpcar.models.comments.models.CommentsLikesModel;
import com.pimpcar.models.comments.models.CommentsResponseModel;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.Utilities;
import com.pimpcar.videoPart.VideoInterface;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChildNewAdapter extends RecyclerView.Adapter<ChildNewAdapter.ViewHolder> {
    AppCompatTextView tvTotalLikes, totalComments;
    AppCompatImageView post_like_button;
    private Context context;
    private List<VideoDataModel> list;
    KProgressHUD hud;
    int category_id;
    List<CommentsDataModel> commentsDataModels;
    List<CommentsLikesModel> commentsLikesModels;
    private int post_total_number_of_likes = 0;
    private ApiService apiService;
    private CompositeDisposable disposable;
    String cat_name, cat_image, userId = "", currentUserId = "";

    public ChildNewAdapter(Context context, List<VideoDataModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_videos_container, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VideoDataModel videoItem = list.get(position);
//        ((Activity)context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        disposable = new CompositeDisposable();
        commentsDataModels = new ArrayList<>();
        commentsLikesModels = new ArrayList<>();
//        getCommentsApi(videoItem.getUnderScoreId());
        currentUserId = HelperMethods.get_user_id();
//        if (userId.equals(currentUserId)) {
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
//        } else {
//            holder.androidLikeButton.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
//            post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);
//
//        }
        holder.txtTitle.setText(videoItem.getPost_user_name());
        holder.txtDesc.setText(videoItem.getPost_description());
        holder.post_users_tag.setText(videoItem.getPost_users_tag());
//        if (fileType.equals("video")) {
        holder.mVideoView.setVideoPath(videoItem.getPost_image_url());
//holder.image_view_profile_pic
        Picasso.with(context).load(videoItem.getUser_profile_pic_url()).fit().centerCrop().into(holder.image_view_profile_pic);

//        }

//        MediaController mediaController = new MediaController(context);
//        holder.mVideoView.setMediaController(mediaController);
//        holder.tvTotalLikes.setText("10.2K");
        totalComments.setText(String.valueOf(videoItem.getPost_comments()));
        tvTotalLikes.setText(String.valueOf(videoItem.getTotal_post_likes()));
        holder.post_hash_tags.setText(videoItem.getPost_hash_tags().toString());

        post_like_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                likeDislikePost(videoItem.post_id);
                likeDislikePost(videoItem.getPost_id());
            }
        });
        holder.image_view_option_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                BottomSheetDialogFragment bottomSheetDialogFragment = new CommentsBottomSheetFragment();
//                bottomSheetDialogFragment.show(((FragmentActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                ActionBottomDialogFragment addPhotoBottomDialogFragment2 = ActionBottomDialogFragment.newInstance();
                addPhotoBottomDialogFragment2.setStyle(ActionBottomDialogFragment.STYLE_NORMAL, R.style
                        .CustomBottomSheetDialogTheme);
                Utilities.saveString(context, "postunderScoreId", videoItem.getUnderScoreId());
                Utilities.saveString(context, "totalComments", videoItem.getPost_comments());

                addPhotoBottomDialogFragment2.show(((FragmentActivity) context).getSupportFragmentManager(),

                        ActionBottomDialogFragment.TAG);


            }
        });

        holder.androidLikeButton.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
//                Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                likeDislikePost(videoItem.getPost_id());

            }

            @Override
            public void unLiked(LikeButton likeButton) {
                Toast.makeText(context, "DisLiked", Toast.LENGTH_SHORT).show();

            }
        });
        holder.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                String fileType = getFileTypeFromURL(videoItem.getPost_image_url());
                if (fileType.equals("video")) {
//                    Toast.makeText(context, "video get", Toast.LENGTH_SHORT).show();
                    holder.mProgressBar.setVisibility(View.GONE);
                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(videoItem.getPost_image_url(), new HashMap<String, String>());
                    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    long timeInMillisec = Long.parseLong(time);
                    retriever.release();
                    String duration = convertMillieToHMmSs(timeInMillisec);
                    //use this duration
//                    Toast.makeText(context, "" + duration, Toast.LENGTH_SHORT).show();
                    float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                    float screenRatio = holder.mVideoView.getWidth() / (float) holder.mVideoView.getHeight();
                    float scale = videoRatio / screenRatio;
                    if (scale >= 1f) {
                        holder.mVideoView.setScaleX(scale);
                    } else {
                        holder.mVideoView.setScaleY(1f / scale);
                    }

                    mp.start();
                }

            }
        });
        holder.toggleButton1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (holder.toggleButton1.isChecked()) {
                    Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                    holder.mVideoView.canPause();

                } else {
                    Toast.makeText(context, "DisLiked", Toast.LENGTH_SHORT).show();

                }
            }
        });
        holder.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
            }
        });
        holder.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(context, "Oops An Error Occur While Playing Video...!!!", Toast.LENGTH_LONG).show(); // display a toast when an error is occured while playing an video
                return false;
            }
        });

        holder.image_view_option_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, videoItem.getPost_image_url());
                sendIntent.setType("text/plain");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                context.startActivity(shareIntent);
//                Intent share = new Intent(Intent.ACTION_SEND);
//                share.setType("video/*");
//                share.putExtra(Intent.EXTRA_STREAM, Uri.parse(videoItem.videoURL));
//                context.startActivity(Intent.createChooser(share, "Share Video"));


            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        VideoView mVideoView;
        TextView txtTitle, txtDesc, post_users_tag, post_hash_tags;
        ProgressBar mProgressBar;
        LikeButton androidLikeButton;
        ToggleButton toggleButton1;
        ShapeableImageView image_view_profile_pic;
        AppCompatImageView image_view_option_share, image_view_option_comment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            androidLikeButton = itemView.findViewById(R.id.likeBtn);
            image_view_profile_pic = itemView.findViewById(R.id.image_view_profile_pic);
            post_like_button = itemView.findViewById(R.id.post_like_button);
            toggleButton1 = itemView.findViewById(R.id.toggleButton1);
            tvTotalLikes = itemView.findViewById(R.id.post_number_of_likes);
            post_users_tag = itemView.findViewById(R.id.post_users_tag);
            totalComments = itemView.findViewById(R.id.totalComments);
            post_hash_tags = itemView.findViewById(R.id.post_hash_tags);
            mVideoView = itemView.findViewById(R.id.videoView);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtDesc = itemView.findViewById(R.id.txtDesc);
            mProgressBar = itemView.findViewById(R.id.progressBar);
            image_view_option_share = itemView.findViewById(R.id.image_view_option_share);
            image_view_option_comment = itemView.findViewById(R.id.image_view_option_comment);
//            cat_names =itemView.findViewById(R.id.text_cat);

        }
    }

    public void shareVideo(final String title, String path) {

        MediaScannerConnection.scanFile(getApplicationContext(), new String[]{path},

                null, new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Intent shareIntent = new Intent(
                                Intent.ACTION_SEND);
                        shareIntent.setType("video/*");
                        shareIntent.putExtra(
                                Intent.EXTRA_SUBJECT, title);
                        shareIntent.putExtra(
                                Intent.EXTRA_TITLE, title);
                        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                        shareIntent
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        context.startActivity(Intent.createChooser(shareIntent,
                                "Share This Video"));

                    }
                });
    }

    public static String convertMillieToHMmSs(long millie) {
        long seconds = (millie / 1000);
        long second = seconds % 60;
        long minute = (seconds / 60) % 60;
        long hour = (seconds / (60 * 60)) % 24;

        String result = "";
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        } else {
            return String.format("%02d:%02d", minute, second);
        }

    }

    private String getFileTypeFromURL(String url) {
        String[] splitedArray = url.split("\\.");
        String lastValueOfArray = splitedArray[splitedArray.length - 1];
        if (lastValueOfArray.equals("mp4") || lastValueOfArray.equals("flv") || lastValueOfArray.equals("m4a") || lastValueOfArray.equals("3gp") || lastValueOfArray.equals("mkv")) {
            return "video";
        } else if (lastValueOfArray.equals("mp3") || lastValueOfArray.equals("ogg")) {
            return "audio";
        } else if (lastValueOfArray.equals("jpg") || lastValueOfArray.equals("png") || lastValueOfArray.equals("gif")) {
            return "photo";
        } else {
            return "";
        }
    }

    private void likeDislikePost(String postId) {

        JsonObject jsonObject = new JsonObject();
        JsonObject data = new JsonObject();
        JsonObject attributes = new JsonObject();
        data.add("attributes", attributes);
        attributes.addProperty("post_id", postId);
        jsonObject.add("data", data);


        KProgressHUD hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setSize(50, 50)
                .show();
        ApiService service = ApiClient.getClient(context).create(ApiService.class);
        Call<PostLikeDislikeResponse> call = service.likeDislikeApi(jsonObject, HelperMethods.get_user_jwt_key());
        call.enqueue(new Callback<PostLikeDislikeResponse>() {
            @Override
            public void onResponse(Call<PostLikeDislikeResponse> call, Response<PostLikeDislikeResponse> response) {

                assert response.body() != null;
                if (response.isSuccessful()) {

                    hud.dismiss();

                    if (response.body().getData().getMessage().equals("post liked successfully")) {
                        post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);
                        Toast.makeText(context, "Post Liked", Toast.LENGTH_SHORT).show();
                        int totalLikes = Integer.parseInt(tvTotalLikes.getText().toString()) + 1;
                        tvTotalLikes.setText(String.valueOf(totalLikes));
                    } else {
                        int totalLikes = Integer.parseInt(tvTotalLikes.getText().toString()) - 1;
                        Toast.makeText(context, "Post unLiked", Toast.LENGTH_SHORT).show();
                        tvTotalLikes.setText(String.valueOf(totalLikes));
                        post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);


                    }
                    Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();

                } else {
                    hud.dismiss();
                    Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostLikeDislikeResponse> call, Throwable t) {
                hud.dismiss();
//                t.printStackTrace();
                Toast.makeText(context, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private int increase_number_of_likes_by_one() {
        return post_total_number_of_likes = post_total_number_of_likes + 1;
    }

    private int decrease_number_of_likes_by_one() {

        return post_total_number_of_likes = post_total_number_of_likes - 1;
    }

    private void getCommentsApi(String id) {
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(VideoInterface.VIDEOURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Create a file object using file path

        VideoInterface getResponse = retrofit.create(VideoInterface.class);
//        Call<String> call = getResponse.uploadImage(fileToUpload,"descriptions","hashtags","userTags","6184be3e6274da6a8865e3ed","1637392740");
        Call<CommentsResponseModel> call = getResponse.getCommetnsApi(id);
        Log.d("assss", "asss");
        call.enqueue(new Callback<CommentsResponseModel>() {
            @Override
            public void onResponse(Call<CommentsResponseModel> call, Response<CommentsResponseModel> response) {
                if (response.code() == 200) {
                    hud.dismiss();
                    commentsDataModels = response.body().getComeentdata();
//                    Toast.makeText(getActivity(), "Parent Success", Toast.LENGTH_SHORT).show();
                    if (!commentsDataModels.isEmpty() || commentsDataModels != null || TextUtils.isEmpty(commentsDataModels.toString())) {
                        for (int i = 0; i <= commentsDataModels.size() - 1; i++) {
                            commentsLikesModels = response.body().getComeentdata().get(i).getComment_likes();
                            if (currentUserId.equals(commentsLikesModels.get(i).getUser_id())) {
                                post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24);


                            } else {
                                post_like_button.setBackgroundResource(R.drawable.ic_baseline_favorite_24_white);

                            }

//                        Toast.makeText(getActivity(), "Child Success", Toast.LENGTH_SHORT).show();
                        }


                    }

                } else {
                    Toast.makeText(context, "fail", Toast.LENGTH_SHORT).show();


                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                hud.dismiss();
                Log.d("gttt", call.toString());
                Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();

            }
        });
    }
}

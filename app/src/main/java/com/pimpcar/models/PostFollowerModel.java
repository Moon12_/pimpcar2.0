package com.pimpcar.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostFollowerModel {
    @SerializedName("_id")
    private String underScoreId;

    @SerializedName("user_id")
    private String user_id;

    public String getUnderScoreId() {
        return underScoreId;
    }

    public void setUnderScoreId(String underScoreId) {
        this.underScoreId = underScoreId;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
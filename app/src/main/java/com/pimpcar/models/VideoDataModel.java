package com.pimpcar.models;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostLikeDislikeResponseModel;
import com.pimpcar.models.comments.models.CommentsLikesModel;
import com.pimpcar.models.comments.models.PostLikesModel;

import java.util.List;

public class VideoDataModel {
    @SerializedName("_id")
    private String underScoreId;

    @SerializedName("post_id")
    private String post_id;
    @SerializedName("post_hash_tags")
    private List<String> post_hash_tags;
    @SerializedName("post_description")
    private String post_description;
    @SerializedName("post_users_tag")
    private String post_users_tag;
    @SerializedName("post_image_url")
    private String post_image_url;
    @SerializedName("is_user_self_liked")
    private Boolean is_user_self_liked;
    @SerializedName("user_id")
    private String user_id;

    @SerializedName("user_profile_pic_url")
    private String user_profile_pic_url;

    @SerializedName("post_user_name")
    private String post_user_name;

    @SerializedName("_id_user_id")
    private String _id_user_id;

    @SerializedName("post_timestamp")
    private String post_timestamp;
    @SerializedName("total_post_likes")
    private int total_post_likes;
    @SerializedName("post_comments")
    private String post_comments;
    @SerializedName("post_likes")
    private List<PostLikesModel> postLikesModels;

    @SerializedName("followers")
    private List<PostFollowerModel> followerModels;

    public List<PostFollowerModel> getFollowerModels() {
        return followerModels;
    }

    public void setFollowerModels(List<PostFollowerModel> followerModels) {
        this.followerModels = followerModels;
    }

    public List<PostLikesModel> getPostLikesModels() {
        return postLikesModels;
    }

    public void setPostLikesModels(List<PostLikesModel> postLikesModels) {
        this.postLikesModels = postLikesModels;
    }
    //    public String get_id() {
//        return _id;
//    }
//
//    public void set_id(String _id) {
//        this._id = _id;
//    }

//    public String getPost_hash_tags() {
//        return post_hash_tags;
//    }
//
//    public void setPost_hash_tags(String post_hash_tags) {
//        this.post_hash_tags = post_hash_tags;
//    }

    public String get_id_user_id() {
        return _id_user_id;
    }

    public void set_id_user_id(String _id_user_id) {
        this._id_user_id = _id_user_id;
    }

    public String getUnderScoreId() {
        return underScoreId;
    }

    public void setUnderScoreId(String underScoreId) {
        this.underScoreId = underScoreId;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getPost_description() {
        return post_description;
    }

    public void setPost_description(String post_description) {
        this.post_description = post_description;
    }

    public List<String> getPost_hash_tags() {
        return post_hash_tags;
    }

    public void setPost_hash_tags(List<String> post_hash_tags) {
        this.post_hash_tags = post_hash_tags;
    }

    public String getPost_users_tag() {
        return post_users_tag;
    }

    public void setPost_users_tag(String post_users_tag) {
        this.post_users_tag = post_users_tag;
    }

    public String getPost_image_url() {
        return post_image_url;
    }

    public void setPost_image_url(String post_image_url) {
        this.post_image_url = post_image_url;
    }

    public Boolean getIs_user_self_liked() {
        return is_user_self_liked;
    }

    public void setIs_user_self_liked(Boolean is_user_self_liked) {
        this.is_user_self_liked = is_user_self_liked;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_profile_pic_url() {
        return user_profile_pic_url;
    }

    public void setUser_profile_pic_url(String user_profile_pic_url) {
        this.user_profile_pic_url = user_profile_pic_url;
    }

    public String getPost_user_name() {
        return post_user_name;
    }

    public void setPost_user_name(String post_user_name) {
        this.post_user_name = post_user_name;
    }

    public String getPost_timestamp() {
        return post_timestamp;
    }

    public void setPost_timestamp(String post_timestamp) {
        this.post_timestamp = post_timestamp;
    }

    public int getTotal_post_likes() {
        return total_post_likes;
    }

    public void setTotal_post_likes(int total_post_likes) {
        this.total_post_likes = total_post_likes;
    }

    public String getPost_comments() {
        return post_comments;
    }

    public void setPost_comments(String post_comments) {
        this.post_comments = post_comments;
    }
}
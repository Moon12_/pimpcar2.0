package com.pimpcar.models.notifications;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.models.VideoDataModel;

import java.util.List;

public class NotifcationResponseModel {
    @SerializedName("notifications")
    private List<NotificationDataModel> notificationDataModels;

    public List<NotificationDataModel> getNotificationDataModels() {
        return notificationDataModels;
    }

    public void setNotificationDataModels(List<NotificationDataModel> notificationDataModels) {
        this.notificationDataModels = notificationDataModels;
    }
}

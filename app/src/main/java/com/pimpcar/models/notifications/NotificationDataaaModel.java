package com.pimpcar.models.notifications;

import com.google.gson.annotations.SerializedName;

public class NotificationDataaaModel {
    @SerializedName("_id")
    private String _id;
    @SerializedName("date_time")
    private String date_time;
    @SerializedName("post_id")
    private String post_id;
    @SerializedName("user_id")
    private String user_id;

    @SerializedName("_id_user_id")
    private String _id_user_id;
    @SerializedName("notification_of")
    private String notification_of;
    @SerializedName("notification_description")
    private String notification_description;
    @SerializedName("user_token")
    private String user_token;

    @SerializedName("user_name")
    private String user_name;


    @SerializedName("profile_pic")
    private String profile_pic;

    public String get_id_user_id() {
        return _id_user_id;
    }

    public void set_id_user_id(String _id_user_id) {
        this._id_user_id = _id_user_id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getNotification_of() {
        return notification_of;
    }

    public void setNotification_of(String notification_of) {
        this.notification_of = notification_of;
    }

    public String getNotification_description() {
        return notification_description;
    }

    public void setNotification_description(String notification_description) {
        this.notification_description = notification_description;
    }

    public String getUser_token() {
        return user_token;
    }

    public void setUser_token(String user_token) {
        this.user_token = user_token;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}

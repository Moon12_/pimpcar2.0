package com.pimpcar.models.notifications;

import com.google.gson.annotations.SerializedName;
import com.pimpcar.models.PostFollowerModel;
import com.pimpcar.models.comments.models.PostLikesModel;

import java.util.List;

public class NotificationDataModel {
    @SerializedName("_id")
    private String underScoreId;

    @SerializedName("d_id")
    private String d_id;

    @SerializedName("l_date")
    private String l_date;

    @SerializedName("data")
    private List<NotificationDataaaModel> notifyData;

    public String getUnderScoreId() {
        return underScoreId;
    }

    public void setUnderScoreId(String underScoreId) {
        this.underScoreId = underScoreId;
    }

    public String getD_id() {
        return d_id;
    }

    public void setD_id(String d_id) {
        this.d_id = d_id;
    }

    public String getL_date() {
        return l_date;
    }

    public void setL_date(String l_date) {
        this.l_date = l_date;
    }

    public List<NotificationDataaaModel> getNotifyData() {
        return notifyData;
    }

    public void setNotifyData(List<NotificationDataaaModel> notifyData) {
        this.notifyData = notifyData;
    }
}
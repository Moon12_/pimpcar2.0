package com.pimpcar.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoListResponseModel {
    @SerializedName("data")
    private List<VideoDataModel> posts;

    public List<VideoDataModel> getPosts() {
        return posts;
    }

    public void setPosts(List<VideoDataModel> posts) {
        this.posts = posts;
    }
}

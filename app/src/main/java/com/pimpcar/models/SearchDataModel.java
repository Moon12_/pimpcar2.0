package com.pimpcar.models;
public class SearchDataModel{
    private String description;
    private int imgId;
    public SearchDataModel(String description, int imgId) {
        this.description = description;
        this.imgId = imgId;
    }

    public SearchDataModel(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public int getImgId() {
        return imgId;
    }
    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}
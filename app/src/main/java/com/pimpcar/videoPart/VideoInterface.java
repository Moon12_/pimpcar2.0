package com.pimpcar.videoPart;

import com.google.gson.JsonObject;
import com.pimpcar.models.VideoListResponseModel;
import com.pimpcar.models.comments.models.CommentsLikeResponseModel;
import com.pimpcar.models.comments.models.CommentsResponseModel;
import com.pimpcar.models.comments.models.FollowUnFollowResponseModel;
import com.pimpcar.models.comments.models.GetFollowerFOLLOWINGResponseModel;
import com.pimpcar.models.notifications.NotifcationResponseModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface VideoInterface {

    String VIDEOURL = "http://3.145.19.246:4042/";

    @Multipart
    @POST("upload-video")
    Call<String> uploadImage(
            @Part MultipartBody.Part file,
//            @Part("file") RequestBody name,
            @Part("description") RequestBody description,
            @Part("hash_tags") RequestBody hash_tags,
            @Part("post_users_tag") RequestBody post_users_tag,
            @Part("user_id") RequestBody user_id,
            @Part("post_timestamp") RequestBody post_timestamp
    );

    @GET("get-post-comments/{post_id}")
    Call<CommentsResponseModel> getCommetnsApi(@Path("post_id") String postId);

    @POST("create-comment-like/{post_id}")
    Call<CommentsLikeResponseModel> getCommetnsLikeApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @POST("create-comment-reply-like/{comment_reply_id}")
    Call<CommentsLikeResponseModel> getCommetnsReplyLikeApi(@Path("comment_reply_id") String postId, @Body JsonObject jsonObject);

    @POST("create-comment/{post_id}")
    Call<CommentsLikeResponseModel> createCommentsApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @DELETE("delete-comment/{comment_id}")
    Call<CommentsLikeResponseModel> delete_commentApi(@Path("comment_id") String postId);

    @POST("delete-comment-reply/{comment_id}")
    Call<CommentsLikeResponseModel> delete_commentReplyApi(@Path("comment_id") String postId,@Body JsonObject jsonObject);

    @POST("create-comment-reply/{post_id}")
    Call<CommentsLikeResponseModel> createCommentsReplyApi(@Path("post_id") String postId, @Body JsonObject jsonObject);

    @PUT("update-user-token/{id}")
    Call<CommentsLikeResponseModel> update_TokenApi(@Path("id") String id, @Body JsonObject jsonObject);

    @POST("follower-followingss/{user_id}")
    Call<FollowUnFollowResponseModel> followUnfollowApi(@Path("user_id") String postId, @Body JsonObject params);

    @GET("get-all-activity/{user_id}")
    Call<NotifcationResponseModel> getAllActivityApi(@Path("user_id") String postId);

    @GET("get-video-posts")
    Call<VideoListResponseModel> getVideolISTPosts();


    @GET("get-follower-following/{user_id}")
    Call<GetFollowerFOLLOWINGResponseModel> get_follower_following(@Path("user_id") String postId);
}
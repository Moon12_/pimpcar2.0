package com.pimpcar.videoPart;

import com.google.gson.annotations.SerializedName;

public class VideoResponseModel {

    @SerializedName("message")
    private int message;

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }
}


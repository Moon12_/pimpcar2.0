package com.pimpcar.TestingActivity;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.pimpcar.R;

public class VideoPimpViewActivity extends AppCompatActivity {
    VideoView videoView;
    MediaController mediaController;
    private String selected_image_path, data = "";
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_pimp_view);

        Bundle bundle = getIntent().getExtras();
        selected_image_path = bundle.getString("selected_video");
        Log.d("checkingValue", "" + selected_image_path);
        data = bundle.getString("data");

        imageView=findViewById(R.id.imageView8);
        videoView = findViewById(R.id.videoView3);

        if (selected_image_path != null) {

            if (data.equals("camera")) {
                videoView.setVideoPath(selected_image_path);
            } else {
                videoView.setVideoURI(Uri.parse(selected_image_path));
            }

            start_Video();

        }
        Bitmap bitmap= ThumbnailUtils.createVideoThumbnail(selected_image_path, MediaStore.Video.Thumbnails.MICRO_KIND);
        imageView.setImageBitmap(bitmap);
    }

    private void start_Video() {
//        mediaController = new MediaController(this);
//        mediaController.setAnchorView(videoView);
//        videoView.setMediaController(mediaController);


        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                float screenRatio = videoView.getWidth() / (float) videoView.getHeight();
                float scaleX = videoRatio / screenRatio;
                if (scaleX >= 1f) {
                    videoView.setScaleX(scaleX);
                } else {
                    videoView.setScaleY(1f / scaleX);
                }
            }
        });

        videoView.start();
    }
}

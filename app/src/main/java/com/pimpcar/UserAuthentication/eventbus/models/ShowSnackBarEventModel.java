package com.pimpcar.UserAuthentication.eventbus.models;

public class ShowSnackBarEventModel {

    private String message;
    private boolean success_or_failure;


    public ShowSnackBarEventModel(boolean success_or_failure, String message) {
        this.message = message;
        this.success_or_failure = success_or_failure;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess_or_failure() {
        return success_or_failure;
    }

    public void setSuccess_or_failure(boolean success_or_failure) {
        this.success_or_failure = success_or_failure;
    }
}

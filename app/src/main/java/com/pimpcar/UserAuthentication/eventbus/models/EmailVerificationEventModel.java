package com.pimpcar.UserAuthentication.eventbus.models;

public class EmailVerificationEventModel {


    private String email_verificaiton_reason;
    private String email;

    public EmailVerificationEventModel(String email_verification_for, String email) {

        this.email_verificaiton_reason = email_verification_for;
        this.email = email;
    }

    public String getEmail_verificaiton_reason() {
        return this.email_verificaiton_reason;
    }

    public void setEmail_verificaiton_reason(String email_verificaiton_reason) {

        this.email_verificaiton_reason = email_verificaiton_reason;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

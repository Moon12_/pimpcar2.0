package com.pimpcar.UserAuthentication.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.login.LoginBody;
import com.pimpcar.Network.model.login.LoginResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.EmailVerificationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.TakeUserToHomeFeed;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.validation.InputValidator;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class LoginFragment extends Fragment {

    @BindView(R.id.user_mail_id)
    AppCompatEditText user_mail_id;
    @BindView(R.id.user_password)
    AppCompatEditText user_password;
    @BindView(R.id.login_button)
    AppCompatTextView login_button;
    @BindView(R.id.forgot_password)
    AppCompatTextView forgot_password;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    private Unbinder unbinder = null;
    private InputValidator validator_username;
    private InputValidator validator_password;
String user_token="";
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.login_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.login_string));
        set_validation();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                user_token = instanceIdResult.getToken();
//                Log.e("newToken",newToken);
//                Toast.makeText(getActivity(), HelperMethods.get_underScoreId(), Toast.LENGTH_SHORT).show();

            }
        });
        return root_view;
    }

    private void set_validation() {

        validator_username = new InputValidator.Builder(getActivity())
                .on(user_mail_id)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_invalid_email))
                .validatorType(InputValidator.IS_EMAIL)
                .showError(true)
                .build();

        validator_password = new InputValidator.Builder(getActivity())
                .on(user_password)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.invalid_password))
                .minLength(8)
                .showError(true)
                .build();
    }

    @OnClick(R.id.login_button)
    public void click_on_login_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {

            if (validator_password.isValid() && validator_username.isValid()) {
                String email_id = user_mail_id.getText().toString();
                String password = user_password.getText().toString();
                login_server(email_id, password);

            }
        } else {

            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));
        }

    }

    private void login_server(String email_id, String password) {
        show_progress_dialog();

        LoginBody loginBody = Utils.create_body_for_login(email_id, password);

        disposable.add(apiService.login(loginBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<LoginResponse>() {
                    @Override
                    public void onSuccess(LoginResponse s) {

                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_LOGGED_IN_SUCCESSFULLY)) {
                            dismiss_progress_dialog();
                            HelperMethods.save_user_jwt_key(s.getData().getJwt_token());
                            HelperMethods.save_user_email_id(email_id);
                            HelperMethods.save_user_id(s.getData().getUid());
                            HelperMethods.save_underScoreId(s.getData().getUser().getUnderScoreid());

                            Utilities.saveString(getActivity(), "userLogged11", "yes");
                            Utilities.saveString(getActivity(), "userLogged", "yes");
                            Utilities.saveString(getActivity(), "user_token", user_token);
                            Log.d("newToken", getActivity().getPreferences(Context.MODE_PRIVATE).getString("fb", "empty :("));
                            Utilities.saveString(getActivity(), "userId", s.getData().getUid());
                            Utilities.saveString(getActivity(), "underScoreId", s.getData().getUser().getUnderScoreid());
                            HelperMethods.set_user_login_status(true);
                            if (s.getData().getUser().getEmail().getEmail_status() == 1) {
                                HelperMethods.set_email_verification_status(true);
                            }

                            if (s.getData().getUser().getProfile_pic() != null && !s.getData().getUser().getProfile_pic().isEmpty()) {
                                HelperMethods.save_user_profile_image(s.getData().getUser().getProfile_pic());
                            }
                            String username = "";

                            if (s.getData().getUser().getFname() != null && !s.getData().getUser().getFname().isEmpty()) {
                                username = s.getData().getUser().getFname();
                            }

                            if (s.getData().getUser().getLname() != null && !s.getData().getUser().getLname().isEmpty()) {
                                username = username + " " + s.getData().getUser().getLname();
                            }

                            HelperMethods.save_user_name(username);

                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            EventBus.getDefault().post(new TakeUserToHomeFeed());
                            click_on_close_button();
                        }
                    }


                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);

                                if (error.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {
                                    click_on_close_button();
                                    EventBus.getDefault().post(new EmailVerificationEventModel(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED, user_mail_id.getText().toString()));
                                }

                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));
    }

    @OnClick(R.id.forgot_password)
    public void click_on_forgot_password() {
        click_on_close_button();
        EventBus.getDefault().post(new EmailVerificationEventModel(Constants.EMAIL_VERIFICATION_FOR_PASSWORD_CHANGE, ""));
    }

    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(LoginFragment.class.toString()));
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();

    }


}

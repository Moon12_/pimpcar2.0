package com.pimpcar.UserAuthentication.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameResponse;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.user_dob.UserDOBResponse;
import com.pimpcar.Network.model.user_dob.UserDOBUpdateBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.GenderProfileEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.datepicker.date.DatePicker;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.validation.InputValidator;
import com.varunjohn1990.iosdialogs4android.IOSDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import es.dmoral.toasty.Toasty;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class UserBirthDateUpdateFragment extends Fragment {

    SimpleDateFormat simpleDateFormat;
    @BindView(R.id.signup_next_button)
    FloatingActionButton signup_next_button;
    @BindView(R.id.user_dob_selected)
    AppCompatEditText user_dob_selected;
    @BindView(R.id.datePicker)
    DatePicker datePicker;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.back_button)
    AppCompatImageView back_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    private Unbinder unbinder;
    private InputValidator user_given_dob;
    Dialog dialog;
    AppCompatEditText user_name_et;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.user_birth_date_update, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        simpleDateFormat = new SimpleDateFormat("dd MM yyyy", Locale.US);
        datePicker.getDayPicker().setMaxDate(System.currentTimeMillis());
        datePicker.getYearPicker().setEndYear(2019);
        datePicker.getMonthPicker().setMaxDate(System.currentTimeMillis());
        close_button.setVisibility(View.GONE);
        back_button.setVisibility(View.VISIBLE);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));
        login_tv.setVisibility(View.GONE);
        set_validator();
        set_date_picker_click_listener();
        show_dialog_box_to_change_username("username");
        return root_view;
    }

    private void set_date_picker_click_listener() {
        user_dob_selected.setEnabled(false);

        datePicker.setOnDateSelectedListener((year, month, day) -> {

            Timber.d("Selected date is " + datePicker.getDate());
            Timber.d("Selected year is" + datePicker.getYear());
            Timber.d("Selected month is" + datePicker.getMonth());

            user_dob_selected.setText(year + "-" + month + "-" + day);
        });


    }

    @OnClick(R.id.login_tv)
    public void on_click_login_tv() {
        on_close_button_clicked();
        EventBus.getDefault().post(new LoginEventModel());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        }
    }

    private void set_validator() {
        user_given_dob = new InputValidator.Builder(getActivity())
                .on(user_dob_selected)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_field_required))
                .showError(true)
                .build();
    }

    @OnClick(R.id.back_button)
    public void on_close_button_clicked() {
        EventBus.getDefault().post(new CloseButtonEventModel(UserBirthDateUpdateFragment.class.toString()));
    }

    @OnClick(R.id.signup_next_button)
    void click_on_float_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {
            if (user_given_dob.isValid()) {
                String date_selected = user_dob_selected.getText().toString();
                update_dob_on_server(date_selected);
            } else {
                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, ResourcesUtil.getString(R.string.string_select_dob)));
            }

        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }

    private void update_dob_on_server(String user_given_date) {
        show_progress_dialog();
        UserDOBUpdateBody userDOBUpdateBody = Utils.create_body_for_dob(user_given_date);
        disposable.add(apiService.updateProfileDOB(HelperMethods.get_user_jwt_key(), userDOBUpdateBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserDOBResponse>() {
                    @Override
                    public void onSuccess(UserDOBResponse s) {
                        dismiss_progress_dialog();
                        EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                        on_close_button_clicked();
                        EventBus.getDefault().post(new GenderProfileEventModel());
                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }));
    }

    private void show_dialog_box_to_change_username(String context_regarding) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.change_user_name_dialog_box);

        user_name_et = dialog.findViewById(R.id.user_name_et_c);
        AppCompatTextView tvHeaderr = dialog.findViewById(R.id.tvHeaderr);
        AppCompatTextView cancel_button = dialog.findViewById(R.id.cancel_button_c);
        AppCompatTextView change_button = dialog.findViewById(R.id.change_button_c);

        tvHeaderr.setText("Choose Another UserName");
        user_name_et.setHint(getResources().getString(R.string.string_user_name));
        user_name_et.setText(HelperMethods.get_user_name());
        user_name_et.setSelection(user_name_et.getText().length());
        String userName = Utilities.getString(getActivity(), "userName");
        user_name_et.setText(userName.toLowerCase());

        cancel_button.setOnClickListener(v -> {
            dialog.dismiss();
        });


        change_button.setOnClickListener(v -> {

            if (HelperMethods.isConnectedToInternet(getActivity())) {
                if (!user_name_et.getText().toString().isEmpty()) {

                    if (user_name_et.getText().toString().contentEquals(HelperMethods.get_user_name())) {
                        Toasty.info(getActivity(), ResourcesUtil.getString(R.string.currently_you_have_same_username));

                        showAlertMessageDialog("username already exists, choose different username");
                    } else {

                        dialog.dismiss();
                        change_user_name_at_server(user_name_et.getText().toString());
//                            changggeUserName(user_name_et.getText().toString());

                    }

                } else {

                    Toasty.error(getActivity(), ResourcesUtil.getString(R.string.please_provide_input));
                    Toast.makeText(getActivity(), R.string.please_provide_input, Toast.LENGTH_SHORT).show();
                }
            } else {

                Toasty.error(getActivity(), ResourcesUtil.getString(R.string.error_internet_not_available));
            }

        });

        dialog.show();
    }

    public void showAlertMessageDialog(String message) {

        new IOSDialog.Builder(getActivity())
                .title(R.string.app_name)              // String or String Resource ID
                .message(message)  // String or String Resource ID
                .positiveButtonText("Yeah, sure")  // String or String Resource ID
                .negativeButtonText("No Thanks")   // String or String Resource ID
                .positiveClickListener(new IOSDialog.Listener() {
                    @Override
                    public void onClick(IOSDialog iosDialog) {
                        iosDialog.dismiss();

                    }
                }).negativeClickListener(new IOSDialog.Listener() {
            @Override
            public void onClick(IOSDialog iosDialog) {
                iosDialog.dismiss();
                dialog.dismiss();

            }
        })
                .build()
                .show();
    }

    private void change_user_name_at_server(String user_name) {

        show_progress_dialog();

        ChangeUserNameRequestBody changeUserNameRequestBody = HelperMethods.create_change_username_body(user_name);
        disposable.add(apiService.changeUsername(HelperMethods.get_user_jwt_key(), changeUserNameRequestBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ChangeUserNameResponse>() {
                    @SuppressLint("CheckResult")
                    @Override
                    public void onSuccess(ChangeUserNameResponse s) {
                        dismiss_progress_dialog();
                        String userName = String.valueOf(s.getData().getUser_name().getUser_name());
                        String token = HelperMethods.get_user_jwt_key();
                        Toasty.success(getActivity().getApplicationContext(), s.getData().getMessage() + "");
                        Toast.makeText(getActivity(), userName, Toast.LENGTH_SHORT).show();
                        Utilities.saveString(getActivity(),"userName",s.getData().getUser_name().getUser_name().toLowerCase());
                        user_name_et.setText(s.getData().getUser_name().getUser_name() + "".toLowerCase());
                        HelperMethods.save_user_name(s.getData().getUser_name().getUser_name() + "");

                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                dismiss_progress_dialog();
                                String error = Utils.get_error_from_response(error_body);
                                Toasty.error(getActivity().getApplicationContext(), error + "");


                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);

        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }
}

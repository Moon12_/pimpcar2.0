package com.pimpcar.UserAuthentication.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoadUserAuthScreenEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.TakeUserToHomeFeed;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utilities;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class CongratulationsFragment extends Fragment {
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    @BindView(R.id.done_button)
    AppCompatTextView done_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    private Unbinder unbinder = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.congratulations_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));
        return root_view;
    }


    @OnClick(R.id.login_tv)
    public void on_click_login_tv() {
        click_on_close_button();
        EventBus.getDefault().post(new LoginEventModel());
    }


    @OnClick(R.id.done_button)
    public void on_click_donebutton() {
        click_on_close_button();
        if (HelperMethods.is_user_logged_in()) {
            new TakeUserToHomeFeed();
        Utilities.saveString(getActivity(), "userLogged", "yes");

        } else {
            EventBus.getDefault().post(new LoadUserAuthScreenEventModel());
        }

    }


    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(CongratulationsFragment.class.toString()));
        if (HelperMethods.is_user_logged_in()) {
            EventBus.getDefault().post(new TakeUserToHomeFeed());
        } else {
            EventBus.getDefault().post(new LoadUserAuthScreenEventModel());
        }
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }
}

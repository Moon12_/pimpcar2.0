package com.pimpcar.UserAuthentication.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.useremailverification.UserEMailOTPResponse;
import com.pimpcar.Network.model.useremailverification.UserEmailOTPVerifyResponse;
import com.pimpcar.Network.model.useremailverification.UserEmailVerificationOTPBody;
import com.pimpcar.Network.model.useremailverification.UserEmailVerifyOTPBody;
import com.pimpcar.Network.model.usergender.UserGenderProfileResponse;
import com.pimpcar.Network.model.userselfprofile.UserSelfProfileResponse;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.BirthDateEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ForgotPasswordEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.otpview.OtpTextView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.validation.InputValidator;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class UserEmailVerificationFragment extends Fragment {

    @BindView(R.id.user_email_id_et)
    AppCompatEditText user_email_id_et;
    @BindView(R.id.text_send_view)
    AppCompatTextView text_send_view;
    @BindView(R.id.otp_view)
    OtpTextView otp_view;
    @BindView(R.id.signup_next_button)
    FloatingActionButton signup_next_button;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.resend_otp_to_user_tv)
    AppCompatTextView resend_otp_to_user_tv;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    private String otp_sent_tag = "otp sent";
    private String otp_not_sent = "otp not sent";
    private Unbinder unbinder = null;
    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private boolean is_otp_sent_to_email = false;
    private InputValidator validator_email;
    private String given_email = null;
    private KProgressHUD hud;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
            if (signup_next_button.getTag().toString().contentEquals("EMAIL_VERIFICATION")) {

                if (HelperMethods.isConnectedToInternet(getActivity())) {
                    get_user_self_details();

                } else {
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

                }

            }


            if (given_email != null && signup_next_button.getTag().toString().contentEquals("EMAIL_VERIFICATION_IN_BETWEEN_LEFT_WITH_LOGIN")) {
                user_email_id_et.setText(given_email);
                user_email_id_et.setEnabled(false);
                user_email_id_et.setKeyListener(null);
                user_email_id_et.getBackground().setAlpha(51);
                user_email_id_et.setTextColor(ResourcesUtil.getColor(R.color.gray));


                if (HelperMethods.isConnectedToInternet(getActivity())) {
                    send_email_otp_to_user(given_email);

                } else {
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.user_email_verification_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));
        login_tv.setVisibility(View.GONE);
        resend_otp_to_user_tv.setVisibility(View.GONE);
        set_validator();

        String start_text = getArguments().getString(Constants.EMAIL_REASON);

        if (start_text.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED)) {
            given_email = getArguments().getString(Constants.EMAIL_GIVEN);
            signup_next_button.setTag("EMAIL_VERIFICATION_IN_BETWEEN_LEFT_WITH_LOGIN");
        }


        if (start_text.contentEquals(Constants.EMAIL_VERIFICATION_FOR_PASSWORD_CHANGE)) {
            signup_next_button.setTag("PASSWORD_CHANGE");
        }

        if (start_text.contentEquals(Constants.EMAIL_VERIFICATION_FIRST_TIME)) {
            signup_next_button.setTag("EMAIL_VERIFICATION");
        }


        Timber.d("-- TAG --" + signup_next_button.getTag());


        if (is_otp_sent_to_email) {
            otp_view.setVisibility(View.VISIBLE);
        } else {
            otp_view.setVisibility(View.GONE);
        }


        text_send_view.setText(ResourcesUtil.getString(R.string.string_we_will_send_you));
        return root_view;
    }

    private void set_validator() {
        validator_email = new InputValidator.Builder(getActivity())
                .on(user_email_id_et)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.error_invalid_email))
                .validatorType(InputValidator.IS_EMAIL)
                .showError(true)
                .build();
    }

    @OnClick(R.id.resend_otp_to_user_tv)
    public void click_on_resend_otp() {
        String email_id = user_email_id_et.getText().toString();
        if (validator_email.isValid())
            send_email_otp_to_user(email_id);
    }

    @OnClick(R.id.login_tv)
    public void click_on_login_tv() {
        click_on_close_button();
        EventBus.getDefault().post(new LoginEventModel());
    }

    @OnClick(R.id.signup_next_button)
    public void onclick_signup_next_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {

            if (signup_next_button.getTag().toString().contentEquals("PASSWORD_CHANGE")) {

                if (is_otp_sent_to_email) {
                    String otp = otp_view.getOTP();
                    String email = user_email_id_et.getText().toString();
                    verify_otp_for_password_change(email, otp);
                } else {
                    if (validator_email.isValid()) {
                        String email_id = user_email_id_et.getText().toString();
                        Timber.d("email id is :: ### " + email_id);
                        send_otp_to_email_for_password_change(email_id);
                    }
                }
            }

            if (signup_next_button.getTag().toString().contentEquals("OTP SENT FOR MAIL VERIFICATION")) {
                String email_otp = otp_view.getOTP();
                verify_email_otp_first_time(email_otp);
            }


            if (signup_next_button.getTag().toString().contentEquals("EMAIL_VERIFICATION_IN_BETWEEN_LEFT_WITH_LOGIN")) {
                String email_otp = otp_view.getOTP();
                verify_email_otp_first_time(email_otp);
            }

        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }

    private void send_otp_to_email_for_password_change(String email_id) {
        show_progress_dialog();
        UserEmailVerificationOTPBody userEmailVerificationOTPBody = Utils.create_body_for_email_otp(email_id);
        disposable.add(apiService.sendEmailOtpForPasswordChange(userEmailVerificationOTPBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserEmailOTPVerifyResponse>() {
                    @Override
                    public void onSuccess(UserEmailOTPVerifyResponse s) {
                        dismiss_progress_dialog();
                        String message = s.getData().getMessage();
                        EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, message.toUpperCase()));
                        otp_view.setVisibility(View.VISIBLE);
                        otp_view.setFocusable(true);
                        user_email_id_et.setEnabled(false);
                        user_email_id_et.setKeyListener(null);
                        user_email_id_et.getBackground().setAlpha(51);
                        user_email_id_et.setTextColor(ResourcesUtil.getColor(R.color.gray));
                        signup_next_button.setTag("PASSWORD_CHANGE");
                        is_otp_sent_to_email = true;

                    }

                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }));


    }

    private void verify_otp_for_password_change(String email, String otp) {

        if (otp.length() == 4) {
            show_progress_dialog();

            UserEmailVerifyOTPBody userEmailVerifyOTPBody = Utils.create_body_for_email_otp_verify(email, Integer.parseInt(otp));
            disposable.add(apiService.verifyEmailOTPPasswordChange(userEmailVerifyOTPBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<UserEMailOTPResponse>() {
                        @Override
                        public void onSuccess(UserEMailOTPResponse s) {
                            dismiss_progress_dialog();
                            String message = s.getData().getMessage();
                            HelperMethods.save_user_jwt_key(s.getData().getJwt_token());
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, message.toUpperCase()));
                            click_on_close_button();
                            EventBus.getDefault().post(new ForgotPasswordEventModel());

                        }

                        @Override
                        public void onError(Throwable e) {
                            dismiss_progress_dialog();
                            if (e instanceof HttpException) {
                                ResponseBody body = ((HttpException) e).response().errorBody();
                                try {
                                    String error_body = body.string();
                                    String error = Utils.get_error_from_response(error_body);
                                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }

                    }));


        }

    }

    private void verify_email_otp_first_time(String email_otp) {
        if (email_otp.length() == 4) {
            show_progress_dialog();
            String email = user_email_id_et.getText().toString();
            UserEmailVerifyOTPBody userEmailVerifyOTPBody = Utils.create_body_for_email_otp_verify(email, Integer.parseInt(email_otp));
            disposable.add(apiService.verifyEmailOTP(userEmailVerifyOTPBody)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<UserEmailOTPVerifyResponse>() {
                        @Override
                        public void onSuccess(UserEmailOTPVerifyResponse s) {
                            dismiss_progress_dialog();
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            HelperMethods.set_email_verification_status(true);
                            click_on_close_button();
                            EventBus.getDefault().post(new BirthDateEventModel());
                        }

                        @Override
                        public void onError(Throwable e) {
                            dismiss_progress_dialog();
                            if (e instanceof HttpException) {
                                ResponseBody body = ((HttpException) e).response().errorBody();
                                try {
                                    String error_body = body.string();
                                    String error = Utils.get_error_from_response(error_body);
                                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                                } catch (IOException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }

                    }));


        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, ResourcesUtil.getString(R.string.otp_should_be_four_digit)));
        }
    }

    private void get_user_self_details() {
        show_progress_dialog();
        disposable.add(apiService.getUserSelfProfile(HelperMethods.get_user_jwt_key())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserSelfProfileResponse>() {
                    @Override
                    public void onSuccess(UserSelfProfileResponse s) {
                        dismiss_progress_dialog();
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_AUTHENTICATED_SUCCESSFULLY)) {
                            Log.d(SignupFragment.class.getSimpleName(), "throwable fees " + s.getData().getMessage());
                            HelperMethods.save_user_email_id(s.getData().getUser_details().getEmail_id());
                            HelperMethods.save_user_fname(s.getData().getUser_details().getFname());
                            HelperMethods.save_user_lname(s.getData().getUser_details().getLname());
                            HelperMethods.save_user_login_type(s.getData().getUser_details().getLogin_type());
                            Log.d("Email id ", HelperMethods.get_user_email_id());
                            user_email_id_et.setText(HelperMethods.get_user_email_id());
                            user_email_id_et.setEnabled(false);
                            user_email_id_et.setKeyListener(null);
                            user_email_id_et.getBackground().setAlpha(51);
                            user_email_id_et.setTextColor(ResourcesUtil.getColor(R.color.gray));
                            send_email_otp_to_user(HelperMethods.get_user_email_id());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }));
    }

    private void send_email_otp_to_user(String _email) {
        String email = _email;
        UserEmailVerificationOTPBody userEmailVerificationOTPBody = Utils.create_body_for_email_otp(email);
        disposable.add(apiService.sendEmailOtp(userEmailVerificationOTPBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserGenderProfileResponse>() {
                    @Override
                    public void onSuccess(UserGenderProfileResponse s) {
                        dismiss_progress_dialog();
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_OTP_SENT_TO_EMAIL + " " + email)) {
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            make_otp_view_visible(s.getData().getMessage());
                            resend_otp_to_user_tv.setVisibility(View.VISIBLE);
                            signup_next_button.setTag("OTP SENT FOR MAIL VERIFICATION");
                            HelperMethods.is_email_otp_sent(true);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }

                }));

    }

    private void make_otp_view_visible(String otp_message_sent) {
        text_send_view.setText(otp_message_sent);
        otp_view.setVisibility(View.VISIBLE);
        otp_view.requestFocus();
    }

    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(UserEmailVerificationFragment.class.toString()));
    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);

        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();
    }
}

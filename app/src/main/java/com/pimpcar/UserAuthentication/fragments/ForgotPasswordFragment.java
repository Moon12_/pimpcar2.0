package com.pimpcar.UserAuthentication.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.password_change.PasswordChanageResponse;
import com.pimpcar.Network.model.password_change.PasswordChangeBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoadUserAuthScreenEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.validation.InputValidator;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

public class ForgotPasswordFragment extends Fragment {
    @BindView(R.id.user_password)
    AppCompatEditText user_password;
    @BindView(R.id.user_confirm_password)
    AppCompatEditText user_confirm_password;
    @BindView(R.id.signup_next_button)
    FloatingActionButton signup_next_button;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    private Unbinder unbinder = null;
    private InputValidator validator_password;
    private InputValidator validator_confirm_password;

    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private KProgressHUD hud;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() != null) {
            apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.forgot_password_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        login_tv.setVisibility(View.GONE);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.forgot_password_title));
        set_up_validator();
        return root_view;
    }

    private void set_up_validator() {
        validator_password = new InputValidator.Builder(getActivity())
                .on(user_password)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.invalid_password))
                .minLength(8)
                .showError(true)
                .build();

        validator_confirm_password = new InputValidator.Builder(getActivity())
                .on(user_confirm_password)
                .required(true)
                .errorMessage(ResourcesUtil.getString(R.string.invalid_password))
                .minLength(8)
                .showError(true)
                .build();
    }

    @OnClick(R.id.login_tv)
    public void click_on_login_button() {
        click_on_close_button();
        EventBus.getDefault().post(new LoginEventModel());
    }

    @OnClick(R.id.close_button)
    public void click_on_close_button() {
        EventBus.getDefault().post(new CloseButtonEventModel(ForgotPasswordFragment.class.toString()));
    }

    @OnClick(R.id.signup_next_button)
    public void on_click_next_button() {


        if (HelperMethods.isConnectedToInternet(getActivity())) {
            String usr_pwd = user_password.getText().toString();
            String cnfrm_pwd = user_confirm_password.getText().toString();

            if (usr_pwd.contentEquals(cnfrm_pwd) && cnfrm_pwd.contentEquals(usr_pwd)) {

                if (usr_pwd.length() >= 8 && cnfrm_pwd.length() >= 8) {


                    change_user_password(usr_pwd);

                } else {

                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_password_should_contain_length)));
                }

            } else {
                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_password_did_not_matched)));

                user_confirm_password.setError(ResourcesUtil.getString(R.string.error_password_did_not_matched));
            }

        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }

    }

    private void change_user_password(String usr_pwd) {
        String user_jwt_key = HelperMethods.get_user_jwt_key();
        PasswordChangeBody passwordChangeBody = Utils.create_body_for_change_password(usr_pwd);
        show_progress_dialog();

        disposable.add(apiService.change_password(user_jwt_key, passwordChangeBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PasswordChanageResponse>() {
                    @Override
                    public void onSuccess(PasswordChanageResponse s) {
                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.USER_PASSWORD_CHANGE_SUCCESSFULLY)) {
                            dismiss_progress_dialog();
                            Log.d(SignupFragment.class.getSimpleName(), "throwable fees " + s.getData().getMessage());
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            click_on_close_button();
                            EventBus.getDefault().post(new LoadUserAuthScreenEventModel());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null)
            unbinder.unbind();
        super.onDestroyView();

    }
}

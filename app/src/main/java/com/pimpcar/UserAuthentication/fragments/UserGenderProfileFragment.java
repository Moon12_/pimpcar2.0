package com.pimpcar.UserAuthentication.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.Network.ApiClient;
import com.pimpcar.Network.ApiService;
import com.pimpcar.Network.model.usergender.UserGenderProfileResponse;
import com.pimpcar.Network.model.usergender.UserGenderProfileUpdateBody;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CongratulationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.Widgets.CircleImageView;
import com.pimpcar.Widgets.progressbar.KProgressHUD;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.HelperMethods;
import com.pimpcar.utils.NetworkResponseMessages;
import com.pimpcar.utils.PermissionUtils;
import com.pimpcar.utils.ResourcesUtil;
import com.pimpcar.utils.Utils;
import com.pimpcar.utils.compressor.Compressor;
import com.pimpcar.utils.compressor.ImageUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import timber.log.Timber;

public class UserGenderProfileFragment extends Fragment {


    private static final int PICK_PHOTO = 12;
    private static final int CAMERA_PIC_REQUEST = 11;
    private static final int REQUEST_ID_PERMISSIONS_READ_WRITE = 1936;
    @BindView(R.id.toolbar_title_tv)
    AppCompatTextView toolbar_title_tv;
    @BindView(R.id.close_button)
    AppCompatImageView close_button;
    @BindView(R.id.user_image_selection)
    AppCompatImageView user_image_selection;
    @BindView(R.id.male_radio_button_rl)
    RelativeLayout male_radio_button_rl;
    @BindView(R.id.female_radio_button_rl)
    RelativeLayout female_radio_button_rl;
    @BindView(R.id.signup_next_button)
    FloatingActionButton signup_next_button;
    @BindView(R.id.user_gender_selection_FL)
    FrameLayout user_gender_selection_FL;
    @BindView(R.id.radio_circle_male)
    View radio_circle_male;
    @BindView(R.id.radio_circle_female)
    View radio_circle_female;
    @BindView(R.id.login_tv)
    AppCompatTextView login_tv;
    @BindView(R.id.above_icon_image)
    View above_icon_image;
    @BindView(R.id.user_image_circular_imageview)
    CircleImageView user_image_circular_imageview;
    private Unbinder unbinder = null;
    private File profileFile;
    private Uri capturedPhotoURI;
    private String user_gender = "male";
    private Bitmap profile_bitmap = null;


    private ApiService apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private boolean is_image_setted = false;
    private KProgressHUD hud;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root_view = inflater.inflate(R.layout.user_profile_and_gender_fragment, container, false);
        unbinder = ButterKnife.bind(this, root_view);
        toolbar_title_tv.setText(ResourcesUtil.getString(R.string.signup_string));
        login_tv.setVisibility(View.GONE);
        user_image_circular_imageview.setDrawingCacheEnabled(true);
        return root_view;
    }

    @OnClick(R.id.login_tv)
    public void click_on_login_tv() {
        close_button_clicked();
        EventBus.getDefault().post(new LoginEventModel());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        apiService = ApiClient.getClient(getActivity().getApplicationContext()).create(ApiService.class);
        check_permission();
    }

    private void check_permission() {
        if (getActivity() != null && PermissionUtils.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) &&
                PermissionUtils.checkPermissions(getActivity(), Manifest.permission.CAMERA) &&
                PermissionUtils.checkPermissions(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Timber.d("user have given all permission");

        } else {
            ask_permission_camera_read_write();
        }
    }

    @OnClick(R.id.close_button)
    void close_button_clicked() {
        EventBus.getDefault().post(new CloseButtonEventModel(UserGenderProfileFragment.class.toString()));
    }

    @OnClick(R.id.signup_next_button)
    void click_on_next_button() {

        if (HelperMethods.isConnectedToInternet(getActivity())) {
            if (profile_bitmap != null) {
                if (getUser_gender() != null) {
                    update_user_data_to_server(profile_bitmap);
                } else {
                    EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, "Please select gender"));
                }
            } else {
                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, "Please select profile"));
            }
        } else {
            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_WARNING, ResourcesUtil.getString(R.string.error_internet_not_available)));

        }


    }

    private void update_user_data_to_server(Bitmap profile_bitmap) {

        show_progress_dialog();

        File image_file = ImageUtil.bitmapToFile(profile_bitmap, HelperMethods.get_user_jwt_key());

        Bitmap compressed_bitmap = Compressor.getDefault(getActivity()).compressToBitmap(image_file);

        UserGenderProfileUpdateBody userGenderProfileUpdateBody = Utils.create_body_for_gender_update(compressed_bitmap, getUser_gender());

        disposable.add(apiService.updateProfileGender(HelperMethods.get_user_jwt_key(), userGenderProfileUpdateBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UserGenderProfileResponse>() {
                    @Override
                    public void onSuccess(UserGenderProfileResponse s) {
                        dismiss_progress_dialog();

                        if (s.getData().getMessage().contentEquals(NetworkResponseMessages.PROFILE_PIC_UPDATED)) {
                            HelperMethods.save_user_profile_image(s.getData().getImage_url());
                            EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_SUCCESS, s.getData().getMessage().toUpperCase()));
                            close_button_clicked();
                            EventBus.getDefault().post(new CongratulationEventModel());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        dismiss_progress_dialog();
                        if (e instanceof HttpException) {
                            ResponseBody body = ((HttpException) e).response().errorBody();
                            try {
                                String error_body = body.string();
                                String error = Utils.get_error_from_response(error_body);
                                EventBus.getDefault().post(new ShowToastEventModel(Constants.TOAST_TYPE_FAILURE, error.toUpperCase()));
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }));

    }

    @OnClick(R.id.user_gender_selection_FL)
    void select_profile_pic() {
        showDialog(getActivity());
    }

    @OnClick(R.id.male_radio_button_rl)
    void male_radio_button_clicked() {

        if (!is_image_setted()) {
            user_image_circular_imageview.setVisibility(View.GONE);
            user_image_selection.setVisibility(View.VISIBLE);
            above_icon_image.setVisibility(View.VISIBLE);
            user_image_selection.setImageDrawable(ResourcesUtil.getDrawableById(R.mipmap.gender_male));
        }

        setUser_gender("male");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            radio_circle_male.setBackground(ResourcesUtil.getDrawableById(R.drawable.radio_button_cirlce));
            radio_circle_female.setBackground(ResourcesUtil.getDrawableById(R.drawable.radio_button_white_cirlce));
        } else {
            radio_circle_male.setBackgroundDrawable(ResourcesUtil.getDrawableById(R.drawable.radio_button_cirlce));
            radio_circle_female.setBackgroundDrawable(ResourcesUtil.getDrawableById(R.drawable.radio_button_white_cirlce));
        }

    }

    @OnClick(R.id.female_radio_button_rl)
    void female_radio_button_clicked() {

        if (!is_image_setted()) {
            user_image_circular_imageview.setVisibility(View.GONE);
            user_image_selection.setVisibility(View.VISIBLE);
            above_icon_image.setVisibility(View.VISIBLE);
            user_image_selection.setImageDrawable(ResourcesUtil.getDrawableById(R.mipmap.gender_female));
        }

        setUser_gender("female");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            radio_circle_male.setBackground(ResourcesUtil.getDrawableById(R.drawable.radio_button_white_cirlce));
            radio_circle_female.setBackground(ResourcesUtil.getDrawableById(R.drawable.radio_button_cirlce));
        } else {
            radio_circle_male.setBackgroundDrawable(ResourcesUtil.getDrawableById(R.drawable.radio_button_white_cirlce));
            radio_circle_female.setBackgroundDrawable(ResourcesUtil.getDrawableById(R.drawable.radio_button_cirlce));
        }
    }

    private void ask_permission_camera_read_write() {

        int read_Permission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int write_premission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int camera_permission = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (camera_permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (read_Permission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (write_premission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }


        Timber.d("No. of permission left or not given by user" + listPermissionsNeeded.size());

        ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded
                        .toArray(new String[listPermissionsNeeded.size()]),
                REQUEST_ID_PERMISSIONS_READ_WRITE);
    }

    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        AppCompatTextView gallery_layout = dialog.findViewById(R.id.gallery_layout);
        AppCompatTextView camera_layout = dialog.findViewById(R.id.camera_layout);

        gallery_layout.setOnClickListener(v -> {

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PICK_PHOTO);

            dialog.dismiss();

        });


        camera_layout.setOnClickListener(v -> {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
            dialog.dismiss();
        });


        dialog.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_ID_PERMISSIONS_READ_WRITE) {

            int read_Permission = ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);
            int write_premission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int camera_permission = ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.CAMERA);

            List<String> listPermissionsNeeded = new ArrayList<>();
            if (camera_permission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }
            if (read_Permission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded
                        .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (write_premission != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }

            if (listPermissionsNeeded.size() == 0) {
                show_snakbar(ResourcesUtil.getString(R.string.string_thanks_for_providing_permission));

                HelperMethods.showToastbar(getActivity(), ResourcesUtil.getString(R.string.string_thanks_for_providing_permission));

            } else {
                show_snakbar(ResourcesUtil.getString(R.string.permission_not_provided));
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            user_image_selection.setVisibility(View.GONE);
            above_icon_image.setVisibility(View.GONE);
            user_image_circular_imageview.setVisibility(View.VISIBLE);
            load_image_in_iv(data);
        }


        if (requestCode == CAMERA_PIC_REQUEST && resultCode == Activity.RESULT_OK) {

            user_image_selection.setVisibility(View.GONE);
            above_icon_image.setVisibility(View.GONE);
            user_image_circular_imageview.setVisibility(View.VISIBLE);

            Timber.d("Image from camera data :: " + data.getStringExtra(MediaStore.EXTRA_OUTPUT));
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            profile_bitmap = photo;
            user_image_circular_imageview.setImageBitmap(photo);
            set_bitmap_into_imageview(true);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void load_image_in_iv(Intent data) {
        if (data == null) {
            return;
        }
        try {
            Uri uri = data.getData();
            String selectedFilePath = HelperMethods.getPath(getActivity(), uri);
            File file = new File(selectedFilePath);
            profileFile = file;

            String filePath = file.getPath();
            profile_bitmap = BitmapFactory.decodeFile(filePath);

            set_bitmap_into_imageview(true);


            if (profileFile != null && user_image_circular_imageview != null) {
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.fitCenter();
                requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
                Glide.with(PimpCarApplication.getApplicationInstance().getApplicationContext()).setDefaultRequestOptions(requestOptions).load(data.getData()).
                        listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {

                                return false;
                            }
                        }).into(user_image_circular_imageview);


            }


        } catch (Exception e) {
            Timber.d("Error : " + e.getMessage());
        }
    }

    private void set_bitmap_into_imageview(boolean is_imageview_set) {
        this.is_image_setted = is_imageview_set;
    }

    private boolean is_image_setted() {
        return this.is_image_setted;
    }

    private void show_snakbar(String message) {


        if (message.contentEquals(ResourcesUtil.getString(R.string.string_thanks_for_providing_permission))) {
            final Snackbar snackbar_ask_again = Snackbar
                    .make(getActivity().findViewById(R.id.fragment_container), message, Snackbar.LENGTH_LONG)
                    .setAction(ResourcesUtil.getString(R.string.string_provide), v -> {

                        ask_permission_camera_read_write();
                    });

            snackbar_ask_again.setActionTextColor(Color.RED);

            View sbView = snackbar_ask_again.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar_ask_again.show();
        } else {

            final Snackbar snackbar_okey = Snackbar
                    .make(getActivity().findViewById(R.id.fragment_container), message, Snackbar.LENGTH_SHORT);

            snackbar_okey.setAction(ResourcesUtil.getString(R.string.string_ok), v -> {
                snackbar_okey.dismiss();
            });
            snackbar_okey.setActionTextColor(Color.RED);

            View sbView = snackbar_okey.getView();
            TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar_okey.show();


        }


    }

    public void show_progress_dialog() {
        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE);
        hud.setSize(50, 50);
        hud.show();
    }

    public void dismiss_progress_dialog() {
        if (hud != null && hud.isShowing())
            hud.dismiss();
    }


    public String getUser_gender() {
        return user_gender;
    }

    public void setUser_gender(String user_gender) {
        this.user_gender = user_gender;
    }
}

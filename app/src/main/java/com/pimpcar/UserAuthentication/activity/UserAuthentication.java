package com.pimpcar.UserAuthentication.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pimpcar.HomeFeed.activity.HomeFeedActivity;
import com.pimpcar.R;
import com.pimpcar.UserAuthentication.eventbus.models.BirthDateEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CloseButtonEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.CongratulationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.EmailVerificationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ForgotPasswordEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.GenderProfileEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoadUserAuthScreenEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.LoginEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.NumberVerificationEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowSnackBarEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.ShowToastEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.SignupEventModel;
import com.pimpcar.UserAuthentication.eventbus.models.TakeUserToHomeFeed;
import com.pimpcar.UserAuthentication.fragments.CongratulationsFragment;
import com.pimpcar.UserAuthentication.fragments.ForgotPasswordFragment;
import com.pimpcar.UserAuthentication.fragments.LoginFragment;
import com.pimpcar.UserAuthentication.fragments.NumberVerificationFragment;
import com.pimpcar.UserAuthentication.fragments.SignupFragment;
import com.pimpcar.UserAuthentication.fragments.UserAuthFragment;
import com.pimpcar.UserAuthentication.fragments.UserBirthDateUpdateFragment;
import com.pimpcar.UserAuthentication.fragments.UserEmailVerificationFragment;
import com.pimpcar.UserAuthentication.fragments.UserGenderProfileFragment;
import com.pimpcar.utils.Constants;
import com.pimpcar.utils.NetworkResponseMessages;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class UserAuthentication extends AppCompatActivity {


    @BindView(R.id.fragment_container)
    FrameLayout fragment_container;
    boolean doubleBackToExitPressedOnce = false;
    private FragmentManager fragmentManager;
    private LoginFragment loginFragment;
    private SignupFragment signupFragment;
    private UserGenderProfileFragment userGenderProfileFragment;
    private UserEmailVerificationFragment userEmailVerificationFragment;
    private UserBirthDateUpdateFragment userBirthDateUpdateFragment;
    private ForgotPasswordFragment forgotPasswordFragment;
    private NumberVerificationFragment numberVerificationFragment;
    private CongratulationsFragment congratulationsFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        setContentView(R.layout.user_authentication_layout_activity);

        ButterKnife.bind(this);


        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);

        load_user_auth_screen();

    }

    public void load_user_auth_screen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        UserAuthFragment userAuthFragment = new UserAuthFragment();
        fragmentTransaction.add(R.id.fragment_container, userAuthFragment);
        fragmentTransaction.commit();
    }

    public void load_signup_screen(String login_type, JSONObject facebook_user_data) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        signupFragment = new SignupFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LOGIN_TYPE, login_type);
        if (facebook_user_data != null)
            bundle.putString(Constants.USER_FACEBOOK_JSON_OBJECT, facebook_user_data.toString());

        signupFragment.setArguments(bundle);
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, signupFragment);
        fragmentTransaction.addToBackStack(signupFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_login_screen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        loginFragment = new LoginFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, loginFragment);
        fragmentTransaction.addToBackStack(loginFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_number_verification_screen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        numberVerificationFragment = new NumberVerificationFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, numberVerificationFragment);
        fragmentTransaction.addToBackStack(numberVerificationFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_gender_profile_screen() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        userGenderProfileFragment = new UserGenderProfileFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, userGenderProfileFragment);
        fragmentTransaction.addToBackStack(userGenderProfileFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_email_verification_screen(String verification_reason, String email) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.EMAIL_REASON, verification_reason);

        if (!email.trim().isEmpty() && verification_reason.contentEquals(NetworkResponseMessages.USER_EMAIL_NOT_VERIFIED))
            bundle.putString(Constants.EMAIL_GIVEN, email);


        userEmailVerificationFragment = new UserEmailVerificationFragment();
        userEmailVerificationFragment.setArguments(bundle);
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, userEmailVerificationFragment);
        fragmentTransaction.addToBackStack(userEmailVerificationFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_birth_date_fragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        userBirthDateUpdateFragment = new UserBirthDateUpdateFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, userBirthDateUpdateFragment);
        fragmentTransaction.addToBackStack(userBirthDateUpdateFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_forgot_password_fragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        forgotPasswordFragment = new ForgotPasswordFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.fragment_container, forgotPasswordFragment);
        fragmentTransaction.addToBackStack(forgotPasswordFragment.toString());
        fragmentTransaction.commit();
    }

    public void load_congratulations_fragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        congratulationsFragment = new CongratulationsFragment();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fragmentTransaction.replace(R.id.fragment_container, congratulationsFragment);
        fragmentTransaction.addToBackStack(congratulationsFragment.toString());
        fragmentTransaction.commit();

    }

    @Subscribe
    public void load_user_auth_screen(LoadUserAuthScreenEventModel loadUserAuthScreenEventModel) {
        load_user_auth_screen();
    }

    @Subscribe
    public void load_congratulations_event(CongratulationEventModel congratulationEventModel) {
        load_congratulations_fragment();
    }

    @Subscribe
    public void load_number_verification_event(NumberVerificationEventModel numberVerificationEventModel) {
        load_number_verification_screen();
    }

    @Subscribe
    public void login_click_event(LoginEventModel loginEventModel) {
        load_login_screen();
    }

    @Subscribe
    public void signup_click_event(SignupEventModel signupEventModel) {
        load_signup_screen(signupEventModel.getLogin_type(), signupEventModel.getFacebook_user_data());
    }

    @Subscribe
    public void forgotpassword_load_event(ForgotPasswordEventModel forgotPasswordEventModel) {
        load_forgot_password_fragment();
    }

    @Subscribe
    public void take_user_home_feed(TakeUserToHomeFeed takeUserToHomeFeed) {
        take_user_to_home();
    }

    private void take_user_to_home() {
        Intent intent = new Intent(this, HomeFeedActivity.class);
        startActivity(intent);
        finish();
    }

    @Subscribe
    public void close_button_click_event(CloseButtonEventModel closeButtonEventModel) {
        String fragment_name = closeButtonEventModel.getFragment_name_closed();

        if (fragment_name.contentEquals(LoginFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(loginFragment);
            trans.commit();
            manager.popBackStack();
        }

        if (fragment_name.contentEquals(SignupFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(signupFragment);
            trans.commit();
            manager.popBackStack();
        }

        if (fragment_name.contentEquals(UserGenderProfileFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();

            trans.remove(userGenderProfileFragment);
            trans.commit();
            manager.popBackStack();
        }

        if (fragment_name.contentEquals(UserEmailVerificationFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();

            trans.remove(userEmailVerificationFragment);
            trans.commit();
            manager.popBackStack();
        }

        if (fragment_name.contentEquals(ForgotPasswordFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();

            trans.remove(forgotPasswordFragment);
            trans.commit();
            manager.popBackStack();
        }

        if (fragment_name.contentEquals(UserBirthDateUpdateFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();

            trans.remove(userBirthDateUpdateFragment);
            trans.commit();
            manager.popBackStack();
        }


        if (fragment_name.contentEquals(CongratulationsFragment.class.toString())) {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction trans = manager.beginTransaction();
            trans.remove(congratulationsFragment);
            trans.commit();
            manager.popBackStack();
        }

    }

    @Subscribe
    public void load_email_verification_fragment(EmailVerificationEventModel emailVerificationEventModel) {
        load_email_verification_screen(emailVerificationEventModel.getEmail_verificaiton_reason(), emailVerificationEventModel.getEmail());
    }

    @Subscribe
    public void load_user_gender_profile_fragment(GenderProfileEventModel genderProfileEventModel) {
        load_gender_profile_screen();
    }

    @Subscribe
    public void show_snackbar(ShowSnackBarEventModel showSnackBarEventModel) {
        show_snackbar_message(showSnackBarEventModel.isSuccess_or_failure(), showSnackBarEventModel.getMessage());
    }

    @Subscribe
    public void load_user_birth_date_update(BirthDateEventModel birthDateEventModel) {
        load_birth_date_fragment();
    }

    @Subscribe
    public void show_toast(ShowToastEventModel showToastEventModel) {

        show_toastbar_message(showToastEventModel.getToasttype(), showToastEventModel.getMessage());

    }

    public void show_snackbar_message(boolean success_or_failure, String message) {

        int button_color = success_or_failure ? Color.GREEN : Color.RED;
        int text_color = success_or_failure ? Color.BLUE : Color.YELLOW;

        Snackbar snackbar = Snackbar
                .make(fragment_container, message, Snackbar.LENGTH_SHORT);

        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                snackbar.dismiss();
            }
        });

        snackbar.setActionTextColor(button_color);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(text_color);
        snackbar.show();

    }

    public void show_toastbar_message(String toast_type, String message) {

        if (toast_type.contentEquals(Constants.TOAST_TYPE_SUCCESS)) {
            Toasty.success(UserAuthentication.this, message + "!", Toast.LENGTH_SHORT, true).show();
        }

        if (toast_type.contentEquals(Constants.TOAST_TYPE_FAILURE)) {
            Toasty.error(UserAuthentication.this, message + "!", Toast.LENGTH_SHORT, true).show();
        }

        if (toast_type.contentEquals(Constants.TOAST_TYPE_WARNING)) {
            Toasty.warning(UserAuthentication.this, message + "!", Toast.LENGTH_SHORT, true).show();
        }

        if (toast_type.contentEquals(Constants.TOAST_TYPE_INFO)) {
            Toasty.info(UserAuthentication.this, message + "!", Toast.LENGTH_SHORT, true).show();

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}



package com.pimpcar.UserAuthentication.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RohitKaLoginResponse implements Parcelable {
    public static final Creator<RohitKaLoginResponse> CREATOR = new Creator<RohitKaLoginResponse>() {
        @Override
        public RohitKaLoginResponse createFromParcel(Parcel in) {
            return new RohitKaLoginResponse(in);
        }

        @Override
        public RohitKaLoginResponse[] newArray(int size) {
            return new RohitKaLoginResponse[size];
        }
    };
    /**
     * success : true
     * message : Sign in successfull
     * user : {"id":8,"email":"rohit@elgroupinternational.com","authentication_token":"Rii1zpx61hQHqJMTRYNIgA=="}
     * code : 200
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("user")
    private UserBean user;
    @SerializedName("code")
    private int code;

    protected RohitKaLoginResponse(Parcel in) {
        success = in.readByte() != 0;
        message = in.readString();
        code = in.readInt();
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (success ? 1 : 0));
        dest.writeString(message);
        dest.writeInt(code);
    }

    public static class UserBean {
        /**
         * id : 8
         * email : rohit@elgroupinternational.com
         * authentication_token : Rii1zpx61hQHqJMTRYNIgA==
         */

        @SerializedName("id")
        private int id;
        @SerializedName("email")
        private String email;
        @SerializedName("authentication_token")
        private String authentication_token;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAuthentication_token() {
            return authentication_token;
        }

        public void setAuthentication_token(String authentication_token) {
            this.authentication_token = authentication_token;
        }
    }
}

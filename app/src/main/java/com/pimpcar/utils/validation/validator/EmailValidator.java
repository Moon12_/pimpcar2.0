package com.pimpcar.utils.validation.validator;


import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

import java.util.regex.Pattern;

public class EmailValidator extends AbstractValidator {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);


    public EmailValidator() {
        mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_email);
    }

    @Override
    public boolean isValid(String value) {
        return VALID_EMAIL_ADDRESS_REGEX.matcher(value).matches();
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}

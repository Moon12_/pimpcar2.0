package com.pimpcar.utils.validation.validator;


import android.util.Patterns;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

public class PhoneNumberValidator extends AbstractValidator {

    public PhoneNumberValidator() {
        mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_phone_number);
    }

    @Override
    public boolean isValid(String value) {
        return Patterns.PHONE.matcher(value).matches();
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}

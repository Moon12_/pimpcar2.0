package com.pimpcar.utils.validation.validator;


import android.text.TextUtils;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;


public class RequiredValidator extends AbstractValidator {

    private boolean mRequired;

    public RequiredValidator(boolean required) {
        mRequired = required;
        mErrorMessage = PimpCarApplication.getInstance().getApplicationContext().getString(R.string.error_field_required);
    }

    @Override
    public boolean isValid(String value) {
        if (mRequired)
            return !TextUtils.isEmpty(value);
        return true;
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(String message) {
        mRequired = true;
        mErrorMessage = message;
    }
}

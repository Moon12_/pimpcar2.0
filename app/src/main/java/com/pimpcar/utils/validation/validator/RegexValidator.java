package com.pimpcar.utils.validation.validator;


import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.R;

import java.util.regex.Pattern;


public class RegexValidator extends AbstractValidator {

    private Pattern mRegexPattern;

    public RegexValidator(String regex) {
        mRegexPattern = Pattern.compile(regex);
        mErrorMessage = PimpCarApplication.getApplicationInstance().getApplicationContext().getString(R.string.error_invalid_field);
    }

    @Override
    public boolean isValid(String value) {
        return mRegexPattern.matcher(value).matches();
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}

package com.pimpcar.utils.timber;

import timber.log.Timber;

import static android.util.Log.ERROR;

public class NotLoggingTree extends Timber.Tree {


    @Override
    protected void log(final int priority, final String tag, final String message, final Throwable throwable) {


        if (priority == ERROR) {
            // Crashlytics code

        }

    }
}

package com.pimpcar.utils;

/**
 * Created by sahitya on 13/11/18.
 */


public class ShareprefConstantKeys {
    public static final String APP_CURRENT_VERSION = "app_current_version";
    public static final String USER_ID = "user_id";
    public static final String IS_USER_LOGIN_KEY = "isLoggedIn";
    public static final String USER_LOGIN_TYPE_KEY = "login_type";
    public static final String USER_NAME_KEY = "user_name";
    public static final String USER_CODE_KEY = "user_code";
    public static final String USER_PROFILE_IMAGE_KEY = "user_profile_image";
    public static final String USER_CITY_KEY = "user_city";
    public static final String USER_CITY_NAME_KEY = "user_city_name";
    public static final String USER_GENDER_KEY = "user_gender";
    public static final String USER_EMAIL_KEY = "email";
    public static final String USER_NUMBER_KEY = "user_number";
    public static final String USER_DOB_KEY = "user_dob";
    public static final String USER_SOCIAL_ID = "user_social_id";
    public static final String USER_FB_MAIL = "user_fb_mail";
    public static final String USER_GOOGLE_MAIL = "user_google_mail";
    public static final String USER_MAIL = "user_mail_simple";
    public static final String USER_MOBILE_NO_KEY = "user_mobile_no";
    public static final String USER_CITY_ID_KEY = "city_id";
    public static final String DOB_EMPTY = "dob_date";


    public static final String AUTH_TOKEN_KEY = "auth_token";
    public static final String EXP_KEY = "exp";
    public static final String USER_FIRST_NAME = "first_name";
    public static final String USER_LAST_NAME = "last_name";
    public static final String USER_FACEBOOK_ID = "fb_id";
    public static final String USER_FACEBOOK_AUTH_TOKEN_KEY = "fb_auth_token";
    public static final String USER_TYPE_KEY = "user_type";
    public static final String USER_PASSWORD_KEY = "password";
    public static final String READ_ME_KEY = "rem_me";
    public static final String USER_LOGIN_TIME = "login_time";


    public static final String USER_ANONYMUS_KEY = "anonymous";
    public static final String FB_AUTH_TOKEN_KEY = "fb_auth_token";
    public static final String FB_ID_KEY = "fb_id";
    public static final String IS_USER_ANONYMUS = "is_user_anonymus";
    public static final String IS_USER_FIRST_TIME = "is_user_first_time";
    public static final String USER_SHARE_COUNT = "user_share_count";
    public static final String USER_FRIEND_LIST = "user_friend_list";
    public static final String USER_LOGGED_IN_AS_GUEST = "loged_user_as_guest";
    public static final String USER_EMIAL_OR_FACEBOOK_TOKEN_KEY = "user_email_or_facebook_token";
    public static final String USER_SAVED_PASSWORD = "user_saved_password";
    public static final String USER_SAVED_LOGIN_METHOD = "user_saved_login_method";
    public static final String ANONYMUS_USER_LATEST_EDITTING_IMAGE_UPPER_TITLE = "anonymus_user_latest_editing_image_upper_title";
    public static final String ANONYMUS_USER_LATEST_EDITTING_IMAGE_LOWER_TITLE = "anonymus_user_latest_editing_image_lower_title";
    public static final String ANONYMUS_USER_LATEST_EDITTING_IMAGE_CAPTION = "anonymus_user_latest_editing_image_caption";
    public static final String ANONYMUS_USER_LATEST_EDITTING_IMAGE_IMAGE_PATH = "anonymus_user_latest_editing_image_image_path";


    public static final String IS_SELECTED_TRENDING_POSITION = "selected_trending_position";
    public static final String FACEBOOK_USER_NAME = "user_facebook_username";
    public static final String USER_TWITTER_USERNAME = "user_twitter_username";
    public static final String USER_INSTAGRAM_USERNAME = "user_instagram_username";
    public static final String USER_TOTAL_NUMBER_SUBSCRIBER = "user_total_number_subscriber";
    public static final String USER_NAME_PREF = "username_pref";
    public static final String APP_CACHE_INITLIZED = "app_cahche_initialized";
    public static final String API_KEY = "user_app_api_key";
    public static final String USER_JWT_KEY = "user_jwt_key";
    public static final String IS_EMAIL_OTP_SENT = "is_email_otp_sent";
    public static final String IS_EMAIL_VERIFIED = "is_email_verified";
    public static final String LOGED_IN_USER_ID = "logged_in_user_id";
    public static final String underScoreId = "logged_underScore_id";
    public static final String USER_FOLLOWING_USER_LIST = "user_following_user_array_list";
    public static final String IS_PROFILE_DATA_CALLED = "is_profile_data_is_saved_or_not";
    public static final String USER_TOTAL_NUMBER_OF_FOLLOWING = "total_number_of_following";
    public static final String USER_TOTAL_NUMBER_OF_POSTS = "total_number_of_posts";
    public static final String USER_TOTAL_NUMBER_OF_FOLLOWERS = "total_number_of_followers";
    public static final String USERNAME_KEY = "username_share_pref";
    public static final String USER_STATUS = "user_status_quote";
}

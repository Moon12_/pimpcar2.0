package com.pimpcar.utils;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.pimpcar.Application.PimpCarApplication;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedRequestBody;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.notification.models.NotificationRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentsBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserStatusRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowedAccountRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.PostUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonPostBodyRequest;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonProfileRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonStoriesBodyRequest;
import com.pimpcar.HomeFeed.pojos.SearchFamousRequestBody;
import com.pimpcar.Network.model.posts.PostCommentRequestBody;
import com.pimpcar.Network.model.posts.PostLikeDislikeRequestBody;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.posts.UserPostsBodyRequest;
import com.pimpcar.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import timber.log.Timber;


public class HelperMethods {

    private static int screenWidth = 0;
    private static int screenHeight = 0;


    //@To Check if the os version is Marshmallow
    public static boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    //@To check if the os version si Lollipop
    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


    //@To check if the os version si JellyBean
    public static boolean isJellyBeanMR2() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
    }

    //@To check if the os version si JellyBean
    public static boolean isJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    //@To check if the os version si JellyBean
    public static boolean isJellyBeanMR1() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
    }

    //@To get the height of the actionbar
    public static int getActionBarHeight(Context context) {
        int mActionBarHeight;
        TypedValue mTypedValue = new TypedValue();

        context.getTheme().resolveAttribute(R.attr.actionBarSize, mTypedValue, true);

        mActionBarHeight = TypedValue.complexToDimensionPixelSize(mTypedValue.data, context.getResources().getDisplayMetrics());

        return mActionBarHeight;
    }


    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    //@To get the screenHeight
    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }

        return screenHeight;
    }

    // To get the screenWidth
    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }


    //@To convert dp into pixel
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    public static boolean isConnectedToInternet(Context context) {

        ConnectivityManager
                cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null
                && activeNetwork.isConnectedOrConnecting();
    }

    public static void showToastbar(Context context, String s) {

        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public static boolean isAppInstalled(String app_name, Context context) {

        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(app_name, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }


    private static void takeUserToPlaystore(Context context) {

        if (context != null) {
            Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                context.startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                context.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
            }
        }

    }

    public static String get_user_profile_pic(String fb_id) {
        return "http://graph.facebook.com/" + fb_id + "/picture?type=large";
    }


    public static String generateString() {
        Random r = new Random();
        String domain = "abcdefghijklmnopqrstuvwxyz" + "1234567890" + "_-.";
        String newString = "";
        int length = 8 + r.nextInt(10);
        int count = 0;
        while (count < length) {
            newString += domain.charAt(r.nextInt(domain.length()));
            count++;
        }
        return newString;
    }

    public static String get_facebook_token_key() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_FACEBOOK_AUTH_TOKEN_KEY, "");
    }

    public static void set_facebook_token_key(String facebook_token_key) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_FACEBOOK_AUTH_TOKEN_KEY, facebook_token_key);
    }

    public static String get_first_name_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_FIRST_NAME, "");
    }

    public static void set_user_name_for_pref(String user_name_for_pref) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_NAME_PREF, user_name_for_pref);
    }

    public static String get_username_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_NAME_PREF, "");
    }

    public static String get_last_name_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LAST_NAME, "");
    }

    public static void set_last_name_for_pref(String user_last_name) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LAST_NAME, user_last_name);
    }


    public static String get_password_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_PASSWORD_KEY, "");
    }

    public static void set_password_for_pref(String password_for_pref) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_PASSWORD_KEY, password_for_pref);
    }


    public static String get_user_id_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_ID, "");
    }

    public static void set_user_id_for_pref(String user_id_for_pref) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_ID, user_id_for_pref);
    }

    public static String get_user_profile_from_pref() {
        String profile_path = SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_PROFILE_IMAGE_KEY, "");
        if (profile_path != null && !profile_path.isEmpty())
            return profile_path;
        return Constants.DEFAULT_PROFILE_PIC;
    }

    public static boolean get_user_anonymus_from_pref() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_ANONYMUS_KEY, false);
    }

    public static String get_usernmae_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_NAME_KEY, "");
    }


    public static String get_user_type_from_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_TYPE_KEY, "");
    }


    public static void increase_user_share_count_in_share_pref() {
        String sahre_count = SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SHARE_COUNT, "0");
        int increase_count = Integer.parseInt(sahre_count);
        increase_count++;
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SHARE_COUNT, increase_count + "");
    }

    public static void decrease_user_share_count_in_share_pref() {
        String sahre_count = SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SHARE_COUNT, "0");
        int increase_count = Integer.parseInt(sahre_count);
        increase_count--;
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SHARE_COUNT, increase_count + "");
    }

    public static String get_user_share_count() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SHARE_COUNT, "0");
    }

    public static int get_dp_as_pixel(int size_in_dp, Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (size_in_dp * scale + 0.5f);
        return dpAsPixels;

    }


    public static float dp2px(Context context, float dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

    public static float sp2px(Context context, float sp) {
        final float scale = context.getResources().getDisplayMetrics().scaledDensity;
        return sp * scale;
    }

    public static String[] reverseString(String[] words) {
        String[] t = new String[words.length];
        for (int i = 0; i < words.length; i++) {
            t[i] = "";
            for (int j = words[i].length() - 1; j >= 0; j--) {
                t[i] += words[i].substring(j, j + 1);
            }
        }
        return t;
    }


    public static boolean is_user_logged_in() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_USER_LOGIN_KEY, false);
    }


    public static List<String> get_trend_data(String array_data[]) {
        List<String> trend_data = new ArrayList<>();

        for (int i = 0; i < array_data.length; i++) {
            trend_data.add(array_data[i]);
        }
        Collections.reverse(trend_data);
        return trend_data;
    }

    public static void set_login_as_guest() {
        SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LOGGED_IN_AS_GUEST, true);
    }

    public static boolean get_login_as_guest() {
        return SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LOGGED_IN_AS_GUEST, false);
    }


    public static String get_saved_user_password_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SAVED_PASSWORD, "");
    }

    public static String get_saved_login_method_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SAVED_LOGIN_METHOD, "");
    }

    public static void save_continue_editting_image_lower_case(String s) {
    }

    public static void save_user_profile_image(String profileImage) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_PROFILE_IMAGE_KEY, profileImage);
    }

    public static String get_saved_user_profile_image() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_PROFILE_IMAGE_KEY, "");
    }

    public static String get_user_facebook_username_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.FACEBOOK_USER_NAME, "");
    }

    public static void set_user_facebook_username_pref(String facebook_user_name) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.FACEBOOK_USER_NAME, facebook_user_name);
    }

    public static void set_user_twitter_username_pref(String twitter_user_name) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_TWITTER_USERNAME, twitter_user_name);
    }

    public static String get_ser_twitter_username_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_TWITTER_USERNAME, "");
    }

    public static String get_user_instagram_username_pref() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_INSTAGRAM_USERNAME, "");
    }

    public static void set_user_instagram_username_pref(String youtube_user_name) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_INSTAGRAM_USERNAME, youtube_user_name);
    }

    public static String get_user_password() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SAVED_PASSWORD, "");
    }

    public static void set_user_password(String password) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_SAVED_PASSWORD, password);
    }

    public static void save_total_number_of_users_following(int total_following) {
        SharedPrefsUtils.setIntegerPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_FOLLOWING, total_following);
    }

    public static int get_total_number_of_users_following() {
        return SharedPrefsUtils.getIntegerPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_FOLLOWING, 0);
    }

    public static void save_total_number_of_user_posts(int total_posts) {
        SharedPrefsUtils.setIntegerPreference(PimpCarApplication.getApplicationInstance(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_POSTS, total_posts);
    }

    public static int get_total_number_of_user_posts() {
        return SharedPrefsUtils.getIntegerPreference(PimpCarApplication.getApplicationInstance(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_POSTS, 0);
    }

    public static void save_total_number_of_followers(int total_followers) {
        SharedPrefsUtils.setIntegerPreference(PimpCarApplication.getApplicationInstance(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_FOLLOWERS, total_followers);
    }

    public static int get_total_number_of_followers() {
        return SharedPrefsUtils.getIntegerPreference(PimpCarApplication.getApplicationInstance(),
                ShareprefConstantKeys.USER_TOTAL_NUMBER_OF_FOLLOWERS, 0);
    }


    public static void save_user_jwt_key(String jwt_token) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_JWT_KEY, jwt_token);
    }

    public static String get_user_jwt_key() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_JWT_KEY, "");

    }

    public static void save_user_email_id(String email_id) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_EMAIL_KEY, email_id);
    }

    public static String get_user_email_id() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_EMAIL_KEY, "");
    }


    public static void save_cointnue_editting_image_path(String path_of_image) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_IMAGE_PATH, path_of_image);
    }

    public static String get_continue_image_editting_path() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_IMAGE_PATH, "");
    }

    public static void save_continue_editting_image_caption(String image_caption) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_CAPTION, image_caption);
    }


    public static String get_continue_editting_image_caption() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_CAPTION, "");
    }

    public static void save_continue_editting_image_top_text(String top_text) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_UPPER_TITLE, top_text);
    }

    public static String get_continue_edditing_image_get_top_text() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.ANONYMUS_USER_LATEST_EDITTING_IMAGE_UPPER_TITLE, "");
    }

    public static int get_selected_trending_position() {
        return SharedPrefsUtils.getIntegerPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_SELECTED_TRENDING_POSITION, 0);
    }

    public static void set_selected_trending_position(int selected_position) {
        SharedPrefsUtils.setIntegerPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_SELECTED_TRENDING_POSITION, selected_position);
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(final Context context, final Uri uri) {

        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri
                .getAuthority());
    }


    // convert from bitmap to byte array
    public static byte[] getBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    // convert from byte array to bitmap
    public static Bitmap getImage(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    public static String get_api_key() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.API_KEY, "");
    }

    public static void set_api_key(String key) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.API_KEY, key);
    }

    public static void save_user_fname(String fname) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_FIRST_NAME, fname);
    }

    public static String get_user_fname() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_FIRST_NAME, "");
    }

    public static void save_user_lname(String lname) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LAST_NAME, lname);
    }


    public static String get_user_lname() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LAST_NAME, "");
    }

    public static void save_user_login_type(String login_type) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LOGIN_TYPE_KEY, login_type);
    }

    public static String get_user_login_type() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_LOGIN_TYPE_KEY, "");
    }

    public static boolean get_user_login_status() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_USER_LOGIN_KEY, false);
    }

    public static void set_user_login_status(boolean login_status) {
        SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_USER_LOGIN_KEY, login_status);
    }

    public static void is_email_otp_sent(boolean otp_sent) {
        SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_EMAIL_OTP_SENT, otp_sent);
    }

    public static boolean get_is_email_otp_sent() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_EMAIL_OTP_SENT, false);
    }

    public static boolean get_email_verification_status() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_EMAIL_VERIFIED, false);
    }

    public static void set_email_verification_status(boolean is_email_verified) {
        SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_EMAIL_VERIFIED, is_email_verified);

    }

    public static String get_facebook_profile_url_link(String facebook_id) {
        String user_image_url = "https://graph.facebook.com/" + facebook_id + "/picture?type=large";

        return user_image_url;
    }

    public static UserPostsBodyRequest create_user_profile_post_get(int page_number) {

        UserPostsBodyRequest userPostsBodyRequest = new UserPostsBodyRequest();
        UserPostsBodyRequest.DataBean dataBean = new UserPostsBodyRequest.DataBean();
        UserPostsBodyRequest.DataBean.AttributesBean attributesBean = new UserPostsBodyRequest.DataBean.AttributesBean();
        attributesBean.setPage_number(page_number);
        dataBean.setAttributes(attributesBean);
        userPostsBodyRequest.setData(dataBean);
        return userPostsBodyRequest;
    }

    public static PostAllCommentsBodyModel create_post_comments_body(int page_numbner, String post_id) {

        PostAllCommentsBodyModel postAllCommentsBodyModel = new PostAllCommentsBodyModel();
        PostAllCommentsBodyModel.DataBean dataBean = new PostAllCommentsBodyModel.DataBean();
        PostAllCommentsBodyModel.DataBean.AttributesBean attributesBean = new PostAllCommentsBodyModel.DataBean.AttributesBean();

        attributesBean.setPage_no(page_numbner);
        attributesBean.setPost_id(post_id);


        dataBean.setAttributes(attributesBean);
        postAllCommentsBodyModel.setData(dataBean);
        return postAllCommentsBodyModel;
    }

    public static PostCommentRequestBody create_post_comment_body(String post_id, String comment) {

        PostCommentRequestBody postCommentRequestBody = new PostCommentRequestBody();
        PostCommentRequestBody.DataBean dataBean = new PostCommentRequestBody.DataBean();
        PostCommentRequestBody.DataBean.AttributesBean attributesBean = new PostCommentRequestBody.DataBean.AttributesBean();
        attributesBean.setPost_id(post_id);
        attributesBean.setUser_comment(comment);
        dataBean.setAttributes(attributesBean);
        postCommentRequestBody.setData(dataBean);
        return postCommentRequestBody;
    }

    public static PostLikeDislikeRequestBody create_post_like_dislike_body(String post_id) {
        PostLikeDislikeRequestBody postLikeDislikeRequestBody = new PostLikeDislikeRequestBody();
        PostLikeDislikeRequestBody.DataBean dataBean = new PostLikeDislikeRequestBody.DataBean();
        PostLikeDislikeRequestBody.DataBean.AttributesBean attributesBean = new PostLikeDislikeRequestBody.DataBean.AttributesBean();
        attributesBean.setPost_id(post_id);
        dataBean.setAttributes(attributesBean);
        postLikeDislikeRequestBody.setData(dataBean);
        return postLikeDislikeRequestBody;
    }

    public static PostAllLikesBodyModel create_post_all_like(String post_id, int page_number) {
        PostAllLikesBodyModel postAllLikesBodyModel = new PostAllLikesBodyModel();
        PostAllLikesBodyModel.DataBean dataBean = new PostAllLikesBodyModel.DataBean();
        PostAllLikesBodyModel.DataBean.AttributesBean attributesBean = new PostAllLikesBodyModel.DataBean.AttributesBean();
        attributesBean.setPage_no(page_number);
        attributesBean.setPost_id(post_id);
        dataBean.setAttributes(attributesBean);
        postAllLikesBodyModel.setData(dataBean);
        return postAllLikesBodyModel;
    }

    public static void save_user_id(String uid) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.LOGED_IN_USER_ID, uid);
    }

    public static String get_user_id() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.LOGED_IN_USER_ID, "");
    }

    public static void save_underScoreId(String _id) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.underScoreId, _id);
    }

    public static String get_underScoreId() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.underScoreId, "");
    }

    public static FollowUserRequestBodyModel create_follow_user_req_body(String user_id) {
        FollowUserRequestBodyModel followUserRequestBodyModel = new FollowUserRequestBodyModel();
        FollowUserRequestBodyModel.DataBean dataBean = new FollowUserRequestBodyModel.DataBean();
        FollowUserRequestBodyModel.DataBean.AttributesBean attributesBean = new FollowUserRequestBodyModel.DataBean.AttributesBean();
        attributesBean.setTo_follow(user_id);
        dataBean.setAttributes(attributesBean);
        followUserRequestBodyModel.setData(dataBean);
        return followUserRequestBodyModel;
    }


    public static UnFollowUserRequestBodyModel create_unfollow_user_req_body(String user_id) {
        UnFollowUserRequestBodyModel unfollowUserRequestBodyModel = new UnFollowUserRequestBodyModel();
        UnFollowUserRequestBodyModel.DataBean dataBean = new UnFollowUserRequestBodyModel.DataBean();
        UnFollowUserRequestBodyModel.DataBean.AttributesBean attributesBean = new UnFollowUserRequestBodyModel.DataBean.AttributesBean();
        attributesBean.setTo_unfollow(user_id);
        dataBean.setAttributes(attributesBean);
        unfollowUserRequestBodyModel.setData(dataBean);
        return unfollowUserRequestBodyModel;
    }

    public static void add_user_to_user_follow(String user_id) {

    }

    public static void save_users_following_list(ArrayList<String> user_following) {
        SharedPrefsUtils.putListString(PimpCarApplication.getApplicationInstance().getApplicationContext(), ShareprefConstantKeys.USER_FOLLOWING_USER_LIST, user_following);
    }

    public static ArrayList<String> get_user_following_list() {
        return SharedPrefsUtils.getListString(PimpCarApplication.getApplicationInstance().getApplicationContext(), ShareprefConstantKeys.USER_FOLLOWING_USER_LIST);
    }

    public static void add_users_to_following_list(Context context, String user_id) {
        ArrayList<String> user_follow_list = get_user_following_list();
        if (!user_follow_list.contains(user_id))
            user_follow_list.add(user_id);
        save_users_following_list(user_follow_list);
    }


    public static void remove_user_from_following_list(Context context, String user_id) {
        ArrayList<String> user_follow_list = get_user_following_list();
        if (user_follow_list.contains(user_id))
            user_follow_list.remove(user_id);
        save_users_following_list(user_follow_list);
    }

    public static boolean is_user_already_following(Context context, String user_id) {
        ArrayList<String> user_follow_list = get_user_following_list();
        if (user_follow_list.contains(user_id))
            return true;
        return false;
    }


    public static void clear_following_list() {

    }


    public static void save_user_gender(String gender) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(), ShareprefConstantKeys.USER_GENDER_KEY, gender);
    }

    public static String get_user_gender() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(), ShareprefConstantKeys.USER_GENDER_KEY, "");
    }

    public static boolean is_profile_data_loaded() {
        return SharedPrefsUtils.getBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_PROFILE_DATA_CALLED, false);
    }

    public static void save_profile_data_called(boolean saved) {
        SharedPrefsUtils.setBooleanPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.IS_PROFILE_DATA_CALLED, saved);
    }


    public static ThirdPersonProfileRequestBody create_third_person_profile_req_body(String user_id) {

        ThirdPersonProfileRequestBody thirdPersonProfileRequestBody = new ThirdPersonProfileRequestBody();
        ThirdPersonProfileRequestBody.DataBean dataBean = new ThirdPersonProfileRequestBody.DataBean();
        ThirdPersonProfileRequestBody.DataBean.AttributesBean attributesBean = new ThirdPersonProfileRequestBody.DataBean.AttributesBean();

        attributesBean.setUser_id(user_id);
        dataBean.setAttributes(attributesBean);
        thirdPersonProfileRequestBody.setData(dataBean);
        return thirdPersonProfileRequestBody;
    }

    public static ThirdPersonPostBodyRequest create_third_person_post_req(String user_id, int first_next_page) {
        ThirdPersonPostBodyRequest thirdPersonPostBodyRequest = new ThirdPersonPostBodyRequest();
        ThirdPersonPostBodyRequest.DataBean dataBean = new ThirdPersonPostBodyRequest.DataBean();
        ThirdPersonPostBodyRequest.DataBean.AttributesBean attributesBean = new ThirdPersonPostBodyRequest.DataBean.AttributesBean();
        attributesBean.setPage_number(first_next_page);
        attributesBean.setUser_id(user_id);
        dataBean.setAttributes(attributesBean);
        thirdPersonPostBodyRequest.setData(dataBean);
        return thirdPersonPostBodyRequest;
    }

    public static void save_user_name(String username) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USERNAME_KEY, username);
    }

    public static String get_user_name() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USERNAME_KEY, "");
    }

    public static ChangeUserNameRequestBody create_change_username_body(String user_name) {
        ChangeUserNameRequestBody changeUserNameRequestBody = new ChangeUserNameRequestBody();
        ChangeUserNameRequestBody.DataBean dataBean = new ChangeUserNameRequestBody.DataBean();
        ChangeUserNameRequestBody.DataBean.AttributesBean attributesBean = new ChangeUserNameRequestBody.DataBean.AttributesBean();

        attributesBean.setUsername(user_name);
        dataBean.setAttributes(attributesBean);
        changeUserNameRequestBody.setData(dataBean);

        return changeUserNameRequestBody;
    }

    public static void save_user_status(String user_status) {
        SharedPrefsUtils.setStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_STATUS, user_status);
    }

    public static String get_user_status() {
        return SharedPrefsUtils.getStringPreference(PimpCarApplication.getApplicationInstance().getApplicationContext(),
                ShareprefConstantKeys.USER_STATUS, "");
    }

    public static ChangeUserStatusRequestBody create_change_user_status_body(String new_user_status) {
        ChangeUserStatusRequestBody changeUserStatusRequestBody = new ChangeUserStatusRequestBody();
        ChangeUserStatusRequestBody.DataBean dataBean = new ChangeUserStatusRequestBody.DataBean();
        ChangeUserStatusRequestBody.DataBean.AttributesBean attributesBean = new ChangeUserStatusRequestBody.DataBean.AttributesBean();
        attributesBean.setUser_status(new_user_status);
        dataBean.setAttributes(attributesBean);
        changeUserStatusRequestBody.setData(dataBean);
        return changeUserStatusRequestBody;
    }

    public static GetUserStoriesRequestBody create_request_body_for_stories(int stories_next_page_number) {
        GetUserStoriesRequestBody getUserStoriesRequestBody = new GetUserStoriesRequestBody();
        GetUserStoriesRequestBody.DataBean dataBean = new GetUserStoriesRequestBody.DataBean();
        GetUserStoriesRequestBody.DataBean.AttributesBean attributesBean = new GetUserStoriesRequestBody.DataBean.AttributesBean();
        attributesBean.setPage_number(stories_next_page_number);
        dataBean.setAttributes(attributesBean);
        getUserStoriesRequestBody.setData(dataBean);
        return getUserStoriesRequestBody;
    }

    public static ThirdPersonStoriesBodyRequest create_third_person_request_body(int page_number, String user_id) {
        ThirdPersonStoriesBodyRequest thirdPersonStoriesBodyRequest = new ThirdPersonStoriesBodyRequest();
        ThirdPersonStoriesBodyRequest.DataBean dataBean = new ThirdPersonStoriesBodyRequest.DataBean();
        ThirdPersonStoriesBodyRequest.DataBean.AttributesBean attributesBean = new ThirdPersonStoriesBodyRequest.DataBean.AttributesBean();

        attributesBean.setPage_number(page_number);
        attributesBean.setUser_id(user_id);
        dataBean.setAttributes(attributesBean);
        thirdPersonStoriesBodyRequest.setData(dataBean);
        return thirdPersonStoriesBodyRequest;
    }

    public static boolean does_like_is_done_by_user(List<UserPostBodyResponse.DataBean.PostsBean.PostLikesBean> post_likes) {
        boolean returnstatement = false;

        if (post_likes != null && post_likes.size() > 0) {
            for (int i = 0; i < post_likes.size(); i++) {
                if (post_likes.get(i).getUser_id().contentEquals(HelperMethods.get_user_id())) {
                    returnstatement = true;
                    break;
                }
            }
        }

        return returnstatement;
    }

    public static boolean does_post_like_is_done_by_user(List<HomeFeedResponse.DataBean.PostsBean.PostLikesBean> postLikesBeans) {
        boolean returnstatement = false;
        for (int i = 0; i < postLikesBeans.size(); i++) {
            if (postLikesBeans.get(i).getUser_id().contentEquals(HelperMethods.get_user_id())) {
                returnstatement = true;
                break;
            }
        }
        return returnstatement;
    }


    public static PostUserStoriesRequestBody create_request_body_for_stories_post(String caption, Bitmap image_bitmap) {
        List<String> bitmapimage = new ArrayList<>();
        PostUserStoriesRequestBody postUserStoriesRequestBody = new PostUserStoriesRequestBody();
        PostUserStoriesRequestBody.DataBean dataBean = new PostUserStoriesRequestBody.DataBean();
        PostUserStoriesRequestBody.DataBean.AttributesBean attributesBean = new PostUserStoriesRequestBody.DataBean.AttributesBean();
        attributesBean.setStory_caption(caption + "");
        attributesBean.setLocation("");
        String base_64 = Utils.bitmapToBase64(image_bitmap);
        bitmapimage.add(base_64);
        attributesBean.setStories_images(bitmapimage);
        dataBean.setAttributes(attributesBean);
        postUserStoriesRequestBody.setData(dataBean);
        return postUserStoriesRequestBody;

    }

    public static NotificationRequestBody create_request_body_for_notification(int page_number) {
        NotificationRequestBody notificationRequestBody = new NotificationRequestBody();
        NotificationRequestBody.DataBean dataBean = new NotificationRequestBody.DataBean();
        NotificationRequestBody.DataBean.AttributesBean attributesBean = new NotificationRequestBody.DataBean.AttributesBean();
        attributesBean.setPage_number(page_number);
        dataBean.setAttributes(attributesBean);
        notificationRequestBody.setData(dataBean);
        return notificationRequestBody;
    }


    public static String timeAgo(long time_ago) {
        long cur_time = (Calendar.getInstance().getTimeInMillis()) / 1000;
        long time_elapsed = cur_time - time_ago;
        long seconds = time_elapsed;
        int minutes = Math.round(time_elapsed / 60);
        int hours = Math.round(time_elapsed / 3600);
        int days = Math.round(time_elapsed / 86400);
        int weeks = Math.round(time_elapsed / 604800);
        int months = Math.round(time_elapsed / 2600640);
        int years = Math.round(time_elapsed / 31207680);

        // Seconds
        if (seconds <= 60) {
            return "just now";
        }
        //Minutes
        else if (minutes <= 60) {
            if (minutes == 1) {
                return "one minute ago";
            } else {
                return minutes + " minutes ago";
            }
        }
        //Hours
        else if (hours <= 24) {
            if (hours == 1) {
                return "an hour ago";
            } else {
                return hours + " hrs ago";
            }
        }
        //Days
        else if (days <= 7) {
            if (days == 1) {
                return "yesterday";
            } else {
                return days + " days ago";
            }
        }
        //Weeks
        else if (weeks <= 4.3) {
            if (weeks == 1) {
                return "a week ago";
            } else {
                return weeks + " weeks ago";
            }
        }
        //Months
        else if (months <= 12) {
            if (months == 1) {
                return "a month ago";
            } else {
                return months + " months ago";
            }
        }
        //Years
        else {
            if (years == 1) {
                return "one year ago";
            } else {
                return years + " years ago";
            }
        }
    }

    public static SearchFamousRequestBody create_famous_search_match(int next_page_number) {

        Timber.d("size of page " + next_page_number);
        SearchFamousRequestBody searchFamousRequestBody = new SearchFamousRequestBody();
        SearchFamousRequestBody.DataBean dataBean = new SearchFamousRequestBody.DataBean();
        SearchFamousRequestBody.DataBean.AttributesBean attributesBean = new SearchFamousRequestBody.DataBean.AttributesBean();

        attributesBean.setPage_number(next_page_number);

        dataBean.setAttributes(attributesBean);

        searchFamousRequestBody.setData(dataBean);

        return searchFamousRequestBody;
    }

    public static HomeFeedRequestBody create_home_feed_request_body(int first_next_page) {

        HomeFeedRequestBody homeFeedRequestBody = new HomeFeedRequestBody();

        HomeFeedRequestBody.DataBean dataBean = new HomeFeedRequestBody.DataBean();

        HomeFeedRequestBody.DataBean.AttributesBean attributesBean = new HomeFeedRequestBody.DataBean.AttributesBean();

        attributesBean.setPage_number(first_next_page);
        dataBean.setAttributes(attributesBean);
        homeFeedRequestBody.setData(dataBean);
        return homeFeedRequestBody;
    }

    public static FollowedAccountRequestBody followed_list(int next_page_number, String type) {

        Timber.d("size of page " + next_page_number);
        FollowedAccountRequestBody followedAccountRequestBody = new FollowedAccountRequestBody();
        FollowedAccountRequestBody.Data dataBean = new FollowedAccountRequestBody.Data();
        FollowedAccountRequestBody.Data.AttributesBean attributesBean = new FollowedAccountRequestBody.Data.AttributesBean();

        attributesBean.setNextPage(next_page_number);

        dataBean.setAttributes(attributesBean);

        followedAccountRequestBody.setData(dataBean);

        return followedAccountRequestBody;
    }

    public static FollowingAccountRequestBody following_list(int next_page_number, String type) {

        Timber.d("size of page " + next_page_number);
        FollowingAccountRequestBody followingAccountRequestBody = new FollowingAccountRequestBody();
        FollowingAccountRequestBody.Data dataBean = new FollowingAccountRequestBody.Data();
        FollowingAccountRequestBody.Data.AttributesBean attributesBean = new FollowingAccountRequestBody.Data.AttributesBean();

        attributesBean.setNextPage(next_page_number);

        dataBean.setAttributes(attributesBean);

        followingAccountRequestBody.setData(dataBean);

        return followingAccountRequestBody;
    }

    public static void LogValue(String TAG, String message) {
        int maxLogSize = 2000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            android.util.Log.d(TAG, message.substring(start, end));
        }
    }
}

package com.pimpcar.utils.cachcedata.observer;


public interface IDataChangeListener {

    void onDataChange(String key, String value);
}

package com.pimpcar.utils.cachcedata.util;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockUtil {
    public static ReentrantReadWriteLock getInstance() {
        return ReentrantLockHolder.lock;
    }


    private static class ReentrantLockHolder {
        static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    }
}

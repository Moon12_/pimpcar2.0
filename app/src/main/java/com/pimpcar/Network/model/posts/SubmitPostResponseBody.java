package com.pimpcar.Network.model.posts;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubmitPostResponseBody {
    /**
     * data : {"message":"post created successfully","post_updated":{"post_location":"location anything","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de2048fcf55e13713c16458","post_id":"Mes7P2Rw21575093379750","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1575093391,"post_user_name":"Henry Cavil","user_profile_pic_url":"","post_likes":[],"__v":0,"post_users_tag":"ybfJF916U1575093391290"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : post created successfully
         * post_updated : {"post_location":"location anything","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5de2048fcf55e13713c16458","post_id":"Mes7P2Rw21575093379750","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1575093391,"post_user_name":"Henry Cavil","user_profile_pic_url":"","post_likes":[],"__v":0,"post_users_tag":"ybfJF916U1575093391290"}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("post_updated")
        private PostUpdatedBean post_updated;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public PostUpdatedBean getPost_updated() {
            return post_updated;
        }

        public void setPost_updated(PostUpdatedBean post_updated) {
            this.post_updated = post_updated;
        }

        public static class PostUpdatedBean {
            /**
             * post_location : location anything
             * post_hash_tags : ["#henrycavil","#Superman","#ManOfSteel"]
             * post_comments : []
             * post_involved_users : []
             * post_reports : []
             * is_user_self_liked : false
             * post_id : Mes7P2Rw21575093379750
             * user_id : 3d248ae7-91c3-5d6e-898a-665435049dad
             * post_image_url : https://s3.us-east-2.amazonaws.com/pimpcarpost/3d248ae7-91c3-5d6e-898a-665435049dad/1575093380-ampOFYDw7RcQ9G8uwumzb6yXNIwO1YaT_3d248ae7-91c3-5d6e-898a-665435049dad_Mes7P2Rw21575093379750_Henry.jpg
             * post_description : My Superman and Man of Steel. Thank you Cavil
             * post_timestamp : 1575093391
             * post_user_name : Henry Cavil
             * user_profile_pic_url :
             * post_likes : []
             * post_users_tag : ybfJF916U1575093391290
             */

            @SerializedName("post_location")
            private String post_location;
            @SerializedName("is_user_self_liked")
            private boolean is_user_self_liked;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("post_image_url")
            private String post_image_url;
            @SerializedName("post_description")
            private String post_description;
            @SerializedName("post_timestamp")
            private int post_timestamp;
            @SerializedName("post_user_name")
            private String post_user_name;
            @SerializedName("user_profile_pic_url")
            private String user_profile_pic_url;
            @SerializedName("post_users_tag")
            private String post_users_tag;
            @SerializedName("post_hash_tags")
            private List<String> post_hash_tags;
            private List<?> post_comments;
            private List<?> post_involved_users;
            private List<?> post_reports;
            private List<?> post_likes;

            public String getPost_location() {
                return post_location;
            }

            public void setPost_location(String post_location) {
                this.post_location = post_location;
            }

            public boolean isIs_user_self_liked() {
                return is_user_self_liked;
            }

            public void setIs_user_self_liked(boolean is_user_self_liked) {
                this.is_user_self_liked = is_user_self_liked;
            }


            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPost_image_url() {
                return post_image_url;
            }

            public void setPost_image_url(String post_image_url) {
                this.post_image_url = post_image_url;
            }

            public String getPost_description() {
                return post_description;
            }

            public void setPost_description(String post_description) {
                this.post_description = post_description;
            }

            public int getPost_timestamp() {
                return post_timestamp;
            }

            public void setPost_timestamp(int post_timestamp) {
                this.post_timestamp = post_timestamp;
            }

            public String getPost_user_name() {
                return post_user_name;
            }

            public void setPost_user_name(String post_user_name) {
                this.post_user_name = post_user_name;
            }

            public String getUser_profile_pic_url() {
                return user_profile_pic_url;
            }

            public void setUser_profile_pic_url(String user_profile_pic_url) {
                this.user_profile_pic_url = user_profile_pic_url;
            }


            public String getPost_users_tag() {
                return post_users_tag;
            }

            public void setPost_users_tag(String post_users_tag) {
                this.post_users_tag = post_users_tag;
            }

            public List<String> getPost_hash_tags() {
                return post_hash_tags;
            }

            public void setPost_hash_tags(List<String> post_hash_tags) {
                this.post_hash_tags = post_hash_tags;
            }

            public List<?> getPost_comments() {
                return post_comments;
            }

            public void setPost_comments(List<?> post_comments) {
                this.post_comments = post_comments;
            }

            public List<?> getPost_involved_users() {
                return post_involved_users;
            }

            public void setPost_involved_users(List<?> post_involved_users) {
                this.post_involved_users = post_involved_users;
            }

            public List<?> getPost_reports() {
                return post_reports;
            }

            public void setPost_reports(List<?> post_reports) {
                this.post_reports = post_reports;
            }

            public List<?> getPost_likes() {
                return post_likes;
            }

            public void setPost_likes(List<?> post_likes) {
                this.post_likes = post_likes;
            }
        }
    }
}

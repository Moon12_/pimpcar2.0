package com.pimpcar.Network.model.userselfprofile;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserProfileFirstData {


    /**
     * data : {"message":"user profile data","user_data":{"usr_info":{"email":{"email_status":1,"email_id":"zack@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":"I am inevitable"},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"knightfall_protocol","user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","user_total_posts":28,"user_total_followers":0,"user_total_following":0,"user_followers":["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"],"user_following":["37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78",null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","3d248ae7-91c3-5d6e-898a-665435049dad","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64"],"user_stories":["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN."],"_id":"5d3ae8820b9de120544d93e3","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","fname":"Zack","lname":"Snyder","login_type":"email","__v":0,"hasshed_password":"$2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui","gender":"male"},"posts":[{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb17b0b9de120544d93f1","post_id":"0jBF30F5d1564389754918","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389755,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb18f0b9de120544d93f2","post_id":"Xh4UDH51x1564389774579","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389774-GkyB44mVhs6sgcGhRIvtGjZscTJPHlZX_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Xh4UDH51x1564389774579_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389775,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb19d0b9de120544d93f3","post_id":"frtEthtX11564389788685","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389788-BwDTZZyFxY6l2lj7BW7rpUzXeQ1Kk69w_22855eb3-fe40-5832-b54e-6f8042ba0f6d_frtEthtX11564389788685_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389789,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d514d993471a1437252a8e5","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565609369"}],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a20b9de120544d93f4","post_id":"DYDXIh0i61564389793541","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389793-NC0In2PjeqpmrSFwvqg4WKAeEnJ3vDPe_22855eb3-fe40-5832-b54e-6f8042ba0f6d_DYDXIh0i61564389793541_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389794,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a70b9de120544d93f5","post_id":"94EP4u-ZR1564389797215","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389799-67BhLg7vE8B8iLHYMCDseKMNbf4Ab3Ta_22855eb3-fe40-5832-b54e-6f8042ba0f6d_94EP4u-ZR1564389797215_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389799,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1aa0b9de120544d93f6","post_id":"TQd0uKMPR1564389802128","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389802-6v6tHGIPelr5UUizrjhNSbIl1HQKbW68_22855eb3-fe40-5832-b54e-6f8042ba0f6d_TQd0uKMPR1564389802128_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389802,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1af0b9de120544d93f7","post_id":"91PIV1E2V1564389807164","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389807-4ES3zAaeBGHueiZv2wilkJ5vmCAhMZGG_22855eb3-fe40-5832-b54e-6f8042ba0f6d_91PIV1E2V1564389807164_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389807,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1d50b9de120544d93f8","post_id":"iv3KPuZi01564389844423","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389844-pIziM3uji1Ry2CyO22tJSWqsiwp1JIzx_22855eb3-fe40-5832-b54e-6f8042ba0f6d_iv3KPuZi01564389844423_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389845,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1df0b9de120544d93f9","post_id":"isck31Eb81564389855100","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389855-b9Hbzpa1K1Iw2Pvbvg1UelCrT77qhp51_22855eb3-fe40-5832-b54e-6f8042ba0f6d_isck31Eb81564389855100_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389855,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1e50b9de120544d93fa","post_id":"oKA-JbGGU1564389861079","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389861-CVQ858MhHNkVbjbv51Eg9w6NzJyWdwDD_22855eb3-fe40-5832-b54e-6f8042ba0f6d_oKA-JbGGU1564389861079_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389861,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0}],"next_page":1}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : user profile data
         * user_data : {"usr_info":{"email":{"email_status":1,"email_id":"zack@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":"I am inevitable"},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"knightfall_protocol","user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","user_total_posts":28,"user_total_followers":0,"user_total_following":0,"user_followers":["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"],"user_following":["37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78",null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","3d248ae7-91c3-5d6e-898a-665435049dad","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64"],"user_stories":["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN."],"_id":"5d3ae8820b9de120544d93e3","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","fname":"Zack","lname":"Snyder","login_type":"email","__v":0,"hasshed_password":"$2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui","gender":"male"},"posts":[{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb17b0b9de120544d93f1","post_id":"0jBF30F5d1564389754918","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389755,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb18f0b9de120544d93f2","post_id":"Xh4UDH51x1564389774579","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389774-GkyB44mVhs6sgcGhRIvtGjZscTJPHlZX_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Xh4UDH51x1564389774579_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389775,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb19d0b9de120544d93f3","post_id":"frtEthtX11564389788685","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389788-BwDTZZyFxY6l2lj7BW7rpUzXeQ1Kk69w_22855eb3-fe40-5832-b54e-6f8042ba0f6d_frtEthtX11564389788685_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389789,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d514d993471a1437252a8e5","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565609369"}],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a20b9de120544d93f4","post_id":"DYDXIh0i61564389793541","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389793-NC0In2PjeqpmrSFwvqg4WKAeEnJ3vDPe_22855eb3-fe40-5832-b54e-6f8042ba0f6d_DYDXIh0i61564389793541_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389794,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a70b9de120544d93f5","post_id":"94EP4u-ZR1564389797215","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389799-67BhLg7vE8B8iLHYMCDseKMNbf4Ab3Ta_22855eb3-fe40-5832-b54e-6f8042ba0f6d_94EP4u-ZR1564389797215_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389799,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1aa0b9de120544d93f6","post_id":"TQd0uKMPR1564389802128","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389802-6v6tHGIPelr5UUizrjhNSbIl1HQKbW68_22855eb3-fe40-5832-b54e-6f8042ba0f6d_TQd0uKMPR1564389802128_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389802,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1af0b9de120544d93f7","post_id":"91PIV1E2V1564389807164","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389807-4ES3zAaeBGHueiZv2wilkJ5vmCAhMZGG_22855eb3-fe40-5832-b54e-6f8042ba0f6d_91PIV1E2V1564389807164_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389807,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1d50b9de120544d93f8","post_id":"iv3KPuZi01564389844423","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389844-pIziM3uji1Ry2CyO22tJSWqsiwp1JIzx_22855eb3-fe40-5832-b54e-6f8042ba0f6d_iv3KPuZi01564389844423_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389845,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1df0b9de120544d93f9","post_id":"isck31Eb81564389855100","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389855-b9Hbzpa1K1Iw2Pvbvg1UelCrT77qhp51_22855eb3-fe40-5832-b54e-6f8042ba0f6d_isck31Eb81564389855100_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389855,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1e50b9de120544d93fa","post_id":"oKA-JbGGU1564389861079","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389861-CVQ858MhHNkVbjbv51Eg9w6NzJyWdwDD_22855eb3-fe40-5832-b54e-6f8042ba0f6d_oKA-JbGGU1564389861079_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389861,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0}],"next_page":1}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("user_data")
        private UserDataBean user_data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public UserDataBean getUser_data() {
            return user_data;
        }

        public void setUser_data(UserDataBean user_data) {
            this.user_data = user_data;
        }

        public static class UserDataBean {
            /**
             * usr_info : {"email":{"email_status":1,"email_id":"zack@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":"I am inevitable"},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"knightfall_protocol","user_blocked_list":[],"user_reports":[],"profile_pic":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","user_total_posts":28,"user_total_followers":0,"user_total_following":0,"user_followers":["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"],"user_following":["37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78",null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","3d248ae7-91c3-5d6e-898a-665435049dad","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64"],"user_stories":["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN."],"_id":"5d3ae8820b9de120544d93e3","uid":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","fname":"Zack","lname":"Snyder","login_type":"email","__v":0,"hasshed_password":"$2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui","gender":"male"}
             * posts : [{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb17b0b9de120544d93f1","post_id":"0jBF30F5d1564389754918","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389755,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb18f0b9de120544d93f2","post_id":"Xh4UDH51x1564389774579","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389774-GkyB44mVhs6sgcGhRIvtGjZscTJPHlZX_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Xh4UDH51x1564389774579_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389775,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb19d0b9de120544d93f3","post_id":"frtEthtX11564389788685","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389788-BwDTZZyFxY6l2lj7BW7rpUzXeQ1Kk69w_22855eb3-fe40-5832-b54e-6f8042ba0f6d_frtEthtX11564389788685_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389789,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[{"_id":"5d514d993471a1437252a8e5","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","like_timestamp":"1565609369"}],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a20b9de120544d93f4","post_id":"DYDXIh0i61564389793541","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389793-NC0In2PjeqpmrSFwvqg4WKAeEnJ3vDPe_22855eb3-fe40-5832-b54e-6f8042ba0f6d_DYDXIh0i61564389793541_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389794,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1a70b9de120544d93f5","post_id":"94EP4u-ZR1564389797215","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389799-67BhLg7vE8B8iLHYMCDseKMNbf4Ab3Ta_22855eb3-fe40-5832-b54e-6f8042ba0f6d_94EP4u-ZR1564389797215_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389799,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1aa0b9de120544d93f6","post_id":"TQd0uKMPR1564389802128","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389802-6v6tHGIPelr5UUizrjhNSbIl1HQKbW68_22855eb3-fe40-5832-b54e-6f8042ba0f6d_TQd0uKMPR1564389802128_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389802,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1af0b9de120544d93f7","post_id":"91PIV1E2V1564389807164","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389807-4ES3zAaeBGHueiZv2wilkJ5vmCAhMZGG_22855eb3-fe40-5832-b54e-6f8042ba0f6d_91PIV1E2V1564389807164_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389807,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1d50b9de120544d93f8","post_id":"iv3KPuZi01564389844423","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389844-pIziM3uji1Ry2CyO22tJSWqsiwp1JIzx_22855eb3-fe40-5832-b54e-6f8042ba0f6d_iv3KPuZi01564389844423_Zack.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564389845,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1df0b9de120544d93f9","post_id":"isck31Eb81564389855100","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389855-b9Hbzpa1K1Iw2Pvbvg1UelCrT77qhp51_22855eb3-fe40-5832-b54e-6f8042ba0f6d_isck31Eb81564389855100_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389855,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0},{"post_location":"Pimp Car","post_hash_tags":["#benaffleck","#batman","#CapeCrusader"],"post_comments":[],"post_involved_users":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3eb1e50b9de120544d93fa","post_id":"oKA-JbGGU1564389861079","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389861-CVQ858MhHNkVbjbv51Eg9w6NzJyWdwDD_22855eb3-fe40-5832-b54e-6f8042ba0f6d_oKA-JbGGU1564389861079_Zack.jpg","post_description":"My Batman and he is tired fucked up and ready to screw things up with alien. Thank you Affleck","post_timestamp":1564389861,"post_user_name":"Zack Snyder","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg","post_likes":[],"post_users_tag":[],"__v":0}]
             * next_page : 1
             */

            @SerializedName("usr_info")
            private UsrInfoBean usr_info;
            @SerializedName("next_page")
            private int next_page;
            @SerializedName("posts")
            private List<PostsBean> posts;

            public UsrInfoBean getUsr_info() {
                return usr_info;
            }

            public void setUsr_info(UsrInfoBean usr_info) {
                this.usr_info = usr_info;
            }

            public int getNext_page() {
                return next_page;
            }

            public void setNext_page(int next_page) {
                this.next_page = next_page;
            }

            public List<PostsBean> getPosts() {
                return posts;
            }

            public void setPosts(List<PostsBean> posts) {
                this.posts = posts;
            }

            public static class UsrInfoBean {
                /**
                 * email : {"email_status":1,"email_id":"zack@gmail.com"}
                 * contact_number : {"contact_no_status":-1}
                 * user_status : {"current_status":"I am inevitable"}
                 * counts : {"failed_login":0}
                 * meta_data : {"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0}
                 * user_name : knightfall_protocol
                 * user_blocked_list : []
                 * user_reports : []
                 * profile_pic : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                 * user_total_posts : 28
                 * user_total_followers : 0
                 * user_total_following : 0
                 * user_followers : ["27f56235-371b-5106-a169-4cb86ff78d3e","37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","1ffa1d8b-249c-5aa5-9ef5-268d03d27507","47662af-cd07-50ab-b41d-6911662a37f2","c016cb14-10c2-500b-aaf6-6e2a629de04d","fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","fe70bb50-26d8-555c-b84e-85cdaa47e800","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","d85b1696-8414-594b-bbae-fbeca04cb49a","581c5f66-257f-51a3-b7f1-cdc8101f7507","699205a1-7dff-5d73-ad2e-e3eca6041e64","854d1587-286e-523c-8d2b-2bb625c6fa97","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","decd6131-1bc7-591e-a896-2388417189b6","147b9033-7292-5d82-a50e-7927a7b7eafd","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","0d019265-a6db-50c5-9324-02ab7764249d",null,null,"fea89666-b728-52e7-84ae-3e95bf8ffd00","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","3d248ae7-91c3-5d6e-898a-665435049dad","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","d577f9e5-4ef9-518c-89b9-3be0dd5d4221","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","e47662af-cd07-50ab-b41d-6911662a37f2","c6bc4395-0b4c-5c9a-8c4a-27c9448fdacd","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","d85b1696-8414-594b-bbae-fbeca04cb49a","decd6131-1bc7-591e-a896-2388417189b6","cd7dacd6-f577-5b6c-923b-0eb8a5cd8f96","27f56235-371b-5106-a169-4cb86ff78d3e","27f56235-371b-5106-a169-4cb86ff78d3e","3d248ae7-91c3-5d6e-898a-665435049dad","581c5f66-257f-51a3-b7f1-cdc8101f7507","b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c"]
                 * user_following : ["37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78",null,"c016cb14-10c2-500b-aaf6-6e2a629de04d","147b9033-7292-5d82-a50e-7927a7b7eafd","b76c6499-27c8-544c-b0a5-c7dfb5d07fa3","854d1587-286e-523c-8d2b-2bb625c6fa97","ac8958e2-d2c5-5361-8eed-ca0e8b6d55a1","27f56235-371b-5106-a169-4cb86ff78d3e","0d019265-a6db-50c5-9324-02ab7764249d","3d248ae7-91c3-5d6e-898a-665435049dad","01c8a8ef-7f29-5f26-8809-c1d81d4362b4","699205a1-7dff-5d73-ad2e-e3eca6041e64"]
                 * user_stories : ["$2a$10$9wbuzPZ5GRSKriGsuntCZeWjoarp8D0fxUWqQ/vy5O3iF3AuhdxPC","$2a$10$fbENxQ3qL1bbwZaHMqO30OECddW9Ht1RN/ENd0kE5coUljbnGK/.C","$2a$10$7MPgIsfIMlnWzJHY28yB0.n4ZWmwSyiYQUjg./OUGDiO2n6WT2yTW","$2a$10$mLsMvN2tKnSjuvFM.7KZwuIvdF1wRg6bwl91022uXfnWL7DN6zGiG","$2a$10$ohGvUGCqGWeOhKDNOMNe/OET8TntQksHICyxKeoqxcQJFkPbRKcTq","$2a$10$bRIUh1Y/u4FddH9j2qgUeOcwIoOleZ.kZ5Xx.GbCm38zW4NBlYT/u","$2a$10$UheZeVpKC1kxvfuM2B5hWuDr7mw8TJhjlhB/A7z3CcPf7kHTO3hjy","$2a$10$c3DSGVhqqrWuIQeNRLwqruX/WMiScwVf5d89nPayP9iyyVkua2Xz6","$2a$10$iaCaED48UoA9EsBCFl2NCOhUuZXw2vj5Zz9z.DsuuqwDk5QD8sXN."]
                 * _id : 5d3ae8820b9de120544d93e3
                 * uid : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * fname : Zack
                 * lname : Snyder
                 * login_type : email
                 * __v : 0
                 * hasshed_password : $2a$13$1RnEDdWCc68vFBqZIfL4f.5Wa2nhCz/2qOyEcmGGxpC2pfZS.g/Ui
                 * gender : male
                 */

                @SerializedName("email")
                private EmailBean email;
                @SerializedName("contact_number")
                private ContactNumberBean contact_number;
                @SerializedName("user_status")
                private UserStatusBean user_status;
                @SerializedName("counts")
                private CountsBean counts;
                @SerializedName("meta_data")
                private MetaDataBean meta_data;
                @SerializedName("user_name")
                private String user_name;
                @SerializedName("profile_pic")
                private String profile_pic;
                @SerializedName("user_total_posts")
                private int user_total_posts;
                @SerializedName("user_total_followers")
                private int user_total_followers;
                @SerializedName("user_total_following")
                private int user_total_following;
                @SerializedName("_id")
                private String _id;
                @SerializedName("uid")
                private String uid;
                @SerializedName("fname")
                private String fname;
                @SerializedName("lname")
                private String lname;
                @SerializedName("login_type")
                private String login_type;
                @SerializedName("__v")
                private int __v;
                @SerializedName("hasshed_password")
                private String hasshed_password;
                @SerializedName("gender")
                private String gender;
                @SerializedName("user_blocked_list")
                private List<String> user_blocked_list;
                @SerializedName("user_reports")
                private List<String> user_reports;
                @SerializedName("user_followers")
                private List<String> user_followers;
                @SerializedName("user_following")
                private ArrayList<String> user_following;
                @SerializedName("user_stories")
                private List<String> user_stories;

                public EmailBean getEmail() {
                    return email;
                }

                public void setEmail(EmailBean email) {
                    this.email = email;
                }

                public ContactNumberBean getContact_number() {
                    return contact_number;
                }

                public void setContact_number(ContactNumberBean contact_number) {
                    this.contact_number = contact_number;
                }

                public UserStatusBean getUser_status() {
                    return user_status;
                }

                public void setUser_status(UserStatusBean user_status) {
                    this.user_status = user_status;
                }

                public CountsBean getCounts() {
                    return counts;
                }

                public void setCounts(CountsBean counts) {
                    this.counts = counts;
                }

                public MetaDataBean getMeta_data() {
                    return meta_data;
                }

                public void setMeta_data(MetaDataBean meta_data) {
                    this.meta_data = meta_data;
                }

                public String getUser_name() {
                    return user_name;
                }

                public void setUser_name(String user_name) {
                    this.user_name = user_name;
                }

                public String getProfile_pic() {
                    return profile_pic;
                }

                public void setProfile_pic(String profile_pic) {
                    this.profile_pic = profile_pic;
                }

                public int getUser_total_posts() {
                    return user_total_posts;
                }

                public void setUser_total_posts(int user_total_posts) {
                    this.user_total_posts = user_total_posts;
                }

                public int getUser_total_followers() {
                    return user_total_followers;
                }

                public void setUser_total_followers(int user_total_followers) {
                    this.user_total_followers = user_total_followers;
                }

                public int getUser_total_following() {
                    return user_total_following;
                }

                public void setUser_total_following(int user_total_following) {
                    this.user_total_following = user_total_following;
                }

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUid() {
                    return uid;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public String getFname() {
                    return fname;
                }

                public void setFname(String fname) {
                    this.fname = fname;
                }

                public String getLname() {
                    return lname;
                }

                public void setLname(String lname) {
                    this.lname = lname;
                }

                public String getLogin_type() {
                    return login_type;
                }

                public void setLogin_type(String login_type) {
                    this.login_type = login_type;
                }

                public int get__v() {
                    return __v;
                }

                public void set__v(int __v) {
                    this.__v = __v;
                }

                public String getHasshed_password() {
                    return hasshed_password;
                }

                public void setHasshed_password(String hasshed_password) {
                    this.hasshed_password = hasshed_password;
                }

                public String getGender() {
                    return gender;
                }

                public void setGender(String gender) {
                    this.gender = gender;
                }

                public List<String> getUser_blocked_list() {
                    return user_blocked_list;
                }

                public void setUser_blocked_list(List<String> user_blocked_list) {
                    this.user_blocked_list = user_blocked_list;
                }

                public List<String> getUser_reports() {
                    return user_reports;
                }

                public void setUser_reports(List<String> user_reports) {
                    this.user_reports = user_reports;
                }

                public List<String> getUser_followers() {
                    return user_followers;
                }

                public void setUser_followers(ArrayList<String> user_followers) {
                    this.user_followers = user_followers;
                }

                public ArrayList<String> getUser_following() {
                    return user_following;
                }

                public void setUser_following(ArrayList<String> user_following) {
                    this.user_following = user_following;
                }

                public List<String> getUser_stories() {
                    return user_stories;
                }

                public void setUser_stories(List<String> user_stories) {
                    this.user_stories = user_stories;
                }

                public static class EmailBean {
                    /**
                     * email_status : 1
                     * email_id : zack@gmail.com
                     */

                    @SerializedName("email_status")
                    private int email_status;
                    @SerializedName("email_id")
                    private String email_id;

                    public int getEmail_status() {
                        return email_status;
                    }

                    public void setEmail_status(int email_status) {
                        this.email_status = email_status;
                    }

                    public String getEmail_id() {
                        return email_id;
                    }

                    public void setEmail_id(String email_id) {
                        this.email_id = email_id;
                    }
                }

                public static class ContactNumberBean {
                    /**
                     * contact_no_status : -1
                     */

                    @SerializedName("contact_no_status")
                    private int contact_no_status;

                    public int getContact_no_status() {
                        return contact_no_status;
                    }

                    public void setContact_no_status(int contact_no_status) {
                        this.contact_no_status = contact_no_status;
                    }
                }

                public static class UserStatusBean {
                    /**
                     * current_status : I am inevitable
                     */

                    @SerializedName("current_status")
                    private String current_status;

                    public String getCurrent_status() {
                        return current_status;
                    }

                    public void setCurrent_status(String current_status) {
                        this.current_status = current_status;
                    }
                }

                public static class CountsBean {
                    /**
                     * failed_login : 0
                     */

                    @SerializedName("failed_login")
                    private int failed_login;

                    public int getFailed_login() {
                        return failed_login;
                    }

                    public void setFailed_login(int failed_login) {
                        this.failed_login = failed_login;
                    }
                }

                public static class MetaDataBean {
                    /**
                     * password_change_otp : 0
                     * contact_number_verification_otp : 0
                     * email_verification_otp : 0
                     */

                    @SerializedName("password_change_otp")
                    private int password_change_otp;
                    @SerializedName("contact_number_verification_otp")
                    private int contact_number_verification_otp;
                    @SerializedName("email_verification_otp")
                    private int email_verification_otp;

                    public int getPassword_change_otp() {
                        return password_change_otp;
                    }

                    public void setPassword_change_otp(int password_change_otp) {
                        this.password_change_otp = password_change_otp;
                    }

                    public int getContact_number_verification_otp() {
                        return contact_number_verification_otp;
                    }

                    public void setContact_number_verification_otp(int contact_number_verification_otp) {
                        this.contact_number_verification_otp = contact_number_verification_otp;
                    }

                    public int getEmail_verification_otp() {
                        return email_verification_otp;
                    }

                    public void setEmail_verification_otp(int email_verification_otp) {
                        this.email_verification_otp = email_verification_otp;
                    }
                }
            }

            public static class PostsBean {
                /**
                 * post_location : Pimp Car
                 * post_hash_tags : ["#henrycavil","#Superman","#ManOfSteel"]
                 * post_comments : []
                 * post_involved_users : []
                 * post_reports : []
                 * is_user_self_liked : false
                 * _id : 5d3eb17b0b9de120544d93f1
                 * post_id : 0jBF30F5d1564389754918
                 * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                 * post_image_url : https://s3.us-east-2.amazonaws.com/pimpcarpost/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564389755-K0nBWO3ENJFOBx2hcT1vaTPk4ZpJgRyS_22855eb3-fe40-5832-b54e-6f8042ba0f6d_0jBF30F5d1564389754918_Zack.jpg
                 * post_description : My Superman and Man of Steel. Thank you Cavil
                 * post_timestamp : 1564389755
                 * post_user_name : Zack Snyder
                 * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564141733-mLwdOn7eTKXLTZunsIfRO3HcuXFs14eB_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                 * post_likes : []
                 * post_users_tag : []
                 * __v : 0
                 */

                @SerializedName("post_location")
                private String post_location;
                @SerializedName("is_user_self_liked")
                private boolean is_user_self_liked;
                @SerializedName("_id")
                private String _id;
                @SerializedName("post_id")
                private String post_id;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("post_image_url")
                private String post_image_url;
                @SerializedName("post_description")
                private String post_description;
                @SerializedName("post_timestamp")
                private long post_timestamp;
                @SerializedName("post_user_name")
                private String post_user_name;
                @SerializedName("user_profile_pic_url")
                private String user_profile_pic_url;
                @SerializedName("__v")
                private int __v;
                @SerializedName("post_hash_tags")
                private List<String> post_hash_tags;
                @SerializedName("post_comments")
                private List<String> post_comments;
                @SerializedName("post_involved_users")
                private List<String> post_involved_users;
                @SerializedName("post_reports")
                private List<String> post_reports;
                @SerializedName("post_likes")
                private List<PostLikesBean> post_likes;

                @SerializedName("post_users_tag")
                private String post_users_tag;

                public static class PostLikesBean {

                    /**
                     * _id : 5d514d993471a1437252a8e5
                     * user_id : 22855eb3-fe40-5832-b54e-6f8042ba0f6d
                     * user_name : Zack Snyder
                     * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/22855eb3-fe40-5832-b54e-6f8042ba0f6d/1564998229-HftJ9A6V6xv3zgqjaUKeCSUDfxJX22MV_22855eb3-fe40-5832-b54e-6f8042ba0f6d_22855eb3-fe40-5832-b54e-6f8042ba0f6d_Zack.jpg
                     * like_timestamp : 1565609369
                     */

                    @SerializedName("_id")
                    private String _id;
                    @SerializedName("user_id")
                    private String user_id;
                    @SerializedName("user_name")
                    private String user_name;
                    @SerializedName("user_profile_pic_url")
                    private String user_profile_pic_url;
                    @SerializedName("like_timestamp")
                    private String like_timestamp;

                    public String get_id() {
                        return _id;
                    }

                    public void set_id(String _id) {
                        this._id = _id;
                    }

                    public String getUser_id() {
                        return user_id;
                    }

                    public void setUser_id(String user_id) {
                        this.user_id = user_id;
                    }

                    public String getUser_name() {
                        return user_name;
                    }

                    public void setUser_name(String user_name) {
                        this.user_name = user_name;
                    }

                    public String getUser_profile_pic_url() {
                        return user_profile_pic_url;
                    }

                    public void setUser_profile_pic_url(String user_profile_pic_url) {
                        this.user_profile_pic_url = user_profile_pic_url;
                    }

                    public String getLike_timestamp() {
                        return like_timestamp;
                    }

                    public void setLike_timestamp(String like_timestamp) {
                        this.like_timestamp = like_timestamp;
                    }
                }

                public long getPost_timestamp() {
                    return post_timestamp;
                }

                public void setPost_timestamp(long post_timestamp) {
                    this.post_timestamp = post_timestamp;
                }

                public String getPost_users_tag() {
                    return post_users_tag;
                }

                public void setPost_users_tag(String post_users_tag) {
                    this.post_users_tag = post_users_tag;
                }

                public String getPost_location() {
                    return post_location;
                }

                public void setPost_location(String post_location) {
                    this.post_location = post_location;
                }

                public boolean isIs_user_self_liked() {
                    return is_user_self_liked;
                }

                public void setIs_user_self_liked(boolean is_user_self_liked) {
                    this.is_user_self_liked = is_user_self_liked;
                }

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getPost_id() {
                    return post_id;
                }

                public void setPost_id(String post_id) {
                    this.post_id = post_id;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public String getPost_image_url() {
                    return post_image_url;
                }

                public void setPost_image_url(String post_image_url) {
                    this.post_image_url = post_image_url;
                }

                public String getPost_description() {
                    return post_description;
                }

                public void setPost_description(String post_description) {
                    this.post_description = post_description;
                }


                public String getPost_user_name() {
                    return post_user_name;
                }

                public void setPost_user_name(String post_user_name) {
                    this.post_user_name = post_user_name;
                }

                public String getUser_profile_pic_url() {
                    return user_profile_pic_url;
                }

                public void setUser_profile_pic_url(String user_profile_pic_url) {
                    this.user_profile_pic_url = user_profile_pic_url;
                }

                public int get__v() {
                    return __v;
                }

                public void set__v(int __v) {
                    this.__v = __v;
                }

                public List<String> getPost_hash_tags() {
                    return post_hash_tags;
                }

                public void setPost_hash_tags(List<String> post_hash_tags) {
                    this.post_hash_tags = post_hash_tags;
                }

                public List<?> getPost_comments() {
                    return post_comments;
                }

                public void setPost_comments(List<String> post_comments) {
                    this.post_comments = post_comments;
                }

                public List<String> getPost_involved_users() {
                    return post_involved_users;
                }

                public void setPost_involved_users(List<String> post_involved_users) {
                    this.post_involved_users = post_involved_users;
                }

                public List<String> getPost_reports() {
                    return post_reports;
                }

                public void setPost_reports(List<String> post_reports) {
                    this.post_reports = post_reports;
                }

                public List<PostLikesBean> getPost_likes() {
                    return post_likes;
                }

                public void setPost_likes(List<PostLikesBean> post_likes) {
                    this.post_likes = post_likes;
                }

//                public List<String> getPost_users_tag() {
//                    return post_users_tag;
//                }
//
//                public void setPost_users_tag(List<String> post_users_tag) {
//                    this.post_users_tag = post_users_tag;
//                }
            }
        }
    }
}

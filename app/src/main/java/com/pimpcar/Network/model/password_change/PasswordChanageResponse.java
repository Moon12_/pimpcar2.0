package com.pimpcar.Network.model.password_change;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PasswordChanageResponse implements Parcelable {
    /**
     * data : {"message":"password changed successfully"}
     */


    public PasswordChanageResponse() {
    }

    @SerializedName("data")
    private DataBean data;

    protected PasswordChanageResponse(Parcel in) {
    }

    public static final Creator<PasswordChanageResponse> CREATOR = new Creator<PasswordChanageResponse>() {
        @Override
        public PasswordChanageResponse createFromParcel(Parcel in) {
            return new PasswordChanageResponse(in);
        }

        @Override
        public PasswordChanageResponse[] newArray(int size) {
            return new PasswordChanageResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : password changed successfully
         */

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

package com.pimpcar.Network.model.posts;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserPostsBodyRequest implements Parcelable {
    /**
     * data : {"attributes":{"page_number":1, "user_id":""}}
     */


    public UserPostsBodyRequest(){

    }


    @SerializedName("data")
    private DataBean data;

    protected UserPostsBodyRequest(Parcel in) {
    }

    public static final Creator<UserPostsBodyRequest> CREATOR = new Creator<UserPostsBodyRequest>() {
        @Override
        public UserPostsBodyRequest createFromParcel(Parcel in) {
            return new UserPostsBodyRequest(in);
        }

        @Override
        public UserPostsBodyRequest[] newArray(int size) {
            return new UserPostsBodyRequest[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"page_number":1}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * page_number : 1
             */

            @SerializedName("page_number")
            private int page_number;

            public int getPage_number() {
                return page_number;
            }

            public void setPage_number(int page_number) {
                this.page_number = page_number;
            }
        }
    }
}

package com.pimpcar.Network.model.signup;

import android.media.MediaDrm;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SignupBody implements Parcelable {
    /**
     * data : {"attributes":{"email":"clark","fname":"kent","lname":"wayne","password":"bruce@123","login_type":"email"}}
     */


    public SignupBody() {

    }

    @SerializedName("data")
    private DataBean data;

    protected SignupBody(Parcel in) {
    }

    public static final Creator<SignupBody> CREATOR = new Creator<SignupBody>() {
        @Override
        public SignupBody createFromParcel(Parcel in) {
            return new SignupBody(in);
        }

        @Override
        public SignupBody[] newArray(int size) {
            return new SignupBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"email":"clark","fname":"kent","lname":"wayne","password":"bruce@123","login_type":"email"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * email : clark
             * fname : kent
             * lname : wayne
             * password : bruce@123
             * login_type : email
             */

            @SerializedName("email")
            private String email;
            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;
            @SerializedName("password")
            private String password;
            @SerializedName("login_type")
            private String login_type;
            @SerializedName("dob")
            private String dob;
            @SerializedName("gender")
            private String gender;
            @SerializedName("profile_pic")
            private String profile_pic;
            @SerializedName("facebook_user_id")
            private String facebook_user_id;
            @SerializedName("facebook_token_access")
            private String facebook_token_access;


            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public String getLogin_type() {
                return login_type;
            }

            public void setLogin_type(String login_type) {
                this.login_type = login_type;
            }

            public String getDob() {
                return dob;
            }

            public void setDob(String dob) {
                this.dob = dob;
            }

            public String getGender() {
                return gender;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }

            public String getFacebook_user_id() {
                return facebook_user_id;
            }

            public void setFacebook_user_id(String facebook_user_id) {
                this.facebook_user_id = facebook_user_id;
            }

            public String getFacebook_token_access() {
                return facebook_token_access;
            }

            public void setFacebook_token_access(String facebook_token_access) {
                this.facebook_token_access = facebook_token_access;
            }
        }
    }
}

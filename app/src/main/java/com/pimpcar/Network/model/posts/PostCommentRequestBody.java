package com.pimpcar.Network.model.posts;

import com.google.gson.annotations.SerializedName;

public class PostCommentRequestBody {
    /**
     * data : {"attributes":{"post_id":"EeouYCCOr1563427591650","user_comment":"That picture is insane brother"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"post_id":"EeouYCCOr1563427591650","user_comment":"That picture is insane brother"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * post_id : EeouYCCOr1563427591650
             * user_comment : That picture is insane brother
             */

            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_comment")
            private String user_comment;

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_comment() {
                return user_comment;
            }

            public void setUser_comment(String user_comment) {
                this.user_comment = user_comment;
            }
        }
    }
}

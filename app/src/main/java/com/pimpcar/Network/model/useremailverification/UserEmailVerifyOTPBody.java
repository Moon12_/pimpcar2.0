package com.pimpcar.Network.model.useremailverification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserEmailVerifyOTPBody implements Parcelable {
    /**
     * data : {"attributes":{"email":"sahityakumarsuman@gmail.com","otp_verify":8895}}
     */

    public UserEmailVerifyOTPBody(){

    }

    @SerializedName("data")
    private DataBean data;

    protected UserEmailVerifyOTPBody(Parcel in) {
    }

    public static final Creator<UserEmailVerifyOTPBody> CREATOR = new Creator<UserEmailVerifyOTPBody>() {
        @Override
        public UserEmailVerifyOTPBody createFromParcel(Parcel in) {
            return new UserEmailVerifyOTPBody(in);
        }

        @Override
        public UserEmailVerifyOTPBody[] newArray(int size) {
            return new UserEmailVerifyOTPBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"email":"sahityakumarsuman@gmail.com","otp_verify":8895}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * email : sahityakumarsuman@gmail.com
             * otp_verify : 8895
             */

            @SerializedName("email")
            private String email;
            @SerializedName("otp_verify")
            private int otp_verify;

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public int getOtp_verify() {
                return otp_verify;
            }

            public void setOtp_verify(int otp_verify) {
                this.otp_verify = otp_verify;
            }
        }
    }
}

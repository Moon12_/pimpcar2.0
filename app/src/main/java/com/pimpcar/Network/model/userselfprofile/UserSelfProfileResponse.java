package com.pimpcar.Network.model.userselfprofile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserSelfProfileResponse implements Parcelable {
    /**
     * data : {"message":"user authenticated successfully","user_details":{"fname":"Sahitya","lname":"Kumar Suman","login_type":"email","email_id":"sahityakumarsuman@gmail.com","email_status":1}}
     */

    @SerializedName("data")
    private DataBean data;

    protected UserSelfProfileResponse(Parcel in) {
    }

    public static final Creator<UserSelfProfileResponse> CREATOR = new Creator<UserSelfProfileResponse>() {
        @Override
        public UserSelfProfileResponse createFromParcel(Parcel in) {
            return new UserSelfProfileResponse(in);
        }

        @Override
        public UserSelfProfileResponse[] newArray(int size) {
            return new UserSelfProfileResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : user authenticated successfully
         * user_details : {"fname":"Sahitya","lname":"Kumar Suman","login_type":"email","email_id":"sahityakumarsuman@gmail.com","email_status":1}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("user_details")
        private UserDetailsBean user_details;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public UserDetailsBean getUser_details() {
            return user_details;
        }

        public void setUser_details(UserDetailsBean user_details) {
            this.user_details = user_details;
        }

        public static class UserDetailsBean {
            /**
             * fname : Sahitya
             * lname : Kumar Suman
             * login_type : email
             * email_id : sahityakumarsuman@gmail.com
             * email_status : 1
             */

            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;
            @SerializedName("login_type")
            private String login_type;
            @SerializedName("email_id")
            private String email_id;
            @SerializedName("email_status")
            private int email_status;

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public String getLogin_type() {
                return login_type;
            }

            public void setLogin_type(String login_type) {
                this.login_type = login_type;
            }

            public String getEmail_id() {
                return email_id;
            }

            public void setEmail_id(String email_id) {
                this.email_id = email_id;
            }

            public int getEmail_status() {
                return email_status;
            }

            public void setEmail_status(int email_status) {
                this.email_status = email_status;
            }
        }
    }
}

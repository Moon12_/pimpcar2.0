package com.pimpcar.Network.model.posts;

import com.google.gson.annotations.SerializedName;

public class PostLikeDislikeRequestBody {
    /**
     * data : {"attributes":{"post_id":"EeouYCCOr1563427591650"}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * attributes : {"post_id":"EeouYCCOr1563427591650"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * post_id : EeouYCCOr1563427591650
             */

            @SerializedName("post_id")
            private String post_id;

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }
        }
    }
}

package com.pimpcar.Network.model.contact_number_verification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OTPVerifyResponse implements Parcelable {
    /**
     * data : {"message":"7506224347 otp verified successfully "}
     */

    @SerializedName("data")
    private DataBean data;

    protected OTPVerifyResponse(Parcel in) {
    }

    public static final Creator<OTPVerifyResponse> CREATOR = new Creator<OTPVerifyResponse>() {
        @Override
        public OTPVerifyResponse createFromParcel(Parcel in) {
            return new OTPVerifyResponse(in);
        }

        @Override
        public OTPVerifyResponse[] newArray(int size) {
            return new OTPVerifyResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : 7506224347 otp verified successfully
         */

        @SerializedName("message")
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

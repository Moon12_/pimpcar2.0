package com.pimpcar.Network.model.user_dob;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserDOBUpdateBody implements Parcelable {


    public UserDOBUpdateBody(){

    }

    /**
     * data : {"attributes":{"user_dob":"23-01-2002"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected UserDOBUpdateBody(Parcel in) {
    }

    public static final Creator<UserDOBUpdateBody> CREATOR = new Creator<UserDOBUpdateBody>() {
        @Override
        public UserDOBUpdateBody createFromParcel(Parcel in) {
            return new UserDOBUpdateBody(in);
        }

        @Override
        public UserDOBUpdateBody[] newArray(int size) {
            return new UserDOBUpdateBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"user_dob":"23-01-2002"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * user_dob : 23-01-2002
             */

            @SerializedName("user_dob")
            private String user_dob;

            public String getUser_dob() {
                return user_dob;
            }

            public void setUser_dob(String user_dob) {
                this.user_dob = user_dob;
            }
        }
    }
}

package com.pimpcar.Network.model.contact_number_verification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OTPVerifyBody implements Parcelable {


    public OTPVerifyBody(){

    }

    /**
     * data : {"attributes":{"otp_verify":7488}}
     */

    @SerializedName("data")
    private DataBean data;

    protected OTPVerifyBody(Parcel in) {
    }

    public static final Creator<OTPVerifyBody> CREATOR = new Creator<OTPVerifyBody>() {
        @Override
        public OTPVerifyBody createFromParcel(Parcel in) {
            return new OTPVerifyBody(in);
        }

        @Override
        public OTPVerifyBody[] newArray(int size) {
            return new OTPVerifyBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"otp_verify":7488}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * otp_verify : 7488
             */

            @SerializedName("otp_verify")
            private int otp_verify;

            public int getOtp_verify() {
                return otp_verify;
            }

            public void setOtp_verify(int otp_verify) {
                this.otp_verify = otp_verify;
            }
        }
    }
}

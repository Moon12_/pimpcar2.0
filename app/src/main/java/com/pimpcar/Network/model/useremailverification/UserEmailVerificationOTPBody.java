package com.pimpcar.Network.model.useremailverification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserEmailVerificationOTPBody implements Parcelable {


    public UserEmailVerificationOTPBody(){

    }


    /**
     * data : {"attributes":{"email":"sahityakumarsuman@gmail.com"}}
     */

    @SerializedName("data")
    private DataBean data;

    protected UserEmailVerificationOTPBody(Parcel in) {
    }

    public static final Creator<UserEmailVerificationOTPBody> CREATOR = new Creator<UserEmailVerificationOTPBody>() {
        @Override
        public UserEmailVerificationOTPBody createFromParcel(Parcel in) {
            return new UserEmailVerificationOTPBody(in);
        }

        @Override
        public UserEmailVerificationOTPBody[] newArray(int size) {
            return new UserEmailVerificationOTPBody[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * attributes : {"email":"sahityakumarsuman@gmail.com"}
         */

        @SerializedName("attributes")
        private AttributesBean attributes;

        public AttributesBean getAttributes() {
            return attributes;
        }

        public void setAttributes(AttributesBean attributes) {
            this.attributes = attributes;
        }

        public static class AttributesBean {
            /**
             * email : sahityakumarsuman@gmail.com
             */

            @SerializedName("email")
            private String email;

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }
        }
    }
}

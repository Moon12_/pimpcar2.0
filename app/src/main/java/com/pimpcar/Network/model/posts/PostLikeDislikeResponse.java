package com.pimpcar.Network.model.posts;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostLikeDislikeResponse {
    /**
     * data : {"message":"post liked successfully","updated_post":{"post_location":"Pimp Car","post_hash_tags":["meera","mom","batman"],"post_comments":["5d30341a637b3bbe69d00367"],"post_involved_users":[],"post_reports":[],"_id":"5d3003155e10eeb8dde526ca","post_users_tag":[],"post_id":"EeouYCCOr1563427591650","user_id":"5b48f793-76e4-5908-b0b7-d5da37fed68e","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/5b48f793-76e4-5908-b0b7-d5da37fed68e/1563427591-MFCNkC4S56SaB3gTppnhsu1CFZaX3z1U_5b48f793-76e4-5908-b0b7-d5da37fed68e_EeouYCCOr1563427591650_clark.jpg","post_description":"this is my profile","post_timestamp":"1563427605","post_user_name":"clark kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg","post_likes":[{"_id":"5d3061047c41b3c443580694","user_id":"5b48f793-76e4-5908-b0b7-d5da37fed68e","user_name":"clark kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg","like_timestamp":"1563451652"}]}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : post liked successfully
         * updated_post : {"post_location":"Pimp Car","post_hash_tags":["meera","mom","batman"],"post_comments":["5d30341a637b3bbe69d00367"],"post_involved_users":[],"post_reports":[],"_id":"5d3003155e10eeb8dde526ca","post_users_tag":[],"post_id":"EeouYCCOr1563427591650","user_id":"5b48f793-76e4-5908-b0b7-d5da37fed68e","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/5b48f793-76e4-5908-b0b7-d5da37fed68e/1563427591-MFCNkC4S56SaB3gTppnhsu1CFZaX3z1U_5b48f793-76e4-5908-b0b7-d5da37fed68e_EeouYCCOr1563427591650_clark.jpg","post_description":"this is my profile","post_timestamp":"1563427605","post_user_name":"clark kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg","post_likes":[{"_id":"5d3061047c41b3c443580694","user_id":"5b48f793-76e4-5908-b0b7-d5da37fed68e","user_name":"clark kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg","like_timestamp":"1563451652"}]}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("updated_post")
        private UpdatedPostBean updated_post;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public UpdatedPostBean getUpdated_post() {
            return updated_post;
        }

        public void setUpdated_post(UpdatedPostBean updated_post) {
            this.updated_post = updated_post;
        }

        public static class UpdatedPostBean {
            /**
             * post_location : Pimp Car
             * post_hash_tags : ["meera","mom","batman"]
             * post_comments : ["5d30341a637b3bbe69d00367"]
             * post_involved_users : []
             * post_reports : []
             * _id : 5d3003155e10eeb8dde526ca
             * post_users_tag : []
             * post_id : EeouYCCOr1563427591650
             * user_id : 5b48f793-76e4-5908-b0b7-d5da37fed68e
             * post_image_url : https://s3.us-east-2.amazonaws.com/pimpcarpost/5b48f793-76e4-5908-b0b7-d5da37fed68e/1563427591-MFCNkC4S56SaB3gTppnhsu1CFZaX3z1U_5b48f793-76e4-5908-b0b7-d5da37fed68e_EeouYCCOr1563427591650_clark.jpg
             * post_description : this is my profile
             * post_timestamp : 1563427605
             * post_user_name : clark kent
             * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg
             * post_likes : [{"_id":"5d3061047c41b3c443580694","user_id":"5b48f793-76e4-5908-b0b7-d5da37fed68e","user_name":"clark kent","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg","like_timestamp":"1563451652"}]
             */

            @SerializedName("post_location")
            private String post_location;
            @SerializedName("_id")
            private String _id;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("post_image_url")
            private String post_image_url;
            @SerializedName("post_description")
            private String post_description;
            @SerializedName("post_timestamp")
            private String post_timestamp;
            @SerializedName("post_user_name")
            private String post_user_name;
            @SerializedName("user_profile_pic_url")
            private String user_profile_pic_url;
            @SerializedName("post_hash_tags")
            private List<String> post_hash_tags;
            @SerializedName("post_comments")
            private List<String> post_comments;
            @SerializedName("post_involved_users")
            private List<String> post_involved_users;
            @SerializedName("post_reports")
            private List<String> post_users_tag;
            @SerializedName("post_likes")
            private List<PostLikesBean> post_likes;

            public String getPost_location() {
                return post_location;
            }

            public void setPost_location(String post_location) {
                this.post_location = post_location;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPost_image_url() {
                return post_image_url;
            }

            public void setPost_image_url(String post_image_url) {
                this.post_image_url = post_image_url;
            }

            public String getPost_description() {
                return post_description;
            }

            public void setPost_description(String post_description) {
                this.post_description = post_description;
            }

            public String getPost_timestamp() {
                return post_timestamp;
            }

            public void setPost_timestamp(String post_timestamp) {
                this.post_timestamp = post_timestamp;
            }

            public String getPost_user_name() {
                return post_user_name;
            }

            public void setPost_user_name(String post_user_name) {
                this.post_user_name = post_user_name;
            }

            public String getUser_profile_pic_url() {
                return user_profile_pic_url;
            }

            public void setUser_profile_pic_url(String user_profile_pic_url) {
                this.user_profile_pic_url = user_profile_pic_url;
            }

            public List<String> getPost_hash_tags() {
                return post_hash_tags;
            }

            public void setPost_hash_tags(List<String> post_hash_tags) {
                this.post_hash_tags = post_hash_tags;
            }

            public List<String> getPost_comments() {
                return post_comments;
            }

            public void setPost_comments(List<String> post_comments) {
                this.post_comments = post_comments;
            }

            public List<String> getPost_involved_users() {
                return post_involved_users;
            }

            public void setPost_involved_users(List<String> post_involved_users) {
                this.post_involved_users = post_involved_users;
            }


            public List<String> getPost_users_tag() {
                return post_users_tag;
            }

            public void setPost_users_tag(List<String> post_users_tag) {
                this.post_users_tag = post_users_tag;
            }

            public List<PostLikesBean> getPost_likes() {
                return post_likes;
            }

            public void setPost_likes(List<PostLikesBean> post_likes) {
                this.post_likes = post_likes;
            }

            public static class PostLikesBean {
                /**
                 * _id : 5d3061047c41b3c443580694
                 * user_id : 5b48f793-76e4-5908-b0b7-d5da37fed68e
                 * user_name : clark kent
                 * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/5b48f793-76e4-5908-b0b7-d5da37fed68e/1560949294-XlS8Wnw8aOvORgi3fMliB8KeUQeBoQwk_5b48f793-76e4-5908-b0b7-d5da37fed68e_5b48f793-76e4-5908-b0b7-d5da37fed68e_clark.jpg
                 * like_timestamp : 1563451652
                 */

                @SerializedName("_id")
                private String _id;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("user_name")
                private String user_name;
                @SerializedName("user_profile_pic_url")
                private String user_profile_pic_url;
                @SerializedName("like_timestamp")
                private String like_timestamp;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public String getUser_name() {
                    return user_name;
                }

                public void setUser_name(String user_name) {
                    this.user_name = user_name;
                }

                public String getUser_profile_pic_url() {
                    return user_profile_pic_url;
                }

                public void setUser_profile_pic_url(String user_profile_pic_url) {
                    this.user_profile_pic_url = user_profile_pic_url;
                }

                public String getLike_timestamp() {
                    return like_timestamp;
                }

                public void setLike_timestamp(String like_timestamp) {
                    this.like_timestamp = like_timestamp;
                }
            }
        }
    }
}

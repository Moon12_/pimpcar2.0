package com.pimpcar.Network.model.useremailverification;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserEMailOTPResponse implements Parcelable {
    /**
     * data : {"message":"sahityakumarsuman@gmail.com otp verified successfully "}
     */

    @SerializedName("data")
    private DataBean data;

    protected UserEMailOTPResponse(Parcel in) {
    }

    public static final Creator<UserEMailOTPResponse> CREATOR = new Creator<UserEMailOTPResponse>() {
        @Override
        public UserEMailOTPResponse createFromParcel(Parcel in) {
            return new UserEMailOTPResponse(in);
        }

        @Override
        public UserEMailOTPResponse[] newArray(int size) {
            return new UserEMailOTPResponse[size];
        }
    };

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static class DataBean {
        /**
         * message : sahityakumarsuman@gmail.com otp verified successfully
         */

        @SerializedName("message")
        private String message;

        @SerializedName("jwt_token")
        private String jwt_token;

        public String getJwt_token() {
            return jwt_token;
        }

        public void setJwt_token(String jwt_token) {
            this.jwt_token = jwt_token;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}

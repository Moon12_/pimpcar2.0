package com.pimpcar.Network.model.user_search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSearchResponseBody {
    /**
     * data : {"message":"searched_data","serached_data":[{"email":{"email_status":-1,"email_id":"rosarch@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"","user_blocked_list":[],"user_reports":[],"profile_pic":"","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"user_stories":[],"notification":[],"_id":"5d39923360278e686925c13d","uid":"01c8a8ef-7f29-5f26-8809-c1d81d4362b4","fname":"Roarch","lname":"Watchmen","login_type":"email","__v":0,"hasshed_password":"$2a$13$AjI5eZPX9qa8PFwvE3a18.2WcB41vvMCfcFNjpE59yCXTeMPOABuW"}]}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : searched_data
         * serached_data : [{"email":{"email_status":-1,"email_id":"rosarch@gmail.com"},"contact_number":{"contact_no_status":-1},"user_status":{"current_status":""},"counts":{"failed_login":0},"meta_data":{"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0},"user_name":"","user_blocked_list":[],"user_reports":[],"profile_pic":"","user_total_posts":0,"user_total_followers":0,"user_total_following":0,"user_followers":[],"user_following":[],"user_stories":[],"notification":[],"_id":"5d39923360278e686925c13d","uid":"01c8a8ef-7f29-5f26-8809-c1d81d4362b4","fname":"Roarch","lname":"Watchmen","login_type":"email","__v":0,"hasshed_password":"$2a$13$AjI5eZPX9qa8PFwvE3a18.2WcB41vvMCfcFNjpE59yCXTeMPOABuW"}]
         */

        @SerializedName("message")
        private String message;
        @SerializedName("serached_data")
        private List<SerachedDataBean> serached_data;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public List<SerachedDataBean> getSerached_data() {
            return serached_data;
        }

        public void setSerached_data(List<SerachedDataBean> serached_data) {
            this.serached_data = serached_data;
        }

        public static class SerachedDataBean {
            /**
             * email : {"email_status":-1,"email_id":"rosarch@gmail.com"}
             * contact_number : {"contact_no_status":-1}
             * user_status : {"current_status":""}
             * counts : {"failed_login":0}
             * meta_data : {"password_change_otp":0,"contact_number_verification_otp":0,"email_verification_otp":0}
             * user_name :
             * user_blocked_list : []
             * user_reports : []
             * profile_pic :
             * user_total_posts : 0
             * user_total_followers : 0
             * user_total_following : 0
             * user_followers : []
             * user_following : []
             * user_stories : []
             * notification : []
             * _id : 5d39923360278e686925c13d
             * uid : 01c8a8ef-7f29-5f26-8809-c1d81d4362b4
             * fname : Roarch
             * lname : Watchmen
             * login_type : email
             * __v : 0
             * hasshed_password : $2a$13$AjI5eZPX9qa8PFwvE3a18.2WcB41vvMCfcFNjpE59yCXTeMPOABuW
             */

            @SerializedName("user_followers")
            private List<String> user_followers;
            @SerializedName("user_tagged")
            private List<UserTageBean> user_tagged;

            @SerializedName("email")
            private EmailBean email;
            @SerializedName("contact_number")
            private ContactNumberBean contact_number;
            @SerializedName("user_status")
            private UserStatusBean user_status;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("profile_pic")
            private String profile_pic;
            @SerializedName("user_total_posts")
            private int user_total_posts;
            @SerializedName("user_total_followers")
            private int user_total_followers;
            @SerializedName("user_total_following")
            private int user_total_following;
            @SerializedName("uid")
            private String uid;
            @SerializedName("fname")
            private String fname;
            @SerializedName("lname")
            private String lname;

            public EmailBean getEmail() {
                return email;
            }

            public void setEmail(EmailBean email) {
                this.email = email;
            }

            public ContactNumberBean getContact_number() {
                return contact_number;
            }

            public void setContact_number(ContactNumberBean contact_number) {
                this.contact_number = contact_number;
            }

            public UserStatusBean getUser_status() {
                return user_status;
            }

            public void setUser_status(UserStatusBean user_status) {
                this.user_status = user_status;
            }


            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getProfile_pic() {
                return profile_pic;
            }

            public void setProfile_pic(String profile_pic) {
                this.profile_pic = profile_pic;
            }

            public int getUser_total_posts() {
                return user_total_posts;
            }

            public void setUser_total_posts(int user_total_posts) {
                this.user_total_posts = user_total_posts;
            }

            public int getUser_total_followers() {
                return user_total_followers;
            }

            public void setUser_total_followers(int user_total_followers) {
                this.user_total_followers = user_total_followers;
            }

            public int getUser_total_following() {
                return user_total_following;
            }

            public void setUser_total_following(int user_total_following) {
                this.user_total_following = user_total_following;
            }


            public String getUid() {
                return uid;
            }

            public void setUid(String uid) {
                this.uid = uid;
            }

            public String getFname() {
                return fname;
            }

            public void setFname(String fname) {
                this.fname = fname;
            }

            public String getLname() {
                return lname;
            }

            public void setLname(String lname) {
                this.lname = lname;
            }

            public List<String> getUser_followers() {
                return user_followers;
            }

            public void setUser_followers(List<String> user_followers) {
                this.user_followers = user_followers;
            }

            public List<UserTageBean> getUser_tagged() {
                return user_tagged;
            }

            public void setUser_tagged(List<UserTageBean> user_tagged) {
                this.user_tagged = user_tagged;
            }

            public static class EmailBean {
                /**
                 * email_status : -1
                 * email_id : rosarch@gmail.com
                 */

                @SerializedName("email_status")
                private int email_status;
                @SerializedName("email_id")
                private String email_id;

                public int getEmail_status() {
                    return email_status;
                }

                public void setEmail_status(int email_status) {
                    this.email_status = email_status;
                }

                public String getEmail_id() {
                    return email_id;
                }

                public void setEmail_id(String email_id) {
                    this.email_id = email_id;
                }
            }

            public static class ContactNumberBean {
                /**
                 * contact_no_status : -1
                 */

                @SerializedName("contact_no_status")
                private int contact_no_status;

                public int getContact_no_status() {
                    return contact_no_status;
                }

                public void setContact_no_status(int contact_no_status) {
                    this.contact_no_status = contact_no_status;
                }
            }

            public static class UserStatusBean {
                /**
                 * current_status :
                 */

                @SerializedName("current_status")
                private String current_status;

                public String getCurrent_status() {
                    return current_status;
                }

                public void setCurrent_status(String current_status) {
                    this.current_status = current_status;
                }
            }

            public static class  UserTageBean{


                @SerializedName("name")
                private String name;
                @SerializedName("id")
                private Integer id;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

            }


        }
    }
}

package com.pimpcar.Network.model.posts;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserPostDetailsResponseBody {


    /**
     * data : {"message":"post details","post":{"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["eFWqGBBUZ1564054336052","XykG-7eLm1564054337759","hm2jXmEl31564054339240","h2ilrJ4yX1564054340486","jzSxQWSSX1564054357188","2ibqnutMX1564054358727","nncKjB8x51564054359784","QH5DtI50i1564054360760","Bzcy9RHIm1564054361754","r-SfRwbkj1564054363815","VfQGkF5_g1564054364830","SL_UHblHP1564054366231","DZPSNFc3H1564054367219","2P8Q0hI4x1564054368177","fh1Mkwke71564054369060","1IVaJW8yi1564054369946","0oLlYj20n1564054377177","nElgOJ_JN1564054378169","bjoC1oMOr1564054379084","3UDeoTNIG1564054379964","yzNnTX-RY1564054380866"],"post_involved_users":[],"post_users_tag":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3992a960278e686925c13e","post_id":"RZEJX2MwH1564054176241","user_id":"147b9033-7292-5d82-a50e-7927a7b7eafd","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/147b9033-7292-5d82-a50e-7927a7b7eafd/1564054176-eHhCIZpBxahAs74xdRuWdn7LaNdNlylM_147b9033-7292-5d82-a50e-7927a7b7eafd_RZEJX2MwH1564054176241_John.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564054185,"post_user_name":"John Wick","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","post_likes":[{"_id":"5d39945560278e686925c159","user_id":"27f56235-371b-5106-a169-4cb86ff78d3e","user_name":"Shakti Pandit","user_profile_pic_url":"","like_timestamp":"1564054613"},{"_id":"5d3994f760278e686925c15b","user_id":"37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","user_name":"Sidharth Kushwaha","user_profile_pic_url":"","like_timestamp":"1564054775"},{"_id":"5d39953160278e686925c15d","user_id":"c016cb14-10c2-500b-aaf6-6e2a629de04d","user_name":"Monika Fatty","user_profile_pic_url":"","like_timestamp":"1564054833"},{"_id":"5d39955960278e686925c15f","user_id":"fea89666-b728-52e7-84ae-3e95bf8ffd00","user_name":"Vipul Baccha","user_profile_pic_url":"","like_timestamp":"1564054873"},{"_id":"5d39958460278e686925c161","user_id":"699205a1-7dff-5d73-ad2e-e3eca6041e64","user_name":"Dooms Day","user_profile_pic_url":"","like_timestamp":"1564054916"},{"_id":"5d3995b160278e686925c163","user_id":"d85b1696-8414-594b-bbae-fbeca04cb49a","user_name":"Iron Man","user_profile_pic_url":"","like_timestamp":"1564054961"},{"_id":"5d3995db60278e686925c165","user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","user_name":"Bruce Wayne","user_profile_pic_url":"","like_timestamp":"1564055003"},{"_id":"5d39962660278e686925c167","user_id":"d577f9e5-4ef9-518c-89b9-3be0dd5d4221","user_name":"Clark Kent","user_profile_pic_url":"","like_timestamp":"1564055078"},{"_id":"5d39967c60278e686925c169","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"","like_timestamp":"1564055164"},{"_id":"5d3996ac60278e686925c16b","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","user_name":"Henry Cavil","user_profile_pic_url":"","like_timestamp":"1564055212"},{"_id":"5d39979260278e686925c16d","user_id":"e47662af-cd07-50ab-b41d-6911662a37f2","user_name":"Sailesh Sharma","user_profile_pic_url":"","like_timestamp":"1564055442"},{"_id":"5d3fef98e7227a1ece052a88","user_id":"147b9033-7292-5d82-a50e-7927a7b7eafd","user_name":"John Wick","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","like_timestamp":"1564471192"}],"__v":0}}
     */

    @SerializedName("data")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * message : post details
         * post : {"post_location":"Pimp Car","post_hash_tags":["#henrycavil","#Superman","#ManOfSteel"],"post_comments":["eFWqGBBUZ1564054336052","XykG-7eLm1564054337759","hm2jXmEl31564054339240","h2ilrJ4yX1564054340486","jzSxQWSSX1564054357188","2ibqnutMX1564054358727","nncKjB8x51564054359784","QH5DtI50i1564054360760","Bzcy9RHIm1564054361754","r-SfRwbkj1564054363815","VfQGkF5_g1564054364830","SL_UHblHP1564054366231","DZPSNFc3H1564054367219","2P8Q0hI4x1564054368177","fh1Mkwke71564054369060","1IVaJW8yi1564054369946","0oLlYj20n1564054377177","nElgOJ_JN1564054378169","bjoC1oMOr1564054379084","3UDeoTNIG1564054379964","yzNnTX-RY1564054380866"],"post_involved_users":[],"post_users_tag":[],"post_reports":[],"is_user_self_liked":false,"_id":"5d3992a960278e686925c13e","post_id":"RZEJX2MwH1564054176241","user_id":"147b9033-7292-5d82-a50e-7927a7b7eafd","post_image_url":"https://s3.us-east-2.amazonaws.com/pimpcarpost/147b9033-7292-5d82-a50e-7927a7b7eafd/1564054176-eHhCIZpBxahAs74xdRuWdn7LaNdNlylM_147b9033-7292-5d82-a50e-7927a7b7eafd_RZEJX2MwH1564054176241_John.jpg","post_description":"My Superman and Man of Steel. Thank you Cavil","post_timestamp":1564054185,"post_user_name":"John Wick","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","post_likes":[{"_id":"5d39945560278e686925c159","user_id":"27f56235-371b-5106-a169-4cb86ff78d3e","user_name":"Shakti Pandit","user_profile_pic_url":"","like_timestamp":"1564054613"},{"_id":"5d3994f760278e686925c15b","user_id":"37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","user_name":"Sidharth Kushwaha","user_profile_pic_url":"","like_timestamp":"1564054775"},{"_id":"5d39953160278e686925c15d","user_id":"c016cb14-10c2-500b-aaf6-6e2a629de04d","user_name":"Monika Fatty","user_profile_pic_url":"","like_timestamp":"1564054833"},{"_id":"5d39955960278e686925c15f","user_id":"fea89666-b728-52e7-84ae-3e95bf8ffd00","user_name":"Vipul Baccha","user_profile_pic_url":"","like_timestamp":"1564054873"},{"_id":"5d39958460278e686925c161","user_id":"699205a1-7dff-5d73-ad2e-e3eca6041e64","user_name":"Dooms Day","user_profile_pic_url":"","like_timestamp":"1564054916"},{"_id":"5d3995b160278e686925c163","user_id":"d85b1696-8414-594b-bbae-fbeca04cb49a","user_name":"Iron Man","user_profile_pic_url":"","like_timestamp":"1564054961"},{"_id":"5d3995db60278e686925c165","user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","user_name":"Bruce Wayne","user_profile_pic_url":"","like_timestamp":"1564055003"},{"_id":"5d39962660278e686925c167","user_id":"d577f9e5-4ef9-518c-89b9-3be0dd5d4221","user_name":"Clark Kent","user_profile_pic_url":"","like_timestamp":"1564055078"},{"_id":"5d39967c60278e686925c169","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"","like_timestamp":"1564055164"},{"_id":"5d3996ac60278e686925c16b","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","user_name":"Henry Cavil","user_profile_pic_url":"","like_timestamp":"1564055212"},{"_id":"5d39979260278e686925c16d","user_id":"e47662af-cd07-50ab-b41d-6911662a37f2","user_name":"Sailesh Sharma","user_profile_pic_url":"","like_timestamp":"1564055442"},{"_id":"5d3fef98e7227a1ece052a88","user_id":"147b9033-7292-5d82-a50e-7927a7b7eafd","user_name":"John Wick","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","like_timestamp":"1564471192"}],"__v":0}
         */

        @SerializedName("message")
        private String message;
        @SerializedName("post")
        private PostBean post;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public PostBean getPost() {
            return post;
        }

        public void setPost(PostBean post) {
            this.post = post;
        }

        public static class PostBean {
            /**
             * post_location : Pimp Car
             * post_hash_tags : ["#henrycavil","#Superman","#ManOfSteel"]
             * post_comments : ["eFWqGBBUZ1564054336052","XykG-7eLm1564054337759","hm2jXmEl31564054339240","h2ilrJ4yX1564054340486","jzSxQWSSX1564054357188","2ibqnutMX1564054358727","nncKjB8x51564054359784","QH5DtI50i1564054360760","Bzcy9RHIm1564054361754","r-SfRwbkj1564054363815","VfQGkF5_g1564054364830","SL_UHblHP1564054366231","DZPSNFc3H1564054367219","2P8Q0hI4x1564054368177","fh1Mkwke71564054369060","1IVaJW8yi1564054369946","0oLlYj20n1564054377177","nElgOJ_JN1564054378169","bjoC1oMOr1564054379084","3UDeoTNIG1564054379964","yzNnTX-RY1564054380866"]
             * post_involved_users : []
             * post_users_tag : []
             * post_reports : []
             * is_user_self_liked : false
             * _id : 5d3992a960278e686925c13e
             * post_id : RZEJX2MwH1564054176241
             * user_id : 147b9033-7292-5d82-a50e-7927a7b7eafd
             * post_image_url : https://s3.us-east-2.amazonaws.com/pimpcarpost/147b9033-7292-5d82-a50e-7927a7b7eafd/1564054176-eHhCIZpBxahAs74xdRuWdn7LaNdNlylM_147b9033-7292-5d82-a50e-7927a7b7eafd_RZEJX2MwH1564054176241_John.jpg
             * post_description : My Superman and Man of Steel. Thank you Cavil
             * post_timestamp : 1564054185
             * post_user_name : John Wick
             * user_profile_pic_url : https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg
             * post_likes : [{"_id":"5d39945560278e686925c159","user_id":"27f56235-371b-5106-a169-4cb86ff78d3e","user_name":"Shakti Pandit","user_profile_pic_url":"","like_timestamp":"1564054613"},{"_id":"5d3994f760278e686925c15b","user_id":"37ce61f7-32d3-5c17-8d3d-e5c2d7c36b78","user_name":"Sidharth Kushwaha","user_profile_pic_url":"","like_timestamp":"1564054775"},{"_id":"5d39953160278e686925c15d","user_id":"c016cb14-10c2-500b-aaf6-6e2a629de04d","user_name":"Monika Fatty","user_profile_pic_url":"","like_timestamp":"1564054833"},{"_id":"5d39955960278e686925c15f","user_id":"fea89666-b728-52e7-84ae-3e95bf8ffd00","user_name":"Vipul Baccha","user_profile_pic_url":"","like_timestamp":"1564054873"},{"_id":"5d39958460278e686925c161","user_id":"699205a1-7dff-5d73-ad2e-e3eca6041e64","user_name":"Dooms Day","user_profile_pic_url":"","like_timestamp":"1564054916"},{"_id":"5d3995b160278e686925c163","user_id":"d85b1696-8414-594b-bbae-fbeca04cb49a","user_name":"Iron Man","user_profile_pic_url":"","like_timestamp":"1564054961"},{"_id":"5d3995db60278e686925c165","user_id":"b8a0cb9d-ea3d-5acc-9ef5-d1c73abe351c","user_name":"Bruce Wayne","user_profile_pic_url":"","like_timestamp":"1564055003"},{"_id":"5d39962660278e686925c167","user_id":"d577f9e5-4ef9-518c-89b9-3be0dd5d4221","user_name":"Clark Kent","user_profile_pic_url":"","like_timestamp":"1564055078"},{"_id":"5d39967c60278e686925c169","user_id":"22855eb3-fe40-5832-b54e-6f8042ba0f6d","user_name":"Zack Snyder","user_profile_pic_url":"","like_timestamp":"1564055164"},{"_id":"5d3996ac60278e686925c16b","user_id":"3d248ae7-91c3-5d6e-898a-665435049dad","user_name":"Henry Cavil","user_profile_pic_url":"","like_timestamp":"1564055212"},{"_id":"5d39979260278e686925c16d","user_id":"e47662af-cd07-50ab-b41d-6911662a37f2","user_name":"Sailesh Sharma","user_profile_pic_url":"","like_timestamp":"1564055442"},{"_id":"5d3fef98e7227a1ece052a88","user_id":"147b9033-7292-5d82-a50e-7927a7b7eafd","user_name":"John Wick","user_profile_pic_url":"https://s3.us-east-2.amazonaws.com/pimpcarprofile/147b9033-7292-5d82-a50e-7927a7b7eafd/1564048252-iDkxPSGah8aw1mZRKOvmqF5dZkLO1uyy_147b9033-7292-5d82-a50e-7927a7b7eafd_147b9033-7292-5d82-a50e-7927a7b7eafd_John.jpg","like_timestamp":"1564471192"}]
             */

            @SerializedName("post_location")
            private String post_location;
            @SerializedName("is_user_self_liked")
            private boolean is_user_self_liked;
            @SerializedName("_id")
            private String _id;
            @SerializedName("post_id")
            private String post_id;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("post_image_url")
            private String post_image_url;
            @SerializedName("post_description")
            private String post_description;
            @SerializedName("post_timestamp")
            private int post_timestamp;
            @SerializedName("post_user_name")
            private String post_user_name;
            @SerializedName("user_profile_pic_url")
            private String user_profile_pic_url;
            @SerializedName("post_hash_tags")
            private List<String> post_hash_tags;
            @SerializedName("post_comments")
            private List<String> post_comments;
            @SerializedName("post_users_tag")
            private List<String> post_users_tag;
            private List<String> post_reports;
            @SerializedName("post_likes")
            private List<PostLikesBean> post_likes;
            @SerializedName("user_name")
            private String user_name;

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getPost_location() {
                return post_location;
            }

            public void setPost_location(String post_location) {
                this.post_location = post_location;
            }

            public boolean isIs_user_self_liked() {
                return is_user_self_liked;
            }

            public void setIs_user_self_liked(boolean is_user_self_liked) {
                this.is_user_self_liked = is_user_self_liked;
            }

            public String get_id() {
                return _id;
            }

            public void set_id(String _id) {
                this._id = _id;
            }

            public String getPost_id() {
                return post_id;
            }

            public void setPost_id(String post_id) {
                this.post_id = post_id;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getPost_image_url() {
                return post_image_url;
            }

            public void setPost_image_url(String post_image_url) {
                this.post_image_url = post_image_url;
            }

            public String getPost_description() {
                return post_description;
            }

            public void setPost_description(String post_description) {
                this.post_description = post_description;
            }

            public int getPost_timestamp() {
                return post_timestamp;
            }

            public void setPost_timestamp(int post_timestamp) {
                this.post_timestamp = post_timestamp;
            }

            public String getPost_user_name() {
                return post_user_name;
            }

            public void setPost_user_name(String post_user_name) {
                this.post_user_name = post_user_name;
            }

            public String getUser_profile_pic_url() {
                return user_profile_pic_url;
            }

            public void setUser_profile_pic_url(String user_profile_pic_url) {
                this.user_profile_pic_url = user_profile_pic_url;
            }


            public List<String> getPost_hash_tags() {
                return post_hash_tags;
            }

            public void setPost_hash_tags(List<String> post_hash_tags) {
                this.post_hash_tags = post_hash_tags;
            }

            public List<String> getPost_comments() {
                return post_comments;
            }

            public void setPost_comments(List<String> post_comments) {
                this.post_comments = post_comments;
            }


            public List<String> getPost_users_tag() {
                return post_users_tag;
            }

            public void setPost_users_tag(List<String> post_users_tag) {
                this.post_users_tag = post_users_tag;
            }


            public List<PostLikesBean> getPost_likes() {
                return post_likes;
            }

            public void setPost_likes(List<PostLikesBean> post_likes) {
                this.post_likes = post_likes;
            }

            public static class PostLikesBean {
                /**
                 * _id : 5d39945560278e686925c159
                 * user_id : 27f56235-371b-5106-a169-4cb86ff78d3e
                 * user_name : Shakti Pandit
                 * user_profile_pic_url :
                 * like_timestamp : 1564054613
                 */

                @SerializedName("_id")
                private String _id;
                @SerializedName("user_id")
                private String user_id;
                @SerializedName("user_name")
                private String user_name;
                @SerializedName("user_profile_pic_url")
                private String user_profile_pic_url;
                @SerializedName("like_timestamp")
                private String like_timestamp;

                public String get_id() {
                    return _id;
                }

                public void set_id(String _id) {
                    this._id = _id;
                }

                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public String getUser_name() {
                    return user_name;
                }

                public void setUser_name(String user_name) {
                    this.user_name = user_name;
                }

                public String getUser_profile_pic_url() {
                    return user_profile_pic_url;
                }

                public void setUser_profile_pic_url(String user_profile_pic_url) {
                    this.user_profile_pic_url = user_profile_pic_url;
                }

                public String getLike_timestamp() {
                    return like_timestamp;
                }

                public void setLike_timestamp(String like_timestamp) {
                    this.like_timestamp = like_timestamp;
                }
            }
        }
    }
}

package com.pimpcar.Network;

import static com.pimpcar.Network.NetworkConstants.POST_LIKE_DISLIKE_ON_POST;

import com.google.gson.JsonObject;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedRequestBody;
import com.pimpcar.HomeFeed.fragments.home.pojos.HomeFeedResponse;
import com.pimpcar.HomeFeed.fragments.notification.models.NotificationRequestBody;
import com.pimpcar.HomeFeed.fragments.notification.models.NotificationResponseBody;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.FollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllCommentsBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.PostAllLikesResponse;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserRequestBodyModel;
import com.pimpcar.HomeFeed.fragments.profile.comments.pojobeans.UnFollowUserResponseModel;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserNameResponse;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserStatusRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.ChangeUserStatusResponse;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowedAccountRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowedAccountResponse;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.pojos.FollowingAccountResponse;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.GetUserStoriesResponse;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.PostUserStoriesRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.stories.models.PostUserStoriesResponseBody;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonPostBodyRequest;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonProfileRequestBody;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonProfileResponse;
import com.pimpcar.HomeFeed.fragments.profile.thirdpersonprofile.pojos.ThirdPersonStoriesBodyRequest;
import com.pimpcar.HomeFeed.pojos.SearchFamousRequestBody;
import com.pimpcar.HomeFeed.pojos.SearchFamousResponseBody;
import com.pimpcar.Network.model.contact_number_verification.OTPSendBody;
import com.pimpcar.Network.model.contact_number_verification.OTPSendResponse;
import com.pimpcar.Network.model.contact_number_verification.OTPVerifyBody;
import com.pimpcar.Network.model.contact_number_verification.OTPVerifyResponse;
import com.pimpcar.Network.model.login.LoginBody;
import com.pimpcar.Network.model.login.LoginResponse;
import com.pimpcar.Network.model.password_change.PasswordChanageResponse;
import com.pimpcar.Network.model.password_change.PasswordChangeBody;
import com.pimpcar.Network.model.posts.PostCommentRequestBody;
import com.pimpcar.Network.model.posts.PostCommentResponse;
import com.pimpcar.Network.model.posts.PostLikeDislikeRequestBody;
import com.pimpcar.Network.model.posts.PostLikeDislikeResponse;
import com.pimpcar.Network.model.posts.SubmitPostRequestBody;
import com.pimpcar.Network.model.posts.SubmitPostResponseBody;
import com.pimpcar.Network.model.posts.UserPostBodyResponse;
import com.pimpcar.Network.model.posts.UserPostDetailsRequestBody;
import com.pimpcar.Network.model.posts.UserPostDetailsResponseBody;
import com.pimpcar.Network.model.posts.UserPostsBodyRequest;
import com.pimpcar.Network.model.signup.SignupBody;
import com.pimpcar.Network.model.signup.SignupResponse;
import com.pimpcar.Network.model.user_dob.UserDOBResponse;
import com.pimpcar.Network.model.user_dob.UserDOBUpdateBody;
import com.pimpcar.Network.model.user_search.UserSearchRequestBody;
import com.pimpcar.Network.model.user_search.UserSearchResponseBody;
import com.pimpcar.Network.model.useremailverification.UserEMailOTPResponse;
import com.pimpcar.Network.model.useremailverification.UserEmailOTPVerifyResponse;
import com.pimpcar.Network.model.useremailverification.UserEmailVerificationOTPBody;
import com.pimpcar.Network.model.useremailverification.UserEmailVerifyOTPBody;
import com.pimpcar.Network.model.usergender.UserGenderProfileResponse;
import com.pimpcar.Network.model.usergender.UserGenderProfileUpdateBody;
import com.pimpcar.Network.model.userselfprofile.UserProfileFirstData;
import com.pimpcar.Network.model.userselfprofile.UserSelfProfileResponse;
import com.pimpcar.PostEditting.fragments.pojos.StickerApiResponse;
import com.pimpcar.PostEditting.fragments.pojos.WheelApiResponse;
import com.pimpcar.models.VideoListResponseModel;

import org.json.JSONObject;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;


public interface ApiService {


    // Register new user
    @POST(NetworkConstants.USER_SIGNUP_END)
    Single<SignupResponse> register(@Body SignupBody signupBody);

    @POST(NetworkConstants.USER_LOGIN_END)
    Single<LoginResponse> login(@Body LoginBody loginBody);

    // Update user profile pic and gender
    @POST(NetworkConstants.USER_GENDER_PROFILE_UPDATE)
    Single<UserGenderProfileResponse> updateProfileGender(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body UserGenderProfileUpdateBody userGenderProfileUpdateBody);

    // Get user profile
    @GET(NetworkConstants.USER_SELF_PROFILE)
    Single<UserSelfProfileResponse> getUserSelfProfile(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key);


    // Send email otp to user
    @POST(NetworkConstants.SEND_EMAIL_OTP)
    Single<UserGenderProfileResponse> sendEmailOtp(@Body UserEmailVerificationOTPBody userEmailVerificationOTPBody);

    // verify email otp
    @POST(NetworkConstants.EMAIL_OTP_VERIFY)
    Single<UserEmailOTPVerifyResponse> verifyEmailOTP(@Body UserEmailVerifyOTPBody userEmailVerifyOTPBody);


    // send email otp to user for password changeemail-otp
    @POST(NetworkConstants.SEND_EMAIL_OTP_PASSWORD_CHANGE)
    Single<UserEmailOTPVerifyResponse> sendEmailOtpForPasswordChange(@Body UserEmailVerificationOTPBody userEmailVerificationOTPBody);

    @POST(NetworkConstants.USER_PASSWORD_RESET)
    Single<PasswordChanageResponse> change_password(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body PasswordChangeBody passwordChangeBody);

    // verify email otp user for password change
    @POST(NetworkConstants.VERIFY_EMAIL_OTP_PWD_CHANGE)
    Single<UserEMailOTPResponse> verifyEmailOTPPasswordChange(@Body UserEmailVerifyOTPBody userEmailVerifyOTPBody);

    // update user dob
    @POST(NetworkConstants.USER_DOB_UPDATE_END)
    Single<UserDOBResponse> updateProfileDOB(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body UserDOBUpdateBody userDOBUpdateBody);


    @POST(NetworkConstants.SEND_CONTACT_NUMBER_OTP_END)
    Single<OTPSendResponse> sendOTPContactNumber(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body OTPSendBody otpSendBody);

    @POST(NetworkConstants.VERIFY_CONTACT_NUMBER_OTP_END)
    Single<OTPVerifyResponse> verifyOTPContactNumber(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body OTPVerifyBody otpVerifyBody);


    @GET(NetworkConstants.GET_USER_PROFILE_FIRST_DATA)
    Single<UserProfileFirstData> getUserProfileData(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key);


    @POST(NetworkConstants.COMMENT_ON_POST)
    Single<PostCommentResponse> postCommentOnUserPost(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body PostCommentRequestBody postCommentRequestBody);

    @POST(POST_LIKE_DISLIKE_ON_POST)
    Single<PostLikeDislikeResponse> postLikeDislikeOnPost(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body PostLikeDislikeRequestBody postLikeDislikeRequestBody);


    @POST(NetworkConstants.GET_ALL_POST_LIKE_END)
    Single<PostAllLikesResponse> getAllPostLikes(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body PostAllLikesBodyModel postAllLikesBodyModel);

    @POST(NetworkConstants.GET_ALL_POST_COMMENT_END)
    Single<PostAllCommentResponseModel> getAllPostComments(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body PostAllCommentsBodyModel postAllCommentsBodyModel);


    @POST(NetworkConstants.USER_POSTS)
    Single<UserPostBodyResponse> getUserPosts(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body UserPostsBodyRequest userPostsBodyRequest);


    @Headers({
            "Content-Type: application/json",
    })
    @POST(NetworkConstants.USER_POSTS)
    Call<UserPostBodyResponse> getUserPostss(@Body JsonObject params, @Header("api_auth_token") String auth);

    @Headers({
            "Content-Type: application/json",
    })
    @POST(NetworkConstants.APP_HOME_FEED)
    Call<VideoListResponseModel> getVideoPosts(@Body JsonObject params, @Header("api_auth_token") String auth);

    //    @FormUrlEncoded
    @GET("get-video-posts")
    Call<VideoListResponseModel> getVideolISTPosts();


    @POST(NetworkConstants.APP_HOME_FEED)
    Single<HomeFeedResponse> getHomeFeedPosts(@Body HomeFeedRequestBody homeFeedRequestBody);


    @POST(NetworkConstants.FOLLOW_USER)
    Single<FollowUserResponseModel> followUser(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body FollowUserRequestBodyModel followUserRequestBodyModel);

    @POST(NetworkConstants.UNFOLLOW_USER)
    Single<UnFollowUserResponseModel> unfollowUser(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body UnFollowUserRequestBodyModel UnFollowUserRequestBodyModel);

    @POST(NetworkConstants.GET_THIRD_PERSON_PROFILE)
    Single<ThirdPersonProfileResponse> getThirdPersonProfile(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String api_auth_key, @Body ThirdPersonProfileRequestBody userPostsBodyRequest);

    @POST(NetworkConstants.GET_THIRD_PERSON_ALL_POST)
    Single<UserPostBodyResponse> getthirdPersonPosts(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body ThirdPersonPostBodyRequest userPostsBodyRequest);

    @POST(NetworkConstants.UPDATE_USERNAME)
    Single<ChangeUserNameResponse> changeUsername(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body ChangeUserNameRequestBody changeUserNameRequestBody);

    @Headers({
            "Content-Type: application/json",
    })
    @POST(NetworkConstants.UPDATE_USERNAME)
    Call<ChangeUserNameResponse> changeUserNameApi(@Body JsonObject params, @Header("api_auth_token") String auth);

    @Headers({
            "Content-Type: application/json",
    })
    @POST(NetworkConstants.POST_LIKE_DISLIKE_ON_POST)
    Call<PostLikeDislikeResponse> likeDislikeApi(@Body JsonObject params, @Header("api_auth_token") String auth);

    @Headers({
            "Content-Type: application/json",
    })
    @POST(NetworkConstants.follower_following)
    Call<PostLikeDislikeResponse> followUnfollowApi(@Body JsonObject params);

    @POST(NetworkConstants.UPDATE_USER_STATUS)
    Single<ChangeUserStatusResponse> updateUserStatus(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body ChangeUserStatusRequestBody changeUserStatusRequestBody);

    @POST(NetworkConstants.GET_USER_STORIES)
    Single<GetUserStoriesResponse> getUserStories(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body GetUserStoriesRequestBody getUserStoriesRequestBody);


    @POST(NetworkConstants.GET_THIRD_PERSON_STORIES)
    Single<GetUserStoriesResponse> getThirdUserStories(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body ThirdPersonStoriesBodyRequest thirdPersonStoriesBodyRequest);


    @POST(NetworkConstants.POST_USER_STORIES)
    Single<PostUserStoriesResponseBody> postUserStories(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body PostUserStoriesRequestBody postUserStoriesRequestBody);

    @POST(NetworkConstants.USER_SEARCH)
    Single<UserSearchResponseBody> getSearchUsers(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body UserSearchRequestBody userSearchRequestBody);


    @POST(NetworkConstants.POST_DETAILS)
    Single<UserPostDetailsResponseBody> getPostDetails(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body UserPostDetailsRequestBody userPostRequestBody);

    @POST(NetworkConstants.SUBMIT_POST)
    Single<SubmitPostResponseBody> submitPost(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body SubmitPostRequestBody body_for_submit_post);

    @POST(NetworkConstants.USER_NOTIFICATION_API_END)
    Single<NotificationResponseBody> getUserNotification(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body NotificationRequestBody notificationRequestBody);

    @POST(NetworkConstants.SEARCH_FAMOUS_API_END)
    Single<SearchFamousResponseBody> getSearchFamousPost(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body SearchFamousRequestBody searchFamousRequestBody);

    @POST(NetworkConstants.WHEEL_API_API_END)
    Single<WheelApiResponse> getWheel(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key);

    @POST(NetworkConstants.ACCOUNT_USER_FOLLOWS)
    Single<FollowedAccountResponse> getFollowsList(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body FollowedAccountRequestBody followedAccountRequestBody);

    @POST(NetworkConstants.ACCOUNT_USER_FOLLOWING)
    Single<FollowingAccountResponse> getFollowingList(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key, @Body FollowingAccountRequestBody followingAccountRequestBody);

    @POST(NetworkConstants.STICKER_API_END)
    Single<StickerApiResponse> getStickerWheel(@Header(NetworkConstants.HEADER_AUTH_TOKEN) String user_jwt_key);
}

package com.pimpcar.Network;

/**
 * Created by sahitya on 9/06/19.
 */

class NetworkConstants {
    // Network Api ends
    static final String BASE_URL = "http://3.145.19.246:8080/";
    //     static final String BASE_URL = "http://3.19.55.207:8079";
//    static final String BASE_URL = "http://pimpcar-app.com:8079";
    static final String USER_SIGNUP_END = "/user/user-register";
    static final String USER_GENDER_PROFILE_UPDATE = "/user/upload-user-profile-pic";
    static final String USER_SELF_PROFILE = "/user/me";
    static final String SEND_EMAIL_OTP = "/user/email-otp";
    static final String EMAIL_OTP_VERIFY = "/user/verify-email-otp";
    static final String USER_LOGIN_END = "/user/user-login";
    static final String USER_DOB_UPDATE_END = "/user/user-dob";
    static final String SEND_EMAIL_OTP_PASSWORD_CHANGE = "/user/email-otp-password-change";
    static final String VERIFY_EMAIL_OTP_PWD_CHANGE = "/user/verify-email-otp-password-change";
    static final String USER_PASSWORD_RESET = "/user/reset-password";
    static final String SEND_CONTACT_NUMBER_OTP_END = "/user/number-otp";
    static final String VERIFY_CONTACT_NUMBER_OTP_END = "/user/verify-number-otp";
    static final String GET_USER_PROFILE_FIRST_DATA = "/user/user-profile";


    // Header data
    static final String HEADER_AUTH_TOKEN = "api_auth_token";
    static final String CONTENT_TYPE = "application/json";


    static final String COMMENT_ON_POST = "/posts/comment";
    static final String USER_POSTS = "/posts/get-user-profile-post";
    static final String GET_ALL_POST_LIKE_END = "/posts/get-all-post-likes";
    static final String GET_ALL_POST_COMMENT_END = "/posts/get-all-post-comments";
    static final String POST_LIKE_DISLIKE_ON_POST = "/posts/like-dislike-post";
    static final String follower_following = "follower-following";
    static final String FOLLOW_USER = "/user/follow-user";
    static final String UNFOLLOW_USER = "/user/unfollow-user";
    static final String GET_THIRD_PERSON_PROFILE = "/user/get-third-user-profile";
    static final String GET_THIRD_PERSON_ALL_POST = "/posts/get-third-user-profile-post";
    static final String UPDATE_USERNAME = "/user/update-user-name";
    static final String UPDATE_USER_STATUS = "/user/update-status";
    static final String GET_USER_STORIES = "/user/get-user-stories";
    static final String APP_HOME_FEED = "/posts/get-home-feed";
    static final String GET_THIRD_PERSON_STORIES = "/user/get-third-user-stories";
    static final String POST_USER_STORIES = "/user/post-user-stories";
    static final String USER_SEARCH = "/user/search-user";
    static final String POST_DETAILS = "/posts/get-post-details";
    static final String SUBMIT_POST = "/posts/post-feed";
    static final String USER_NOTIFICATION_API_END = "/user/activity-notification";
    static final String SEARCH_FAMOUS_API_END = "/posts/search-famous-feed";
    static final String WHEEL_API_API_END = "/user/user-wheels";
    static final String ACCOUNT_USER_FOLLOWS="/user/accounts-user-follows";
    static final String ACCOUNT_USER_FOLLOWING="/user/accounts-user-following";
    static final String STICKER_API_END="/user/user-stickers";
}
